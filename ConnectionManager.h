//
//  ConnectionManager.h
//  ProximityApp
//
//  Copyright (c) 2012 ytt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import "ProximityTag.h"
#import "ProximityTagStorage.h"

@protocol ConnectionManagerDelegate <ProximityTagDelegate>
- (void) isBluetoothEnabled:(bool) enabled;
- (void) didDiscoverTag:(ProximityTag*) tag;

@optional
-(void)ChangeLayoutData;
-(void)ChangeLayoutDataConnected;
@end

@interface ConnectionManager : NSObject <CBCentralManagerDelegate,CLLocationManagerDelegate>{
    CLLocationManager * locationManager;

}
@property (strong,nonatomic)CBPeripheral *cp;
@property id<ConnectionManagerDelegate> delegate;

+ (ConnectionManager*) sharedInstance;

- (void) startScanForTags;
- (void) stopScanForTags;
- (void) retrieveKnownPeripherals;
- (void) disconnectTag:(ProximityTag*) tag;
- (void) connectToTag:(ProximityTag*) tag;

@end
