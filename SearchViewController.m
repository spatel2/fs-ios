//
//  SearchViewController.m
//  FYIProject
//
//  Created by Hay on 13-10-16.
//  Copyright (c) 2013年 apple. All rights reserved.
//

#import "SearchViewController.h"
#import "ConnectionManager.h"
#import "AppDelegate.h"
#import "Proxy.h"
#import "Constant.h"

@interface SearchViewController ()
@property(nonatomic,strong)NSMutableArray *arrayData;
@end

@implementation SearchViewController
@synthesize deviceTabview=_deviceTabview;

- (void) didwatchTophone:(id) type{
    ;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    
    [self.ActivityView stopAnimating];

    [self.deviceTabview reloadData];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title=@"Search Devices";
    self.view.layer.contents=(id)[UIImage imageNamed:@"3.png"].CGImage;
 
    [[ConnectionManager sharedInstance] setDelegate:self];
    
    //[[ProximityTagStorage sharedInstance]loadTags];
    
    
    
    [self.deviceTabview reloadData];
    
}

- (void) didDiscoverTag:(ProximityTag*) tag
{
    tag.delegate = self;
    
}

- (void) isBluetoothEnabled:(bool)enabled
{
    
    ;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ChangeLayoutDataConnected {
    
}

- (void) didUpdateData:(id) tag{
    
    [self.deviceTabview reloadData];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return [self.arrayData count];
    return [[[ProximityTagStorage sharedInstance] tags] count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *BLEIden=@"BLEIdentifier";
    UITableViewCell *Cell=[tableView dequeueReusableCellWithIdentifier:BLEIden];
    if (Cell==nil) {
        Cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:BLEIden];
        
        UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(4.0, 4.0, 36.0, 36.0)];
        imageView.tag=11;
        [Cell.contentView addSubview:imageView];
        
        UILabel *lable=[[UILabel alloc] initWithFrame:CGRectMake(44.0, 0.0, 200, 44.0)];
        lable.tag=12;
        [lable setBackgroundColor:[UIColor clearColor]];
        [lable setFont:[UIFont boldSystemFontOfSize:15.0]];
        [Cell.contentView addSubview:lable];
        
    }
    ProximityTag * tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:indexPath.row];
    
    UIImageView *imageVIew=(UIImageView *)[Cell.contentView
                                           viewWithTag:11];
    if (tag.pothoimage) {
        
            [imageVIew setImage:tag.pothoimage];
    }else{
            [imageVIew setImage:[UIImage imageNamed:@"23.png"]];
    }
    UILabel *Lable=(UILabel *)[Cell.contentView viewWithTag:12];
    if (tag.name) {
        [Lable setText:tag.name];
    }else{
        [Lable setText:@"FYI watch"];
    }
    if ([tag.peripheral isConnected]) {
        [Cell setAccessoryType:UITableViewCellAccessoryCheckmark];
    }else{
        [Cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    }
    
    return Cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   
    
    
    UIStoryboard *storyboad=[UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [storyboad instantiateViewControllerWithIdentifier:@"Synergy"];
    
    FYIInfoViewController *SportVC=(FYIInfoViewController *)[storyboad instantiateViewControllerWithIdentifier:@"Synergy"];
    [SportVC setProximityTag:[[[ProximityTagStorage sharedInstance] tags] objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:SportVC animated:YES];
    
 
    
    
    [Proxy shared].flagConnectView = YES;
    
}

- (IBAction)SearchTags:(id)sender
{
    [[ConnectionManager sharedInstance]startScanForTags];
    self.ActivityView.hidden=NO;
    [self.ActivityView startAnimating];
    
    [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(stopScanForTags) userInfo:nil repeats:NO];
}


-(void)stopScanForTags{
    
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    [self.deviceTabview reloadData];
    
    [self.ActivityView stopAnimating];
}

@end
