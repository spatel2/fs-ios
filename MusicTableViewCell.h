//
//  MusicTableViewCell.h
//  KennethCole
//
//  Created by iBlazing Mac Mini 3 on 21/01/15.
//  Copyright (c) 2015 iBlazing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MusicTableViewCell : UITableViewCell
@property(nonatomic,retain)IBOutlet UIImageView *imgalbum;
@property(nonatomic,retain)IBOutlet UILabel *lblsongname;
@property(nonatomic,retain)IBOutlet UILabel *lblartistname;

@end
