//
//  FYIInfoViewController.m
//  FYIProject
//
//  Created by Hay on 13-10-29.
//  Copyright (c) 2013年 apple. All rights reserved.
//

#import "FYIInfoViewController.h"
#import "ProximityTagStorage.h"
#import "ConnectionManager.h"
#import "AppDelegate.h"
#import "Proxy.h"
@interface FYIInfoViewController ()

@end

@implementation FYIInfoViewController

@synthesize proximityTag=_proximityTag;
@synthesize pickerController=_pickerController;
@synthesize nameLable=_nameLable,rssiLable=_rssiLable;
@synthesize iamgeView=_iamgeView;

- (void) setProximityTag:(ProximityTag *)proximityTag
{
    
    if (proximityTag != _proximityTag)
    {
        _proximityTag = proximityTag;
        _proximityTag.delegate = self;
        self.nameLable.text=self.proximityTag.name;
    }
}

-(void)imagePickerControllerDIdCancel:(UIImagePickerController*)picker

{
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

//拍照delegate 保存图片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    if ([mediaType isEqualToString:@"public.image"]) {
        //将图片存放到本地
        UIImageWriteToSavedPhotosAlbum(image,self,nil,nil);
        self.proximityTag.pothoimage=image;
        [self.iamgeView setImage:image];
    }
    
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

    
-(IBAction)clickCramera:(id)sender{
    
    
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Select Image"
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take Photo", @"Gallery photos",nil];
    [actionSheet setBackgroundColor:[UIColor colorWithRed:54 green:54 blue:54 alpha:1]];
    actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
    [actionSheet showInView:self.view];
    

    
}

-(IBAction)clickRename:(id)sender{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Rename" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"NO",nil];
    alert.alertViewStyle=UIAlertViewStylePlainTextInput;
    alert.tag=190;
    [alert show];
    
}

- (void) didwatchTophone:(id) type{
    ;
}
-(IBAction)clickConnect:(id)sender{
    
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
 
    //用户点击连接，必须先干掉以前的别的设备，保证每次只连接一个
    
    if ([app.appdelegateProximity.peripheral isConnected]&&![app.appdelegateProximity isEqual:self.proximityTag]) {//不是同一个设备
        
        //先干掉以前的，在连接当前的
        [[ConnectionManager sharedInstance] disconnectTag:app.appdelegateProximity];
        [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
        
        [Proxy shared].flagConnectView = NO;
        [self.navigationController popViewControllerAnimated:FALSE];
        
    }else{//是同一个设备
        
        if ([self.proximityTag.peripheral isConnected]) {
            app.reConnBtnTag =@"1";
            [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
            [self.Connect setText:@"Connect"];
            
            
        }else{
            if (self.proximityTag.peripheral==nil) {
                UIAlertView * alt =  [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Device information is lost, please delete this device and try to rebuild the link" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alt show];
            }else{
                 [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
                [Proxy shared].flagConnectView = NO;
                [self.navigationController popViewControllerAnimated:FALSE];
            }
           
        }
    }
    
    
}
-(IBAction)clickForget:(id)sender{
    UIAlertView* question = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                       message:[NSString stringWithFormat:@"Are you sure you want to delete %@?",[self.proximityTag name]]
                                                      delegate:self
                                             cancelButtonTitle:@"YES"  otherButtonTitles:@"NO", nil];
    question.tag=135;
    [question show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (alertView.tag==135) {//删除tag
        if(buttonIndex == 0) {
            
            [[ProximityTagStorage sharedInstance] removeTag:self.proximityTag];
            
            [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
            
            [Proxy shared].flagConnectView = NO;

            [[self navigationController] popViewControllerAnimated:YES];
        }
    }
    else if (alertView.tag==190) {//
        UITextField *tf=[alertView textFieldAtIndex:0];
        if (buttonIndex==0) {
            
            self.proximityTag.name=tf.text;
            self.nameLable.text=tf.text;
            
        }else{
            ;
        }
    }
}


 
-(void)ChangeLayoutData {
    
}
-(void)ChangeLayoutDataConnected {
    
}

-(void)didUpdateData:(id)tag{
    
    self.rssiLable.text=[NSString  stringWithFormat:@"RSSI:%f",self.proximityTag.rssiLevel];
    if (self.proximityTag.rssiLevel==0.0f) {
        
        [self.Connect setText:@"CONNECT"];

    }else{
        [self.Connect setText:@"DISCONNECT"];

    }
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}
- (void)viewDidAppear:(BOOL)animated{
    
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
    self.title=@"FIND WATCH";
    UIView *titlView=[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 44.0)];
    [titlView setBackgroundColor:[UIColor clearColor]];
    
    self.nameLable=[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 200, 24.0)];
    [self.nameLable setBackgroundColor:[UIColor clearColor]];
    [self.nameLable setTextColor:[UIColor whiteColor]];
    if (self.proximityTag.name) {
        
        [self.nameLable setText:self.proximityTag.name];
    }else{
        [self.nameLable setText:@"FYI watch"];
    }
    
    if (self.proximityTag.pothoimage) {
        [self.iamgeView setImage:self.proximityTag.pothoimage];
    }
    
    [self.nameLable setFont:[UIFont boldSystemFontOfSize:15]];
    [self.nameLable setLineBreakMode:NSLineBreakByWordWrapping];
    [self.nameLable setTextAlignment:NSTextAlignmentCenter];
    [self.nameLable setNumberOfLines:2];
    [titlView addSubview:self.nameLable];
    
    
    self.rssiLable=[[UILabel alloc] initWithFrame:CGRectMake(0.0, 24.0, 200, 20.0)];
    [self.rssiLable setBackgroundColor:[UIColor clearColor]];
    [self.rssiLable setTextColor:[UIColor whiteColor]];
    
    if (self.proximityTag.rssiLevel) {
        [self.rssiLable setText:[NSString stringWithFormat:@"RSSI:-%f",self.proximityTag.rssiLevel]];
    }
    
    [self.rssiLable setText:@"RSSI:"];
    [self.rssiLable setFont:[UIFont boldSystemFontOfSize:13]];
    [self.rssiLable setLineBreakMode:NSLineBreakByWordWrapping];
    [self.rssiLable setTextAlignment:NSTextAlignmentCenter];
    [self.rssiLable setNumberOfLines:2];
    [titlView addSubview:self.rssiLable];
    
    self.navigationItem.titleView=titlView;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidDisappear:(BOOL)animated{
 //页面消失，保存BLE设备的信息
    
    
    
    [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
    
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        if (!hasCamera) {
            return;
        }
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        self.pickerController.allowsEditing=YES;
        //能够使用iphone的内置摄像机拍照
        self.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.pickerController animated:YES completion:nil];
        
    }else if (buttonIndex == 1) {
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
        if (!hasCamera) {
            return;
        }
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        //能够使用iphone的内置摄像机拍照
        self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.pickerController.allowsEditing = YES;
        
        [self presentViewController:self.pickerController animated:YES completion:nil];
        
    }else if(buttonIndex == 2) {
        
 
    }
}
@end
