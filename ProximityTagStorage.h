//
//  ProximityTagStorage.h
//  ProximityApp
//
//  Copyright (c) 2012 ytt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProximityTag.h"

@interface ProximityTagStorage : NSObject <UITableViewDataSource>

@property (retain) NSMutableArray *tags;

+ (ProximityTagStorage*) sharedInstance;

- (id) init;

- (void) saveTags;
- (void) loadTags;

- (void) insertTag:(ProximityTag*) tag;
- (void) removeTag:(ProximityTag*) tag;

- (bool) isTagInList:(ProximityTag*) tag;
- (bool) isPeripheralInList:(CBPeripheral*) peripheral;

- (ProximityTag*) tagAtIndex:(NSUInteger) index;
- (ProximityTag*) tagWithPeripheral:(CBPeripheral*) peripheral;
- (ProximityTag*) tagWithUUID:(CFUUIDRef) UUIDRef;

- (NSMutableArray*) tagsWithoutPeripheral;


//-(void)cbtnclick;

@end
