//
//  ServerManager.m
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//
//

#import "ServerManager.h"
#import "AppDelegate.h"

@implementation ServerManager
{
    CBPeripheralManager *pm;
    CBMutableService *immediateAlertService;
    SystemSoundID soundFileObject;
}
@synthesize player,playerytt, didpausemusic;

static ServerManager* sharedServerManager;

+ (CBUUID*) immediateAlertServiceUUID
{
    return [CBUUID UUIDWithString:@"1802"];
}

+ (CBUUID*) immediateAlertCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"2A06"];
}

-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if (didpausemusic == true)
    {
        AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
        //[app.mpc play];
    }
}

+ (ServerManager*) sharedInstance
{
    if (sharedServerManager == nil)
    {
        sharedServerManager = [[ServerManager alloc] init];
    }
    return sharedServerManager;
}

- (ServerManager*) init{
    return self;
}

- (void) setupService{
    
    CBMutableCharacteristic *c = [[CBMutableCharacteristic alloc] initWithType:ServerManager.immediateAlertCharacteristicUUID properties:CBCharacteristicPropertyWriteWithoutResponse value:nil permissions:CBAttributePermissionsWriteable];
    
    immediateAlertService = [[CBMutableService alloc] initWithType:ServerManager.immediateAlertServiceUUID primary:YES];
    immediateAlertService.characteristics = [NSArray arrayWithObject:c];
    
    [pm addService:immediateAlertService];
    
}

- (void) startPlayingAlarmSound
{
    AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    MPMusicPlaybackState playbackState = [app.mpc playbackState];
//    if (playbackState == MPMusicPlaybackStatePlaying)
//    {
//        [app.mpc pause];
//        didpausemusic = true;
//    }
//    else
//        didpausemusic = false;

    CFBundleRef mainBundle = CFBundleGetMainBundle ();
    CFURLRef soundFileURLRef  = CFBundleCopyResourceURL (
                                                mainBundle,
                                                CFSTR ("Alarm-sound"),
                                                CFSTR ("wav"),
                                                NULL
                                                );
    
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
                                      soundFileURLRef,
                                      &soundFileObject
                                      );
    
    
    //vibration functionality
    AudioServicesPlaySystemSound (1352);
    //AudioServicesPlayAlertSound (1105);
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i=0;i<7;i++){
            AudioServicesPlaySystemSound(soundFileObject);
            sleep(3);
        }
    });
}

- (void) stopPlayingAlarmSound{
    AudioServicesDisposeSystemSoundID(soundFileObject);
}

- (void) peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    if ([pm state] == CBCentralManagerStatePoweredOn){
        [self setupService];
    }
}

- (void) peripheralManager:(CBPeripheralManager *)peripheral didAddService:(CBService *)service error:(NSError *)error{
    
}


- (void) peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests{
    
    
}

@end