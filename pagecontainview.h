//
//  pagecontainview.h
//  KennethCole
//
//  Created by Dharmesh on 06/01/15.
//  Copyright (c) 2015 iBlazing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pagecontainview : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property NSUInteger pageIndex;
@property NSString *titleText;
@property NSString *imageFile;

-(IBAction)LogIn:(id)sender;
-(IBAction)SignUp:(id)sender;

@end
