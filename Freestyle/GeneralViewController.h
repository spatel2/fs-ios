//
//  GeneralViewController.h
//  Freestyle
//
//  Created by BIT on 19/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SikinButton.h"
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "Helper.h"
#import "AppDelegate.h"
#import "SikinLabel.h"


@interface GeneralViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate>{
    AppDelegate* appDelegate;
}


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForScrollViewContentWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenSurfDataUpDateButtonAndTideAlertButton;//112
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenTideAlertButtonAndDistanceOrHeightButton;//60

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenDistanceOrHeightButtonTempButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenTempButtonAndActivityProfileButton;//60
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenActivityProfileButtonAndBasicProfileButton;//216
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView; //130

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForScrollViewBottomToView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenBasicProfileButtonAndUpdatePhotoButton;//320

@property (strong, nonatomic) IBOutlet UIView *IBViewAuto;
@property (strong, nonatomic) IBOutlet UIView *IBViewManual;
@property (strong, nonatomic) IBOutlet UIView *IBViewOn;
@property (strong, nonatomic) IBOutlet UIView *IBViewOff;

@property (strong, nonatomic) IBOutlet UIView *IBViewDistHeightImperial;
@property (strong, nonatomic) IBOutlet UIView *IBViewDistHeightMetric;
@property (strong, nonatomic) IBOutlet UIView *IBViewTempImperial;
@property (strong, nonatomic) IBOutlet UIView *IBViewTempMetric;

@property (strong, nonatomic) IBOutlet UIView *IBViewWeight;
@property (strong, nonatomic) IBOutlet UIView *IBViewHeight;
@property (strong, nonatomic) IBOutlet UIView *IBViewAge;
@property (strong, nonatomic) IBOutlet UIView *IBViewGender;

@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewUserPic;
@property (strong, nonatomic) IBOutlet UIPickerView *IBPickerView;
@property (strong, nonatomic) IBOutlet UIScrollView* IBScrollViewContainData;
@property (strong, nonatomic) IBOutlet UIToolbar *IBToolBarForPickerViewHide;

@property (strong, nonatomic) IBOutlet UISwitch* IBSwitchTempImperial;
@property (strong, nonatomic) IBOutlet UISwitch* IBSwitchTempMetric;

@property (strong, nonatomic) IBOutlet UISwitch* IBSwitchDistImperial;
@property (strong, nonatomic) IBOutlet UISwitch* IBSwitchDistMetric;
@property (strong, nonatomic) IBOutlet UISwitch *IBSwitchManual;
@property (strong, nonatomic) IBOutlet UISwitch *IBSwitchAuto;
@property (strong, nonatomic) IBOutlet UISwitch *IBSwitchTideAlert;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonManual;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonWeight;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonHeight;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonAge;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonGender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *IBBarButtonTitle;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWeight;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelHeight;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAge;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelGender;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonAutoCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonManualCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonTideAlertOnCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonTideAlertOffCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonImperialUnitCheckBox;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonMetricUnitCheckBox;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelManual;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldLastName;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmail;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldBirthdate;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldPhone;


@property (strong, nonatomic) IBOutlet UIView *IBViewFirstName;
@property (strong, nonatomic) IBOutlet UIView *IBViewLastName;
@property (strong, nonatomic) IBOutlet UIView *IBViewEmail;
@property (strong, nonatomic) IBOutlet UIView *IBViewPassword;
@property (strong, nonatomic) IBOutlet UIView *IBViewBirthdate;
@property (strong, nonatomic) IBOutlet UIView *IBViewPhone;

@end
