//
//  ActivityDetailViewController.h
//  Freestyle
//
//  Created by BIT on 18/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FreestyleHelper.h"
#import "SikinLabel.h"
#import "SikinButton.h"
#import "ActivityInfo.h"
#import "Helper.h"
@interface ActivityDetailViewController : UIViewController{
    AppDelegate* appDelegate;
     GMSMapView *mapView_;
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintWidthForLabel;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActivityDetailTitle;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActivity;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelLocation;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelLocation1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWeather;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDate;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelTime;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDuration;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDistance;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary2;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary3;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary4;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSummary5;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDirection;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAltitude;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonExport;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonShare;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonDelete;

@property (strong, nonatomic) ActivityInfo *activity;
@property (strong, nonatomic) IBOutlet UIView *IBViewForActivityMap;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewActivityMap;

@end
