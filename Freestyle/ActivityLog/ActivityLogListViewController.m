

#import "ActivityLogListViewController.h"
#import "ActivityDetailViewController.h"
#import "ActivityTableViewCell.h"
#import "ActivityInfo.h"
@interface ActivityLogListViewController ()

@end

@implementation ActivityLogListViewController{
    
    NSMutableArray* arrayActivityList;
}


-(BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    DBHelper* dbHelper = [DBHelper getSharedInstance];
   arrayActivityList = [dbHelper getActivityLogs];
    [_IBTableViewActivityList reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return arrayActivityList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActivityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivityCell" forIndexPath:indexPath];
    
    ActivityInfo* ativityInfo =[arrayActivityList objectAtIndex:indexPath.row];
    cell.IBLabelActivityName.text = ativityInfo.ActName;
    cell.IBLabelActivityDate.text = [Helper getDateFromString:ativityInfo.ActDate];
    cell.IBLabelActivityInfo.text = [NSString stringWithFormat:@"%@",ativityInfo.Distance];
    
    // Configure the cell...
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"segueActivityDetailViewController" sender:[arrayActivityList objectAtIndex:indexPath.row]];
    
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"segueActivityDetailViewController"]) {
    
        ActivityDetailViewController* activityDetail = [segue destinationViewController];
        activityDetail.activity = sender;
    }
    

}
@end
