//
//  ActivityTableViewCell.h
//  Freestyle
//
//  Created by BIT on 18/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SikinLabel.h"
@interface ActivityTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActivityName;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActivityDate;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActivityInfo;

@end
