//
//  ActivityTableViewCell.m
//  Freestyle
//
//  Created by BIT on 18/03/15.
//
//

#import "ActivityTableViewCell.h"

@implementation ActivityTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}
@end
