//
//  ActivityDetailViewController.m
//  Freestyle
//
//  Created by BIT on 18/03/15.
//
//

#import "ActivityDetailViewController.h"
#import <MessageUI/MessageUI.h>
@interface ActivityDetailViewController ()<MFMailComposeViewControllerDelegate,UIAlertViewDelegate>

@end

@implementation ActivityDetailViewController{
    
    NSMutableString* sShare;
}

@synthesize IBNSLayoutConstraintWidthForLabel;
@synthesize activity;

#pragma mark - View Life Cycle
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    self.IBButtonExport.layer.cornerRadius = 2.0;
    self.IBButtonShare.layer.cornerRadius = 2.0;
    self.IBButtonDelete.layer.cornerRadius = 2.0;

    /*
     CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
     
     [geoCoder reverseGeocodeLocation:[[CLLocation alloc] initWithLatitude:[activity.EndLat doubleValue] longitude:[activity.EndLong doubleValue]] completionHandler:^(NSArray *placemarks, NSError *error)
     {
     
     CLPlacemark *placemark= [placemarks firstObject];
     
     NSString* sLocation = [NSString  stringWithFormat:@"%@, %@",[placemark locality],[placemark administrativeArea]];
     
     [self.IBLabelLocation1 setText:[NSString stringWithFormat:@"LOCATION : %@",[sLocation uppercaseString]]];
     }];*/
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_IBViewForActivityMap setHidden:true];
    [_IBViewForActivityMap setAlpha:0];
    
   // [_IBImageViewActivityMap setImage:[Helper getImageFromDocumentDirectoryOfAppImageName:[NSString stringWithFormat:@"%@_%@ %@.jpg",activity.ActName,[Helper getDateFromString:activity.ActDate],activity.ActTime]]];
    
    [self.IBLabelActivityDetailTitle setText:activity.ActName];
    
    
    [self.IBLabelSummary setText:[NSString stringWithFormat:@"ACTIVITY : %@",activity.ActName]];
    
    [self.IBLabelLocation1 setText:[NSString stringWithFormat:@"LOCATION : %@",activity.Location]];
    
    [self.IBLabelActivity setText:[NSString stringWithFormat:@"DATE : %@",[Helper getDateFromString:activity.ActDate]]];
    
    [self.IBLabelLocation setText:[NSString stringWithFormat:@"TIME : %@",activity.ActTime]];
    
    
    [self.IBLabelWeather setText:[NSString stringWithFormat:@"DURATION: %@",activity.Duration]];
    
    [self.IBLabelDate setText:[NSString stringWithFormat:@"AVERAGE SPEED : %@",activity.AvgSpeed]];
    
    [self.IBLabelTime setText:[NSString stringWithFormat:@"MIN SPEED : %@",activity.MinSpeed]];
    
    [self.IBLabelDuration setText:[NSString stringWithFormat:@"MAX SPEED : %@",activity.MaxSpeed]];
    
    [self.IBLabelDistance setText:[NSString stringWithFormat:@"DISTANCE : %@",activity.Distance]];
    
    [self.IBLabelSummary1 setText:[NSString stringWithFormat:@"HEART RATE : %@",@"--"]];
    //activity.HeartRate
    
    [self.IBLabelSummary2 setText:[NSString stringWithFormat:@"CALORIES : %@ KCAL",activity.Calories]];
    
    
    [self.IBLabelSummary3 setText:[NSString stringWithFormat:@"DIRECTION : %@",activity.Direction]];
    
    
    [self.IBLabelSummary4 setText:[NSString stringWithFormat:@"ELEVATION : %@",activity.Altitude]];
    
    /*
     [self.IBLabelSummary5 setText:[NSString stringWithFormat:@"TIME : %@",activity.ActTime]];
     [self.IBLabelDirection setText:[NSString stringWithFormat:@"TIME : %@",activity.ActTime]];
     [self.IBLabelAltitude setText:[NSString stringWithFormat:@"TIME : %@",activity.ActTime]];
     */
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[activity.StartLat doubleValue]
                                                            longitude:[[activity StartLong] doubleValue]                                                                 zoom:16];
    mapView_ = [GMSMapView mapWithFrame:_IBImageViewActivityMap.bounds camera:camera];
    mapView_.mapType = kGMSTypeNormal;
  
    //self.view = mapView_;
    
    
    [mapView_ clear];
    
    GMSMarker *startMarker = [[GMSMarker alloc] init];
    startMarker.position = CLLocationCoordinate2DMake([activity.StartLat doubleValue], [activity.StartLong doubleValue]);
    startMarker.title = activity.ActName;//@"START";
    //startMarker.snippet = self.strActTitle;
    startMarker.map = mapView_;
    
    GMSMutablePath *path = [GMSMutablePath path];
    [path addCoordinate:CLLocationCoordinate2DMake([activity.StartLat doubleValue], [activity.StartLong doubleValue])];
    
    
    NSArray* arrayOfLocation =[activity.stringOfLocations componentsSeparatedByString:@"|"];
    
    for (NSString* location in arrayOfLocation) {
        
        if(location.length >= 2){
        NSArray* coordinate =[location componentsSeparatedByString:@","];
        [path addCoordinate:CLLocationCoordinate2DMake([[coordinate objectAtIndex:0] doubleValue], [[coordinate objectAtIndex:1] doubleValue])];
        }
    }
    
    GMSPolyline *poly = [[GMSPolyline alloc] init];
    poly.path = path;
    poly.strokeWidth = 2;
    poly.geodesic = NO;
    poly.map = mapView_;
    [_IBImageViewActivityMap setUserInteractionEnabled:true];
    [_IBImageViewActivityMap addSubview:mapView_];
    [_IBImageViewActivityMap bringSubviewToFront:mapView_];
}


-(void) shareStringGenerate{
    sShare =[[NSMutableString alloc] initWithString:self.IBLabelSummary.text];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelLocation1.text]];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelActivity.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelLocation.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelWeather.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelDate.text]];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelTime.text]];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelDuration.text]];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelDistance.text]];
    
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelSummary1.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelSummary2.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelSummary3.text]];
    [sShare appendString:[NSString stringWithFormat:@"\n%@",self.IBLabelSummary4.text]];
}
- (void)viewDidLayoutSubviews{
    
    IBNSLayoutConstraintWidthForLabel.constant = self.view.bounds.size.width - 40;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Button Action
- (IBAction)deleteButtonTouchUpInsideAction:(UIButton*)sender{
    
    UIAlertView* alertView =[[UIAlertView alloc] initWithTitle:@"" message:@" Are you sure you want to delete activity ?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag = 1213;
    [alertView show];
    
}
- (IBAction)showActivityMapButtonTouchUpInsideAction:(UIButton *)sender {
  
    _IBViewForActivityMap.hidden = NO;
    [UIView animateWithDuration:0.2
                     animations:^{
                         //viewServeyCode.transform = CGAffineTransformMakeScale(1.05, 1.05);
                         //_IBViewForActivityMap.alpha = 0.8;
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:1/15.0
                                          animations:^{
                                              //viewServeyCode.transform = CGAffineTransformMakeScale(0.9, 0.9);
                                              //_IBViewForActivityMap.alpha = 0.9;
                                          }
                                          completion:^(BOOL finished) {
                                              [UIView animateWithDuration:1/7.5
                                                               animations:^{
                                                                   _IBViewForActivityMap.transform = CGAffineTransformIdentity;
                                                                   _IBViewForActivityMap.alpha = 1.0;
                                                               }
                                               ];
                                          }
                          ];
                     }
     ];
}
- (IBAction)closeActivityMapBottonTouchUpInsideAction:(UIButton *)sender {
    
    [_IBViewForActivityMap setHidden:true];

}
- (IBAction)shareButtonTouchUpInsideAction:(UIButton *)sender{
    [self shareStringGenerate];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[sShare]
                                      applicationActivities:nil];
    [self.navigationController presentViewController:activityViewController
                                            animated:YES
                                          completion:^{
                                              
                                              // ...
                                          }];
    
}

- (IBAction)exportButtonTouchUpInsideAction:(UIButton *)sender
{
    [self shareStringGenerate];
    
    NSArray *csvArray = [[sShare stringByReplacingOccurrencesOfString:@": " withString:@","] componentsSeparatedByString:@"\n"];
    NSString *csvString = [csvArray componentsJoinedByString:@",\n"];
    
    
    // Create .csv file and save in Documents Directory.
    
    /*
     //create instance of NSFileManager
     NSFileManager *fileManager = [NSFileManager defaultManager];
     
     //create an array and store result of our search for the documents directory in it
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     
     //create NSString object, that holds our exact path to the documents directory
     NSString *documentsDirectory = [paths objectAtIndex:0];
     NSLog(@"Document Dir: %@",documentsDirectory);
     
     NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv", @"userdata"]]; //add our file to the path
     [fileManager createFileAtPath:fullPath contents:[csvString dataUsingEncoding:NSUTF8StringEncoding] attributes:nil]; //finally save the path (file)
     */
    
    if([MFMailComposeViewController canSendMail]){
        MFMailComposeViewController *  mailComposeViewController = [[MFMailComposeViewController alloc] init];
        mailComposeViewController.mailComposeDelegate = self;
        [mailComposeViewController addAttachmentData:[csvString dataUsingEncoding:NSUTF8StringEncoding] mimeType:@"text/csv" fileName:[NSString stringWithFormat:@"%@_%@ %@.csv",activity.ActName,[Helper getDateFromString:activity.ActDate],activity.ActTime]];
        
        
        /*
         [mailComposeViewController setSubject:emailTitle];
         [mailComposeViewController setMessageBody:emailBody isHTML:NO];
         [mailComposeViewController setToRecipients:toRecipents];
         */
        // Present mail view controller on screen
        [self presentViewController:mailComposeViewController animated:YES completion:nil];
        mailComposeViewController = nil ;
    }else{
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:@"Sorry!"
                                  message:@"You can't send a email right now, make sure your device has an internet connection and you have                                          at least one email account setup"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:@"Setting",nil];
        alertView.tag = 1212;
        [alertView show];
    }
    
}

- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark - Alert View Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    float iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:{
            
            if (alertView.tag == 1212) {
                
                
                if (iOSVersion >=8.0){
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                }else{
                    
                    
                }
            }else if (alertView.tag == 1213){
                
                DBHelper* helper =[DBHelper getSharedInstance];
                [helper deleteActivityLog:activity.aID];
                [self.navigationController popViewControllerAnimated:true];
            }
        }break;
        default:
            break;
    }
}
#pragma mark - Mail Compose Controller View Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    switch (result) {
        case MFMailComposeResultSent:
            NSLog(@"You sent the email.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"You saved a draft of this email");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"You cancelled sending this email.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed:  An error occurred when trying to compose this email");
            break;
        default:
            NSLog(@"An error occurred when trying to compose this email");
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    
    //self.menuContainerViewController.toggleLeftSideMenuCompletion
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    
    
}


@end
