
#import <UIKit/UIKit.h>
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "Helper.h"
#import "DBHelper.h"

@interface ActivityLogListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *IBTableViewActivityList;
@end
