//
//  GeneralViewController.m
//  Freestyle
//
//  Created by BIT on 19/03/15.
//
//

#import "GeneralViewController.h"

@interface GeneralViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end

@implementation GeneralViewController{
    
    NSMutableArray* arrayPickerViewData;
    UIButton* selectedButton;
    UIImagePickerController *imagePickerController;
    int noOfComponentInPickerView;
    NSMutableArray* arrayOfInch;
    
    UITextField* textfieldCurrentEditing;
   
    CGFloat animatedDistance;
    
    CGFloat KEYBOARD_ANIMATION_DURATION;
    CGFloat MINIMUM_SCROLL_FRACTION;
    CGFloat MAXIMUM_SCROLL_FRACTION;
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    CGFloat LANDSCAPE_KEYBOARD_HEIGHT;
}

@synthesize IBNSLayoutConstraintForScrollViewContentWidth,
IBNSLayoutConstraintForDistanceBetweenSurfDataUpDateButtonAndTideAlertButton,
IBNSLayoutConstraintForDistanceBetweenTideAlertButtonAndDistanceOrHeightButton,
IBNSLayoutConstraintForDistanceBetweenDistanceOrHeightButtonTempButton,
IBNSLayoutConstraintForDistanceBetweenTempButtonAndActivityProfileButton,
IBNSLayoutConstraintForDistanceBetweenActivityProfileButtonAndBasicProfileButton,
IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView,
IBNSLayoutConstraintForScrollViewBottomToView,IBNSLayoutConstraintForDistanceBetweenBasicProfileButtonAndUpdatePhotoButton;

@synthesize IBViewAuto,
IBViewManual,
IBViewOn,
IBViewOff,
IBViewDistHeightImperial,IBViewDistHeightMetric,
IBViewTempImperial,IBViewTempMetric,
IBViewWeight,
IBViewHeight,
IBViewAge,
IBViewGender,
IBImageViewUserPic,
IBPickerView,IBScrollViewContainData,IBToolBarForPickerViewHide;


@synthesize IBSwitchTempImperial,
IBSwitchTempMetric,
IBSwitchDistImperial,
IBSwitchDistMetric,IBSwitchAuto,IBSwitchManual,IBSwitchTideAlert,IBBarButtonTitle;

@synthesize IBButtonAge,IBButtonGender,IBButtonHeight,IBButtonManual,IBButtonWeight;


#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    arrayPickerViewData = [[NSMutableArray alloc] init];
    
    IBPickerView.delegate = self;
    IBPickerView.dataSource = self;
    
    [IBSwitchDistMetric setOn:false];
    
    [IBSwitchTempMetric setOn:false];
    [IBSwitchAuto setOn:false];
    
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    
    appDelegate =(AppDelegate*)[[UIApplication sharedApplication] delegate];
    noOfComponentInPickerView = 1;
    arrayOfInch = [NSMutableArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",nil];
    
    KEYBOARD_ANIMATION_DURATION = 0.3;
    MINIMUM_SCROLL_FRACTION = 0.2;
    MAXIMUM_SCROLL_FRACTION = 0.8;
    if (IS_IPAD) {
        
        PORTRAIT_KEYBOARD_HEIGHT = 266 + 50;
        LANDSCAPE_KEYBOARD_HEIGHT = 352 + 50 ;
        
    }else{
        
        PORTRAIT_KEYBOARD_HEIGHT = 216 + 50;
        LANDSCAPE_KEYBOARD_HEIGHT = 162 + 50;
    }
    
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    IBViewAuto.alpha = 0;
    IBViewManual.alpha = 0;
    IBViewOn.alpha = 0;
    IBViewOff.alpha = 0;
    
    IBViewDistHeightImperial.alpha = 0;
    IBViewDistHeightMetric.alpha = 0;
    
    IBViewTempImperial.alpha = 0;
    IBViewTempMetric.alpha = 0;
    
    IBViewWeight.alpha = 0;
    IBViewHeight.alpha = 0;
    IBViewAge.alpha = 0;
    IBViewGender.alpha = 0 ;
    
    _IBViewBirthdate.alpha = 0;
    _IBViewEmail.alpha = 0;
    _IBViewFirstName.alpha = 0;
    _IBViewLastName.alpha = 0;
    _IBViewPassword.alpha = 0;
    _IBViewPhone.alpha = 0;
    
    
    NSDictionary* userInfo =[Helper getPREFID:@"prefUserLoginInfo"];
    
         // NSDictionary* dicParam=[[NSDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"],@"Email",@"",@"Password",[result objectForKey:@"id"],@"FacebookId",[result objectForKey:@"first_name"],@"FirstName",[result objectForKey:@"last_name"],@"LastName",[result objectForKey:@"gender"],@"Gender",finalDOB,@"BirthDate",@"",@"PhoneNo",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.latitude],@"Lat",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.longitude],@"Long",nil];
    
    
    UIToolbar *nextPreviousAndDoneToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    nextPreviousAndDoneToolbar.barStyle = UIBarStyleDefault;
    nextPreviousAndDoneToolbar.tintColor = [UIColor blackColor];
    
    
    UIBarButtonItem *btnDone =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBarButtonActionaa:)];
    
    //    [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(doneBarButtonAction)];
    
    UIBarButtonItem *btnPrevious =[[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleDone target:self action:@selector(previousBarButtonAction:)];
    
    UIBarButtonItem *btnNext =[[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextBarButtonAction:)];
    
    //[btnDone setTitleTextAttributes:@{ NSFontAttributeName:FONT_LATO_LIGHT_15} forState:UIControlStateNormal];
    nextPreviousAndDoneToolbar.items = [NSArray arrayWithObjects:btnPrevious,btnNext,
                                        [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                        btnDone,
                                        nil];
    [nextPreviousAndDoneToolbar sizeToFit];
    
    
    _IBTextFieldEmail.text =[userInfo objectForKey:@"Email"];
    _IBTextFieldEmail.inputAccessoryView = nextPreviousAndDoneToolbar;
    
    
    UIDatePicker* datePicker=[[UIDatePicker alloc] init];
    
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setTimeZone:[NSTimeZone localTimeZone]];
    [datePicker setLocale:[NSLocale autoupdatingCurrentLocale]];
    [datePicker setCalendar:[NSCalendar currentCalendar]];
    [datePicker setMaximumDate:[NSDate date]];
    

    
    NSDateFormatter* dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];

    NSString* sDOB = [userInfo objectForKey:@"BirthDate"];
    if(sDOB != nil && sDOB.length>0 && ([sDOB isEqualToString:@"0000-00-00"] != true)){
        [datePicker setDate:[dateFormatter dateFromString:sDOB]];
        _IBTextFieldBirthdate.text =sDOB;
    }else{
        
        [datePicker setDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*(-1)]];
        _IBTextFieldBirthdate.text=@"";
    }
    
    [datePicker addTarget:self action:@selector(datePickerDateChange:) forControlEvents:UIControlEventValueChanged];

    [datePicker setBackgroundColor:[UIColor clearColor]];
    
    
    
    
    _IBTextFieldBirthdate.inputView=datePicker;
    _IBTextFieldBirthdate.inputView.backgroundColor=[UIColor clearColor];
    _IBTextFieldBirthdate.inputAccessoryView = nextPreviousAndDoneToolbar;
     _IBTextFieldFirstName.text =[userInfo objectForKey:@"FirstName"];
     _IBTextFieldFirstName.inputAccessoryView = nextPreviousAndDoneToolbar;
     _IBTextFieldLastName.text =[userInfo objectForKey:@"LastName"];
    _IBTextFieldLastName.inputAccessoryView = nextPreviousAndDoneToolbar;
     _IBTextFieldPassword.text =[userInfo objectForKey:@"Password"];
    _IBTextFieldPassword.inputAccessoryView = nextPreviousAndDoneToolbar;
     _IBTextFieldPhone.text =[userInfo objectForKey:@"PhoneNo"];
    _IBTextFieldPhone.inputAccessoryView = nextPreviousAndDoneToolbar;
    
    
    datePicker=nil;
    nextPreviousAndDoneToolbar = nil;
    btnDone = nil;
    
    IBImageViewUserPic.alpha = 1;
    UIImage* image = [Helper getImageFromDocumentDirectoryOfAppImageName:@"userProfile@2x.jpg"];
    if (image !=nil) {
        
        
        IBImageViewUserPic.image = image;//[Helper resizeImage:image toAspectWidth:(int)IBImageViewUserPic.frame.size.width andAspectHeight:(int)IBImageViewUserPic.frame.size.height isAspectFill:YES ignoresRotation:NO];
        
        
    }
    
    IBPickerView.alpha = 0;
    IBToolBarForPickerViewHide.alpha = 0;
    IBNSLayoutConstraintForDistanceBetweenSurfDataUpDateButtonAndTideAlertButton.constant = 8;
    
    IBNSLayoutConstraintForDistanceBetweenTideAlertButtonAndDistanceOrHeightButton.constant = 8;
    IBNSLayoutConstraintForDistanceBetweenDistanceOrHeightButtonTempButton.constant = 8;
    IBNSLayoutConstraintForDistanceBetweenTempButtonAndActivityProfileButton.constant = 0;
    IBNSLayoutConstraintForDistanceBetweenActivityProfileButtonAndBasicProfileButton.constant = 8;
    IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView.constant = 140;
    IBNSLayoutConstraintForScrollViewBottomToView.constant = 20;
    IBNSLayoutConstraintForDistanceBetweenBasicProfileButtonAndUpdatePhotoButton.constant = 8;
    
    [_IBButtonImperialUnitCheckBox setSelected:true];
    
    NSString* sUnit=[Helper getPREF:@"prfUnit"];
    if ([sUnit isEqualToString:@"IMPERIAL"]) {
        [IBSwitchDistImperial setOn:true];
        [IBSwitchDistMetric setOn:false];
        
        
        [_IBButtonImperialUnitCheckBox setSelected:true];
        [_IBButtonImperialUnitCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        [_IBButtonMetricUnitCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [_IBButtonMetricUnitCheckBox setSelected:false];
        
        
        
    }else if([sUnit isEqualToString:@"METRIC"]){
        [_IBButtonMetricUnitCheckBox setSelected:true];
        [_IBButtonMetricUnitCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        [_IBButtonImperialUnitCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [_IBButtonImperialUnitCheckBox setSelected:false];
    }
    NSString* sManlAut=[Helper getPREF:@"prfManualAuto"];
    
    if ([sManlAut isEqualToString:@"MANUAL"]){
        [IBSwitchManual setOn:true];
        [IBSwitchAuto setOn:false];
        
        
        
        [_IBButtonManualCheckBox setSelected:true];
        [_IBButtonManualCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        [_IBButtonAutoCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [_IBButtonAutoCheckBox setSelected:false];
        
        NSString* sManualUpdate = [Helper getPREF:@"prfManual"];
        if (sManualUpdate!=nil) {
            _IBLabelManual.text = sManualUpdate;
        }
        else{
            _IBLabelManual.text = @"3 hr";
        }
    
    }else if([sManlAut isEqualToString:@"AUTO"]){
        [IBSwitchAuto setOn:true];
        [IBSwitchManual setOn:false];
        
        [_IBButtonAutoCheckBox setSelected:true];
        [_IBButtonAutoCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        [_IBButtonManualCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [_IBButtonManualCheckBox setSelected:false];
    }
    
    NSString* sTideAlert=[Helper getPREF:@"prfTideAlert"];
    
    if ([sTideAlert isEqualToString:@"ON"]) {
        [_IBButtonTideAlertOnCheckBox setSelected:true];
        [_IBButtonTideAlertOnCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
        
        [IBSwitchTideAlert setOn:true];
        
    }else if([sTideAlert isEqualToString:@"OFF"]){
        [_IBButtonTideAlertOnCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
        [_IBButtonTideAlertOnCheckBox setSelected:false];
        [IBSwitchTideAlert setOn:false];
    }
    NSString* sAge = [Helper getPREF:@"prfAge"];
    if (sAge != nil) {
        _IBLabelAge.text = sAge;
    }else{
        _IBLabelAge.text = @"-";
    }
    
    NSString* sGender = [Helper getPREF:@"prfGender"];
    if (sGender != nil) {
        _IBLabelGender.text = sGender;
    }else{
        _IBLabelGender.text = @"-";
    }
    
    NSString* userGender= [userInfo objectForKey:@"Gender"];
    if (sGender == nil || userGender == nil || userGender.length==0 || userGender.length == 1) {
        _IBLabelGender.text =@"MALE";
    }else{
       _IBLabelGender.text = [userInfo objectForKey:@"Gender"];
    }

    [Helper setPREFID:_IBLabelGender.text :@"prfGender"];
    //[IBButtonGender setTitle:[Helper getPREF:@"prfGender"]
    //  forState:UIControlStateNormal];
    
    NSString* sHeight = [Helper getPREF:@"prfHeight"];
    if (sHeight != nil) {
        
        if (self.IBButtonImperialUnitCheckBox.selected == true) {
            
            //1ft = 12 inch IMperial
            // 1 inch = 2.54 cm
            
            float heightInCm = [[Helper getDigitFromString:[Helper getPREF:@"prfHeight"]] floatValue] / 100;
            float heightinft = heightInCm / 30.48;
            
            int remining = INT16_C(heightinft);
            
            float heightinInc = (heightinft - remining) * 12;
            
            _IBLabelHeight.text = [NSString stringWithFormat:@"%d' %.0f\" ft",remining,heightinInc];
        }else {
            _IBLabelHeight.text = sHeight;
            if ([_IBLabelHeight.text containsString:@"."]){
                NSArray* heightData=[sHeight componentsSeparatedByString:@"."];
                
                _IBLabelHeight.text=[NSString stringWithFormat:@"%@ cm",[heightData objectAtIndex:0]];
                
                
            }
            
        }
    }else{
        _IBLabelHeight.text = @"-";
    }

    // [IBButtonHeight setTitle:[Helper getPREF:@"prfHeight"]
    //  forState:UIControlStateNormal];
    
    NSString* sWeight = [Helper getPREF:@"prfWeight"];
    if (sWeight!=nil) {
        _IBLabelWeight.text = sWeight;
    }else {
        _IBLabelWeight.text = @"-";
        
    }
    
    
    // [IBButtonWeight setTitle:[Helper getPREF:@"prfWeight"]
    // forState:UIControlStateNormal];
    
    //.text = [Helper getPREF:@"prfWeight"];
    
    //[IBButtonManual setTitle:[Helper getPREF:@"prfManual"]
    //          forState:UIControlStateNormal];
    
    
    
    
    
    
}

- (void)viewWillLayoutSubviews{
    
    IBNSLayoutConstraintForScrollViewContentWidth.constant = self.view.frame.size.width - 40 ;
}
- (void) viewDidLayoutSubviews{
    
    if ([selectedButton isEqual:IBButtonManual]) {
        CGRect viewScrollRect = IBViewManual.frame;
        viewScrollRect.size = IBScrollViewContainData.bounds.size;
        [IBScrollViewContainData scrollRectToVisible:viewScrollRect animated:true];
    }else if ([selectedButton isEqual:IBButtonWeight]){
        
        CGRect viewScrollRect = IBViewWeight.frame;
        viewScrollRect.size = IBScrollViewContainData.bounds.size;
        [IBScrollViewContainData scrollRectToVisible:viewScrollRect animated:true];
    }else if ([selectedButton isEqual:IBViewHeight]){
        
        CGRect viewScrollRect = IBViewHeight.frame;
        viewScrollRect.size = IBScrollViewContainData.bounds.size;
        [IBScrollViewContainData scrollRectToVisible:viewScrollRect animated:true];
    }else if([selectedButton isEqual:IBButtonAge]){
        CGRect viewScrollRect = IBViewAge.frame;
        viewScrollRect.size = IBScrollViewContainData.bounds.size;
        [IBScrollViewContainData scrollRectToVisible:viewScrollRect animated:true];
    }else if ([selectedButton isEqual:IBButtonGender]) {
        CGRect viewScrollRect = IBViewGender.frame;
        viewScrollRect.size = IBScrollViewContainData.bounds.size;
        [IBScrollViewContainData scrollRectToVisible:viewScrollRect animated:true];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Picker View data Source And delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return noOfComponentInPickerView;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    NSInteger noOfRow = 0;
    
    switch (component) {
        case 0:
            noOfRow = arrayPickerViewData.count;
            break;
        case 1:
            noOfRow = arrayOfInch.count;
            break;
        default:
            break;
    }
    
    return noOfRow;
}
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    
    NSAttributedString *attString;
    switch (component) {
        case 0:
            attString  = [[NSAttributedString alloc] initWithString:[arrayPickerViewData objectAtIndex:row] attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
            break;
        case 1:
            attString  = [[NSAttributedString alloc] initWithString:[arrayOfInch objectAtIndex:row] attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
        default:
            break;
    }
    
    
    return attString;
}
/*
 - (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
 
 
 if ([selectedButton isEqual:IBButtonManual]) {
 
 [selectedButton setTitle:[NSString stringWithFormat:@"MANUAL : %@ hr",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
 
 }else if ([selectedButton isEqual:IBButtonWeight]){
 
 [selectedButton setTitle:[NSString stringWithFormat:@"WEIGHT : %@ kg",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
 
 }else if ([selectedButton isEqual:IBButtonHeight]){
 
 [selectedButton setTitle:[NSString stringWithFormat:@"HEIGHT : %@ cm",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
 
 }else if ([selectedButton isEqual:IBButtonAge]){
 
 [selectedButton setTitle:[NSString stringWithFormat:@"AGE : %@ year",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
 
 }else if ([selectedButton isEqual:IBButtonGender]){
 
 [selectedButton setTitle:[NSString stringWithFormat:@"GENDER : %@",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
 
 }
 }
 */

#pragma mark - Button Action

- (IBAction)surfDataUpdateButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenSurfDataUpDateButtonAndTideAlertButton.constant = 112;
            IBViewAuto.alpha = 1;
            IBViewManual.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenSurfDataUpDateButtonAndTideAlertButton.constant = 8;
            IBViewAuto.alpha = 0;
            IBViewManual.alpha = 0;
            
        }];
        sender.selected = false;
    }
}
- (IBAction)tideAlertButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenTideAlertButtonAndDistanceOrHeightButton.constant = 60;
            IBViewOn.alpha = 1;
            IBViewOff.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenTideAlertButtonAndDistanceOrHeightButton.constant = 8;
            IBViewOff.alpha = 0;
            IBViewOn.alpha = 0;
            
        }];
        sender.selected = false;
    }
}
- (IBAction)distanceOrHeightButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenDistanceOrHeightButtonTempButton.constant = 112;
            
            IBViewDistHeightImperial.alpha = 1;
            IBViewDistHeightMetric.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenDistanceOrHeightButtonTempButton.constant = 8;
            IBViewDistHeightImperial.alpha = 0;
            IBViewDistHeightMetric.alpha = 0;
            
            
        }];
        sender.selected = false;
    }
}
- (IBAction)tempButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenTempButtonAndActivityProfileButton.constant = 112;
            IBViewTempImperial.alpha = 1;
            IBViewTempMetric.alpha = 1;
            
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenTempButtonAndActivityProfileButton.constant = 8;
            IBViewTempImperial.alpha = 0;
            IBViewTempMetric.alpha = 0;
            
            
            
        }];
        sender.selected = false;
    }
}
- (IBAction)activityProfileButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            
            IBNSLayoutConstraintForDistanceBetweenActivityProfileButtonAndBasicProfileButton.constant = 216;
            IBViewWeight.alpha = 1;
            IBViewHeight.alpha = 1;
            IBViewAge.alpha = 1;
            IBViewGender.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenActivityProfileButtonAndBasicProfileButton.constant = 8;
            IBViewWeight.alpha = 0;
            IBViewHeight.alpha = 0;
            IBViewAge.alpha = 0;
            IBViewGender.alpha = 0;
            
            
        }];
        sender.selected = false;
    }
}

- (IBAction)basicProfileButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            
            IBNSLayoutConstraintForDistanceBetweenBasicProfileButtonAndUpdatePhotoButton.constant = 320;
            _IBViewBirthdate.alpha = 1;
            _IBViewEmail.alpha = 1;
            _IBViewFirstName.alpha = 1;
            _IBViewLastName.alpha = 1;
            _IBViewPassword.alpha = 1;
            _IBViewPhone.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenBasicProfileButtonAndUpdatePhotoButton.constant = 8;
            _IBViewBirthdate.alpha = 0;
            _IBViewEmail.alpha = 0;
            _IBViewFirstName.alpha = 0;
            _IBViewLastName.alpha = 0;
            _IBViewPassword.alpha = 0;
            _IBViewPhone.alpha = 0;
            
            
        }];
        sender.selected = false;
    }
}

- (IBAction)updatePhotoButtonTouchUpInsideAction:(UIButton *)sender{
    /*IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView.constant = 130;
     IBImageViewUserPic.alpha = 1;*/
  /*
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView.constant = 130;
            IBImageViewUserPic.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView.constant = 8;
            IBImageViewUserPic.alpha = 0;
            
        }];
        sender.selected = false;
    }*/
    
}


- (IBAction)autoButtonTouchUpInsideAction:(UIButton *)sender {
    
}
- (IBAction)manualButtonTouchUpInsideAction:(UIButton *)sender {
    if (_IBButtonManualCheckBox.selected == true) {
        
        noOfComponentInPickerView = 1;
        
        IBBarButtonTitle.title = @"SURF DATA UPDATE";
        selectedButton = sender;
        IBNSLayoutConstraintForScrollViewBottomToView.constant  = IBToolBarForPickerViewHide.frame.size.height + IBPickerView.frame.size.height;
        
        [arrayPickerViewData removeAllObjects];
        [arrayPickerViewData addObject:@"1"];
        [arrayPickerViewData addObject:@"2"];
        [arrayPickerViewData addObject:@"3"];
        
        [IBPickerView reloadAllComponents];
        
        if (_IBLabelManual.text.length > 0) {
            
            
            [IBPickerView selectRow:[[Helper getDigitFromString:_IBLabelManual.text] intValue] - 1  inComponent:0 animated:false];
        }
        [UIView animateWithDuration:1 animations:^{
            IBToolBarForPickerViewHide.alpha = 1;
            IBPickerView.alpha = 1;
        }];
        
    }
    
}
- (IBAction)weightButtonTouchUpInsideAction:(UIButton *)sender {
    
    IBBarButtonTitle.title = @"WEIGHT";
    noOfComponentInPickerView = 1;
    
    selectedButton = sender;
    IBNSLayoutConstraintForScrollViewBottomToView.constant  = IBToolBarForPickerViewHide.frame.size.height + IBPickerView.frame.size.height;
    
    [arrayPickerViewData removeAllObjects];
    
    //1kg = 2.20462 lbs imperial
    
    float weightMultiplayingFactor = 0.0;
    if (_IBButtonImperialUnitCheckBox.selected == true) {
        
        weightMultiplayingFactor = 2.20462;
        
    }else{
        weightMultiplayingFactor = 1;
    }
    
    float finalMin = 30 * weightMultiplayingFactor;
    float finalMax = 180 * weightMultiplayingFactor;
    
    for (int i = finalMin; i <= finalMax ; i++) {
        [arrayPickerViewData addObject:[NSString stringWithFormat:@"%i",i]];
    }
    
    [IBPickerView reloadAllComponents];
    
    if (appDelegate.gActWeight != 0) {
        
        [IBPickerView selectRow:[[Helper getDigitFromString:_IBLabelWeight.text] intValue] - finalMin   inComponent:0 animated:false];
    }
    
    [UIView animateWithDuration:1 animations:^{
        IBToolBarForPickerViewHide.alpha = 1;
        IBPickerView.alpha = 1;
        
    }];
    
    
}
- (IBAction)heightButtonTouchUpInsideAction:(UIButton *)sender {
    
    //1ft = 12 inch IMperial
    // 1 inch = 2.54 cm
    
    
    IBBarButtonTitle.title = @"HEIGHT";
    selectedButton = sender;
    [arrayPickerViewData removeAllObjects];
    
    if(self.IBButtonImperialUnitCheckBox.selected == true){
        
        noOfComponentInPickerView = 2;
        
        for (int i = 3; i <= 6 ; i++) {
            [arrayPickerViewData addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        [IBPickerView reloadAllComponents];
        
        if ([_IBLabelHeight.text isEqualToString:@"-"] == false)
        {
            NSString* height = [Helper getDigitFromString:_IBLabelHeight.text];
            
            if (height.length ==3){
                
                NSNumber* num = [NSNumber numberWithDouble:[height doubleValue]];
                double compo =  [num doubleValue]/100;
                
                int compoSelected = INT16_C(compo);
                [IBPickerView selectRow:compoSelected - 3  inComponent:0 animated:false];
                float lastdigit = (compo - compoSelected) * 100;
                
                [IBPickerView selectRow:INT16_C(lastdigit)   inComponent:1 animated:false];
                
            }else if (height.length <=2 ){
                
                NSNumber* num = [NSNumber numberWithDouble:[height doubleValue]];
                double compo =  [num doubleValue]/10;
                int compoSelected = INT16_C(compo);
                [IBPickerView selectRow:compoSelected - 3  inComponent:0 animated:false];
                float lastdigit = (compo - compoSelected) * 10;
                [IBPickerView selectRow:INT16_C(lastdigit)   inComponent:1 animated:false];
                
            }
            
            
        }
        
        
    }else if(self.IBButtonMetricUnitCheckBox.selected == true){
        
        noOfComponentInPickerView = 1;
        for (int i = 91; i <= 212 ; i++) {
            [arrayPickerViewData addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        [IBPickerView reloadAllComponents];
        
        if ([_IBLabelHeight.text isEqualToString:@"-"] == false) {
            NSString* height = [Helper getDigitFromString:_IBLabelHeight.text];
            float finalHeight;
            if (height.length>3) {
                finalHeight  = [height floatValue]/100;
            }
            else{
                finalHeight =[height integerValue];
            }
            
            [IBPickerView selectRow:INT16_C(finalHeight) - 91  inComponent:0 animated:false];
        }
        
    }
    
    IBNSLayoutConstraintForScrollViewBottomToView.constant  = IBToolBarForPickerViewHide.frame.size.height + IBPickerView.frame.size.height;
    
    
    
    
    
    /*
     if (appDelegate.gActHeight != 0) {
     [IBPickerView selectRow:[[Helper getDigitFromString:_IBLabelHeight.text] intValue] - 10  inComponent:0 animated:false];
     }
     */
    
    [UIView animateWithDuration:1 animations:^{
        IBToolBarForPickerViewHide.alpha = 1;
        IBPickerView.alpha = 1;
        
        
    }];
    
}
- (IBAction)ageButtonTouchUpInsideAction:(UIButton *)sender {
    
    if(IBPickerView.alpha){
        
        
    }
    
    IBBarButtonTitle.title = @"AGE";
    
    noOfComponentInPickerView = 1;
    
    selectedButton = sender;
    
    IBNSLayoutConstraintForScrollViewBottomToView.constant  = IBToolBarForPickerViewHide.frame.size.height + IBPickerView.frame.size.height;
    
    [arrayPickerViewData removeAllObjects];
    
    for (int i = 12; i <= 100 ; i++) {
        [arrayPickerViewData addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [IBPickerView reloadAllComponents];
    
    if(appDelegate.gActAge != 0){
        
        [IBPickerView selectRow:[[Helper getDigitFromString:_IBLabelAge.text] intValue] - 12  inComponent:0 animated:false];
    }
    
    [UIView animateWithDuration:1 animations:^{
        IBToolBarForPickerViewHide.alpha = 1;
        IBPickerView.alpha = 1;
    }];
    
    
}
- (IBAction)genderButtonTouchUpInsideAction:(UIButton *)sender {
    
    
    IBBarButtonTitle.title = @"GENDER";
    
    selectedButton = sender;
    
    noOfComponentInPickerView = 1;
    
    
    IBNSLayoutConstraintForScrollViewBottomToView.constant  = IBToolBarForPickerViewHide.frame.size.height + IBPickerView.frame.size.height;
    
    
    [arrayPickerViewData removeAllObjects];
    [arrayPickerViewData addObject:@"MALE"];
    [arrayPickerViewData addObject:@"FEMALE"];
    [IBPickerView reloadAllComponents];
    
    if ([_IBLabelGender.text isEqualToString:@"MALE"]) {
        [IBPickerView selectRow:0  inComponent:0 animated:false];
    }
    else{
        [IBPickerView selectRow:1  inComponent:0 animated:false];
    }
    
    [UIView animateWithDuration:1 animations:^{
        IBToolBarForPickerViewHide.alpha = 1;
        IBPickerView.alpha = 1;
        
    }];
    
}

- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)tideAlertOnOffSwitchValueChangedAction:(UISwitch *)sender {
    
    NSString* sTideAlert=nil;
    if (sender.on == true) {
        sTideAlert = @"ON";
    }else if(sender.on == false){
        sTideAlert = @"OFF";
    }
    [Helper setPREFStringValue:sTideAlert sKey:@"prfTideAlert"];
}
- (IBAction)distanceOrHeightImperialSwitchValueChangedAction:(UISwitch *)sender {
    [IBSwitchDistMetric setOn:!sender.on];
    
    [self saveUnitInPrf];
    
}
- (IBAction)distanceOrHeightMetricSwitchValueChangedAction:(UISwitch *)sender {
    [IBSwitchDistImperial setOn:!sender.on];
    [self saveUnitInPrf];
}
- (IBAction)temperatureImperialSwitchValueChangedAction:(UISwitch *)sender {
    [IBSwitchTempMetric setOn:!sender.on];
}
- (IBAction)temperatureMetricSwitchValueChangedAction:(UISwitch *)sender {
    [IBSwitchTempImperial setOn:!sender.on];
}
- (IBAction)surfDataUpdateAutoSwitchValueChangedAction:(UISwitch *)sender{
    [IBSwitchManual setOn:!sender.on];
    [self saveSurfDataUpdateAutoManual];
}
- (IBAction)surfDataUpdateManualSwitchValueChangedAction:(UISwitch *)sender{
    [IBSwitchAuto setOn:!sender.on];
    [self saveSurfDataUpdateAutoManual];
}
- (IBAction)doneBarButtonAction:(UIBarButtonItem *)sender {
    

    [UIView animateWithDuration:0 animations:^{
        
        IBToolBarForPickerViewHide.alpha = 0;
        IBPickerView.alpha = 0;
        IBNSLayoutConstraintForScrollViewBottomToView.constant = 20;
        
    }];
    
    if ([sender.title isEqual:@"Done"]) {
        
        NSUInteger row = [IBPickerView selectedRowInComponent:0];
        if ([selectedButton isEqual:IBButtonManual]) {
            //[selectedButton setTitle:[NSString stringWithFormat:@"MANUAL : %@ hr",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
            _IBLabelManual.text = [NSString stringWithFormat:@"%@ hr",[arrayPickerViewData objectAtIndex:row]];
            [Helper setPREFStringValue:_IBLabelManual.text sKey:@"prfManual"];
            
        }else if ([selectedButton isEqual:IBButtonWeight]){
            
            //[selectedButton setTitle:[NSString stringWithFormat:@"WEIGHT : %@ kg",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
            NSString* stringweightUnit;
            if (_IBButtonImperialUnitCheckBox.selected == true) {
                
                stringweightUnit = @"lbs";
                appDelegate.dWeight =  [[Helper getDigitFromString:[arrayPickerViewData objectAtIndex:row]] floatValue] * 0.453592;
                
            }else{
                
                stringweightUnit = @"kg";
                appDelegate.dWeight = [[arrayPickerViewData objectAtIndex:row] doubleValue];
            }
            
            
            _IBLabelWeight.text = [NSString stringWithFormat:@"%@ %@",[arrayPickerViewData objectAtIndex:row],stringweightUnit];
            appDelegate.gActWeight= [[arrayPickerViewData objectAtIndex:row] intValue];
            
            [Helper setPREFStringValue:_IBLabelWeight.text sKey:@"prfWeight"];
            
        }else if ([selectedButton isEqual:IBButtonHeight]){
            
            //[selectedButton setTitle:[NSString stringWithFormat:@"HEIGHT : %@ cm",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
            float heightInCm = 0;
            
            if (self.IBButtonImperialUnitCheckBox.selected == true) {
                
                //1ft = 12 inch IMperial
                // 1 inch = 2.54 cm
                _IBLabelHeight.text = [NSString stringWithFormat:@"%@' %@\" ft",[arrayPickerViewData objectAtIndex:row],[arrayOfInch objectAtIndex:[IBPickerView selectedRowInComponent:1]]];
                
                heightInCm = (([[arrayPickerViewData objectAtIndex:row] floatValue] * 12 ) + [[arrayOfInch objectAtIndex:[IBPickerView selectedRowInComponent:1]] floatValue]) * 2.54;
                
                
            }else if (self.IBButtonMetricUnitCheckBox.selected == true){
                
                _IBLabelHeight.text = [NSString stringWithFormat:@"%@ cm",[arrayPickerViewData objectAtIndex:row]];
                
                heightInCm = [[arrayPickerViewData objectAtIndex:row] intValue];
            }
            
            appDelegate.gActHeight = [[NSString stringWithFormat:@"%.0f",heightInCm] intValue];
            
            [Helper setPREFStringValue:[NSString stringWithFormat:@"%.2f cm",heightInCm] sKey:@"prfHeight"];
            
        }else if ([selectedButton isEqual:IBButtonAge]){
            
            //[selectedButton setTitle:[NSString stringWithFormat:@"AGE : %@",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
            _IBLabelAge.text = [NSString stringWithFormat:@"%@",[arrayPickerViewData objectAtIndex:row]];
            appDelegate.gActAge = [[arrayPickerViewData objectAtIndex:row] intValue];
            
            [Helper setPREFStringValue:_IBLabelAge.text sKey:@"prfAge"];
            
        }else if ([selectedButton isEqual:IBButtonGender]){
            
            // [selectedButton setTitle:[NSString stringWithFormat:@"GENDER : %@",[arrayPickerViewData objectAtIndex:row]] forState:UIControlStateNormal];
            _IBLabelGender.text = [NSString stringWithFormat:@"%@",[arrayPickerViewData objectAtIndex:row]];
            appDelegate.gActGender = [arrayPickerViewData objectAtIndex:row];
            [Helper setPREFStringValue:_IBLabelGender.text sKey:@"prfGender"];
        }
        
        //IBButtonAge.titleLabel.text
        //IBButtonGender.titleLabel.text
        //IBButtonHeight.titleLabel.text
        //IBButtonWeight.titleLabel.text
        //IBButtonManual.titleLabel.text
        //[self saveUnitInPrf];
    }
    
}

- (IBAction)imageViewTapGestureAction:(UITapGestureRecognizer *)sender {
    
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
}
-(void)saveUnitInPrf{
    NSString* sUnit=nil;
    
    if (IBSwitchDistImperial.on == true) {
        sUnit = @"IMPERIAL";
    }else if(IBSwitchDistMetric.on == true){
        sUnit = @"METRIC";
    }
    appDelegate.gActUnit = sUnit;
    
    [Helper setPREFStringValue:sUnit sKey:@"prfUnit"];
}
-(void)saveSurfDataUpdateAutoManual{
    NSString* sManlAut=nil;
    
    if (IBSwitchManual.on == true) {
        sManlAut = @"MANUAL";
    }else if(IBSwitchAuto.on == true){
        sManlAut = @"AUTO";
    }
    [Helper setPREFStringValue:sManlAut sKey:@"prfManualAuto"];
}

#pragma mark - Image Picker View Controller Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //Or you can get the image url from AssetsLibrary
    
   
    //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
         [IBImageViewUserPic setImage:image];
        
        [Helper saveImageToDocumentDirectoryOfAppImage:image nameOfImage:@"userProfile@2x.jpg"];
        
        [UIView animateWithDuration:0.1 animations:^{
            //IBNSLayoutConstraintForDistanceBetweenUpdatePhotoButtonAndScrollView.constant = 130;
            IBImageViewUserPic.alpha = 1;
            
        }];
    }];
}

- (IBAction)UnitChangeTouchUpInsideAction:(UIButton *)sender {
    
    NSString* sUnit=nil;
    
    if (IBPickerView.alpha == 1){
        
        [self doneBarButtonAction:nil];
        
    }
    
    if ([sender isEqual:_IBButtonImperialUnitCheckBox]) {
        
        if (sender.selected == true) {
            sUnit = @"METRIC";
            sender.selected = false;
            [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [_IBButtonMetricUnitCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            _IBButtonMetricUnitCheckBox.selected = true;
        }else if (sender.selected == false) {
            sender.selected = true;
            sUnit = @"IMPERIAL";
            [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            [_IBButtonMetricUnitCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            _IBButtonMetricUnitCheckBox.selected = false;
        }
    }else if ([sender isEqual:_IBButtonMetricUnitCheckBox]) {
        
        if (sender.selected == true) {
            sUnit = @"IMPERIAL";
            sender.selected = false;
            [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [_IBButtonImperialUnitCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            _IBButtonImperialUnitCheckBox.selected = true;
        }else if (sender.selected == false) {
            sender.selected = true;
            sUnit = @"METRIC";
            [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            [_IBButtonImperialUnitCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            _IBButtonImperialUnitCheckBox.selected = false;
        }
    }
    [self changeWeightAndHeightAccordingToUnit];
    appDelegate.gActUnit = sUnit;
    [Helper setPREFStringValue:sUnit sKey:@"prfUnit"];
    
}
-(void)changeWeightAndHeightAccordingToUnit{
    
    if (_IBButtonMetricUnitCheckBox.selected == true) {
        NSString* weight= [Helper getPREF:@"prfWeight"];
        if ([weight containsString:@"lbs"]) {
            
            float fWeight = [[Helper getDigitFromString:weight] floatValue] * 0.453592;
            
            [Helper setPREFStringValue:[NSString stringWithFormat:@"%.0f kg",fWeight] sKey:@"prfWeight"];
            [_IBLabelWeight setText:[NSString stringWithFormat:@"%.0f kg",fWeight]];
            
        }
        NSString* sHeight = [Helper getPREF:@"prfHeight"];
        if (sHeight != nil) {
            _IBLabelHeight.text = sHeight;
            if ([_IBLabelHeight.text containsString:@"."]){
                NSArray* heightData=[sHeight componentsSeparatedByString:@"."];
                
                _IBLabelHeight.text=[NSString stringWithFormat:@"%@ cm",[heightData objectAtIndex:0]];
            }
        }
        
    }else if (_IBButtonImperialUnitCheckBox.selected == true){
        
        NSString* weight= [Helper getPREF:@"prfWeight"];
        if ([weight containsString:@"kg"]) {
            
            float fWeight = [[Helper getDigitFromString:weight] floatValue] * 2.20462;
            
            [Helper setPREFStringValue:[NSString stringWithFormat:@"%.0f lbs",fWeight] sKey:@"prfWeight"];
            [_IBLabelWeight setText:[NSString stringWithFormat:@"%.0f lbs",fWeight]];
            
            
        }
        
        NSString* sHeight = [Helper getPREF:@"prfHeight"];
        if (sHeight != nil) {
            
            
            float heightInCm = [[Helper getDigitFromString: sHeight] floatValue] / 100;
            float heightinft = heightInCm / 30.48;
            
            int remining = INT16_C(heightinft);
            
            float heightinInc = (heightinft - remining) * 12;
            
            _IBLabelHeight.text = [NSString stringWithFormat:@"%d' %.0f\" ft",remining,heightinInc];
        }
        
    }
    
}
- (IBAction)surfDataAutoManualBtnTouchUpInsideAction:(UIButton *)sender {
    
    
    NSString* sManlAut=nil;
    
    
    if ([sender isEqual:_IBButtonAutoCheckBox]) {
        
        if (sender.selected == true) {
            sManlAut = @"MANUAL";
            sender.selected = false;
            [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [_IBButtonManualCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            _IBButtonManualCheckBox.selected = true;
            
            _IBLabelManual.text = @"3 hr";
            [Helper setPREFStringValue:_IBLabelManual.text sKey:@"prfManual"];
            
            
        }else if (sender.selected == false) {
            sender.selected = true;
            sManlAut = @"AUTO";
            [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            [_IBButtonManualCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            _IBButtonManualCheckBox.selected = false;
            _IBLabelManual.text = @"";
        }
    }else if ([sender isEqual:_IBButtonManualCheckBox]) {
        
        if (sender.selected == true) {
            sManlAut = @"AUTO";
            sender.selected = false;
            [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [_IBButtonAutoCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            _IBButtonAutoCheckBox.selected = true;
            _IBLabelManual.text =@"";
        }else if (sender.selected == false) {
            sender.selected = true;
            sManlAut = @"MANUAL";
            [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            [_IBButtonAutoCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            _IBButtonAutoCheckBox.selected = false;
            _IBLabelManual.text = @"3 hr";
            [Helper setPREFStringValue:_IBLabelManual.text sKey:@"prfManual"];
        }
    }
    
    [Helper setPREFStringValue:sManlAut sKey:@"prfManualAuto"];
}
- (IBAction)tideAlertOnOffBtnTouchUpInsideAction:(UIButton *)sender {
    NSString* sTideAlert=nil;
    
    if ([sender isEqual:_IBButtonTideAlertOnCheckBox]) {
        
        if (sender.selected == true) {
            
            sender.selected = false;
            [sender setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            [_IBButtonTideAlertOffCheckBox setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            _IBButtonTideAlertOffCheckBox.selected = true;
            sTideAlert = @"OFF";
        }else if (sender.selected == false) {
            sender.selected = true;
            [sender setImage:[UIImage imageNamed:@"checked"] forState:UIControlStateNormal];
            [_IBButtonTideAlertOffCheckBox setImage:[UIImage imageNamed:@"unchecked"] forState:UIControlStateNormal];
            _IBButtonTideAlertOffCheckBox.selected = false;
            sTideAlert = @"ON";
        }
    }
    
    [Helper setPREFStringValue:sTideAlert sKey:@"prfTideAlert"];
    
}
#pragma mark - text Related Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    [UIView animateWithDuration:0 animations:^{
        
        IBToolBarForPickerViewHide.alpha = 0;
        IBPickerView.alpha = 0;
        IBNSLayoutConstraintForScrollViewBottomToView.constant = 20;
        
    }];

    
    selectedButton = nil;
    
    textfieldCurrentEditing=textField;
    
    //    textfieldCurrentEditing.layer.borderWidth=0.0;
    //    textfieldCurrentEditing.layer.borderColor=[UIColor redColor].CGColor;
    //    [textfieldCurrentEditing.layer setShadowColor:[UIColor blackColor].CGColor];
    //    [textfieldCurrentEditing.layer setShadowOpacity:0.0];
    //    [textfieldCurrentEditing.layer setShadowRadius:0.0];
    //    [textfieldCurrentEditing.layer setShadowOffset:CGSizeMake(0, 0)];
    
    
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    if([textField isEqual:_IBTextFieldBirthdate]){
        if (_IBTextFieldBirthdate.text.length <= 0) {
            
            UIDatePicker* datePicker = (UIDatePicker*)_IBTextFieldBirthdate.inputView;
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            
            // NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
            // [timeFormat setDateFormat:@"HH:mm:ss"];
            
            _IBTextFieldBirthdate.text=[dateFormat stringFromDate:[datePicker date]];
        }
    }
    
    //
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    if (viewFrame.origin.y > 0){
        viewFrame.origin.y = 0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
-(void)datePickerDateChange:(UIDatePicker*)datePicker{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    // NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    // [timeFormat setDateFormat:@"HH:mm:ss"];
    
    _IBTextFieldBirthdate.text=[dateFormat stringFromDate:[datePicker date]];
    
}
-(void)previousBarButtonAction:(id)sender{
    
    if ([_IBTextFieldPhone isFirstResponder]) {
        
        [_IBTextFieldPhone resignFirstResponder];
        [_IBTextFieldBirthdate becomeFirstResponder];
        
    }else if ([_IBTextFieldBirthdate isFirstResponder]) {
        
        [_IBTextFieldBirthdate resignFirstResponder];
        [_IBTextFieldPassword becomeFirstResponder];
        
    }else if ([_IBTextFieldPassword isFirstResponder]){
        
        [_IBTextFieldPassword resignFirstResponder];
        [_IBTextFieldEmail becomeFirstResponder];
        
    }else if ([_IBTextFieldEmail isFirstResponder]){
        
        [_IBTextFieldEmail resignFirstResponder];
        [_IBTextFieldLastName becomeFirstResponder];
        
    }else if ([_IBTextFieldLastName isFirstResponder]){
        
        [_IBTextFieldLastName resignFirstResponder];
        [_IBTextFieldFirstName becomeFirstResponder];
    }else if ([_IBTextFieldFirstName isFirstResponder]){
        
        [_IBTextFieldFirstName resignFirstResponder];
        [_IBTextFieldPhone becomeFirstResponder];
        
    }
    
    
}

-(void)nextBarButtonAction:(id)sender{
    
    if ([_IBTextFieldFirstName isFirstResponder]) {
        
        [_IBTextFieldFirstName resignFirstResponder];
        [_IBTextFieldLastName becomeFirstResponder];
        
    }else if ([_IBTextFieldLastName isFirstResponder]){
        
        [_IBTextFieldLastName resignFirstResponder];
        [_IBTextFieldEmail becomeFirstResponder];
        
    }else if ([_IBTextFieldEmail isFirstResponder]){
        
        [_IBTextFieldEmail resignFirstResponder];
        [_IBTextFieldPassword becomeFirstResponder];
        
    }else if ([_IBTextFieldPassword isFirstResponder]){
        
        [_IBTextFieldPassword resignFirstResponder];
        [_IBTextFieldBirthdate becomeFirstResponder];
        
    }else if ([_IBTextFieldBirthdate isFirstResponder]){
        
        [_IBTextFieldBirthdate resignFirstResponder];
        [_IBTextFieldPhone becomeFirstResponder];
        
    }else if ([_IBTextFieldPhone isFirstResponder]){
        
        [_IBTextFieldPhone resignFirstResponder];
        [_IBTextFieldFirstName becomeFirstResponder];
        
    }
}

-(void)doneBarButtonActionaa:(id)sender{
    
    [textfieldCurrentEditing resignFirstResponder];
    
   // NSDictionary* userInfo =[Helper getPREFID:@"prefUserLoginInfo"];
    
    if (_IBTextFieldEmail.text.length == 0) {
        [Helper displayAlertView:@"" message:@"Enter email address."];
        return;
    }
    if ([Helper IsValidEmail:_IBTextFieldEmail.text] == false) {
        
        [Helper displayAlertView:@"" message:@"Enter valid email address."];
        return;
    }
    
    
    NSMutableDictionary* userInfo =[[NSMutableDictionary alloc] initWithDictionary:[Helper getPREFID:@"prefUserLoginInfo"]];

    
    [userInfo setObject:_IBTextFieldBirthdate.text forKey:@"BirthDate"];
    [userInfo setObject:_IBTextFieldEmail.text forKey:@"Email"];
    [userInfo setObject:_IBTextFieldFirstName.text forKey:@"FirstName"];
    [userInfo setObject:_IBTextFieldLastName.text forKey:@"LastName"];
    [userInfo setObject:_IBTextFieldPassword.text forKey:@"Password"];
    [userInfo setObject:_IBTextFieldPhone.text forKey:@"PhoneNo"];
    [userInfo setObject:_IBLabelGender.text forKey:@"Gender"];
   
    [self upDateWebService:userInfo];
    
    //_IBLabelGender.text
    
}

-(void)upDateWebService:(NSMutableDictionary*)userInfo{
    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    
    [self.view setUserInteractionEnabled:false];
    
    WSFrameWork* wsFrameWork=[[WSFrameWork alloc] initWithURLAndParams:@"http://likebit.com/freestyle/UpdateAccount.php" dicParams:userInfo];
    
    wsFrameWork.isSync=false;
    
    wsFrameWork.WSDatatype=kJSON;
    
    wsFrameWork.onSuccess=^(NSDictionary* dic){
        
        NSLog(@"Registered user %@",dic);
        
        NSString* message =[dic objectForKey:@"message"];
        NSString* success =[dic objectForKey:@"success"];
        
        if ([success isEqualToString:@"1"]) {
            
            [Helper setPREFID:userInfo :@"prefUserLoginInfo"];
            
        }else{
            
            [Helper displayAlertView:@"" message:message];
        }
        
        [self.view setUserInteractionEnabled:true];
        [viewForActivity removeFromSuperview];
    };
    
    
    [wsFrameWork send];
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
