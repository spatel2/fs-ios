
#import "MenuViewController.h"
#import "MenuTableViewCell.h"
#import "HomeViewController.h"
#import "MFSideMenu.h"
#import "AppDelegate.h"
#import "ConnectViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController{
    NSArray* arrayMenuTitleList;
    NSArray* arrayMenuImageList;
    CGFloat heightForRow;
}
@synthesize IBTableViewMenu,IBImageViewUserPic;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrayMenuTitleList =[NSArray arrayWithObjects:@"HOME",@"SURF",@"START ACTIVITY",@"ACTIVITY LOG",@"SETTINGS",@"CAMERA",@"CONNECT", nil];
    
    arrayMenuImageList = [NSArray arrayWithObjects:@"slid_home",@"slid_surf",@"slid_startactivity",@"slid_activilog",@"slid_seting",@"slid_camera",@"slid_photo", nil];
    // IBTableViewMenu.estimatedRowHeight = 52.0;
    //IBTableViewMenu.rowHeight = UITableViewAutomaticDimension;
    
    heightForRow = (([UIScreen mainScreen].bounds.size.height - IBImageViewUserPic.frame.size.height) / arrayMenuTitleList.count);

    
    IBTableViewMenu.dataSource = self;
    IBTableViewMenu.delegate = self;
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    IBImageViewUserPic.clipsToBounds = true;
    UIImage* image = [Helper getImageFromDocumentDirectoryOfAppImageName:@"userProfile@2x.jpg"];
    
    if (image !=nil) {
        //UIImage *temp = [self resizeImage:image toAspectWidth:(int)IBImageViewUserPic.frame.size.width andAspectHeight:(int)IBImageViewUserPic.frame.size.height isAspectFill:YES ignoresRotation:NO];

        
        [IBImageViewUserPic setImage:image];
    
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return arrayMenuTitleList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"menuCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    cell.IBLabelTitle.text = [arrayMenuTitleList objectAtIndex:indexPath.row];
    cell.IBImageView.image = [UIImage imageNamed:[arrayMenuImageList objectAtIndex:indexPath.row]];
    
    // Configure the cell...
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return heightForRow;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
           
            
            UIViewController* homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"];
            
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:homeViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            
            
        }
            break;
        case 1:{
            UIViewController* surfViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"surfViewController"];
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:surfViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
            break;
        case 2:
        
            {
                UIViewController* startActivityViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"startActivityViewController"];
                UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
                navigationControllerSlider.viewControllers = [NSArray arrayWithObject:startActivityViewController];
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                
            }
            break;
        case 3:
        {
            //activityLogListViewController.
            UIViewController* activityLogListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"activityLogListViewController"];
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:activityLogListViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
            break;
        case 4:
        {
            UIViewController* settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:settingsViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            
        }break;
        case 5:
        {
            //camera functionality
            [self BtnCamera:nil];
            
        }
            
            break;
        case 6:
            //connectViewController
            
        {
            ConnectViewController* connectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"connectViewController"];
            connectViewController.source = @"MENU";
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:connectViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
            
            
            break;
        default:
            break;
    }
    
}


#pragma camera functionality

#pragma camera function
-(IBAction)BtnCamera:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        
        
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        app.pickerController = [[UIImagePickerController alloc] init];
                        
                        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
                        if (!hasCamera) {
                            return;
                        }
                        
                        app.pickerController.delegate = app;
                        app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                        app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
                        app.pickerController.showsCameraControls = NO;
                        
                        UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
                        cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
                        app.pickerController.cameraOverlayView = cameraFrameView;
                        app.xiangjiisopen = @"yes";
                        
                        UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
                        if ([activeController isKindOfClass:[UINavigationController class]]) {
                            activeController = [(UINavigationController*) activeController visibleViewController];
                        }
                        [activeController presentViewController:app.pickerController animated:YES completion:NULL];
                    });
                } else {
                    // Permission has been denied.
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Camera permission needed"
                                                                      message:@"Please change review your configuration and restart the app."
                                                                     delegate:self
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                    
                    message.tag = 3491832;
                    [message addButtonWithTitle:@"Settings"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [message show];
                    });
                    
                }
            }];
        } else {
            AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            app.pickerController = [[UIImagePickerController alloc] init];
            
            BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
            if (!hasCamera) {
                return;
            }
            
            app.pickerController.delegate = app;
            app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
            app.pickerController.showsCameraControls = NO;
            
            UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
            cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
            app.pickerController.cameraOverlayView = cameraFrameView;
            app.xiangjiisopen = @"yes";
            
            UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
            if ([activeController isKindOfClass:[UINavigationController class]]) {
                activeController = [(UINavigationController*) activeController visibleViewController];
            }
            [activeController presentViewController:app.pickerController animated:YES completion:NULL];
        }
    }
    

    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView.tag == 3491832 && [title isEqualToString:@"Settings"] ) // Settings clicked
    {
        if (&UIApplicationOpenSettingsURLString != NULL) {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
    }
}


-(void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo {
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"
                                                       delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [alert show];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

@end
