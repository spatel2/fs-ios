
#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *IBImageView;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelTitle;

@end
