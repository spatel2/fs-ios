
#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Proxy.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "Constant.h"

@interface MenuViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UITableView *IBTableViewMenu;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewUserPic;

@end
