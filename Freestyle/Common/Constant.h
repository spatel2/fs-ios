

#ifndef Freestyle_Constant_h
#define Freestyle_Constant_h

//Dark blue Color
#define COLOR_DARKBLUE	[UIColor colorWithRed:0/255.0 green:107/255.0 blue:207/255.0 alpha:1.0]


//--------------------Font-------------

#define FONT_HELVETICA_NEUE_MEDIUM    @"helvetica-neue-medium"
#define FONT_HELVETICA_NEUE_MEDIUM_WithSize(s) [UIFont fontWithName:@"helvetica-neue-medium" size:s]

#define FONT_PROXIMANOVA_BOLD_WEBFONT	@"proximanova-bold-webfont"
#define FONT_PROXIMANOVA_BOLD_WEBFONT_WithSize(s) [UIFont fontWithName:@"proximanova-bold-webfont" size:s]

#define FONT_PROXIMANOVA_BOLDIT_WEBFONT	@"proximanova-boldit-webfont"
#define FONT_PROXIMANOVA_BOLDIT_WEBFONT_WithSize(s) [UIFont fontWithName:@"proximanova-boldit-webfont" size:s]

#define FONT_PROXIMANOVA_SEMIBOLD	@"ProximaNova-Semibold"
#define FONT_PROXIMANOVA_SEMIBOLD_WithSize(s) [UIFont fontWithName:@"ProximaNova-Semibold" size:s]
#define seeTutorialNextTime @"tutorial"

#endif
