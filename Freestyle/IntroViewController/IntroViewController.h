

#import <UIKit/UIKit.h>

@interface IntroViewController : UIViewController<UIPageViewControllerDataSource>{
    
    UIPageViewController *pageController;
    //NSArray *pageContent;
}
@property (strong, nonatomic) IBOutlet UIView *vwPageContainer;
@property (strong, nonatomic) UIPageViewController *pageController;
//@property (strong, nonatomic) NSArray *pageContent;
-(void)DisplayNextPage;
@end
