

#import <UIKit/UIKit.h>
#import "MFSideMenuContainerViewController.h"
@interface IntroDataViewController : UIViewController<UIAlertViewDelegate>

@property (assign, nonatomic) NSUInteger index;

@property (strong, nonatomic) IBOutlet UIView *IBViewPageOne;
@property (strong, nonatomic) IBOutlet UIView *IBViewPageTwo;
@property (strong, nonatomic) IBOutlet UIScrollView *IBScrollViewPageThree;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelNumberOne;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelNumberTwo;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelNumberThree;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutScrollViewTopConstraint;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV2_2;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV2_3;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_2;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_3;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_4;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_5;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_6;


@property (strong, nonatomic) IBOutlet UILabel *IBLabelV1_2;
@end
