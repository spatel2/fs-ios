//
//  ActivateBluetoothViewController.h
//  Freestyle
//
//  Created by BIT on 16/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import <QuartzCore/QuartzCore.h>
#import "ProximityTag.h"

#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "WatchViewController.h"
#import "ConnectViewController.h"

#import "FYIInfoViewController.h"
#import "ProximityTagStorage.h"
#import "ConnectionManager.h"
#import "FMDBServers.h"

@interface ActivateBluetoothViewController : UIViewController<UIAlertViewDelegate,ProximityTagDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CommandFunction,ConnectionManagerDelegate,UIActionSheetDelegate,UIApplicationDelegate>{

    AppDelegate* appDelegate;
    
    int useforpaizhao ;
    BOOL  kaiguan;
    ConnectViewController *coonectObj;
    WatchViewController *controlObj;
    
 
    
    IBOutlet UIView *viewSearchController;
    IBOutlet UIView *findsyncview;
    IBOutlet UIView *mainview;
    
    
    IBOutlet UIButton *btn_active;
    
    
    int setflag;
    int cameraflag;
    UIActivityIndicatorView *mySpinner;
    UILabel *SpinnerLabel;

}
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV2_2;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV2_3;


//connection

@property(nonatomic) UINavigationController *nav;

@property(nonatomic) IBOutlet UIImageView * connImageview;



@property(nonatomic, retain)  IBOutlet UIButton *btn_active;



@property(nonatomic, retain)UIView *viewSearchController;
@property(nonatomic, retain)UIView *findsyncview;

@property(nonatomic, retain)UIView *mainview;

@property(nonatomic,retain)IBOutlet UIScrollView *scroll;

- (IBAction)SynTime:(id)sender;
- (IBAction)ToSearchView:(id)sender;
- (IBAction)openSupportURL:(id)sender;
-(void)usingPicMa;


//search view files
@property (nonatomic) IBOutlet UITableView  *  deviceTabview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityView;

- (IBAction)SearchTags:(id)sender;


//info view
@property(nonatomic) ProximityTag * proximityTag;
@property(nonatomic,strong)UIImagePickerController* pickerController;

@property(nonatomic)  IBOutlet UIImageView * iamgeView;

@property(nonatomic) UILabel * nameLable;
@property(nonatomic) UILabel * rssiLable;

@property (weak, nonatomic) IBOutlet UILabel *Connect;



@property (retain, nonatomic) IBOutlet UILabel *datesys;
@property (retain, nonatomic) IBOutlet UILabel *time24;
@property (retain, nonatomic) IBOutlet UILabel *time12;


-(IBAction)clickConnect:(id)sender;
-(IBAction)clickForget:(id)sender;

@property NSTimer *timer1;



//music add

@property(nonatomic,retain)IBOutlet UILabel *songname;
@property(nonatomic,retain)IBOutlet UILabel *songdetail1;
@property(nonatomic,retain)IBOutlet UIImageView *songgimg;

//seek control
@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;


@property BOOL isPaused;
@property BOOL scrubbing;
@property (nonatomic,strong) MPMusicPlayerController *mpc;


@end
