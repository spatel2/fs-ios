
#import "IntroDataViewController.h"
#import "IntroViewController.h"
#import "Constant.h"
#import "Helper.h"
@interface IntroDataViewController ()

@end

@implementation IntroDataViewController
@synthesize index,IBViewPageOne,IBViewPageTwo,IBScrollViewPageThree,IBLabelNumberOne,IBLabelNumberTwo,IBLabelNumberThree;

@synthesize IBNSLayoutScrollViewTopConstraint;

#pragma mark - View Life Cycle

-(BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.IBLabelV1_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV2_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV2_3.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV3_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV3_3.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV3_4.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    self.IBLabelV3_5.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    self.IBLabelV3_6.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    if (index == 0) {
       
        IBViewPageOne.hidden = false;
        IBViewPageTwo.hidden = true;
        IBScrollViewPageThree.hidden = true;
        
    } else if (index == 1){
        
        IBViewPageOne.hidden = true;
        IBViewPageTwo.hidden = false;
        IBScrollViewPageThree.hidden = true;
       
    }else if (index == 2){
        
        IBViewPageOne.hidden = true;
        IBViewPageTwo.hidden = true;
        IBScrollViewPageThree.hidden = false;
        [self setScrollerViewData];
        [IBScrollViewPageThree setContentSize:CGSizeMake(320, 665)];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setScrollerViewData{
    
    IBLabelNumberOne.layer.backgroundColor = COLOR_DARKBLUE.CGColor;
    
    IBLabelNumberOne.layer.cornerRadius = IBLabelNumberOne.frame.size.width / 2;
    IBLabelNumberOne.layer.borderWidth = 2.0;
    IBLabelNumberOne.layer.borderColor = [UIColor whiteColor].CGColor;
    
    IBLabelNumberTwo.layer.backgroundColor = COLOR_DARKBLUE.CGColor;
    
    IBLabelNumberTwo.layer.cornerRadius = IBLabelNumberTwo.frame.size.width / 2;
    IBLabelNumberTwo.layer.borderWidth = 2.0;
    IBLabelNumberTwo.layer.borderColor = [UIColor whiteColor].CGColor;
    
    IBLabelNumberThree.layer.backgroundColor = COLOR_DARKBLUE.CGColor;
    IBLabelNumberThree.layer.cornerRadius = IBLabelNumberThree.frame.size.width / 2;
    IBLabelNumberThree.layer.borderWidth = 2.0;
    IBLabelNumberThree.layer.borderColor = [UIColor whiteColor].CGColor;
}
#pragma mark - IB Button Action
- (IBAction)nextButtonTouchUpInsideAction:(UIButton *)sender{
    
    
    IntroViewController* parentViewController = (IntroViewController*)[[self parentViewController] parentViewController];
    [parentViewController DisplayNextPage];

}
- (IBAction)exitButtonTouchUpInsideAction:(UIButton *)sender{
    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Would you like to see this tutorial the next time you open the app?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];

}
#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    switch (buttonIndex) {
        case 0:
            [Helper setPREFint:1 :seeTutorialNextTime];
            break;
        case 1:
            //segueHomeViewController
            [Helper setPREFint:0 :seeTutorialNextTime];
            break;
            
        default:
            break;
    }
    
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];

    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
