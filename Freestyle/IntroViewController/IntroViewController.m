

#import "IntroViewController.h"
#import "IntroDataViewController.h"
@interface IntroViewController ()

@end

@implementation IntroViewController{
    int totalNoOfPages;
    NSUInteger manualIndex;
}

@synthesize pageController;


-(BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:nil action:nil]];
    
    totalNoOfPages = 3;
    
    NSDictionary *options =
    [NSDictionary dictionaryWithObject:
     [NSNumber numberWithInteger:UIPageViewControllerSpineLocationMin]
                                forKey: UIPageViewControllerOptionSpineLocationKey];
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];
    self.pageController.dataSource = self;
    [[self.pageController view] setFrame:[[self vwPageContainer] bounds]];
    
    IntroDataViewController *initialViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self vwPageContainer] addSubview:[pageController view]];
    [self.vwPageContainer bringSubviewToFront:[pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    
    
    self.view.gestureRecognizers = self.pageController.gestureRecognizers;
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IntroDataViewController *)viewControllerAtIndex:(NSUInteger)index{
    // Return the data view controller for the given index.
    if ((totalNoOfPages == 0) || (index >= totalNoOfPages)) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    IntroDataViewController *dataViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"introDataViewController"];
    dataViewController.index = index;
    return dataViewController;
}

- (NSUInteger)indexOfViewController:(IntroDataViewController*)viewController {
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return viewController.index;
}
#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(IntroDataViewController *)viewController];
    manualIndex = index;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(IntroDataViewController *)viewController];
    manualIndex = index;
    
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == totalNoOfPages) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

-(void)DisplayNextPage
{
    //check that current page isn't first page
    if (manualIndex < totalNoOfPages - 1){
        manualIndex = manualIndex + 1;
        
        //get the page to go to
        IntroDataViewController *targetPageViewController = [self viewControllerAtIndex:manualIndex];
        
        //put it(or them if in landscape view) in an array
        NSArray *theViewControllers = nil;
        theViewControllers = [NSArray arrayWithObjects:targetPageViewController, nil];
        
        //add page view
        [self.pageController setViewControllers:theViewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
