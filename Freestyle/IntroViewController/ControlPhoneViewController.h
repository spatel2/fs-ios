//
//  ControlPhoneViewController.h
//  Freestyle
//
//  Created by BIT on 16/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "ActionSheetStringPicker.h"
#import "HowToUseController.h"


@protocol CommandFunction <NSObject>
@optional


@end



@interface ControlPhoneViewController : UIViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIApplicationDelegate>{

    AppDelegate* appDelegate;
}
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintWidthForView;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_2;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_3;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_4;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_5;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelV3_6;

@property(nonatomic) UIImagePickerController * pickerController;

@property (weak, nonatomic) IBOutlet UILabel *MiddLabel;
@property (weak, nonatomic) IBOutlet UILabel *LeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *RightLable;

@property (weak, nonatomic) IBOutlet UIImageView *C1Image;
@property (weak, nonatomic) IBOutlet UIImageView *C2Image;
@property (weak, nonatomic) IBOutlet UIImageView *C3Image;
@property (weak, nonatomic) IBOutlet UIButton *ButtonMusic;
@property (weak, nonatomic) IBOutlet UIButton *ButtonCamera;
@property (weak, nonatomic) IBOutlet UIButton *ButtonOther;

@property(nonatomic,retain)IBOutlet UIScrollView *scroll;

- (IBAction)ButtonMusic:(id)sender;

- (IBAction)ButtonCamera:(id)sender;

- (IBAction)ButtonOther:(id)sender;

- (IBAction)SelectValue:(id)sender;
- (void)usingPicMa;




@end
