 //
//  ControlPhoneViewController.m
//  Freestyle
//
//  Created by BIT on 16/03/15.
//
//

#import "ControlPhoneViewController.h"

#import "AppDelegate.h"
#import "ConnectViewController.h"



@interface ControlPhoneViewController (){
    
    BOOL isLoad;
    
}

@end

@implementation ControlPhoneViewController

@synthesize LeftLabel=_LeftLabel,MiddLabel=_MiddLabel,RightLable=_RightLable,scroll; // hhh;

@synthesize IBNSLayoutConstraintWidthForView;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.IBLabelV3_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV3_3.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV3_4.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    self.IBLabelV3_5.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    self.IBLabelV3_6.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:9.0f];
    
     [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
           appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    //comand code
    
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap.numberOfTapsRequired = 1; // 单击
    [self.C1Image setUserInteractionEnabled:YES];
    [self.C2Image setUserInteractionEnabled:YES];
    [self.C3Image setUserInteractionEnabled:YES];
    
    [self.C1Image addGestureRecognizer:singtap];
    
    UITapGestureRecognizer *singtap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap1.numberOfTapsRequired = 1; // 单击
    
    [self.C2Image addGestureRecognizer:singtap1];
    
    UITapGestureRecognizer *singtap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap2.numberOfTapsRequired = 1; // 单击
    
    [self.C3Image addGestureRecognizer:singtap2];
    
//    // Do any additional setup after loading the view.
//    if (floorf(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.automaticallyAdjustsScrollViewInsets = YES;
//        self.extendedLayoutIncludesOpaqueBars = YES;
//        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
//        
//    }else{
//        
//    }

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
    
    [[NSUserDefaults standardUserDefaults] objectForKey:@"C1"];
    
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Camera"]){
        [self.LeftLabel setText:@"Camera"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"img_camera"]];
        [self.ButtonMusic setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Tag location"]){
        [self.LeftLabel setText:@"Tag location"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"controller-location.png"]];
        [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Find Phone"]){
        [self.LeftLabel setText:@"Find Phone"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"img_phone"]];
        [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Play/Pause Music"]){
        
        [self.LeftLabel setText:@"Play/Pause Music"];
        [self.C1Image setImage:[UIImage imageNamed:@"img_player"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Next Song"]){
        
        [self.LeftLabel setText:@"Next Song"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"img_player"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Reject Incoming Call"]){
        
        [self.LeftLabel setText:@"Reject Incoming Call"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"controller-music.png"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Off"]){
        
        [self.LeftLabel setText:@"Off"];
        
        [self.C1Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }
    
    
    
    
    
    
    
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Camera"]){
        [self.MiddLabel setText:@"Camera"];
        // [self.Middbtn setBackgroundImage:[UIImage imageNamed:@"fafraf.png"] forState:UIControlStateNormal];
        [self.C2Image setImage:[UIImage imageNamed:@"img_camera"]];
        [self.ButtonCamera setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Tag location"]){
        [self.MiddLabel setText:@"Tag location"];
        //  [self.Middbtn setBackgroundImage:[UIImage imageNamed:@"dff.png"] forState:UIControlStateNormal];
        [self.C2Image setImage:[UIImage imageNamed:@"controller-location.png"]];
        [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Find Phone"]){
        [self.MiddLabel setText:@"Find Phone"];
        // [self.Middbtn setBackgroundImage:[UIImage imageNamed:@"afag.png"] forState:UIControlStateNormal];
        [self.C2Image setImage:[UIImage imageNamed:@"img_phone"]];
        [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Play/Pause Music"]){
        
        [self.MiddLabel setText:@"Play/Pause Music"];
        [self.C2Image setImage:[UIImage imageNamed:@"img_player"]];
        [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Next Song"]){
        
        [self.MiddLabel setText:@"Next Song"];
        
        [self.C2Image setImage:[UIImage imageNamed:@"img_player"]];
        [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Reject Incoming Call"]){
        
        [self.MiddLabel setText:@"Reject Incoming Call"];
        
        [self.C2Image setImage:[UIImage imageNamed:@"controller-music.png"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Off"]){
        
        [self.MiddLabel setText:@"Off"];
        
        [self.C2Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }
    
    
    
    
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Camera"]){
        [self.RightLable setText:@"Camera"];
        //  [self.Rightbtn setBackgroundImage:[UIImage imageNamed:@"fafraf.png"] forState:UIControlStateNormal];
        [self.C3Image setImage:[UIImage imageNamed:@"img_camera"]];
        [self.ButtonOther setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Tag location"]){
        [self.RightLable setText:@"Tag location"];
        // [self.Rightbtn setBackgroundImage:[UIImage imageNamed:@"dff.png"] forState:UIControlStateNormal];
        [self.C3Image setImage:[UIImage imageNamed:@"controller-location.png"]];
        [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Find Phone"]){
        [self.RightLable setText:@"Find Phone"];
        //  [self.Rightbtn setBackgroundImage:[UIImage imageNamed:@"afag.png"] forState:UIControlStateNormal];
        [self.C3Image setImage:[UIImage imageNamed:@"img_phone"]];
        [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Play/Pause Music"]){
        
        [self.RightLable setText:@"Play/Pause Music"];
        [self.C3Image setImage:[UIImage imageNamed:@"img_player" ]];
        [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Next Song"]){
        
        [self.RightLable setText:@"Next Song"];
        
        [self.C3Image setImage:[UIImage imageNamed:@"img_player" ]];
        [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
        
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Reject Incoming Call"]){
        
        [self.RightLable setText:@"Reject Incoming Call"];
        
        [self.C3Image setImage:[UIImage imageNamed:@"controller-music.png"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Off"]){
        
        [self.RightLable setText:@"Off"];
        
        [self.C3Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
        
    }
    
}

- (void)viewDidLayoutSubviews{
    IBNSLayoutConstraintWidthForView.constant = self.view.bounds.size.width;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)finishButtonTouchUpInsideAction:(UIButton *)sender{
    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Would you like to see this tutorial the next time you open the app?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [Helper setPREFint:1 :seeTutorialNextTime];
            break;
        case 1:
            //segueHomeViewController
            [Helper setPREFint:0 :seeTutorialNextTime];
            break;
            
        default:
            break;
    }
    
    
   // UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    
    
    

    
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    
    NSString* viewControllerID=@"signUpViewController";
    
    if (accessToken!=nil) {
        viewControllerID = @"homeViewController";
    }else if([Helper getPREFID:@"prefUserLoginInfo"]){
        viewControllerID = @"homeViewController";
    }
    
    
    
    
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewControllerID]];
    
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
    
    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
    /*
    
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpViewController"] ];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    [self presentViewController:navigationCont animated:true completion:nil];
    */
}

 // comand functionality

- (void)usingPicMa
{
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (!hasCamera) {
        return;
    }
    
    //能够使用iphone的内置摄像机拍照
    app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    app.xiangjiisopen = @"yes";
    [self  presentViewController:app.pickerController animated:YES completion:nil];
    
}

-(void)clickimg:(id) sender{
    
    UIImageView *v = (UIImageView*)((UITapGestureRecognizer *)[sender view]);
    
    if ([[[v image] accessibilityIdentifier]isEqualToString:@"1.png"]) {
        [self usingPicMa];
    }
    
}

-(void)CallMusic
{
    [self ButtonMusic:nil];
}

- (IBAction)ButtonMusic:(id)sender {
    
    
    if ([self.LeftLabel.text isEqualToString:@"Play/Pause Music"]||[self.LeftLabel.text isEqualToString:@"Next Song"]) {
        
        
        
        
    }else if ([self.LeftLabel.text isEqualToString:@"Camera"]){
        
        [self usingPicMa];
        
    }else{
        
    }
    
}

- (IBAction)ButtonCamera:(id)sender {
    
    if ([self.MiddLabel.text isEqualToString:@"Play/Pause Music"]||[self.MiddLabel.text isEqualToString:@"Next Song"]) {
        
        
        
    }else if ([self.MiddLabel.text isEqualToString:@"Camera"]){
        
        [self usingPicMa];
        
    }else{
        
    }
}


- (IBAction)ButtonOther:(id)sender {
    
    if ([self.RightLable.text isEqualToString:@"Play/Pause Music"]||[self.RightLable.text isEqualToString:@"Next Song"]) {
        
        
    }else if ([self.RightLable.text isEqualToString:@"Camera"]){
        
        [self usingPicMa];
        
    }else{
        
    }
    
}

- (IBAction)SelectValue:(id)sender {
    UIButton *Button=(UIButton *)sender;
    int index=(int)Button.tag;
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        if ([self respondsToSelector:@selector(setText:)]) {
            
            [self performSelector:@selector(setText:) withObject:[NSArray arrayWithObjects:selectedValue,[NSNumber numberWithInt:index], nil]];
        }
    };
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker) {
        
    };
    
    //@"Tag location"
    NSArray *array=[NSArray arrayWithObjects:@"Camera",@"Play/Pause Music",@"Find Phone", @"Next Song",@"Off",nil];
    
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select One" rows:array initialSelection:0 doneBlock:done cancelBlock:cancel origin:sender];
    
}
-(void)setText:(NSArray *)array{
    
    int index=[[array objectAtIndex:1] integerValue];
    // UIButton *button=(UIButton *)[self.view viewWithTag:index];
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    switch (index) {
        case 11:
        {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            
            if ([app.appdelegateProximity.peripheral isConnected]){
                
                app.appdelegateProximity.c1=selectimage;
            }
            
            
            [self.LeftLabel setText:[array objectAtIndex:0]];
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"]) {
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
                
            }else if([selectimage isEqualToString:@"Camera"]){
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_camera"]];
                [self.C1Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonMusic setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
                
            }else if ([selectimage isEqualToString:@"Tag location"]){
                
                [self.C1Image setImage:[UIImage imageNamed:@"controller-location.png"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-location.png"];
                [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
                
            }else if ([selectimage isEqualToString:@"Find Phone"]){
                [self.C1Image setImage:[UIImage imageNamed:@"img_phone"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Next Song"]){
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Reject Incoming Call"]){
                
                [self.C1Image setImage:[UIImage imageNamed:@"controller-music.png"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Off"]){
                
                [self.C1Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }
            
            
            
        }
            break;
            
        case 12:{
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C2"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            [self.MiddLabel setText:[array objectAtIndex:0]];
            
            if ([app.appdelegateProximity.peripheral isConnected]) {
                
                app.appdelegateProximity.c2=selectimage;
            }
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"]) {
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
                
            }else if([selectimage isEqualToString:@"Camera"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_camera"]];
                [self.C2Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonCamera setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
                
            }else if ([selectimage isEqualToString:@"Tag location"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"controller-location.png"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-location.png"];
                [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
                
            }else if ([selectimage isEqualToString:@"Find Phone"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_phone"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Next Song"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Reject Incoming Call"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"controller-music.png"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Off"]){
                
                [self.C2Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }
            
            
            
        }break;
        case 13:
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C3"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            if ([app.appdelegateProximity.peripheral isConnected]) {
                app.appdelegateProximity.c3=selectimage;
            }
            [self.RightLable setText:[array objectAtIndex:0]];
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"]) {
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
                
            }else  if([selectimage isEqualToString:@"Camera"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_camera"]];
                [self.C3Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonOther setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Tag location"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"controller-location.png"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-location.png"];
                [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
                
            }else if ([selectimage isEqualToString:@"Find Phone"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_phone"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Next Song"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_player"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Reject Incoming Call"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"controller-music.png"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }else if ([selectimage isEqualToString:@"Off"]){
                
                [self.C3Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }
            
            
            
        }
            break;
        default:
            break;
    }
    
    
    
}


@end
