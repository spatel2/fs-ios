//
//  ConnectYourWatchViewController.m
//  Freestyle
//
//  Created by BIT on 16/03/15.
//
//

#import "ConnectYourWatchViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@interface ConnectYourWatchViewController ()

@end

@implementation ConnectYourWatchViewController

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    self.IBLabelV1_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    
           appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)exitButtonTouchUpInsideAction:(UIButton *)sender{
    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Would you like to see this tutorial the next time you open the app?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
    
}
#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [Helper setPREFint:1 :seeTutorialNextTime];
            break;
        case 1:
            //segueHomeViewController
            [Helper setPREFint:0 :seeTutorialNextTime];
            break;
            
        default:
            break;
    }
    
    //UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    
    

    
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    
    NSString* viewControllerID=@"signUpViewController";
    
    if (accessToken!=nil) {
        viewControllerID = @"homeViewController";
    }else if([Helper getPREFID:@"prefUserLoginInfo"]){
        viewControllerID = @"homeViewController";
    }
    
    
    
     UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewControllerID]];
    
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
    
    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
    
    /*
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpViewController"] ];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    [self presentViewController:navigationCont animated:true completion:nil];*/
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
