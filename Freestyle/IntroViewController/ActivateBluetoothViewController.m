//
//  ActivateBluetoothViewController.m
//  Freestyle
//
//  Created by BIT on 16/03/15.
//
//

#import "ActivateBluetoothViewController.h"

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "ConnectionManager.h"
#import <CoreText/CoreText.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "ServerManager.h"
#import "ConnectViewTableViewCell.h"

#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "Proxy.h"
#import "Constant.h"

#import "ProximityTagStorage.h"

#import "musicViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>


#define DELAY_TIMER 4.0
@interface ActivateBluetoothViewController (){
    
    AVAudioPlayer           *_audioPlayer;
    BOOL                    _isCountdownTimer;
    NSTimer                 *_captureTimer;
    int                     _captureCounter;
    
    //find view
    int move;
    int flag;
    
    int NEXTSONGFLAG;
    
    
    UIAlertView *alertexit;
    
     int flagsong;
    
     UIView* viewForActivity;
}

//search view
@property(nonatomic,strong)NSMutableArray *arrayData;

//find view
@property(nonatomic,strong)NSTimer *timer;


@end

@implementation ActivateBluetoothViewController

@synthesize findsyncview,scroll;
@synthesize nav=_nav;
@synthesize connImageview=_connImageview;
//search view
@synthesize deviceTabview=_deviceTabview;
@synthesize viewSearchController,btn_active;
//info view
@synthesize proximityTag=_proximityTag;
@synthesize pickerController=_pickerController;
@synthesize nameLable=_nameLable,rssiLable=_rssiLable;
@synthesize iamgeView=_iamgeView;
@synthesize datesys,time12,time24,mainview;

@synthesize  songdetail1,songname,songgimg;

CGSize scrollViewOriginalSize;
CGRect keyboardBounds;
CGRect applicationFrame;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    musicViewController *musivobj=[[musicViewController alloc]init];
    
    scrollViewOriginalSize = scroll.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    [scroll setScrollEnabled:TRUE];
    
    
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
       appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    self.IBLabelV2_2.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    self.IBLabelV2_3.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:12.0f];
    
    
    //connection code
    
    NSDate * time =  [NSDate new];
    NSDateFormatter * form= [[NSDateFormatter alloc] init];
    [form setLocale:[NSLocale currentLocale]];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDateFormatter *sysdate=[[NSDateFormatter alloc]init];
    [sysdate setDateFormat:@"dd-MM-yyyy"];
    NSString *strdate=[sysdate stringFromDate:time];
    [[ConnectionManager sharedInstance] setDelegate:self];
    datesys.text=strdate;
    [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
    [self.deviceTabview reloadData];
    
    // Do any additional setup after loading the view.
    self.view.layer.contents=(id)[UIImage imageNamed:@"3.png"].CGImage;
    
//    if (floorf(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//        self.automaticallyAdjustsScrollViewInsets = YES;
//        self.extendedLayoutIncludesOpaqueBars = YES;
//        self.navigationController.interactivePopGestureRecognizer.enabled=NO;
//        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:18.0/255.0 green:143.0/255.0 blue:215.0/255.0 alpha:1]];
//        [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
//        
//    }
    //search view
    [self.deviceTabview reloadData];
    [self performSelector:@selector(TableHide) withObject:nil afterDelay:1.0];
    
    
    //one time music
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    // NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    NSString *stralert = [savedStock objectForKey:@"alertstr"]?[savedStock objectForKey:@"alertstr"]:@"";
    
    if ([stralert isEqualToString:@"1"])
    {
        NSLog(@"allready login");
        
        
    }
    else if ([stralert isEqualToString:@"0"])
    {
        
        
        NSLog(@"not login");
    }
    else
    {
        
    }



    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
    self.timer1 = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    //self.navigationController.navigationBarHidden=YES;
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.appdelegateProximity.delegate=self;
    if ([app.appdelegateProximity.peripheral isConnected]) {
        [self.connImageview setImage:[UIImage imageNamed:@"AFGGSGSG.png"]];
    }else{
        [self.connImageview setImage:[UIImage imageNamed:@"disconnjianrou.png"]];
    }
    //search view
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    [self.deviceTabview setHidden:YES];
    [self.ActivityView stopAnimating];
    [self.deviceTabview reloadData];
    self.ActivityView.hidden=YES;

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)exitButtonTouchUpInsideAction:(UIButton *)sender{
    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Would you like to see this tutorial the next time you open the app?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
    
}
#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    
    if (alertView.tag == 510) {
        [[ServerManager sharedInstance] stopPlayingAlarmSound];
        
        if ([ServerManager sharedInstance].didpausemusic == true)
        {
            AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
            
            //[app.mpc play];
        }
        
        
        
        
    } else {
        if (alertView.tag==135) {//删除tag
            if(buttonIndex == 0) {
                [[ProximityTagStorage sharedInstance] removeTag:self.proximityTag];
                [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
                [Proxy shared].flagConnectView = NO;
                [self.deviceTabview reloadData];
            }
        } else if (alertView.tag==190) {//
            UITextField *tf=[alertView textFieldAtIndex:0];
            if (buttonIndex==0) {
                self.proximityTag.name=tf.text;
                self.nameLable.text=tf.text;
            }else{
                
            }
            
        }
    }

    
    
    
    switch (buttonIndex) {
        case 0:
            [Helper setPREFint:1 :seeTutorialNextTime];
            break;
        case 1:
            //segueHomeViewController
            [Helper setPREFint:0 :seeTutorialNextTime];
            break;
            
        default:
            break;
    }
    
    

    
    
    //UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    
    

    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    
    NSString* viewControllerID=@"signUpViewController";
    
    if (accessToken!=nil) {
        viewControllerID = @"homeViewController";
    }else if([Helper getPREFID:@"prefUserLoginInfo"]){
        viewControllerID = @"homeViewController";
    }
    
    
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewControllerID]];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
    
    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
    
    /*
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpViewController"]];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    [self presentViewController:navigationCont animated:true completion:nil];
    */
    
}


//connection code

//info view
- (void) setProximityTag:(ProximityTag *)proximityTag {
    if (proximityTag != _proximityTag) {
        _proximityTag = proximityTag;
        _proximityTag.delegate = self;
        self.nameLable.text=self.proximityTag.name;
    }
}
-(IBAction)clickConnect:(id)sender{
    
    [self setProximityTag:[[[ProximityTagStorage sharedInstance] tags] objectAtIndex:[sender tag]]];
    [Proxy shared].flagConnectView = YES;
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([app.appdelegateProximity.peripheral isConnected]&&![app.appdelegateProximity isEqual:self.proximityTag]&& flag==0) {//不是同一个设备 Not the same device
        
        [[ConnectionManager sharedInstance] disconnectTag:app.appdelegateProximity];
        [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
        [Proxy shared].flagConnectView = NO;
    } else {//是同一个设备 Is the same device
        
        if ([self.proximityTag.peripheral isConnected]) {
            app.reConnBtnTag =@"1";
            [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
            [self.Connect setText:@"Connect"];
            [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
            [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
            [self.deviceTabview reloadData];
            [scroll setContentSize:CGSizeMake(320, 0)];
            [self TableHide];
            // btn_active.hidden=NO;
            
            btn_active.enabled = TRUE;
            btn_active.backgroundColor = [UIColor whiteColor];
            //
            
            
        }else{
            if (self.proximityTag.peripheral!=nil) {
                //                [self clickForget:[[[ProximityTagStorage sharedInstance] tags] objectAtIndex:[sender tag]]];
                //                [self.deviceTabview reloadData];
                //            }else{
                
                [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
                [Proxy shared].flagConnectView = NO;
                //btn_active.hidden=YES;
                btn_active.enabled = FALSE;
                btn_active.backgroundColor = [UIColor grayColor];
                
                UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [activityIndicatorView startAnimating];
                
                viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
                viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
                
                activityIndicatorView.center = viewForActivity.center;
                [viewForActivity addSubview:activityIndicatorView];
                [self.view addSubview:viewForActivity];
                
                [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(stopcont) userInfo:nil repeats:NO];
            }
        }
    }
}

-(void)stopcont {
    
    [viewForActivity removeFromSuperview];
}


-(IBAction)clickForget:(id)sender{
    
    UIAlertView* question = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                       message:[NSString stringWithFormat:@"Are you sure you want to delete %@?",[self.proximityTag name]]
                                                      delegate:self
                                             cancelButtonTitle:@"YES"  otherButtonTitles:@"NO", nil];
    question.tag=135;
    [question show];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        if (!hasCamera) {
            return;
        }
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        self.pickerController.allowsEditing=YES;
        //能够使用iphone的内置摄像机拍照
        self.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.pickerController animated:YES completion:nil];
        
    }else if (buttonIndex == 1) {
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
        if (!hasCamera) {
            return;
        }
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        //能够使用iphone的内置摄像机拍照
        self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.pickerController.allowsEditing = YES;
        
        [self presentViewController:self.pickerController animated:YES completion:nil];
        
    }else if(buttonIndex == 2) {
        
    }
}


-(void)updateTimer
{
    
    
    //Get current time
    NSDate* now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
    NSInteger hour = [dateComponents hour];
    if (hour>24)
    {
        hour=hour%24;
    }
    NSInteger minute = [dateComponents minute];
    NSInteger second = [dateComponents second];
    time24.text=[NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hour, (long)minute, (long)second ];//,am_OR_pm];
    
}

-(void)TableHide {
    
    
    if ([self.proximityTag.peripheral isConnected]) {
        int newHeight = 0;
        if ([[[ProximityTagStorage sharedInstance] tags] count] == 0) {
            newHeight = 0;
        } else if([[[ProximityTagStorage sharedInstance] tags] count]>0) {
            int total = 3;
            if([[[ProximityTagStorage sharedInstance] tags] count]<3){
                total = (int)[[[ProximityTagStorage sharedInstance] tags] count];
            }
            newHeight = total*76;
        }
        viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, newHeight);
        viewSearchController.hidden = NO;
        findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
        self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y,self.deviceTabview.frame.size.width, newHeight);
        [self.deviceTabview reloadData];
        self.ActivityView.hidden=YES;
        [self.ActivityView stopAnimating];
    } else {
        viewSearchController.hidden = YES;
        viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, 0);
        findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
        self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y, self.deviceTabview.frame.size.width, 0);
    }
}
-(void)tablehide
{
    int NumEntries = (int)[[[ProximityTagStorage sharedInstance] tags] count];
    int newHeight = NumEntries*76;
    
    viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, 0);
    findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
    self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y, self.deviceTabview.frame.size.width, 0);
    
    
    [scroll setContentSize:CGSizeMake(320, 440+newHeight)];
}


-(void)ChangeLayoutData {
    
    [self performSelector:@selector(TableHide) withObject:nil afterDelay:3.0];
    
    
    btn_active.hidden=NO;
    btn_active.enabled = TRUE;
    btn_active.backgroundColor = [UIColor whiteColor];
    
}
-(void)ChangeLayoutDataConnected {
    
    btn_active.enabled = FALSE;
    btn_active.backgroundColor = [UIColor grayColor];
    
    
    
    [self performSelector:@selector(TableHide) withObject:nil afterDelay:3.0];
     
    
}

- (void) didUpdateData:(id) tag{
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([app.appdelegateProximity.peripheral isConnected]) {
        [self.connImageview setImage:[UIImage imageNamed:@"AFGGSGSG.png"]];
    }else{
        [self.connImageview setImage:[UIImage imageNamed:@"disconnjianrou.png"]];
    }
    
    //info view
    self.rssiLable.text=[NSString  stringWithFormat:@"RSSI:%f",self.proximityTag.rssiLevel];
    if (self.proximityTag.rssiLevel==0.0f) {
        
        [self.Connect setText:@"CONNECT >"];
        flag=0;
        // btn_active.hidden=NO;
    }else{
        [self.Connect setText:@"DISCONNECT >"];
        flag=1;
        // btn_active.hidden=YES;
        
        
        
        [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
    }
}
//====================================================
//自动调用拍照
-(void) takePic{
    
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([app.xiangjiisopen isEqualToString:@"yes"]) {
        [app cameraTakePicture:nil];
    }else{
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Tips" message:@"Please Open the Camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}
//拍照delegate 保存图片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if ([mediaType isEqualToString:@"public.image"]) {
        //将图片存放到本地
        UIImageWriteToSavedPhotosAlbum(image,self,nil,nil);
    }
    app.xiangjiisopen=@"no";
    [app.pickerController dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.xiangjiisopen=@"no";
    [app.pickerController dismissViewControllerAnimated:YES completion:nil];
}

//====================================================
-(void)yourNewFunctionytt{
}

- (void)_captureImage {
    
    if (!_isCountdownTimer)
    {
        _isCountdownTimer = YES;
        _captureCounter=0;
        _captureTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(captureDelay) userInfo:nil repeats:YES];
    }
}

- (void)captureDelay
{
    
    _captureCounter++;
    if (_captureCounter >= DELAY_TIMER)
    {
        _captureCounter = 0;
        _isCountdownTimer = NO;
        [_captureTimer invalidate];
        [self  takePic];
    }
    else
    {
        
        
    }
    
}



//camera function
- (void)usingPicMa {
    
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        
        
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)]) {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if (granted) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                        app.pickerController = [[UIImagePickerController alloc] init];
                        
                        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
                        if (!hasCamera) {
                            return;
                        }
                        
                        app.pickerController.delegate = app;
                        app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                        app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
                        app.pickerController.showsCameraControls = NO;
                        
                        UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
                        cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
                        app.pickerController.cameraOverlayView = cameraFrameView;
                        app.xiangjiisopen = @"yes";
                        
                        UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
                        if ([activeController isKindOfClass:[UINavigationController class]]) {
                            activeController = [(UINavigationController*) activeController visibleViewController];
                        }
                        [activeController presentViewController:app.pickerController animated:YES completion:NULL];
                    });
                } else {
                    // Permission has been denied.
                    
                    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Camera permission needed"
                                                                      message:@"Please change review your configuration and restart the app."
                                                                     delegate:self
                                                            cancelButtonTitle:@"OK"
                                                            otherButtonTitles:nil];
                    
                    message.tag = 3491832;
                    [message addButtonWithTitle:@"Settings"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [message show];
                    });
                    
                }
            }];
        } else {
            AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            app.pickerController = [[UIImagePickerController alloc] init];
            
            BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
            if (!hasCamera) {
                return;
            }
            
            app.pickerController.delegate = app;
            app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
            app.pickerController.showsCameraControls = NO;
            
            UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
            cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
            app.pickerController.cameraOverlayView = cameraFrameView;
            app.xiangjiisopen = @"yes";
            
            UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
            if ([activeController isKindOfClass:[UINavigationController class]]) {
                activeController = [(UINavigationController*) activeController visibleViewController];
            }
            [activeController presentViewController:app.pickerController animated:YES completion:NULL];
        }
    }
    
    
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView.tag == 3491832 && [title isEqualToString:@"Settings"] ) // Settings clicked
    {
        if (&UIApplicationOpenSettingsURLString != NULL) {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
    }
}

-(void) CommandCamera
{
    
    
    
    AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(app.flagcmd==0)
    {
        if ([app.xiangjiisopen isEqualToString:@"yes"]) {
            [self  _captureImage]; // hide by me
        } else {
            [self usingPicMa];
        }
        
    }
    else
    {
        
    }
}
-(void) CommandFindPhone
{
    //Show Window
    UIAlertView * alt = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You Found Me!" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    alt.tag = 510;
    [alt show];
    [[ServerManager sharedInstance] startPlayingAlarmSound];
}

-(void) CommandPlayPauseMusic
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommandPlayPause" object:nil userInfo:nil];
}

-(void) CommandNextSong
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommandNextSong" object:nil userInfo:nil];
}

- (void) didwatchTophone:(id) type
{
    NSString * str =(NSString *)type;
    AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    app.appdelegateProximity.delegate =self;
    
    
    
    if ([str isEqualToString:@"b0"])        //Command 1  ---------------------------------------
    {
        if ([app.appdelegateProximity.c1 isEqualToString:@"Camera"])
        {
            [self CommandCamera];
        }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Find Phone"]){
            [self CommandFindPhone];
        }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Play/Pause Music"]){
            
            [self CommandPlayPauseMusic];return;
            
                    }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Next Song"]){
            [self CommandNextSong];return;
                    }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Off"])
        {   // Do nothing
        }
    }
    else if([str isEqualToString:@"b1"])    //Command 2 ---------------------------------------
    {
        if ([app.appdelegateProximity.c2 isEqualToString:@"Camera"])
            [self CommandCamera];
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Find Phone"])
            [self CommandFindPhone];
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Play/Pause Music"]){
            
            
            [self CommandPlayPauseMusic];
        }
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Next Song"]){
            
            [self CommandNextSong];
        }
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Off"])
        {   // Do nothing
        }
    }
    else if([str isEqualToString:@"b2"]) //Command 3 ---------------------------------------
    {
        if ([app.appdelegateProximity.c3 isEqualToString:@"Camera"])
            [self CommandCamera];
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Find Phone"])
            [self CommandFindPhone];
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Play/Pause Music"]){
            
            
            [self CommandPlayPauseMusic];
        }
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Next Song"]){
            
                      [self CommandNextSong];
            
        }
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Off"])
        {   //  Do nothing
        }
    }
}






- (IBAction)SynTime:(id)sender {
    
    NSDate * time =  [NSDate new];
    NSDateFormatter * form= [[NSDateFormatter alloc] init];
    [form setLocale:[NSLocale currentLocale]];
    [form setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDateFormatter *sysdate=[[NSDateFormatter alloc]init];
    [sysdate setDateFormat:@"dd-MM-yyyy"];
    // NSString *strdate=[sysdate stringFromDate:time];
    
    
    NSDateFormatter *systime=[[NSDateFormatter alloc]init];
    [systime setDateFormat:@"HH:mm"];
    // NSString *strtime=[systime stringFromDate:time];
    
    
    NSDateFormatter *systime12=[[NSDateFormatter alloc]init];
    [systime12 setDateFormat:@"hh:mm a"];
    // NSString *strtime12=[systime12 stringFromDate:time];
    
    
    
    
    NSString * str = [form stringFromDate:time];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Tips" message:[NSString stringWithFormat:@"Now is %@,Synchronous Time ?",str] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
    
}

- (IBAction)ToSearchView:(id)sender {
    
    [[ConnectionManager sharedInstance] startScanForTags];
    
    [self performSegueWithIdentifier:@"segueSearchWatch" sender:nil];
    
    //    SearchViewController *searchView=[[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    //    [self.navigationController pushViewController:searchView animated:YES];
    
    
}


- (void)SynchroniseTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags =   NSCalendarUnitYear      | NSCalendarUnitMonth   |
    NSCalendarUnitDay       | NSCalendarUnitHour    |
    NSCalendarUnitMinute    | NSCalendarUnitSecond  | NSCalendarUnitWeekday;
    
    NSDate *now =[NSDate date];
    
    comps = [calendar components:unitFlags fromDate:now];
    
    int weekday = (int) [comps weekday]; //1..Sunday and so on..
    int year =  (int) [comps year];
    int month = (int) [comps month];
    int day =   (int) [comps day];
    int hour =  (int) [comps hour];
    int min =   (int) [comps minute];
    int sec =   (int) [comps second];
    
    if ([[[ProximityTagStorage sharedInstance] tags]count] >0)
    {
        AppDelegate * app= (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        if ([app.appdelegateProximity isConnected])
        {
            Byte bytetongbushijian[12];
            bytetongbushijian[0]=0x04;  //Type
            bytetongbushijian[1]=0x0c;  //Length of packet
            
            bytetongbushijian[2]=0x00;  //Time Zone
            bytetongbushijian[3]=0x00;  //changdu
            
            bytetongbushijian[4]=0x14;  //Year Hi byte (20..)
            bytetongbushijian[5]=year - 2000;   //Year low byte (..15)
            
            bytetongbushijian[6]=month; //Month
            bytetongbushijian[7]=day;   //Day
            
            bytetongbushijian[8]=weekday-1; //Day of the week
            
            bytetongbushijian[9]=hour;  //Hour
            bytetongbushijian[10]=min;  //Minute
            bytetongbushijian[11]=sec;  //Second
            
            NSData * dataff = [NSData dataWithBytes:&bytetongbushijian length:sizeof(bytetongbushijian)];
            [app.appdelegateProximity  phoneWriteDataToTagBig:dataff];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartActivityIndicator" object:self];
    [self performSelector:@selector(timessync) withObject:nil afterDelay:4.0];
}
-(void)timessync {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopActivityIndicator" object:self];
}
- (IBAction)openSupportURL:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.fyiwatches.com/pages/support"]];
}

-(IBAction)btnSelectSearchWatch:(id)sender {
    [self performSegueWithIdentifier:@"segueSearchWatch" sender:nil];
}

//search view
- (void) didDiscoverTag:(ProximityTag*) tag
{
    tag.delegate = self;
}
- (void) isBluetoothEnabled:(bool)enabled
{
    ;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([[[ProximityTagStorage sharedInstance] tags] count] == 0)
        [self.deviceTabview setHidden:YES];
    else
        [self.deviceTabview setHidden:NO];
    
    return [[[ProximityTagStorage sharedInstance] tags] count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConnectViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectCell"];
    if(cell==nil){
        cell=[[ConnectViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectCell"];
    }
    cell.contentLabel.text=@"Shark Tooth";
    cell.iconView.tag=11;
    cell.connectButton.tag=indexPath.row;
    [cell.connectButton addTarget:self action:@selector(clickConnect:) forControlEvents:UIControlEventTouchUpInside];
    
    ProximityTag * tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:indexPath.row];
    if (tag.pothoimage) {
        [cell.iconView setImage:tag.pothoimage];
    }else{
        [cell.iconView setImage:[UIImage imageNamed:@"fs_color"]];
    }
    UILabel *Lable=(UILabel *)[cell.contentView viewWithTag:12];
    if (tag.name) {
        [Lable setText:tag.name];
    }else{
        [Lable setText:@"FYI watch"];
    }
    if ([tag.peripheral isConnected]) {
        [cell.markimg setImage:[UIImage imageNamed:@"markimg"]];
        cell.stateLabel.hidden=NO;
        [cell.stateLabel setText:@"CONNECT >"];
        [cell.connectButton setTitle:@"DISCONNECT >" forState:UIControlStateNormal];
        
    }else{
        [cell.connectButton setTitle:@"CONNECT >" forState:UIControlStateNormal];
        cell.stateLabel.hidden=YES;
        [cell.markimg setImage:[UIImage imageNamed:@""]];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (IBAction)SearchTags:(id)sender
{
    [[ConnectionManager sharedInstance]startScanForTags];
    self.ActivityView.hidden=NO;
    [self.ActivityView startAnimating];
    if ([[[ProximityTagStorage sharedInstance] tags]count] >0) {
        AppDelegate * app= (AppDelegate*)[[UIApplication sharedApplication] delegate];
        ProximityTag * tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:0];
        [[ProximityTagStorage sharedInstance] removeTag:tag];
        [[ConnectionManager sharedInstance] disconnectTag:tag];
        [Proxy shared].flagConnectView = NO;
        [self.deviceTabview reloadData];
        //        if ([app.appdelegateProximity isConnected]) {
        //            [app.appdelegateProximity addObserver:self forKeyPath:@"reverseData" options:NSKeyValueObservingOptionNew context:nil];
        //        }
    } else {
        viewSearchController.hidden = NO;
    }
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(stopScanForTags) userInfo:nil repeats:NO];
    
    
    
    
}
-(void)stopScanForTags{
    
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    int newHeight = 0;
    if ([[[ProximityTagStorage sharedInstance] tags] count] == 0) {
        newHeight = 0;
    } else if([[[ProximityTagStorage sharedInstance] tags] count]>0) {
        int total = 3;
        if([[[ProximityTagStorage sharedInstance] tags] count]<3){
            total = (int)[[[ProximityTagStorage sharedInstance] tags] count];
        }
        newHeight = total*76;
    }
    viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, newHeight);
    viewSearchController.hidden = NO;
    findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
    self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y,self.deviceTabview.frame.size.width, newHeight);
    [self.deviceTabview reloadData];
    self.ActivityView.hidden=YES;
    [self.ActivityView stopAnimating];
    
    [scroll setContentSize:CGSizeMake(320, 440+newHeight)];
    
    
}


//music functionality methods

-(void)nextPlay{
    return;
    if(flagsong==1)
    {
    NSLog(@"执行通知==========================0000000===");
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ((app.playNum+1)<[app.items count]) {
        NSLog(@"执行通知==========================11111111===");
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum+1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        //if ([app.player prepareToPlay]) {
        app.playNum = app.playNum+1;
        NSLog(@"执行通知==========================222222===");
        [app.player play];
        
        MPMediaItem *item = app.items[app.playNum];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        //        ViewController *obj=[[ViewController alloc]init];
        //        [obj.btn_mymusic setBackgroundImage:img forState:UIControlStateNormal];
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        int playnum=app.playNum;
        NSLog (@"playnum %d", playnum);
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:songTitle forKey:@"name"];
        [defaults setObject:songdetail forKey:@"detailname"];
        [defaults setInteger:playnum forKey:@"playnum"];
        
        
        [defaults synchronize];
        
        
        self.currentTimeSlider.maximumValue = [app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        self.timeElapsed.text = @"0:00";
        
        self.duration.text = [NSString stringWithFormat:@"-%@",
                              [app timeFormat:[app getAudioDuration]]];
        
        if ([app.player prepareToPlay]) {
            [app.player play];
            
            //new
            [self.timer invalidate];
            
            //start a timer to update the time label display
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateTime:)
                                                        userInfo:nil
                                                         repeats:YES];
            
        }
        
        
        
        app.isPlayorPause=YES;
        
        
    }
     else
     {
         app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum-1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
         //if ([app.player prepareToPlay]) {
         app.playNum = app.playNum-1;
         NSLog(@"执行通知==========================222222===");
         [app.player play];


     }
    }
}

-(void)playOrPause{
    return;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:@"" forKey:@"flag"];
        
        [data writeToFile: path atomically:YES];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    if ([strUserName isEqualToString:@"1"])
    {
        if(flagsong==1)
        {
        MPMediaItem *item = [app.items objectAtIndex:0];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        //    CGSize itemsize=CGSizeMake(40, 40);
        //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:name forKey:@"name"];
        [defaults setObject:detailname forKey:@"detailname"];
        
        [defaults synchronize];
        NSLog(@"data saved");
        
        NSLog (@" name %@",  name);
        NSLog (@"detailname %@", detailname);
        
        
        
        app.flag=@"0";
        }
    }
    else if ([strUserName isEqualToString:@"0"])
    {
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);
        
        
    }
    
    
    
    
    
    NSLog(@"----------------执行通知------------");
    
    
    if (app.isPlayorPause==YES) {
        
        //[self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
        
        //    [app.player stop];
        
        [app.player pause];
        self.isPaused = FALSE;
        
        app.isPlayorPause=NO;
        
        
        
        
    }
    else if(app.isPlayorPause ==NO){
        app.isPlayorPause=YES;
        
        
        //duration
        
        if (app.player == Nil) {
            app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        }
        
        
        MPMediaItem *item = app.items[app.playNum];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        
        
        musicViewController *vc=[[musicViewController alloc]init];
        
        vc.songname.text=[NSString stringWithFormat:@"%@",songTitle];
        NSLog (@"%@", songTitle);
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        vc.songdetail1.text=[NSString stringWithFormat:@"%@",songdetail];
        
        vc.songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        //        ViewController *obj=[[ViewController alloc]init];
        //        [obj.btn_mymusic setBackgroundImage:img forState:UIControlStateNormal];
        
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        int playnum=app.playNum;
        NSLog (@"playnum %d", playnum);
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:songTitle forKey:@"name"];
        [defaults setObject:songdetail forKey:@"detailname"];
        [defaults setInteger:playnum forKey:@"playnum"];
        
        [defaults synchronize];
        
        [app.player play];
        
        
        
        
        
        
        
        
        
        //start a timer to update the time label display
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [app.player play];
        self.isPaused = TRUE;
        
        
        
        
        
        if ([app.player prepareToPlay]) {
            [app.player play];
        }
        
    }
    
    
}

// av
/*
 * Updates the time label display and
 * the current value of the slider
 * while audio is playing
 */
- (void)updateTime:(NSTimer *)timer {return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //to don't update every second. When scrubber is mouseDown the the slider will not set
    if (!self.scrubbing) {
        self.currentTimeSlider.value = [app getCurrentAudioTime];
    }
    self.timeElapsed.text = [NSString stringWithFormat:@"%@",
                             [app timeFormat:[app getCurrentAudioTime]]];
    
    self.duration.text = [NSString stringWithFormat:@"-%@",
                          [app timeFormat:[app getAudioDuration] - [app getCurrentAudioTime]]];
    
    //When resetted/ended reset the playButton
    
    NSString *strtime=[NSString stringWithFormat:@"-%@",
                       [app timeFormat:[app getAudioDuration] - [app getCurrentAudioTime]]];
    
    //When resetted/ended reset the playButton
    NSLog(@"%@",strtime);
    
    if ([strtime isEqualToString:@"-0:00"]) {
        [self nextPlay];
    }
    
}

/*
 * Sets the current value of the slider/scrubber
 * to the audio file when slider/scrubber is used
 */
- (IBAction)setCurrentTime:(id)scrubber {return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //if scrubbing update the timestate, call updateTime faster not to wait a second and dont repeat it
    [NSTimer scheduledTimerWithTimeInterval:0.01
                                     target:self
                                   selector:@selector(updateTime:)
                                   userInfo:nil
                                    repeats:NO];
    
    [app setCurrentAudioTime:self.currentTimeSlider.value];
    self.scrubbing = FALSE;
}

/*
 * Sets if the user is scrubbing right now
 * to avoid slider update while dragging the slider
 */
- (IBAction)userIsScrubbing:(id)sender {
    self.scrubbing = TRUE;
}



-(void)initMusicItems{
    return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    
    
    
    app.items = [NSMutableArray array];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"songList"]){
        
        NSData *MusicData=[[NSUserDefaults standardUserDefaults]objectForKey:@"songList"];
        NSArray *SongList= (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:MusicData];
        
        
        if([SongList count]>0){
            MPMediaItemCollection *_mediaCollection = [[MPMediaItemCollection alloc]initWithItems:(NSArray *)SongList];
            
            for (MPMediaItem *item in _mediaCollection.items) {
                [app.items addObject:item];
            }
            
            //获得应用播放器
            self.mpc = [MPMusicPlayerController iPodMusicPlayer];
            // self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:_mediaCollection];
            
        }
        
        
    }else{
        
        
        //获得query，用于请求本地歌曲集合
        MPMediaQuery *query = [MPMediaQuery songsQuery];
        
        [query addFilterPredicate:[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithBool:NO] forProperty:MPMediaItemPropertyIsCloudItem]];
        
        NSArray *albumlists = query.collections;
        
        //循环获取得到query获得的集合
        for (MPMediaItemCollection *conllection in albumlists) {
            //MPMediaItem为歌曲项，包含歌曲信息
            for (MPMediaItem *item in conllection.items) {
                [app.items addObject:item];
            }
        }
        
        if ([app.items count]>0) {
            //通过歌曲items数组创建一个collection
            MPMediaItemCollection *mic = [[MPMediaItemCollection alloc] initWithItems:app.items];
            //获得应用播放器
            self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:mic];
            
            flagsong=1;
        }
    }
}


@end
