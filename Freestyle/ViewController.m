

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden = true ;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - Button Action
- (IBAction)getConnectButtonTouchUpInsideAction:(UIButton *)sender {
    //
    [self performSegueWithIdentifier:@"segueConnectYourWatch" sender:nil];
}
- (IBAction)skipIntroButtonTouchUpInsideAction:(UIButton *)sender {

    UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Would you like to see this tutorial the next time you open the app?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];

}
#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (buttonIndex) {
        case 0:
            [Helper setPREFint:1 :seeTutorialNextTime];
            break;
        case 1:
            //segueHomeViewController
            [Helper setPREFint:0 :seeTutorialNextTime];
            break;
            
        default:
            break;
    }
    //[self performSegueWithIdentifier:@"segueHomeViewController" sender:nil];
    

    //UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    
    
    
    
    
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    
    NSString* viewControllerID=@"signUpViewController";
    
    if (accessToken!=nil) {
        viewControllerID = @"homeViewController";
    }else if([Helper getPREFID:@"prefUserLoginInfo"]){
        viewControllerID = @"homeViewController";
    }
    
     UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:viewControllerID]];
    
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
    
    
    
    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
    
    /*
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpViewController"] ];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    [self presentViewController:navigationCont animated:true completion:nil];*/
}


@end
