
#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize IBNSLayoutConstraintForScrollViewContentWidth,IBNSLayoutConstraintForDistanceBetweenWatchFaqAndAppFaq,IBViewInfo,IBWebViewHelp;
#pragma mark - View Life Cycle

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    IBViewInfo.backgroundColor = [[UIColor alloc] initWithRed:34/255 green:35/255 blue:35/255 alpha:0.5];
    IBViewInfo.alpha = 0;
    
    [IBWebViewHelp setBackgroundColor:[UIColor clearColor]];
    [IBWebViewHelp setOpaque:NO];
    [IBWebViewHelp setDelegate:self];
    NSURL*  url = [[NSURL alloc] initWithString:@"http://kennethcoletime.com/app_api/fs/faq/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewHelp  loadRequest:request];
    
}
- (void)viewWillLayoutSubviews{
    IBNSLayoutConstraintForScrollViewContentWidth.constant = self.view.frame.size.width - 40 ;
    IBNSLayoutConstraintForDistanceBetweenWatchFaqAndAppFaq.constant = 8;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)watchFaqsButtonTouchUpInsideAction:(UIButton *)sender{
   
    if (sender.selected == false) {
        
    
        [UIView animateWithDuration:0.5 animations:^{
            
            IBNSLayoutConstraintForDistanceBetweenWatchFaqAndAppFaq.constant = 188;
             IBViewInfo.alpha = 1.0;
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{

              IBNSLayoutConstraintForDistanceBetweenWatchFaqAndAppFaq.constant = 8;
              IBViewInfo.alpha = 0;
        }];
        sender.selected = false;
    }
}
#pragma mark - Navigation Button Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark - WebView Delegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
