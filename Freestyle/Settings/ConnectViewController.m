

#import "ConnectViewController.h"
#import "HomeViewController.h"

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "ConnectionManager.h"
#import <CoreText/CoreText.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "ServerManager.h"
#import "Proxy.h"

#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import "ConnectViewTableViewCell.h"
#import "ViewController.h"
#import "ProximityTagStorage.h"

#import "musicViewController.h"

#define DELAY_TIMER 4.0

@interface ConnectViewController ()
{
    
    int deviceLevel11;
    int deviceLevel22;
   
    BOOL                    _isCountdownTimer;
    NSTimer                 *_captureTimer;
    int                     _captureCounter;
    
    int flag;//This is used as a connection flag 0..disconnected, 1..connected
    
    //music functionality
    AVAudioPlayer           *_audioPlayer;
    
    
    
    int NEXTSONGFLAG;
    
    UIAlertView *alertexit;
    UIView* viewForActivity;

}
//search view
@property(nonatomic,strong)NSMutableArray *arrayData;

//find view
@property(nonatomic,strong)NSTimer *timer;

@end

@implementation ConnectViewController



@synthesize findsyncview,btn_avtive,viewSearchController,scroll;


@synthesize connImageview=_connImageview;


//search view
@synthesize deviceTabview=_deviceTabview;


//info view
@synthesize proximityTag=_proximityTag;
@synthesize pickerController=_pickerController;

@synthesize nameLable=_nameLable,rssiLable=_rssiLable;
@synthesize iamgeView=_iamgeView;
@synthesize datesys,time12,time24,mainview;

@synthesize songdetail1,songname,songgimg;

@synthesize source, btnBack;


CGSize scrollViewOriginalSize;
CGRect keyboardBounds;
CGRect applicationFrame;
#pragma mark - View Life Cycle

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    musicViewController *musivobj=[[musicViewController alloc]init];
    
//    [btnBack setHidden:true];
//    if (![source isEqualToString:@"MENU"])
//    {
//        [btnBack setHidden:true];
//    }
   // btnBack
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    scrollViewOriginalSize = scroll.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    [scroll setScrollEnabled:TRUE];
    
    [super viewDidLoad];
    NSDate * time =  [NSDate new];
    
    NSDateFormatter * form= [[NSDateFormatter alloc] init];
    [form setLocale:[NSLocale currentLocale]];
    [form setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDateFormatter *sysdate=[[NSDateFormatter alloc]init];
    [sysdate setDateFormat:@"MM-dd-yyyy"];
    datesys.text=[sysdate stringFromDate:time];
    
    scrollViewOriginalSize = scroll.contentSize;
    applicationFrame = [[UIScreen mainScreen] applicationFrame];
    [scroll setScrollEnabled:TRUE];
    
    [[ConnectionManager sharedInstance] setDelegate:self];
    
    // Do any additional setup after loading the view.
    self.view.layer.contents=(id)[UIImage imageNamed:@"3.png"].CGImage;
    
    deviceLevel11=0;
    deviceLevel22=0;
    
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    self.automaticallyAdjustsScrollViewInsets = YES;
//    self.extendedLayoutIncludesOpaqueBars = YES;
//    self.navigationController.interactivePopGestureRecognizer.enabled=NO;
//    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:18.0/255.0 green:143.0/255.0 blue:215.0/255.0 alpha:1]];
//    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    for (int i=0; i<[[[ProximityTagStorage sharedInstance] tags] count]; i++) {
        
        ProximityTag *tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:i];
        if (tag.peripheral==nil) {
            [[ProximityTagStorage sharedInstance] removeTag:tag];
            [[ConnectionManager sharedInstance] disconnectTag:tag];
        }
    }
    
    //search view
    if ([[[ProximityTagStorage sharedInstance] tags] count]>0)
    {
        viewSearchController.hidden=NO;
        [self performSelector:@selector(tablehide) withObject:nil afterDelay:1.0];
        //btn_avtive.hidden=YES;
        btn_avtive.enabled = FALSE;
        btn_avtive.backgroundColor = [UIColor grayColor];
        
    } else {
        //btn_avtive.hidden=NO;
        btn_avtive.enabled = TRUE;
        btn_avtive.backgroundColor = [UIColor whiteColor];
        
        [self.deviceTabview reloadData];
        [self performSelector:@selector(TableHide) withObject:nil afterDelay:1.0];
    }
    
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.timer1 = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    
    //self.navigationController.navigationBarHidden=YES;
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.appdelegateProximity.delegate=self;
    if (app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected) {
        [self.connImageview setImage:[UIImage imageNamed:@"AFGGSGSG.png"]];
    }else{
        [self.connImageview setImage:[UIImage imageNamed:@"disconnjianrou.png"]];
    }
    
    //search view
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    
    
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    [self.deviceTabview setHidden:YES];
    [self.ActivityView stopAnimating];
    [self.deviceTabview reloadData];
    self.ActivityView.hidden=YES;
    self.ActivityViewfind.hidden=YES;
    self.ActivityViewsync.hidden=YES;

    


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
   
    UINavigationController* nav = (UINavigationController*)self.parentViewController;
    UIViewController* vc  = [nav.viewControllers firstObject];
    
    if ([vc isKindOfClass:[HomeViewController class]]) {
        UIViewController* settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
        UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
        navigationControllerSlider.viewControllers = [NSArray arrayWithObject:settingsViewController];
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    else
    {
        if ([source isEqualToString:@"MENU"])
        {
            //[btnBack setHidden:false];
            UIViewController* settingsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsViewController"];
            UINavigationController* navigationControllerSlider = self.menuContainerViewController.centerViewController;
            navigationControllerSlider.viewControllers = [NSArray arrayWithObject:settingsViewController];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            return;
        }
        
        [self.navigationController popViewControllerAnimated:true];
    }


}
#pragma mark - Navigation Button Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}


#pragma locate watch

-(IBAction)locatewatchclick:(id)sender
{
//    self.ActivityViewfind.hidden=NO;
//    [self.ActivityViewfind startAnimating];
    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    

    [self becomeFirstResponder];
    if ([[[ProximityTagStorage sharedInstance] tags]count] >0) {
        
        AppDelegate * app= (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if (app.appdelegateProximity.state == CBPeripheralStateConnected)
        {
            //Todo: Cancel Finding
            Byte byte = 0x55;//Find Me alert cancelled
            
            byte = 0x56; //Find Me alert triggered
            
            [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
           [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(StopAnimator) userInfo:nil repeats:NO];
        }
    }
}
-(void)StopAnimator {
    
//    self.ActivityViewfind.hidden=YES;
//    [self.ActivityViewfind stopAnimating];
    [viewForActivity removeFromSuperview];
    
}


//info view
- (void) setProximityTag:(ProximityTag *)proximityTag
{
    if (proximityTag != _proximityTag)
    {
        _proximityTag = proximityTag;
        _proximityTag.delegate = self;
        self.nameLable.text=self.proximityTag.name;
    }
}

-(IBAction)clickConnect:(id)sender
{
    [self setProximityTag:[[[ProximityTagStorage sharedInstance] tags] objectAtIndex:[sender tag]]];
    [Proxy shared].flagConnectView = YES;
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if ((app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected) &&
        ![app.appdelegateProximity isEqual:self.proximityTag]&& flag==0)
    {
        //Not the same device
        //First get rid of the past ,
        [[ConnectionManager sharedInstance] disconnectTag:app.appdelegateProximity];
        
        //and then connect to the current connection
        [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
        [Proxy shared].flagConnectView = NO;
    }
    else
    {   //Is the same device
        if (self.proximityTag.peripheral.state == CBPeripheralStateConnected)
        {
            app.reConnBtnTag =@"1";
            [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
            [self.Connect setText:@"Connect"];
            [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
            [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
            [self.deviceTabview reloadData];
            [scroll setContentSize:CGSizeMake(320, 0)];
            [self TableHide];
            btn_avtive.hidden=NO;
            
            btn_avtive.enabled = TRUE;
            btn_avtive.backgroundColor = [UIColor whiteColor];
            
        }
        else
        {
            //Device is not connected
            if (self.proximityTag.peripheral!=nil)
            {
                //                //No device information->Forget
                //                [self clickForget:[[[ProximityTagStorage sharedInstance] tags] objectAtIndex:[sender tag]]];
                //                [self.deviceTabview reloadData];
                //            }
                //            else
                //            {
                //DEvice information present->Connect
                [[ConnectionManager sharedInstance] connectToTag:self.proximityTag];
                [Proxy shared].flagConnectView = NO;
                
                // btn_avtive.hidden=YES;
                btn_avtive.enabled = FALSE;
                btn_avtive.backgroundColor = [UIColor whiteColor];
                
                UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                [activityIndicatorView startAnimating];
                
                viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
                viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
                
                activityIndicatorView.center = viewForActivity.center;
                [viewForActivity addSubview:activityIndicatorView];
                [self.view addSubview:viewForActivity];
                
                [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(stopcont) userInfo:nil repeats:NO];
            }
        }
    }
}
-(void)stopcont {
    
    [viewForActivity removeFromSuperview];
}

-(IBAction)clickForget:(id)sender{
    
    UIAlertView* question = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                       message:[NSString stringWithFormat:@"Are you sure you want to delete %@?",[self.proximityTag name]]
                                                      delegate:self
                                             cancelButtonTitle:@"YES"  otherButtonTitles:@"NO", nil];
    question.tag=135;
    [question show];
}

//Delegate for the Alert views
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 510)//Find Phone Dialog
    {
        [[ServerManager sharedInstance] stopPlayingAlarmSound];
        if ([ServerManager sharedInstance].didpausemusic == true)
        {
            
            
            
        }
        
        
    }
    else
    {
        if (alertView.tag==135)//Forget Device Dialog
        {
            if(buttonIndex == 0)
            {
                [[ProximityTagStorage sharedInstance] removeTag:self.proximityTag];
                [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
                
                [Proxy shared].flagConnectView = NO;
                [self.deviceTabview reloadData];
            }
        }
        else if (alertView.tag==190)//Rename Dialog (Unused)
        {
            if (buttonIndex==0)
            {
                UITextField *tf=[alertView textFieldAtIndex:0];
                self.proximityTag.name=tf.text;
                self.nameLable.text=tf.text;
            }
        }
        
        if (buttonIndex==1) {
            [self SynchroniseTime];
        }
        
        
    }
}

- (void)DisconnectPeriPheral:(NSNotification*) notification
{
    
    //    [[ProximityTagStorage sharedInstance] removeTag:self.proximityTag];
    //    [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
    
    [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
    //  [[FMDBServers ShareFMDB] InsertBLETable:self.proximityTag.name UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
    [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
}

-(void)updateTimer
{
    //Get current time
    NSDate* now = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour  | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
    NSInteger hour = [dateComponents hour];
    NSString *am_OR_pm=@"AM";
    
    if (hour>12) {
        hour=hour%12;
        am_OR_pm = @"PM";
    }
    NSInteger minute = [dateComponents minute];
    NSInteger second = [dateComponents second];
    time24.text=[NSString stringWithFormat:@"%02ld:%02ld:%02ld %@", (long)hour, (long)minute, (long)second ,am_OR_pm];
}

-(void)TableHide
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopActivityIndicator" object:self];
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(app.appdelegateProximity.peripheral.state==CBPeripheralStateConnected)
        //if (self.proximityTag.peripheral.state == CBPeripheralStateConnected)
    {
        int NumEntries = (int)[[[ProximityTagStorage sharedInstance] tags] count];
        int newHeight = NumEntries*76;
        
        viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, newHeight);
        viewSearchController.hidden = NO;
        
        findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
        findsyncview.hidden=NO;
        
        self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y,self.deviceTabview.frame.size.width, newHeight);
        self.deviceTabview.scrollEnabled = false;
        [self.deviceTabview reloadData];
        
        self.ActivityView.hidden=YES;
        [self.ActivityView stopAnimating];
    }
    else
    {
        
        
        viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, 0);
        viewSearchController.hidden = YES;
        
        findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
        findsyncview.hidden=YES;
        
        self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y, self.deviceTabview.frame.size.width, 0);
    }
}

-(void)tablehide
{
    int NumEntries = (int)[[[ProximityTagStorage sharedInstance] tags] count];
    int newHeight = NumEntries*76;
    
    viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, newHeight);
    viewSearchController.hidden = NO;
    
    findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
    findsyncview.hidden=NO;
    
    
    self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y,self.deviceTabview.frame.size.width, newHeight);
    self.deviceTabview.scrollEnabled = false;
    
//    //Note: Christian this is not set in the other TableHide function
    [scroll setContentSize:CGSizeMake(320, 440+newHeight)];
    
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.timer1 invalidate];
}



-(void)ChangeLayoutData
{
    
    
    viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, 0);
    viewSearchController.hidden = YES;
    
    findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
    findsyncview.hidden=YES;
    
    self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y, self.deviceTabview.frame.size.width, 0);
    btn_avtive.enabled = TRUE;
    btn_avtive.backgroundColor = [UIColor whiteColor];
}

-(void)ChangeLayoutDataConnected
{
    btn_avtive.enabled = FALSE;
    btn_avtive.backgroundColor = [UIColor grayColor];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartActivityIndicator" object:self];
    [self performSelector:@selector(TableHide) withObject:nil afterDelay:3.0];
    
}

- (void) didUpdateData:(id) tag
{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected) {
        [self.connImageview setImage:[UIImage imageNamed:@"AFGGSGSG.png"]];
    }else{
        [self.connImageview setImage:[UIImage imageNamed:@"disconnjianrou.png"]];
    }
    
    //info view
    self.rssiLable.text=[NSString  stringWithFormat:@"RSSI:%f",self.proximityTag.rssiLevel];
    if (self.proximityTag.rssiLevel==0.0f) {
        [self.Connect setText:@"CONNECT"];
        flag=0;
    }else{
        [self.Connect setText:@"DISCONNECT"];
        flag=1;
        
        [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
        
        
        
    }
}

- (IBAction)SynTime:(id)sender
{
    NSDateFormatter * form= [[NSDateFormatter alloc] init];
    [form setLocale:[NSLocale currentLocale]];
    //[form setDateFormat:@"MM-dd-yyyy hh:mm a"];
    [form setDateFormat:@"hh:mm a MM-dd-yyyy"];
    
    NSString * str = [form stringFromDate:[NSDate new]];
    
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Time Sync" message:[NSString stringWithFormat:@"The current time is: \n %@ \n Sync this time ?",str] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
 
    [alert show];
}

- (void)SynchroniseTime
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags =   NSCalendarUnitYear      | NSCalendarUnitMonth   |
    NSCalendarUnitDay       | NSCalendarUnitHour    |
    NSCalendarUnitMinute    | NSCalendarUnitSecond  | NSCalendarUnitWeekday;
    
    NSDate *now =[NSDate date];
    
    comps = [calendar components:unitFlags fromDate:now];
    
    int weekday = (int) [comps weekday]; //1..Sunday and so on..
    int year =  (int) [comps year];
    int month = (int) [comps month];
    int day =   (int) [comps day];
    int hour =  (int) [comps hour];
    int min =   (int) [comps minute];
    int sec =   (int) [comps second];
    
    if ([[[ProximityTagStorage sharedInstance] tags]count] >0)
    {
        AppDelegate * app= (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        if ([app.appdelegateProximity isConnected])
        {
            Byte bytetongbushijian[12];
            bytetongbushijian[0]=0x04;  //Type
            bytetongbushijian[1]=0x0c;  //Length of packet
            
            bytetongbushijian[2]=0x00;  //Time Zone
            bytetongbushijian[3]=0x00;  //changdu
            
            bytetongbushijian[4]=0x14;  //Year Hi byte (20..)
            bytetongbushijian[5]=year - 2000;   //Year low byte (..15)
            
            bytetongbushijian[6]=month; //Month
            bytetongbushijian[7]=day;   //Day
            
            bytetongbushijian[8]=weekday-1; //Day of the week
            
            bytetongbushijian[9]=hour;  //Hour
            bytetongbushijian[10]=min;  //Minute
            bytetongbushijian[11]=sec;  //Second
            
            NSData * dataff = [NSData dataWithBytes:&bytetongbushijian length:sizeof(bytetongbushijian)];
            [app.appdelegateProximity  phoneWriteDataToTagBig:dataff];
        }
    }
//    self.ActivityViewsync.hidden=NO;
//    [self.ActivityViewsync startAnimating];
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(timessync) userInfo:nil repeats:NO];
    
    
}
-(void)timessync {
//    self.ActivityViewsync.hidden=YES;
//    [self.ActivityViewsync stopAnimating];
    
    [viewForActivity removeFromSuperview];
}
//search view
- (void) didDiscoverTag:(ProximityTag*) tag {
    tag.delegate = self;
}

//Hide whenever the connection state changes
- (void) isBluetoothEnabled:(bool)enabled
{
    if (!enabled)
    {
        [[ConnectionManager sharedInstance] disconnectTag:self.proximityTag];
        [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
        [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
        [self.deviceTabview reloadData];
        btn_avtive.enabled = TRUE;
        btn_avtive.backgroundColor = [UIColor whiteColor];
        flag=0;
         
        
        viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width, 0);
        viewSearchController.hidden = YES;
        
        findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
        findsyncview.hidden=YES;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([[[ProximityTagStorage sharedInstance] tags] count] == 0)
        [self.deviceTabview setHidden:YES];
    else
        [self.deviceTabview setHidden:NO];
    
    return [[[ProximityTagStorage sharedInstance] tags] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConnectViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConnectCell"];
    if(cell==nil)
    {
        cell=[[ConnectViewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ConnectCell"];
    }
    
    cell.contentLabel.text=@"Shark Tooth";
    cell.iconView.tag=11;
    
    cell.connectButton.tag=indexPath.row;
    [cell.connectButton addTarget:self action:@selector(clickConnect:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.iconView setImage:[UIImage imageNamed:@"fs_color"]];
    
    ProximityTag * tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:indexPath.row];
    
    UILabel *Lable=(UILabel *)[cell.contentView viewWithTag:12];
    if (tag.name) {
        [Lable setText:tag.name];
    }else{
        [Lable setText:@"KC Watch"];
    }
    
    if (tag.peripheral.state == CBPeripheralStateConnected)
    {
        [cell.markimg setImage:[UIImage imageNamed:@"markimg"]];
        cell.stateLabel.hidden=NO;
        [cell.stateLabel setText:@"CONNECT >"];
        [cell.connectButton setTitle:@"DISCONNECT >" forState:UIControlStateNormal];
        //        cell.connectButton.enabled = FALSE;
        //        [self performSelector:@selector(btnEnable:) withObject:cell.connectButton afterDelay:5.0];
    }
    else
    {
        [cell.connectButton setTitle:@"CONNECT >" forState:UIControlStateNormal];
        cell.stateLabel.hidden=YES;
        [cell.markimg setImage:[UIImage imageNamed:@""]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(IBAction)btnEnable:(id)sender {
    UIButton *btnConnect = (UIButton *)sender;
    btnConnect.enabled = TRUE;
    
}
- (IBAction)SearchTags:(id)sender
{
    [[ConnectionManager sharedInstance]startScanForTags];
    self.ActivityView.hidden=NO;
    [self.ActivityView startAnimating];
    
    self.rssiLable.text=[NSString  stringWithFormat:@"RSSI:%f",self.proximityTag.rssiLevel];
    if (self.proximityTag.rssiLevel==0.0f) {
        [self.Connect setText:@"CONNECT"];
        flag=0;
    }else{
        [self.Connect setText:@"DISCONNECT"];
        flag=1;
        
        [[FMDBServers ShareFMDB] InsertBLETable:self.nameLable.text UUID:[NSString stringWithFormat:@"%@",self.proximityTag.peripheralCFUUIDRef] imageName:@""];
        
        [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
    }
    
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopScanForTags) userInfo:nil repeats:NO];
}

-(void)stopScanForTags
{
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
    {
        tag.delegate = self;
    }
    
    int NumEntries = (int)[[[ProximityTagStorage sharedInstance] tags] count];
    int newHeight = NumEntries*76;
    
    
    
    viewSearchController.frame = CGRectMake(viewSearchController.frame.origin.x, viewSearchController.frame.origin.y, viewSearchController.frame.size.width,newHeight);
    viewSearchController.hidden = NO;
    
    findsyncview.frame = CGRectMake(findsyncview.frame.origin.x, (viewSearchController.frame.origin.y+viewSearchController.frame.size.height), findsyncview.frame.size.width, findsyncview.frame.size.height);
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (app.appdelegateProximity.peripheral.state != CBPeripheralStateConnected) {
        findsyncview.hidden=YES;
    }
    
    
    self.deviceTabview.frame = CGRectMake(self.deviceTabview.frame.origin.x, self.deviceTabview.frame.origin.y,self.deviceTabview.frame.size.width,newHeight);
    [self.deviceTabview reloadData];
    
    self.ActivityView.hidden=YES;
    [self.ActivityView stopAnimating];
    [scroll setContentSize:CGSizeMake(320, 440+newHeight)];
}


-(void) moveScrollView:(UIView *) theView
{
    CGFloat viewCenterY = theView.center.y;
    CGFloat freeSpaceHeight = applicationFrame.size.height - keyboardBounds.size.height;
    CGFloat scrollAmount = viewCenterY - freeSpaceHeight /5.0;
    if (scrollAmount < 0) scrollAmount = 0;
    scroll.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height + keyboardBounds.size.height);
    [scroll setContentOffset:CGPointMake(0, scrollAmount) animated:YES];
}

//command code
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        app.pickerController = [[UIImagePickerController alloc] init];
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        if (!hasCamera) {
            return;
        }
        
        app.pickerController.delegate = app;
        app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
        app.pickerController.showsCameraControls = NO;
        
        UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
        cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
        app.pickerController.cameraOverlayView = cameraFrameView;
        app.xiangjiisopen = @"yes";
        
        UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
        if ([activeController isKindOfClass:[UINavigationController class]]) {
            activeController = [(UINavigationController*) activeController visibleViewController];
        }
        [activeController presentViewController:app.pickerController animated:YES completion:NULL];
        
    }else if (buttonIndex == 1) {
        
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
        if (!hasCamera) {
            return;
        }
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        //能够使用iphone的内置摄像机拍照
        self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.pickerController.allowsEditing = YES;
        
        [self presentViewController:self.pickerController animated:YES completion:nil];
        
    }else if(buttonIndex == 2) {
        
        
    }
}

#pragma mark Camera Functions
//Automatic call Camera
-(void) takePic
{
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([app.xiangjiisopen isEqualToString:@"yes"])
    {
        [app cameraTakePicture:nil];
    }else{
        //        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Tips" message:@"Please Open the Camera" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        //        [alert show];
    }
}

- (void)_captureImage
{
    if (!_isCountdownTimer)
    {
        _isCountdownTimer = YES;
        _captureCounter=0;
        _captureTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(captureDelay) userInfo:nil repeats:YES];
    }
}

- (void)captureDelay
{
    _captureCounter++;
    if (_captureCounter >= DELAY_TIMER)
    {
        _captureCounter = 0;
        _isCountdownTimer = NO;
        [_captureTimer invalidate];
        [self  takePic];
    }
}

- (void)usingPicMa
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType: completionHandler:)])
        {
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
             {
                 if (granted) //Permission is granted
                 {
                     dispatch_async(dispatch_get_main_queue(), ^
                                    {
                                        [self OpenCamera];
                                    });
                 }
                 else // Permission has been denied.
                 {
                     UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Camera permission needed"
                                                                       message:@"Please change review your configuration and restart the app."
                                                                      delegate:self
                                                             cancelButtonTitle:@"OK"
                                                             otherButtonTitles:nil];
                     
                     message.tag = 3491832;
                     [message addButtonWithTitle:@"Settings"];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [message show];
                     });
                     
                 }
             }];
        }
        else
        {
            [self OpenCamera];
        }
    }
}

-(void) OpenCamera
{
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.pickerController = [[UIImagePickerController alloc] init];
    app.pickerController.delegate = app;
    app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
    app.pickerController.showsCameraControls = NO;
    
    UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
    cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
    app.pickerController.cameraOverlayView = cameraFrameView;
    app.xiangjiisopen = @"yes";
    
    UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([activeController isKindOfClass:[UINavigationController class]]) {
        activeController = [(UINavigationController*) activeController visibleViewController];
    }
    
    [activeController presentViewController:app.pickerController animated:YES completion:NULL];
}

//Delegate from Camera Permission Dialog
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if (alertView.tag == 3491832 && [title isEqualToString:@"Settings"] ) // Settings clicked
    {
        if (&UIApplicationOpenSettingsURLString != NULL) {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }
    }
}

-(void) CommandCamera
{
    AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if(app.flagcmd==0) //Note: This means App is in foreground, maybe can be removed
    {
        if ([app.xiangjiisopen isEqualToString:@"yes"]) {
            [self  _captureImage]; // hide by me
        } else {
            [self usingPicMa];
        }
    }
}

-(void) CommandFindPhone
{
    //Show Window
    UIAlertView * alt = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"You Found Me!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alt.tag = 510;
    [alt show];
    
    [[ServerManager sharedInstance] startPlayingAlarmSound];
}

-(void) CommandPlayPauseMusic
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommandPlayPause" object:nil userInfo:nil];
}

-(void) CommandNextSong
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CommandNextSong" object:nil userInfo:nil];
}

- (void) didwatchTophone:(id) type
{
    NSString * str =(NSString *)type;
    AppDelegate * app =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    app.appdelegateProximity.delegate =self;
    
    
    
    if ([str isEqualToString:@"b0"])        //Command 1  ---------------------------------------
    {
        if ([app.appdelegateProximity.c1 isEqualToString:@"Camera"])
        {
            [self CommandCamera];
        }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Find Phone"])
            [self CommandFindPhone];
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Play/Pause Music"]){
            
            
            
              [self CommandPlayPauseMusic];
        }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Next Song"]){
            
                        [self CommandNextSong];
        }
        else if ([app.appdelegateProximity.c1 isEqualToString:@"Off"])
        {   // Do nothing
        }
    }
    else if([str isEqualToString:@"b1"])    //Command 2 ---------------------------------------
    {
        if ([app.appdelegateProximity.c2 isEqualToString:@"Camera"])
            [self CommandCamera];
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Find Phone"])
            [self CommandFindPhone];
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Play/Pause Music"])
        {
            
  [self CommandPlayPauseMusic];
        }
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Next Song"]){
           [self CommandNextSong];
        }
        else if ([app.appdelegateProximity.c2 isEqualToString:@"Off"])
        {   // Do nothing
        }
    }
    else if([str isEqualToString:@"b2"]) //Command 3 ---------------------------------------
    {
        if ([app.appdelegateProximity.c3 isEqualToString:@"Camera"])
            [self CommandCamera];
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Find Phone"])
            [self CommandFindPhone];
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Play/Pause Music"]){
                [self CommandPlayPauseMusic];
        }
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Next Song"]){
         
          [self CommandNextSong];
        }
        else if ([app.appdelegateProximity.c3 isEqualToString:@"Off"])
        {   //  Do nothing
        }
    }
}


//music functionality add

-(void)nextPlay{
    return;
    NSLog(@"执行通知==========================0000000===");
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ((app.playNum+1)<[app.items count]) {
        NSLog(@"执行通知==========================11111111===");
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum+1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        //if ([app.player prepareToPlay]) {
        app.playNum = app.playNum+1;
        NSLog(@"执行通知==========================222222===");
        [app.player play];
        
        MPMediaItem *item = app.items[app.playNum];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        //    CGSize itemsize=CGSizeMake(40, 40);
        //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        //        ViewController *obj=[[ViewController alloc]init];
        //        [obj.btn_mymusic setBackgroundImage:img forState:UIControlStateNormal];
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        int playnum=app.playNum;
        NSLog (@"playnum %d", playnum);
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:songTitle forKey:@"name"];
        [defaults setObject:songdetail forKey:@"detailname"];
        [defaults setInteger:playnum forKey:@"playnum"];
        
        
        [defaults synchronize];
        
        
        self.currentTimeSlider.maximumValue = [app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        self.timeElapsed.text = @"0:00";
        
        self.duration.text = [NSString stringWithFormat:@"-%@",
                              [app timeFormat:[app getAudioDuration]]];
        
        if ([app.player prepareToPlay]) {
            [app.player play];
            
            //new
            [self.timer invalidate];
            //            //play audio for the first time or if pause was pressed
            //            if (!self.isPaused) {
            //                [self.playButton setBackgroundImage:[UIImage imageNamed:@"audioplayer_pause.png"]
            //                                           forState:UIControlStateNormal];
            //
            //start a timer to update the time label display
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateTime:)
                                                        userInfo:nil
                                                         repeats:YES];
            //
            //                [app.player play];
            //                self.isPaused = TRUE;
            
            // }
        }
        
        
        
        app.isPlayorPause=YES;
        
        //}
    }else
    {
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum-1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        //if ([app.player prepareToPlay]) {
        app.playNum = app.playNum-1;
        NSLog(@"执行通知==========================222222===");
        [app.player play];

    }
    
}

-(void)playOrPause{
    return;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:@"" forKey:@"flag"];
        
        [data writeToFile: path atomically:YES];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    if ([strUserName isEqualToString:@"1"])
    {
        
        MPMediaItem *item = [app.items objectAtIndex:0];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        //    CGSize itemsize=CGSizeMake(40, 40);
        //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:name forKey:@"name"];
        [defaults setObject:detailname forKey:@"detailname"];
        
        [defaults synchronize];
        NSLog(@"data saved");
        
        NSLog (@" name %@",  name);
        NSLog (@"detailname %@", detailname);
        
        
        
        app.flag=@"0";
    }
    else if ([strUserName isEqualToString:@"0"])
    {
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);
        
        
    }
    
    
    
    
    
    NSLog(@"----------------执行通知------------");
    
    
    if (app.isPlayorPause==YES) {
        
        //[self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
        
        //    [app.player stop];
        
        [app.player pause];
        self.isPaused = FALSE;
        
        app.isPlayorPause=NO;
        
        
        
        
    }
    else if(app.isPlayorPause ==NO){
        app.isPlayorPause=YES;
        
        
        //duration
        
        if (app.player == Nil) {
            app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        }
        
        
        MPMediaItem *item = app.items[app.playNum];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        
        
        musicViewController *vc=[[musicViewController alloc]init];
        
        vc.songname.text=[NSString stringWithFormat:@"%@",songTitle];
        NSLog (@"%@", songTitle);
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        vc.songdetail1.text=[NSString stringWithFormat:@"%@",songdetail];
        
        vc.songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        //        ViewController *obj=[[ViewController alloc]init];
        //        [obj.btn_mymusic setBackgroundImage:img forState:UIControlStateNormal];
        
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        int playnum=app.playNum;
        NSLog (@"playnum %d", playnum);
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:songTitle forKey:@"name"];
        [defaults setObject:songdetail forKey:@"detailname"];
        [defaults setInteger:playnum forKey:@"playnum"];
        
        [defaults synchronize];
        
        [app.player play];
        
        
        
        
        
        
        
        
        
        //start a timer to update the time label display
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [app.player play];
        self.isPaused = TRUE;
        
        
        
        
        
        if ([app.player prepareToPlay]) {
            [app.player play];
        }
        
    }
    
    
    //    if (app.isPlayorPause==YES) {
    //
    //        [app.player pause];
    //        app.isPlayorPause=NO;
    //    }else if(app.isPlayorPause ==NO){
    //        app.isPlayorPause=YES;
    //        NSLog(@"--------------------------------------------");
    //
    //
    //        if (app.player == Nil) {
    //        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
    //        }
    //
    //
    //        MPMediaItem *item = app.items[app.playNum];
    //        //获得专辑对象
    //        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    //        //专辑封面
    //
    //
    //
    //        UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
    //        if (!img) {
    //            img = [UIImage imageNamed:@"my music"];
    //        }
    //
    //
    //        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
    //
    //
    //        musicViewController *vc=[[musicViewController alloc]init];
    //
    //        vc.songname.text=[NSString stringWithFormat:@"%@",songTitle];
    //          NSLog (@"%@", songTitle);
    //
    //        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
    //        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
    //        NSLog (@"songdetail %@", songdetail);
    //        vc.songdetail1.text=[NSString stringWithFormat:@"%@",songdetail];
    //
    //        vc.songgimg.image=img;
    //
    //        //create instace of NSData
    //        UIImage *song_img=img;
    //        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
    //
    //        ViewController *obj=[[ViewController alloc]init];
    //        [obj.btn_mymusic setBackgroundImage:img forState:UIControlStateNormal];
    //
    //
    //        NSString *name=[songname text];
    //        NSString *detailname=[songdetail1 text];
    //        int playnum=app.playNum;
    //        NSLog (@"playnum %d", playnum);
    //
    //        //store the data
    //        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //
    //        [defaults setObject:imagedata forKey:@"image"];
    //        [defaults setObject:songTitle forKey:@"name"];
    //        [defaults setObject:songdetail forKey:@"detailname"];
    //        [defaults setInteger:playnum forKey:@"playnum"];
    //
    //        [defaults synchronize];
    //
    //        [app.player play];
    //
    //
    //
    //
    //        if ([app.player prepareToPlay]) {
    //            [app.player play];
    //
    //            //new
    //            [self.timer invalidate];
    //
    //            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
    //                                                          target:self
    //                                                        selector:@selector(updateTime:)
    //                                                        userInfo:nil
    //                                                         repeats:YES];
    //            //
    //            //                [app.player play];
    //            //                self.isPaused = TRUE;
    //
    //            // }
    //        }
    //
    //
    //
    //        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[playnum] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
    //        if ([app.player prepareToPlay]) {
    //            [app.player play];
    //        }
    //    }
}


// av
/*
 * Updates the time label display and
 * the current value of the slider
 * while audio is playing
 */
- (void)updateTime:(NSTimer *)timer {return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //to don't update every second. When scrubber is mouseDown the the slider will not set
    if (!self.scrubbing) {
        self.currentTimeSlider.value = [app getCurrentAudioTime];
    }
    self.timeElapsed.text = [NSString stringWithFormat:@"%@",
                             [app timeFormat:[app getCurrentAudioTime]]];
    
    self.duration.text = [NSString stringWithFormat:@"-%@",
                          [app timeFormat:[app getAudioDuration] - [app getCurrentAudioTime]]];
    
    //When resetted/ended reset the playButton
    
    NSString *strtime=[NSString stringWithFormat:@"-%@",
                       [app timeFormat:[app getAudioDuration] - [app getCurrentAudioTime]]];
    
    //When resetted/ended reset the playButton
   // NSLog(@"%@",strtime);
    
    if ([strtime isEqualToString:@"-0:00"]) {
        [self nextPlay];
    }
}

/*
 * Sets the current value of the slider/scrubber
 * to the audio file when slider/scrubber is used
 */
- (IBAction)setCurrentTime:(id)scrubber {return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //if scrubbing update the timestate, call updateTime faster not to wait a second and dont repeat it
    [NSTimer scheduledTimerWithTimeInterval:0.01
                                     target:self
                                   selector:@selector(updateTime:)
                                   userInfo:nil
                                    repeats:NO];
    
    [app setCurrentAudioTime:self.currentTimeSlider.value];
    self.scrubbing = FALSE;
}

/*
 * Sets if the user is scrubbing right now
 * to avoid slider update while dragging the slider
 */
- (IBAction)userIsScrubbing:(id)sender {
    self.scrubbing = TRUE;
}



-(void)initMusicItems{
    return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    
    
    
    app.items = [NSMutableArray array];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"songList"]){
        
        NSData *MusicData=[[NSUserDefaults standardUserDefaults]objectForKey:@"songList"];
        NSArray *SongList= (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:MusicData];
        
        
        if([SongList count]>0){
            MPMediaItemCollection *_mediaCollection = [[MPMediaItemCollection alloc]initWithItems:(NSArray *)SongList];
            
            for (MPMediaItem *item in _mediaCollection.items) {
                [app.items addObject:item];
            }
            
            //获得应用播放器
            self.mpc = [MPMusicPlayerController iPodMusicPlayer];
            // self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:_mediaCollection];
            
        }
        
        
    }else{
        
        
        //获得query，用于请求本地歌曲集合
        MPMediaQuery *query = [MPMediaQuery songsQuery];
        
        [query addFilterPredicate:[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithBool:NO] forProperty:MPMediaItemPropertyIsCloudItem]];
        
        NSArray *albumlists = query.collections;
        
        //循环获取得到query获得的集合
        for (MPMediaItemCollection *conllection in albumlists) {
            //MPMediaItem为歌曲项，包含歌曲信息
            for (MPMediaItem *item in conllection.items) {
                [app.items addObject:item];
            }
        }
        
        if ([app.items count]>0) {
            //通过歌曲items数组创建一个collection
            MPMediaItemCollection *mic = [[MPMediaItemCollection alloc] initWithItems:app.items];
            //获得应用播放器
            self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:mic];
        }
    }
}

 


@end
