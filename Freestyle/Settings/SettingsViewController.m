

#import "SettingsViewController.h"
#import "AppDelegate.h"
@interface SettingsViewController ()

@end

@implementation SettingsViewController

#pragma mark - View Life Cycle

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)logoutButtonTouchUpInsideAction:(UIButton *)sender {
    UIAlertView* alertView=[[UIAlertView alloc] initWithTitle:@"" message:@"Do you want to logout?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}
#pragma mark - AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
        {
        
//           [Helper getPREFID:@"prefUserLoginInfo"]
            
            [[FBSDKLoginManager new] logOut];
            
            [Helper delPREF:@"prefUserLoginInfo"];
            
            UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"signUpViewController"]];
            
            [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
            
            
            MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
            
            mFSideMenuContainerViewController.centerViewController = navigationCont;
            mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
            
            
            
            [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
        }
            break;
        default:
            break;
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
