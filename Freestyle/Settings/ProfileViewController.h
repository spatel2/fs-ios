//
//  ProfileViewController.h
//  Freestyle
//
//  Created by BIT on 09/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SikinButton.h"
#import "FreestyleHelper.h"
#import "MFSideMenu.h"

@interface ProfileViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewUserPic;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldUserName;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldAge;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmailId;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldFacebookInfo;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldTwitterInfo;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonEditProfile;

@end
