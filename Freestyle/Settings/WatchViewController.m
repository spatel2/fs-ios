//
//  WatchViewController.m
//  Freestyle
//
//  Created by BIT on 09/03/15.
//
//

#import "WatchViewController.h"
#import "AppDelegate.h"
#import "ConnectViewController.h"

@interface WatchViewController ()
{
    BOOL isLoad;
}

@end

@implementation WatchViewController
@synthesize IBNSLayoutConstraintWidthForView;
@synthesize LeftLabel=_LeftLabel, MiddLabel=_MiddLabel,RightLable=_RightLable;
#pragma mark - View Life cycle

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    
    //---------- First Button ----------
    UITapGestureRecognizer *singtap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap.numberOfTapsRequired = 1;
    [self.C1Image setUserInteractionEnabled:YES];
    [self.C1Image addGestureRecognizer:singtap];
    
    //---------- Second Button ----------
    UITapGestureRecognizer *singtap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap1.numberOfTapsRequired = 1;
    [self.C2Image setUserInteractionEnabled:YES];
    [self.C2Image addGestureRecognizer:singtap1];
    
    //---------- Third Button ----------
    UITapGestureRecognizer *singtap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickimg:)];
    singtap2.numberOfTapsRequired = 1;
    [self.C3Image setUserInteractionEnabled:YES];
    [self.C3Image addGestureRecognizer:singtap2];
    
}
- (void)viewDidLayoutSubviews{
    IBNSLayoutConstraintWidthForView.constant = self.view.bounds.size.width - 40;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}
//command functionality

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBarHidden=YES;
//    self.tabBarController.tabBar.hidden = NO;
    
    //---------------------------------------------------------------------------
    //------------------------------  First Button ------------------------------
    //---------------------------------------------------------------------------
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Camera"])
    {
        [self.LeftLabel setText:@"Camera"	];
        [self.C1Image setImage:[UIImage imageNamed:@"img_camera_white"]];
        [self.ButtonMusic setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Find Phone"])
    {
        [self.LeftLabel setText:@"Find Phone"];
        [self.C1Image setImage:[UIImage imageNamed:@"img_phone_white"]];
        [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Play/Pause Music"])
    {
        [self.LeftLabel setText:@"Play/Pause Music"];
        [self.C1Image setImage:[UIImage imageNamed:@"img_player_white"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Next Song"])
    {
        [self.LeftLabel setText:@"Next Song"];
        [self.C1Image setImage:[UIImage imageNamed:@"img_player_white"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C1"] isEqualToString:@"Off"])
    {
        [self.LeftLabel setText:@"Off"];
        [self.C1Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    
    
    //---------------------------------------------------------------------------
    //------------------------------ Second Button ------------------------------
    //---------------------------------------------------------------------------
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Camera"])
    {
        [self.MiddLabel setText:@"Camera"];
        [self.C2Image setImage:[UIImage imageNamed:@"img_camera_white"]];
        [self.ButtonCamera setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Find Phone"])
    {
        [self.MiddLabel setText:@"Find Phone"];
        [self.C2Image setImage:[UIImage imageNamed:@"img_phone_white"]];
        [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Play/Pause Music"])
    {
        [self.MiddLabel setText:@"Play/Pause Music"];
        [self.C2Image setImage:[UIImage imageNamed:@"img_player_white"]];
        [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Next Song"])
    {
        [self.MiddLabel setText:@"Next Song"];
        [self.C2Image setImage:[UIImage imageNamed:@"img_player_white"]];
        [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C2"] isEqualToString:@"Off"])
    {
        [self.MiddLabel setText:@"Off"];
        [self.C2Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    
    //---------------------------------------------------------------------------
    //------------------------------ Third Button -------------------------------
    //---------------------------------------------------------------------------
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Camera"])
    {
        [self.RightLable setText:@"Camera"];
        [self.C3Image setImage:[UIImage imageNamed:@"img_camera_white"]];
        [self.ButtonOther setTitle:@"Camera" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Find Phone"])
    {
        [self.RightLable setText:@"Find Phone"];
        [self.C3Image setImage:[UIImage imageNamed:@"img_phone_white"]];
        [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Play/Pause Music"])
    {
        [self.RightLable setText:@"Play/Pause Music"];
        [self.C3Image setImage:[UIImage imageNamed:@"img_player_white" ]];
        [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Next Song"])
    {
        [self.RightLable setText:@"Next Song"];
        [self.C3Image setImage:[UIImage imageNamed:@"img_player_white" ]];
        [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"C3"] isEqualToString:@"Off"])
    {
        [self.RightLable setText:@"Off"];
        [self.C3Image setImage:[UIImage imageNamed:@"findbtn"]];
        [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
        [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
    }
}

- (void)usingPicMa
{
    
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.pickerController = [[UIImagePickerController alloc] init];
    
    BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (!hasCamera) {
        return;
    }
    
    app.pickerController.delegate = app;
    app.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    app.pickerController.cameraDevice=UIImagePickerControllerCameraDeviceFront;
    app.pickerController.showsCameraControls = NO;
    
    UIView *cameraFrameView = [[[NSBundle mainBundle] loadNibNamed:@"CameraOverlay" owner:app options:nil] objectAtIndex:0];
    cameraFrameView.frame = app.pickerController.cameraOverlayView.frame;
    app.pickerController.cameraOverlayView = cameraFrameView;
    app.xiangjiisopen = @"yes";
    
    UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([activeController isKindOfClass:[UINavigationController class]]) {
        activeController = [(UINavigationController*) activeController visibleViewController];
    }
    [activeController presentViewController:app.pickerController animated:YES completion:NULL];
    
}

-(void)clickimg:(id) sender
{
    UIImageView *v = (UIImageView*)((UITapGestureRecognizer *)[sender view]);
    
    if ([[[v image] accessibilityIdentifier]isEqualToString:@"1.png"])
    {
        [self usingPicMa];
    }
    
}


-(void)CallMusic
{
    [self ButtonMusic:nil];
}

- (IBAction)ButtonMusic:(id)sender
{
    if ([self.LeftLabel.text isEqualToString:@"Camera"])
    {
        [self usingPicMa];
    }
}

- (IBAction)ButtonCamera:(id)sender
{
    if ([self.LeftLabel.text isEqualToString:@"Camera"])
    {
        [self usingPicMa];
    }
}

- (IBAction)ButtonOther:(id)sender
{
    if ([self.LeftLabel.text isEqualToString:@"Camera"])
    {
        [self usingPicMa];
    }
}

- (IBAction)SelectValue:(id)sender
{
    UIButton *Button=(UIButton *)sender;
    int index=(int)Button.tag;
    
    ActionStringDoneBlock done = ^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue)
    {
        if ([self respondsToSelector:@selector(setText:)])
        {
            [self performSelector:@selector(setText:) withObject:[NSArray arrayWithObjects:selectedValue,[NSNumber numberWithInt:index], nil]];
        }
    };
    
    
    ActionStringCancelBlock cancel = ^(ActionSheetStringPicker *picker)
    {
        
        
    };
    
    //@"Tag location"
    NSArray *array=[NSArray arrayWithObjects:@"Camera",@"Play/Pause Music",@"Find Phone", @"Next Song",@"Off",nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Choose functionality" rows:array initialSelection:0 doneBlock:done cancelBlock:cancel origin:sender];
}

-(void)setText:(NSArray *)array{
    
    int index=(int)[[array objectAtIndex:1] integerValue];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    switch (index)
    {
            //--------------------------------------------------------------------
            //--------------------------- First Button ---------------------------
            //--------------------------------------------------------------------
        case 11:
        {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C1"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            
            if (app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected)
            {
                
                app.appdelegateProximity.c1=selectimage;
            }
            
            
            [self.LeftLabel setText:[array objectAtIndex:0]];
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"])
            {
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
            }
            else if([selectimage isEqualToString:@"Camera"])
            {
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_camera_white"]];
                [self.C1Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonMusic setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
                
            }
            else if ([selectimage isEqualToString:@"Find Phone"])
            {
                [self.C1Image setImage:[UIImage imageNamed:@"img_phone_white"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonMusic setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
                
                
            }
            else if ([selectimage isEqualToString:@"Next Song"])
            {
                
                [self.C1Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
                
            }
            else if ([selectimage isEqualToString:@"Off"])
            {
                
                [self.C1Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C1Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
        }
            break;
            //--------------------------------------------------------------------
            //--------------------------- Second Button --------------------------
            //--------------------------------------------------------------------
        case 12:
        {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C2"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            [self.MiddLabel setText:[array objectAtIndex:0]];
            
            if (app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected)
            {
                app.appdelegateProximity.c2=selectimage;
            }
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"])
            {
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else if([selectimage isEqualToString:@"Camera"])
            {
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_camera_white"]];
                [self.C2Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonCamera setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Find Phone"])
            {
                [self.C2Image setImage:[UIImage imageNamed:@"img_phone_white"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonCamera setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Next Song"])
            {
                
                [self.C2Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonCamera setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonCamera setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Off"])
            {
                
                [self.C2Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C2Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
        }
            break;
            //--------------------------------------------------------------------
            //--------------------------- Third Button ---------------------------
            //--------------------------------------------------------------------
        case 13:
        {
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ] forKey:@"C3"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            NSString *selectimage=[NSString stringWithFormat:@"%@",[array objectAtIndex:0] ];
            if (app.appdelegateProximity.peripheral.state == CBPeripheralStateConnected)
            {
                app.appdelegateProximity.c3=selectimage;
            }
            [self.RightLable setText:[array objectAtIndex:0]];
            
            if ( [selectimage isEqualToString:@"Play/Pause Music"])
            {
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else  if([selectimage isEqualToString:@"Camera"])
            {
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_camera_white"]];
                [self.C3Image.image setAccessibilityIdentifier:@"1.png"];
                [self.ButtonOther setTitle:@"Camera" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Find Phone"])
            {
                [self.C3Image setImage:[UIImage imageNamed:@"img_phone_white"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-find.png"];
                [self.ButtonOther setTitle:@"" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniutou.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Next Song"])
            {
                
                [self.C3Image setImage:[UIImage imageNamed:@"img_player_white"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonOther setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonOther setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
            else if ([selectimage isEqualToString:@"Off"])
            {
                [self.C3Image setImage:[UIImage imageNamed:@"findbtn"]];
                [self.C3Image.image setAccessibilityIdentifier:@"controller-music"];
                [self.ButtonMusic setTitle:@"Music" forState:UIControlStateNormal];
                [self.ButtonMusic setBackgroundImage:[UIImage imageNamed:@"anniuhui.png"] forState:UIControlStateNormal];
            }
        }
            break;
    }
}


@end
