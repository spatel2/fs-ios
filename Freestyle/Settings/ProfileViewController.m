//
//  ProfileViewController.m
//  Freestyle
//
//  Created by BIT on 09/03/15.
//
//

#import "ProfileViewController.h"
#import "Macros.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController{
    
    CGFloat KEYBOARD_ANIMATION_DURATION;
    CGFloat MINIMUM_SCROLL_FRACTION;
    CGFloat MAXIMUM_SCROLL_FRACTION;
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    CGFloat LANDSCAPE_KEYBOARD_HEIGHT;
    CGFloat animatedDistance;
}
@synthesize IBImageViewUserPic;

@synthesize IBTextFieldUserName,IBTextFieldAge,IBTextFieldEmailId,IBTextFieldFacebookInfo,IBTextFieldTwitterInfo;

@synthesize IBButtonEditProfile;



- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    KEYBOARD_ANIMATION_DURATION = 0.3;
    MINIMUM_SCROLL_FRACTION = 0.2;
    MAXIMUM_SCROLL_FRACTION = 0.8;
    
    if (IS_IPAD) {
        
        PORTRAIT_KEYBOARD_HEIGHT = 266;
        LANDSCAPE_KEYBOARD_HEIGHT = 352;
        
    }else{
        
        PORTRAIT_KEYBOARD_HEIGHT = 216;
        LANDSCAPE_KEYBOARD_HEIGHT = 162;
    }

    IBButtonEditProfile.layer.cornerRadius = 2.0;
    
    
    IBTextFieldUserName.delegate = self;
    IBTextFieldAge.delegate = self;
    IBTextFieldEmailId.delegate = self;
    IBTextFieldFacebookInfo.delegate = self;
    IBTextFieldTwitterInfo.delegate = self;
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    IBImageViewUserPic.layer.borderWidth = 2;
    IBImageViewUserPic.layer.borderColor = [UIColor whiteColor].CGColor;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    if (viewFrame.origin.y < 0) {
        
        viewFrame.origin.y += animatedDistance;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:IBTextFieldUserName]) {
        
        [IBTextFieldUserName resignFirstResponder];
        [IBTextFieldAge becomeFirstResponder];
    
    }
    else if ([textField isEqual:IBTextFieldAge]) {
        
        [IBTextFieldAge resignFirstResponder];
        [IBTextFieldEmailId becomeFirstResponder];
        
    }else if ([textField isEqual:IBTextFieldEmailId]) {
    
        [IBTextFieldEmailId resignFirstResponder];
        [IBTextFieldFacebookInfo becomeFirstResponder];
    
    }else if ([textField isEqual:IBTextFieldFacebookInfo]) {
        
        [IBTextFieldFacebookInfo resignFirstResponder];
        [IBTextFieldTwitterInfo becomeFirstResponder];
        
    }else if ([textField isEqual:IBTextFieldTwitterInfo]) {
        
        [textField resignFirstResponder];
        
    }


    
    return YES;
}

#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)genderButtonTouchUpInsideAction:(UIButton *)sender{
 
    if (sender.selected == false) {
        
        [sender setTitle:@"FEMALE" forState:UIControlStateNormal];
        sender.selected = true;
        
    }else if(sender.selected == true){
        
        [sender setTitle:@"MALE" forState:UIControlStateNormal];
        sender.selected = false;
    
    }
    
}

#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{

    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
