
#import <UIKit/UIKit.h>
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "ProximityTag.h"
#import "ConnectViewController.h"

#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "WatchViewController.h"

// serching information
#import "FYIInfoViewController.h"
#import "ProximityTagStorage.h"
#import "ConnectionManager.h"

//get information
#import "FMDBServers.h"
@interface ConnectViewController : UIViewController<ProximityTagDelegate,ConnectionManagerDelegate,UIActionSheetDelegate,UIApplicationDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CommandFunction>

{
    
    int useforpaizhao ;
    BOOL  kaiguan;
    ConnectViewController *coonectObj;
    WatchViewController *controlObj;
    
    
    
    
    
    int setflag;
    int cameraflag;

    
    IBOutlet UIView *viewSearchController;
    IBOutlet UIView *findsyncview;
    IBOutlet UIView *mainview;
    
    IBOutlet UIButton *btn_avtive;
    
    NSString *source;

}

@property (nonatomic, retain)NSString *source;
@property (nonatomic, retain) IBOutlet UIButton *btnBack;


@property(nonatomic) IBOutlet UIImageView * connImageview;



@property(nonatomic, retain) IBOutlet UIButton *btn_avtive;



@property(nonatomic, retain)UIView *viewSearchController;
@property(nonatomic, retain)UIView *findsyncview;


@property(nonatomic, retain)UIView *mainview;

- (IBAction)SynTime:(id)sender;


//search view files
@property (nonatomic) IBOutlet UITableView  *  deviceTabview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityViewfind;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityViewsync;

- (IBAction)SearchTags:(id)sender;


//info view
@property(nonatomic) ProximityTag * proximityTag;
@property(nonatomic,strong)UIImagePickerController* pickerController;


@property(nonatomic,retain)IBOutlet UIScrollView *scroll;

@property(nonatomic)  IBOutlet UIImageView * iamgeView;

@property(nonatomic) UILabel * nameLable;
@property(nonatomic) UILabel * rssiLable;

@property (weak, nonatomic) IBOutlet UILabel *Connect;



@property (retain, nonatomic) IBOutlet UILabel *datesys;
@property (retain, nonatomic) IBOutlet UILabel *time24;
@property (retain, nonatomic) IBOutlet UILabel *time12;





-(IBAction)clickConnect:(id)sender;
-(IBAction)clickForget:(id)sender;

//find view
-(IBAction)locatewatchclick:(id)sender;

-(void)usingPicMa;



@property NSTimer *timer1;


@property(nonatomic,retain)IBOutlet UILabel *songname;
@property(nonatomic,retain)IBOutlet UILabel *songdetail1;
@property(nonatomic,retain)IBOutlet UIImageView *songgimg;
//seek control
@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;
@property BOOL isPaused;
@property BOOL scrubbing;
@property (nonatomic,strong) MPMusicPlayerController *mpc;






@end
