

#import <UIKit/UIKit.h>
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
@interface HelpViewController : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForScrollViewContentWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenWatchFaqAndAppFaq;
@property (strong, nonatomic) IBOutlet UIView *IBViewInfo;

@property (strong, nonatomic) IBOutlet UIWebView *IBWebViewHelp;

@end
