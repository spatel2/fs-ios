//
//  ActivityInfo.h
//  Freestyle
//
//  
//
//

#import <Foundation/Foundation.h>

@interface ActivityInfo : NSObject

@property (nonatomic) int aID;
@property (nonatomic, retain)NSString *ActName;
@property (nonatomic, retain)NSString *ActDate;
@property (nonatomic, retain)NSString *ActTime;
@property (nonatomic, retain)NSString *Duration;
@property (nonatomic, retain)NSString *Distance;
@property (nonatomic, retain)NSString *Direction;
@property (nonatomic, retain)NSString *Altitude;
@property (nonatomic, retain)NSString *AvgSpeed;
@property (nonatomic, retain)NSString *MaxSpeed;
@property (nonatomic, retain)NSString *MinSpeed;
@property (nonatomic, retain)NSString *HeartRate;
@property (nonatomic, retain)NSString *Calories;
@property (nonatomic, retain)NSString *StartLat;
@property (nonatomic, retain)NSString *StartLong;
@property (nonatomic, retain)NSString *EndLat;
@property (nonatomic, retain)NSString *EndLong;
@property (nonatomic, retain)NSString *Location;
@property (nonatomic, retain)NSString *stringOfLocations;

@end
