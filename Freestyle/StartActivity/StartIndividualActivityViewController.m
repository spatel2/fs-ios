#import "StartIndividualActivityViewController.h"

@interface StartIndividualActivityViewController ()<GMSMapViewDelegate>
{
    int avgspeed;
    int hartrate;
    float distance;
    
}


@property(nonatomic,strong)NSMutableData *myMutableData;
@property(nonatomic,strong)NSDate *SendSportInfoDate;
@property(nonatomic,strong)NSTimer *timer;

@end

@implementation StartIndividualActivityViewController{
    NSString* sUnitForDistance;
    NSString* sImagetitle;
}

@synthesize strActTitle,mapView_;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.IBLabelActTitle.text = self.strActTitle;
    
    self.myMutableData=[NSMutableData data];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    if (appDelegate.gActStartActivityName == nil) {
        NSString* stUnit;
        float fMultiplayingFactor=0;
        if ([sUnitForDistance isEqualToString:@"KM"]) {
            stUnit = @"KM/H";
            fMultiplayingFactor = 1;
            
        }else if ([sUnitForDistance isEqualToString:@"MI"]){
            stUnit = @"MI/H";
            fMultiplayingFactor = 0.62137;
        }
        
        //set font
        NSMutableAttributedString *strAvgSpeed = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"00 %@",stUnit]];
        [strAvgSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                            range:NSMakeRange(0, 2)];
        [strAvgSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                            range:NSMakeRange(3, 4)];
        self.IBLabelAvgSpeed.attributedText = strAvgSpeed;
        
        
        NSMutableAttributedString *strMaxSpeed = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"00 %@",stUnit]];
        [strMaxSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                            range:NSMakeRange(0, 2)];
        [strMaxSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                            range:NSMakeRange(3, 4)];
        self.IBLabelMaxSpeed.attributedText = strMaxSpeed;
        
        NSMutableAttributedString *strMinSpeed = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"00 %@",stUnit]];
        [strMinSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                            range:NSMakeRange(0, 2)];
        [strMinSpeed addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                            range:NSMakeRange(3, 4)];
        self.IBLabelMinSpeed.attributedText = strMinSpeed;
        
        NSMutableAttributedString *strHeartRate = [[NSMutableAttributedString alloc] initWithString:@"--"];
        [strHeartRate addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                             range:NSMakeRange(0, 2)];
        self.IBLabelHeartRate.attributedText = strHeartRate;
    }
}
- (void)viewDidLayoutSubviews
{
    self.IBScrollWidthConstraints.constant = self.view.bounds.size.width;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:true];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //self.IBLabelHeartRate.text = [NSString stringWithFormat:@"%d",appDelegate.gActHeartRate];
            
            self.IBButtonStart.selected = appDelegate.gActButtonStartSelectionState;
            self.IBButtonPause.selected = appDelegate.gActButtonPauseSelectionState;
            
            if (_IBButtonPause.selected == true) {
                [self.IBButtonPause setTitle:@"RESUME" forState:UIControlStateNormal];
                _IBButtonPause.backgroundColor=[UIColor darkGrayColor];
                [_IBButtonPause setImage:[UIImage imageNamed:@"img_activity_resume"] forState:UIControlStateNormal];
            }else{
                [self.IBButtonPause setTitle:@"PAUSE" forState:UIControlStateNormal];
                [_IBButtonPause setImage:[UIImage imageNamed:@"img_activity_pause"] forState:UIControlStateNormal];
            }
            
            if ([APP_DELEGATE.gActUnit isEqualToString:@"IMPERIAL"]) {
                sUnitForDistance = @"MI";
            }else if([APP_DELEGATE.gActUnit isEqualToString:@"METRIC"]){
                sUnitForDistance = @"KM";
            }
            self.IBLabelDistanceUnit.text = sUnitForDistance;
            
            [self.IBButtonTimer setTitle:[NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f",appDelegate.gActHours,appDelegate.gActMinutes,appDelegate.gActSeconds] forState:UIControlStateNormal];
            
            self.IBLabelCalorie.text = [NSString stringWithFormat:@"%.0f",appDelegate.gActCalculateCalories];
            if (appDelegate.myLocation.course > 0) {
                self.IBLabelDirection.text = [self getDirection:appDelegate.myLocation.course];
            }
            self.IBLabelAltitude.text =  [NSString stringWithFormat:@"%.0f",appDelegate.myLocation.altitude];
            //    self.IBLabelDistance.text = [NSString stringWithFormat:@"%.3f",(appDelegate.gActDistance / 1000)];
            self.IBLabelDistance.text = [NSString stringWithFormat:@"%.2f",(appDelegate.gActDistance / 1000)];
            hartrate=0;//110;//[self.IBLabelHeartRate.text intValue];
            
            //-----------------
            NSString* stUnit;
            float fMultiplayingFactor=0;
            if ([sUnitForDistance isEqualToString:@"KM"]) {
                stUnit = @"KM/H";
                fMultiplayingFactor = 1;
                
            }else if ([sUnitForDistance isEqualToString:@"MI"]){
                stUnit = @"MI/H";
                fMultiplayingFactor = 0.62137;
            }
            
            self.IBLabelMinSpeed.text = [[NSString stringWithFormat:@"%.1f %@",appDelegate.gActMinSpeed * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
            self.IBLabelMaxSpeed.text = [[NSString stringWithFormat:@"%.1f %@",appDelegate.gActMaxSpeed * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
            if (appDelegate.myLocation.speed > 0 && appDelegate.gActButtonStartSelectionState == true) {
                
                self.IBLabelAvgSpeed.text = [[NSString stringWithFormat:@"%.0f %@", appDelegate.myLocation.speed * 3.6 * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                avgspeed=[self.IBLabelAvgSpeed.text floatValue];
            }else{
                
                self.IBLabelAvgSpeed.text = [NSString stringWithFormat:@"00 %@",stUnit];
            }
            
            NSMutableAttributedString *strAvgSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelAvgSpeed.text];
            [strAvgSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                range:NSMakeRange(0, strAvgSpeed.length-4)];
            [strAvgSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                range:NSMakeRange(strAvgSpeed.length-4, 4)];
            self.IBLabelAvgSpeed.attributedText = strAvgSpeed;
            
            NSMutableAttributedString *strMaxSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelMaxSpeed.text];
            [strMaxSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                range:NSMakeRange(0, strMaxSpeed.length-4)];
            [strMaxSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                range:NSMakeRange(strMaxSpeed.length-4, 4)];
            self.IBLabelMaxSpeed.attributedText = strMaxSpeed;
            
            NSMutableAttributedString *strMinSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelMinSpeed.text];
            [strMinSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                range:NSMakeRange(0, strMinSpeed.length-4)];
            [strMinSpeed addAttribute:NSFontAttributeName
                                value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                range:NSMakeRange(strMinSpeed.length-4, 4)];
            self.IBLabelMinSpeed.attributedText = strMinSpeed;
            //-----------------
            
        });
    });
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timerTickUpdateData:) name:@"TimeElapsed" object:nil];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [[UIApplication sharedApplication] setIdleTimerDisabled:false];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    if (appDelegate.gActAge == 0 || appDelegate.gActWeight == 0){
        
        [Helper displayAlertView:@"" message:@"Please set profile information in Settings > General > Activity Profile to populate proper calories."];
    }
    
    [mapView_ setHidden:true];
    // Create a GMSCameraPosition that tells the map to display the
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appDelegate.myLocation.coordinate.latitude
                                                            longitude:appDelegate.myLocation.coordinate.longitude                                                                zoom:17];
    mapView_.camera = camera;
    mapView_.mapType = kGMSTypeNormal;
    mapView_.delegate = self;
    //self.view = mapView_;
    appDelegate.gActMapViewZoomLevel=17;
    
    [self.view bringSubviewToFront:self.IBViewTitle];
    [self.view bringSubviewToFront:self.IBScrollView];
    
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
 #pragma mark - Private Method
 - (float)calculateCalories{
 
 float caloriesBurned = 0;
 
 if ([sGender isEqualToString:@"MALE"]) {
 
 caloriesBurned = (((age * 0.2017) - (weight * 0.09036) + (heartRate * 0.6309) - 55.0969 ) * ((_ticks * 10) / 3600)) / 4.184 ;
 
 }else if ([sGender isEqualToString:@"FEMALE"]){
 
 caloriesBurned = (((age * 0.074) - (weight * 0.05741) + (heartRate * 0.4472) - 20.4022 ) * ((_ticks * 10) / 3600)) / 4.184;
 }
 Men use the following formula:
 Calories Burned = [(Age x 0.2017) — (Weight x 0.09036) + (Heart Rate x 0.6309) — 55.0969] x Time / 4.184.
 
 Women use the following formula:
 Calories Burned = [(Age x 0.074) — (Weight x 0.05741) + (Heart Rate x 0.4472) — 20.4022] x Time / 4.184.
 return caloriesBurned;
 }
 */
#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    if (mapView_.hidden == true) {
        [self.navigationController popViewControllerAnimated:true];
        
    }else if (mapView_.hidden == false){
        mapView_.hidden = true;
        [self.view bringSubviewToFront:self.IBScrollView];
        [self.view bringSubviewToFront:self.IBViewTitle];
        [appDelegate.gActLiveMutablePathList removeAllCoordinates];
        appDelegate.gActLiveMutablePathList = nil;
        appDelegate.gActCurrentMapView = nil;
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appDelegate.gActStartLocation.coordinate.latitude
                                                                longitude:appDelegate.gActStartLocation.coordinate.longitude                                                                 zoom:16];
        [mapView_ setCamera:camera];
        
        
    }
    
}

#pragma mark - Navigation Button Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (IBAction)startAndPauseTimerButtonTouchUpInsideAction:(UIButton *)sender{
    
    if ([sender.titleLabel.text isEqualToString:@"START"] || [sender.titleLabel.text isEqualToString:@"RESUME"]) {
        //RESUME
        
        if ([sender.titleLabel.text isEqualToString:@"START"] && _IBButtonPause.selected == false && _IBButtonStart.selected == false){
            
            _IBButtonStart.selected = true;
            _IBButtonPause.selected = false;
            
            
            [appDelegate startTimerWithlable];
            
            appDelegate.gActButtonStartSelectionState = true;
            appDelegate.gActButtonPauseSelectionState = false;
            
            appDelegate.arrayOfLocations = [[NSMutableArray alloc] init];
            
            appDelegate.myLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
            
            [appDelegate.myLocationManager startUpdatingLocation];
            
            
            appDelegate.gActStartActivityName = self.strActTitle;
            
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //                [appDelegate startTimer];
            //            });
            
            
            
            appDelegate.gActStartLocation = appDelegate.myLocation;
            appDelegate.gActTempLocation = appDelegate.myLocation;
            appDelegate.gActMinSpeed = appDelegate.myLocation.speed;
            appDelegate.gActMaxSpeed = appDelegate.myLocation.speed;
            
            
            
        }else if([sender.titleLabel.text isEqualToString:@"RESUME"] && _IBButtonPause.selected == true && _IBButtonStart.selected == false) {
            
            _IBButtonStart.selected = true;
            _IBButtonPause.selected = false;
            
            appDelegate.gActButtonStartSelectionState = true;
            appDelegate.gActButtonPauseSelectionState = false;
            
            _IBButtonPause.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:197.0f/255.0f blue:21.0f/255.0f alpha:1];
            
            [_IBButtonPause setTitle:@"PAUSE" forState:UIControlStateNormal];
            [_IBButtonPause setImage:[UIImage imageNamed:@"img_activity_pause"] forState:UIControlStateNormal];
            
            [appDelegate.myLocationManager startUpdatingLocation];
            
            
            appDelegate.gActStartActivityName = self.strActTitle;
            //
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //                [appDelegate startTimer];
            //            });
            [appDelegate startTimerLable];
            
            
            appDelegate.gActTempLocation = appDelegate.myLocation;
            appDelegate.gActMinSpeed = appDelegate.myLocation.speed;
            appDelegate.gActMaxSpeed = appDelegate.myLocation.speed;
            
        }
        
        
        
        /*
         startLocation = appDelegate.myLocation;
         tempLocation = appDelegate.myLocation;
         minSpeed = appDelegate.myLocation.speed;
         maxSpeed = appDelegate.myLocation.speed;
         */
        
        
    }else if (_IBButtonStart.selected == true && _IBButtonPause.selected == false){
        _IBButtonStart.selected = false;
        _IBButtonPause.selected = true;
        
        appDelegate.gActButtonStartSelectionState = false;
        appDelegate.gActButtonPauseSelectionState = true;
        _IBButtonPause.backgroundColor=[UIColor darkGrayColor];
        [_IBButtonPause setTitle:@"RESUME" forState:UIControlStateNormal];
        [_IBButtonPause setImage:[UIImage imageNamed:@"img_activity_resume"] forState:UIControlStateNormal];
        //sender.backgroundColor = [UIColor colorWithRed:76.0f/255.0f green:197.0f/255.0f blue:21.0f/255.0f alpha:1];
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //            [appDelegate.gAct_timer invalidate];
        //        });
        
        [appDelegate pauseTimerLabel];
        
        Byte byte =0x58;//stop Sport Data Sending
        [appDelegate.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
        NSLog(@"First issued instructions 0x58");
        
    }
    //    self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(UPdateUI:) userInfo:nil repeats:YES];
    //    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    
    //    self.timer=[NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(WriteSportValue) userInfo:nil repeats:YES];
    //    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
}
- (IBAction)stopAndResetTimerButtonTouchUpInsideAction:(UIButton *)sender{
    
    if(_IBButtonStart.selected == true){
        UIAlertView* alertView =[[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to stop the activity and save it to the log?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [alertView show];
        if ([appDelegate.appdelegateProximity isConnected]) {
            
            Byte byte[20];
            byte[0]=0x06;//leixing
            byte[1]=0x14;//changdu
            
            
            byte[2]=0x00;//Hour
            byte[3]=0x00;
            byte[4]=0x00;//second
            byte[5]=0x00;//Distance in meters
            byte[6]=0x00;
            byte[7]=0x00;
            byte[8]=0x00;
            byte[9]=0x00;//Calorie consumption units kcal
            byte[10]=0x00;
            byte[11]=0x00;
            byte[12]=0x00;
            
            byte[13]=0x00;//Speedbyte[2];//Speed km/h
            byte[14]=0x00;//Speedbyte[3];
            
            byte[15]=0x00;//Height m
            byte[16]=0x00;
            byte[18]=0x00;//(TempDirection %100);//FXbyte[3];
            byte[17]=0x00;//(TempDirection %100);//FXbyte[2];//   Degree direction;
            
            byte[19]=0x00;//byteFR;//Heart Rate
            
            NSData * data = [NSData dataWithBytes:&byte length:sizeof(byte)];
            [appDelegate.appdelegateProximity  phoneWriteDataToTagBig :data];
        }
        /*
         
         
         //sent watch data null
         Byte byte1[20];
         
         Byte byte[20];
         byte[0]=0x06;//leixing
         byte[1]=0x14;//changdu
         
         
         byte1[2]=00;//Hour
         
         byte1[3]=00;//minut
         
         byte1[4]=00;//second
         
         
         
         
         distance=(appDelegate.gActDistance / 1000);
         NSLog(@"%f",distance);
         //Byte *distbyte=[self FloatToData:(int)([self.IBLabelDistance.text floatValue]*1000.0)];
         byte1[5]=(Byte) 00;//Distance in meters
         byte1[6]=(Byte) 00;
         byte1[7]=(Byte) 00;
         byte1[8]=(Byte) 00;
         
         
         
         // Byte *Calbyte=[self FloatToData:(int)self.IBLabelCalorie*1000];
         
         byte1[9]=(Byte) 00;//Calorie consumption units kcal
         byte1[10]=(Byte)00;
         byte1[11]=(Byte) 00;
         byte1[12]=(Byte) 00;
         
         
         byte1[13]=(Byte)00;//Speedbyte[2];//Speed km/h
         byte1[14]=(Byte)00;//Speedbyte[3];
         
         
         byte1[15]=  00;//Height m
         byte1[16]=  00;
         
         
         byte1[18]=(Byte) 00;//(TempDirection %100);//FXbyte[3];
         byte1[17]=(Byte)00;//(TempDirection %100);//FXbyte[2];//   Degree direction;
         
         
         byte1[19]=(Byte)00;//byteFR;//Heart Rate
         
         
         [appDelegate.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte1 length:sizeof(byte1)]];*/
    }
}

-(void)stopAndSaveActData:(NSString*)info{
    
    //[appDelegate.gAct_timer invalidate];
    
    
    [appDelegate stopTimerLabel];
    
    
    appDelegate.gActEndLocation = appDelegate.myLocation;
    if (self.IBButtonStart.selected == true) {
        self.IBButtonStart.selected = false;
        appDelegate.gActButtonStartSelectionState = false;
        appDelegate.gActButtonPauseSelectionState = false;
        
        // [self.IBButtonStart setTitle:@"START" forState:UIControlStateNormal];
        //  self.IBButtonStart.backgroundColor = [UIColor colorWithRed:76.0f/255.0f green:197.0f/255.0f blue:21.0f/255.0f alpha:1];
    }
    
    
    if ([info isEqualToString:@"yes"]) {
        
        
        
        UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activityIndicatorView startAnimating];
        
        UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
        viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
        
        activityIndicatorView.center = viewForActivity.center;
        [viewForActivity addSubview:activityIndicatorView];
        [self.view addSubview:viewForActivity];
        [self.navigationController.navigationBar setUserInteractionEnabled:false];
        
        CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
        [geoCoder reverseGeocodeLocation:appDelegate.gActEndLocation completionHandler:^(NSArray *placemarks, NSError *error)
         {
             CLPlacemark *placemark= [placemarks firstObject];
             NSString* sLocation = [[NSString  stringWithFormat:@"%@, %@",[placemark locality],[placemark administrativeArea]] uppercaseString];
             
             if ([placemark locality] == NULL ||[placemark administrativeArea] == NULL || sLocation == nil) {
                 sLocation = @"";
             }
             
             DBHelper* dbHelper = [DBHelper getSharedInstance];
             
             NSString* sDate = [self getCurrentDate];
             NSString* sTime = [self getCurrentTime];
             
             NSMutableString* loactionString=[[NSMutableString alloc] init];
             
             
             
             for(CLLocation* location  in appDelegate.arrayOfLocations){
                 
                 [loactionString appendString:[NSString stringWithFormat:@"%@,%@|",
                                               [NSString stringWithFormat:@"%f",location.coordinate.latitude]
                                               ,[NSString stringWithFormat:@"%f",location.coordinate.longitude]]];
                 
             }
             
             appDelegate.gActLastSaveActivityID = [dbHelper insertActivityLog:self.strActTitle ActDate:sDate ActTime:sTime Duration:[self.IBLableTimer text] Distance: [NSString stringWithFormat:@"%@ %@",self.IBLabelDistance.text,sUnitForDistance] Direction:self.IBLabelDirection.text Altitude:self.IBLabelAltitude.text AvgSpeed:self.IBLabelAvgSpeed.text MaxSpeed:self.IBLabelMaxSpeed.text MinSpeed:self.IBLabelMinSpeed.text HeartRate:self.IBLabelHeartRate.text Calories:self.IBLabelCalorie.text StartLat:[NSString stringWithFormat:@"%f",appDelegate.gActStartLocation.coordinate.latitude] StartLong:[NSString stringWithFormat:@"%f",appDelegate.gActStartLocation.coordinate.longitude]  EndLat:[NSString stringWithFormat:@"%f",appDelegate.gActEndLocation.coordinate.latitude] EndLong:[NSString stringWithFormat:@"%f",appDelegate.gActEndLocation.coordinate.longitude] Location:sLocation ArrayOfLocations:loactionString];
             
             //[self getMapViewScreeshort:[NSString stringWithFormat:@"%@_%@ %@.jpg",self.strActTitle,[Helper getDateFromString:sDate],sTime]];
             
             
             [viewForActivity removeFromSuperview];
             
             
             
             [appDelegate resetTimerLabel];
             
             
             appDelegate.gActStartActivityName = nil;
             
             appDelegate.gActHours=0;
             appDelegate.gActMinutes=0;
             appDelegate.gActSeconds=0;
             appDelegate.gActMinSpeed=0;
             appDelegate.gActMaxSpeed=0;
             appDelegate.gActCalculateCalories=0;
             //[self getDirection:appDelegate.myLocation.course];
             //appDelegate.myLocation.altitude;
             
             appDelegate.gActDistance = 0;
             
             appDelegate.gAct_timer = nil;
             appDelegate.gAct_timer = 0;
             appDelegate.gAct_ticks = 0.0;
             
             [self.navigationController popViewControllerAnimated:true];
             /*
              UIAlertView* alertView =[[UIAlertView alloc] initWithTitle:@"" message:@"Your activity is saved. You can view it from activity log section." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
              [alertView show];
              */
             
         }];
        
    }
    else if ([info isEqualToString:@"no"]) {
        
        [appDelegate resetTimerLabel];
        
        appDelegate.gActStartActivityName = nil;
        
        appDelegate.gActHours=0;
        appDelegate.gActMinutes=0;
        appDelegate.gActSeconds=0;
        
        appDelegate.gActCalculateCalories=0;
        //[self getDirection:appDelegate.myLocation.course];
        //appDelegate.myLocation.altitude;
        
        appDelegate.gActDistance = 0;
        
        appDelegate.gAct_timer = nil;
        appDelegate.gAct_timer = 0;
        appDelegate.gAct_ticks = 0.0;
        appDelegate.gActMinSpeed=0;
        appDelegate.gActMaxSpeed=0;
        
        [self.navigationController popViewControllerAnimated:true];
    }
    
    
}


-(void)getMapViewScreeshort:(NSString*)title{
    
    sImagetitle = title;
    
    [mapView_ clear];
    
    GMSMarker *startMarker = [[GMSMarker alloc] init];
    startMarker.position = CLLocationCoordinate2DMake(appDelegate.gActStartLocation.coordinate.latitude, appDelegate.gActStartLocation.coordinate.longitude);
    startMarker.title = self.strActTitle;//@"START";
    //startMarker.snippet = self.strActTitle;
    startMarker.map = mapView_;
    
    [self livePolyLine:@"yes"];
    [mapView_ setHidden:false];
    
    
    if(sImagetitle != nil){
        CGRect rect = [mapView_ bounds];
        UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
        CGContextRef context = UIGraphicsGetCurrentContext();
        [mapView_.layer renderInContext:context];
        UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",sImagetitle]];
        [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
    }
    /*
     GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appDelegate.gActStartLocation.coordinate.latitude
     longitude:appDelegate.gActStartLocation.coordinate.longitude                                                                 zoom:16];
     [mapView_ setCamera:camera];
     */
    
    
    
    /*
     CGRect rect = [mapView_ bounds];
     UIGraphicsBeginImageContextWithOptions(rect.size,YES,0.0f);
     CGContextRef context = UIGraphicsGetCurrentContext();
     [mapView_.layer renderInContext:context];
     UIImage *capturedImage = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     NSString  *imagePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",sImagetitle]];
     [UIImageJPEGRepresentation(capturedImage, 0.95) writeToFile:imagePath atomically:YES];
     */
    
}

//- (void)mapView:(GMSMapView *)mapView
//idleAtCameraPosition:(GMSCameraPosition *)position{
//
//    [self.navigationController.navigationBar setUserInteractionEnabled:true];
//
//}

/*
 NSTimer *_timer;
 _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
 
 */
- (void)timerTickUpdateData:(id)timer
{
    // Timers are not guaranteed to tick at the nominal rate specified, so this isn't technically accurate.
    // However, this is just an example to demonstrate how to stop some ongoing activity, so we can live with that inaccuracy.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //[self.IBButtonTimer setTitle:[NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f",appDelegate.gActHours,appDelegate.gActMinutes,appDelegate.gActSeconds] forState:UIControlStateNormal];
            _IBLableTimer.text = appDelegate.lableTimer.text;
            
            if (appDelegate.myLocation.course > 0) {
                
                self.IBLabelDirection.text = [self getDirection:appDelegate.myLocation.course];
            }
            self.IBLabelAltitude.text =  [NSString stringWithFormat:@"%.0f",appDelegate.myLocation.altitude];
            
            //    self.IBLabelDistance.text = [NSString stringWithFormat:@"%.3f",(appDelegate.gActDistance / 1000)];
            
            
            NSString* stUnit;
            float fMultiplayingFactor=0;
            if ([sUnitForDistance isEqualToString:@"KM"]) {
                stUnit = @"KM/H";
                fMultiplayingFactor = 1;
                
            }else if ([sUnitForDistance isEqualToString:@"MI"]){
                stUnit = @"MI/H";
                fMultiplayingFactor = 0.62137;
            }
            self.IBLabelCalorie.text = [NSString stringWithFormat:@"%.0f",appDelegate.gActCalculateCalories];
            
//            NSArray* timeArray=[_IBLableTimer.text componentsSeparatedByString:@":"];
//            
//            int second =[[timeArray objectAtIndex:2] intValue];
//            
//            if (second % 10 == 0 && second >= 10) {
            
            
          //  if ((int)appDelegate.gActSeconds % 10 == 0 && appDelegate.gActSeconds >= 10) {
            
            
            
                self.IBLabelDistance.text = [NSString stringWithFormat:@"%.2f",(appDelegate.gActDistance / 1000)];
                distance=[self.IBLabelDistance.text floatValue];
                
                if (appDelegate.gActButtonStartSelectionState == true && appDelegate.myLocation.speed > 0){
                self.IBLabelMinSpeed.text = [[NSString stringWithFormat:@"%.1f %@",appDelegate.gActMinSpeed * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                self.IBLabelMaxSpeed.text = [[NSString stringWithFormat:@"%.1f %@",appDelegate.gActMaxSpeed * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
            
                    self.IBLabelAvgSpeed.text = [[NSString stringWithFormat:@"%.0f %@", appDelegate.myLocation.speed * 3.6 * fMultiplayingFactor,stUnit] stringByReplacingOccurrencesOfString:@"-" withString:@""];
                }
                avgspeed=[self.IBLabelAvgSpeed.text floatValue];
                
                NSMutableAttributedString *strAvgSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelAvgSpeed.text];
                [strAvgSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                    range:NSMakeRange(0, strAvgSpeed.length-4)];
                [strAvgSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                    range:NSMakeRange(strAvgSpeed.length-4, 4)];
                self.IBLabelAvgSpeed.attributedText = strAvgSpeed;
                
                NSMutableAttributedString *strMaxSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelMaxSpeed.text];
                [strMaxSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                    range:NSMakeRange(0, strMaxSpeed.length-4)];
                [strMaxSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                    range:NSMakeRange(strMaxSpeed.length-4, 4)];
                self.IBLabelMaxSpeed.attributedText = strMaxSpeed;
                
                NSMutableAttributedString *strMinSpeed = [[NSMutableAttributedString alloc] initWithString:self.IBLabelMinSpeed.text];
                [strMinSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:20.0f]
                                    range:NSMakeRange(0, strMinSpeed.length-4)];
                [strMinSpeed addAttribute:NSFontAttributeName
                                    value:[UIFont fontWithName:@"SinkinSans-400Regular" size:10.0f]
                                    range:NSMakeRange(strMinSpeed.length-4, 4)];
                self.IBLabelMinSpeed.attributedText = strMinSpeed;
                  // NSLog(@"%@",[self class]);
           // }
        });
    });
}

/*
 -(NSString*)getDirection:(double)value{
 int iDirection = value/27;
 NSString *Direction=@"";
 switch (iDirection)
 {
 case 0: Direction =@"N"; break;
 case 1: Direction =@"NNE"; break;
 case 2: Direction =@"NE"; break;
 case 3: Direction =@"ENE"; break;
 case 4: Direction =@"E"; break;
 case 5: Direction =@"ESE"; break;
 case 6: Direction =@"SE"; break;
 case 7: Direction =@"SSE"; break;
 case 8: Direction =@"S"; break;
 case 9: Direction =@"SSW"; break;
 case 10: Direction =@"SW"; break;
 case 11: Direction =@"WSW";break;
 case 12: Direction =@"W"; break;
 case 13: Direction =@"WNW";break;
 case 14: Direction =@"NW"; break;
 case 15: Direction =@"NNW";break;
 default: Direction =@"N/A";break;
 }
 return Direction;
 }*/

- (IBAction)imageViewMapTapGestureAction:(UITapGestureRecognizer *)sender {
    
    //    // Creates a marker in the center of the map.
    
    if(appDelegate.gActButtonStartSelectionState == true){
        [mapView_ clear];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:appDelegate.gActStartLocation.coordinate.latitude
                                                                longitude:appDelegate.gActStartLocation.coordinate.longitude                                                                 zoom:appDelegate.gActMapViewZoomLevel];
        [mapView_ setCamera:camera];
        
        GMSMarker *startMarker = [[GMSMarker alloc] init];
        startMarker.position = CLLocationCoordinate2DMake(appDelegate.gActStartLocation.coordinate.latitude, appDelegate.gActStartLocation.coordinate.longitude);
        startMarker.title = self.strActTitle;//@"START";
        //startMarker.snippet = self.strActTitle;
        startMarker.map = mapView_;
        
        
        // [appDelegate.myLocationManager stopUpdatingLocation];
        
        /*
         GMSMutablePath *path = [GMSMutablePath path];
         [path addCoordinate:CLLocationCoordinate2DMake(startLocation.coordinate.latitude, startLocation.coordinate.longitude)];
         
         for (CLLocation* location in appDelegate.arrayOfLocations) {
         [path addCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
         }
         
         GMSPolyline *polyLine = [GMSPolyline polylineWithPath:path];
         polyLine.map = mapView_;
         
         GMSMarker *currentMarker = [[GMSMarker alloc] init];
         currentMarker.position = CLLocationCoordinate2DMake(startLocation.coordinate.latitude, startLocation.coordinate.longitude);
         currentMarker.title = @"CURRENT";
         currentMarker.snippet = self.strActTitle;
         currentMarker.map = mapView_;*/
        
        [mapView_ setHidden:false];
        [self.view bringSubviewToFront:mapView_];
        [self livePolyLine:@"yes"];
    }
}
-(void)livePolyLine:(id)sender{
    GMSMutablePath *path = [GMSMutablePath path];
    if ([sender isKindOfClass:[NSString class]]) {
        [path addCoordinate:CLLocationCoordinate2DMake(appDelegate.gActStartLocation.coordinate.latitude, appDelegate.gActStartLocation.coordinate.longitude)];
    }
    
    for (CLLocation* location in appDelegate.arrayOfLocations) {
        [path addCoordinate:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
    }
    
    GMSPolyline *poly = [[GMSPolyline alloc] init];
    poly.path = path;
    poly.strokeWidth = 2;
    poly.geodesic = YES;
    poly.map = mapView_;
    
    
    appDelegate.gActLiveMutablePathList = [GMSMutablePath path];
    CLLocation* lastLocation = [appDelegate.arrayOfLocations lastObject];
    [appDelegate.gActLiveMutablePathList addCoordinate:lastLocation.coordinate];
    appDelegate.gActCurrentMapView = mapView_;
    
}
#pragma mark - Private Method
-(NSString*)getDirection:(CLLocationDirection)Direction{
    
    
    NSString* sFinalDirection;
    if(Direction <= 190 &&  Direction >= 170){
        sFinalDirection = @"S";
    }else if (Direction <= 10){
        sFinalDirection = @"N";
    }else if (Direction >= 350){
        sFinalDirection = @"N";
    }else if (Direction <= 100 && Direction >=80){
        sFinalDirection = @"E";
    }else if (Direction<= 280  && Direction >= 260)
    {
        sFinalDirection = @"W";
    }else if (Direction<= 190 && Direction >= 170){
        sFinalDirection = @"S";
    }else if (Direction > 280 && Direction <= 310){
        sFinalDirection = @"WNW";
    }else if (Direction > 310 && Direction <= 330){
        sFinalDirection = @"NW";
    }else if (Direction > 330 && Direction < 350){
        sFinalDirection = @"NNW";
    }else if (Direction > 10 && Direction <= 30){
        sFinalDirection = @"NNE";
    }else if (Direction > 30 && Direction <= 50){
        sFinalDirection = @"NE";
    }else if (Direction > 50 && Direction < 80){
        sFinalDirection = @"ENE";
    }else if (Direction > 100 && Direction <= 130){
        sFinalDirection = @"ESE";
    }else if (Direction > 130 && Direction <= 150){
        sFinalDirection = @"SE";
    }else if (Direction > 150 && Direction < 170){
        sFinalDirection = @"SSE";
    }else if (Direction > 190 && Direction <= 210){
        sFinalDirection = @"SSW";
    }else if (Direction > 210 && Direction <= 230){
        sFinalDirection = @"SW";
    }else if (Direction > 230 && Direction < 260){
        sFinalDirection = @"WSW";
    }
    return sFinalDirection;
    
}
-(NSString* )getCurrentDate
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:today];
    dateFormat = nil;
    //
    return dateString;
}
-(NSString* )getCurrentTime
{
    /*
     NSDate* sourceDate = [NSDate date];
     
     NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
     NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
     
     NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
     NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
     NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
     
     NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
     
     */
    
    NSDate *today = [NSDate date];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"hh:mm:ss"];
    NSString *timeString = [dateFormat stringFromDate:today];
    dateFormat = nil;
    // HH:MM:SS
    return timeString;
}
#pragma mark - Map View Delegate
- (void)mapView:(GMSMapView *)mapView
didChangeCameraPosition:(GMSCameraPosition *)position{
    
    appDelegate.gActMapViewZoomLevel = position.zoom;
}

#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            //[self getMapViewScreeshort];
            
            //[appDelegate performSelectorInBackground:@selector(getActivityEndLocationDetails) withObject:nil];
            [self stopAndSaveActData:@"no"];
            break;
        case 1:
            [self stopAndSaveActData:@"yes"];
            break;
            
        default:
            break;
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


//sent data to watch

#pragma mark - 写入数据
-(Byte *)FloatToData:(NSInteger )floatvalue{
    NSString *string=[NSString stringWithFormat:@"%d",floatvalue];
    NSInteger zhuanInt=0;
    
    for (int i=1; i<string.length; i++) {
        int sixty=(int)pow(16,(double)i);
        int tenty=(int)pow(10, (double)i);
        zhuanInt=zhuanInt+(floatvalue/(tenty)%10)*sixty;
        
    }
    zhuanInt=zhuanInt+floatvalue%10;
    
    int32_t unswapped = zhuanInt;
    int32_t swapped = CFSwapInt32HostToBig(unswapped);
    char* a = (char*) &swapped;
    //NSMutableData 清空
    [self.myMutableData resetBytesInRange:NSMakeRange(0, [self.myMutableData length])];
    [self.myMutableData setLength:0];
    [self.myMutableData appendBytes:a length:sizeof(int32_t)];
    Byte *testbyte=(Byte *)[self.myMutableData bytes];
    
    return testbyte;
    
}

//-(void)UPdateUI:(NSTimer *)timer{
//
//
//    [self WriteSportValue];
//}
//

-(void)WriteSportValue{
    
    
    
    //    if (!self.SendSportInfoDate) {
    //        //The last time to send data nil, the first transmission of data , starting 0x57
    //        self.SendSportInfoDate=[NSDate date];
    //
    //        if ([[[ProximityTagStorage sharedInstance] tags]count] >0) {
    //            AppDelegate * app= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //            if ([app.appdelegateProximity isConnected]) {
    //
    //                Byte byte =0x57;
    //                [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
    //                NSLog(@"First issued instructions 0x57");
    //
    //            }
    //        }
    //
    //
    //    }
    //    NSTimeInterval time=[self.SendSportInfoDate timeIntervalSinceNow];
    //
    //    if (fabs(time)<0.0) {
    //        return ;
    //    }
    //    self.SendSportInfoDate=[NSDate date];
    
    
    
    
    
    
    AppDelegate * app= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //ProximityTag * tag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:0];
    if ([app.appdelegateProximity isConnected]) {
        
        //totaltime=self.IBButtonTimer;
        //todo  Send sports information
        Byte byte[20];
        byte[0]=0x06;//leixing
        byte[1]=0x14;//changdu
        
        Byte *hourbyte=[self FloatToData:appDelegate.gActHours];
        byte[2]=hourbyte[3];//Hour
        Byte *minutebyte=[self FloatToData:appDelegate.gActMinutes];
        byte[3]=minutebyte[3];//minut
        Byte *secondbyte=[self FloatToData:appDelegate.gActSeconds];
        byte[4]=secondbyte[3];//second
        
        
        NSLog(@"%f %f %f",appDelegate.gActHours, appDelegate.gActMinutes,appDelegate.gActSeconds);
        
        // Byte *Julibyte=[self FloatToData:(int)([self.IBLabelDistance.text integerValue]*1000.0)];
        // Byte *Julibyte=[self FloatToData:(int)([self.IBLabelDistance.text floatValue]*1000.0)];
        // Byte *Julibyte=(Byte)[[self.IBLabelDistance.text floatValue]*1000.0 ];
        // Byte *Julibyte=[self FloatToData:(int)(appDelegate.gActDistance % 100)];
        
        distance=(appDelegate.gActDistance / 1000);
        NSLog(@"%f",distance);
        // Byte *Julibyte=[self FloatToData:(int)([self.IBLabelDistance.text floatValue]*1000.0)];
        Byte *distbyte=[self FloatToData:distance*1000.0];
        byte[5]=(Byte) distbyte[0];//Distance in meters
        byte[6]=(Byte) distbyte[1];
        byte[7]=(Byte) distbyte[2];
        //byte[8]=(Byte) Julibyte[3];//distance
        
        if (distbyte[3] < 10) distbyte[3] = (Byte) (distbyte[3] * 10);
        // Package2.Data[6]= TempByte; //Note: Always 2 digits after comma:5.1 = 10 5.06 = 6 5.64 = 64
        byte[8]=(Byte) distbyte[3];
        
        
        
        
        
        
        
        // Byte *Calbyte=[self FloatToData:(int)self.IBLabelCalorie*1000];
        Byte *Calbyte=[self FloatToData:(int)(appDelegate.gActCalculateCalories)];
        byte[9]=(Byte) Calbyte[0];//Calorie consumption units kcal
        byte[10]=(Byte) Calbyte[1];
        byte[11]=(Byte) Calbyte[2];
        byte[12]=(Byte) Calbyte[3];
        
        
        //Byte *Speedbyte=[self FloatToData:[self.IBLabelAvgSpeed.text integerValue]];
        byte[13]=(Byte)avgspeed;//Speedbyte[2];//Speed km/h
        byte[14]=(Byte)00;//Speedbyte[3];
        
        
        
        
        Byte *GaoDubyte=[self FloatToData:[self.IBLabelAltitude.text integerValue]];
        byte[15]=  GaoDubyte[2];//Height m
        byte[16]=  GaoDubyte[3];
        
        //  int windir=[self getDirectionByte:WindTempDirection];
        int TempDirection = (int)([self getDirectionByte:appDelegate.myLocation.course]);
        NSLog(@"%d",TempDirection);
        //Byte *FXbyte=[self FloatToData:[self.IBLabelDirection.text integerValue]];
        byte[18]=(Byte) (TempDirection);//(TempDirection %100);//FXbyte[3];
        byte[17]=(Byte)00;//(TempDirection %100);//FXbyte[2];//   Degree direction;
        
        
        byte[19]=(Byte)hartrate;//byteFR;//Heart Rate
        
        
        
        NSData * data = [NSData dataWithBytes:&byte length:sizeof(byte)];
        [app.appdelegateProximity  phoneWriteDataToTagBig :data];
        
        
        
    }
    
    
}

//direction degree method
-(int)getDirectionByte:(int)Direction{
    
    int sFinalDirection;
    if(Direction <= 190 &&  Direction >= 170){
        //        sFinalDirection = @"S";
        sFinalDirection = 180;
    }else if (Direction <= 10){
        //sFinalDirection = @"N";
        sFinalDirection =0;
    }else if (Direction >= 350){
        //sFinalDirection = @"N";
        sFinalDirection = 0;
    }else if (Direction <= 100 && Direction >=80){
        //sFinalDirection = @"E";
        sFinalDirection = 90;
    }else if (Direction<= 280  && Direction >= 260)
    {
        //sFinalDirection = @"W";
        sFinalDirection = 270;
    }else if (Direction<= 190 && Direction >= 170){
        //sFinalDirection = @"S";
        sFinalDirection = 180;
    }else if (Direction > 280 && Direction <= 310){
        // sFinalDirection = @"WNW";
        sFinalDirection = 292;
    }else if (Direction > 310 && Direction <= 330){
        //sFinalDirection = @"NW";
        sFinalDirection = 315;
    }else if (Direction > 330 && Direction < 350){
        // sFinalDirection = @"NNW";
        sFinalDirection = 338;
    }else if (Direction > 10 && Direction <= 30){
        // sFinalDirection = @"NNE";
        sFinalDirection = 23;
    }else if (Direction > 30 && Direction <= 50){
        //sFinalDirection = @"NE";
        sFinalDirection = 45;
    }else if (Direction > 50 && Direction < 80){
        // sFinalDirection = @"ENE";
        sFinalDirection = 69;
    }else if (Direction > 100 && Direction <= 130){
        // sFinalDirection = @"ESE";
        sFinalDirection = 112;
    }else if (Direction > 130 && Direction <= 150){
        //sFinalDirection = @"SE";
        sFinalDirection = 135;
    }else if (Direction > 150 && Direction < 170){
        // sFinalDirection = @"SSE";
        sFinalDirection = 158;
    }else if (Direction > 190 && Direction <= 210){
        //sFinalDirection = @"SSW";
        sFinalDirection = 202;
    }else if (Direction > 210 && Direction <= 230){
        // sFinalDirection = @"SW";
        sFinalDirection = 225;
    }else if (Direction > 230 && Direction < 260){
        //sFinalDirection = @"WSW";
        sFinalDirection = 249;
    }
    return sFinalDirection;
    
}


@end
