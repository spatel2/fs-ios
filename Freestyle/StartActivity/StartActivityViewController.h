

#import <UIKit/UIKit.h>
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "SikinButton.h"

@interface StartActivityViewController : UIViewController

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForScrollViewContentWidth;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenSnowButtonAndSupButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenBikeButtonAndRunButton;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForDistanceBetweenWakeButtonAndScrollView;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSnow;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonWake;

@property (strong, nonatomic) IBOutlet UIView *IBViewSnowBoard;
@property (strong, nonatomic) IBOutlet UIView *IBViewSking;
@property (strong, nonatomic) IBOutlet UIView *IBViewMountainBike;
@property (strong, nonatomic) IBOutlet UIView *IBViewRoadBike;
@property (strong, nonatomic) IBOutlet UIView *IBViewWakeBoarding;
@property (strong, nonatomic) IBOutlet UIView *IBViewWakeSurfing;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonBike;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonMountainBike;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonRoadBike;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonRun;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSkate;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSnowboard;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSkiing;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSup;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonSurf;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonWakeboarding;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonWakeSurfing;

@end
