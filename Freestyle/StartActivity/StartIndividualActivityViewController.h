

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "SikinLabel.h"
#import "SikinButton.h"
#import "Helper.h"
#import "DBHelper.h"
#import "ProximityTagStorage.h"

@interface StartIndividualActivityViewController : UIViewController<UIAlertViewDelegate>{

        AppDelegate* appDelegate;
}
@property (strong, nonatomic) IBOutlet GMSMapView *mapView_;
@property (strong, nonatomic) IBOutlet UIView *IBViewTitle;
@property (strong, nonatomic) IBOutlet UIScrollView *IBScrollView;

@property (strong, nonatomic) NSString *strActTitle;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelActTitle;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBScrollWidthConstraints;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelAvgSpeed;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelMaxSpeed;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelMinSpeed;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelHeartRate;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonTimer;

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonStart;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonPause;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelCalorie;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDirection;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAltitude;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDistance;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelDistanceUnit;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLableTimer;

@end
