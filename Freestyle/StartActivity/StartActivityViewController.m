

#import "StartActivityViewController.h"
#import "StartIndividualActivityViewController.h"

@interface StartActivityViewController ()

@end

@implementation StartActivityViewController{

    AppDelegate* appDelegate;
}

@synthesize IBNSLayoutConstraintForScrollViewContentWidth,IBNSLayoutConstraintForDistanceBetweenSnowButtonAndSupButton,IBNSLayoutConstraintForDistanceBetweenBikeButtonAndRunButton,IBNSLayoutConstraintForDistanceBetweenWakeButtonAndScrollView;


@synthesize IBViewSnowBoard,IBViewSking,IBViewMountainBike,IBViewRoadBike,IBViewWakeBoarding,IBViewWakeSurfing;

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.IBButtonSnow.selected = false;
    self.IBButtonWake.selected = false;
    self.IBButtonBike.selected = false;
    
    IBViewSnowBoard.alpha = 0;
    IBViewSking.alpha = 0;
    IBViewMountainBike.alpha = 0;
    IBViewRoadBike.alpha = 0;
    
    IBViewWakeSurfing.alpha = 0;
    IBViewWakeBoarding.alpha = 0;
     appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     [appDelegate.myLocationManager startUpdatingLocation];
    
    
    if (APP_DELEGATE.gActStartActivityName != nil && APP_DELEGATE.gActStartActivityName.length > 0) {
        
        if([self.IBButtonMountainBike.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            //self.IBButtonMountainBike.titleLabel.textColor = [UIColor whiteColor];
            [self.IBButtonMountainBike setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewMountainBike.backgroundColor = COLOR_DARKBLUE;
            
            
        }else if([self.IBButtonRoadBike.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            
            
            [self.IBButtonRoadBike setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewRoadBike.backgroundColor = COLOR_DARKBLUE;
            
        }else if([self.IBButtonRun.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            
            [self.IBButtonRun setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBButtonRun.superview.backgroundColor = COLOR_DARKBLUE;
        }else if([self.IBButtonSkate.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            [self.IBButtonSkate setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBButtonSkate.superview.backgroundColor = COLOR_DARKBLUE;
        }else if([self.IBButtonSnowboard.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            [self.IBButtonSnowboard setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewSnowBoard.backgroundColor = COLOR_DARKBLUE;
            
            
        }else if([self.IBButtonSkiing.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            [self.IBButtonSkiing setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewSking.backgroundColor = COLOR_DARKBLUE;
            
        }else if([self.IBButtonSup.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            
            [self.IBButtonSup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBButtonSup.superview.backgroundColor = COLOR_DARKBLUE;
        }else if([self.IBButtonSurf.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            [self.IBButtonSurf setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBButtonSurf.superview.backgroundColor = COLOR_DARKBLUE;
        }else if([self.IBButtonWakeboarding.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            [self.IBButtonWakeboarding setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewWakeBoarding.backgroundColor = COLOR_DARKBLUE;
            
        }else if([self.IBButtonWakeSurfing.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName]){
            
            [self.IBButtonWakeSurfing setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            self.IBViewWakeSurfing.backgroundColor = COLOR_DARKBLUE;
        }
    }else{
        
        
        //self.IBButtonMountainBike.titleLabel.textColor = [UIColor whiteColor];
        [self.IBButtonMountainBike setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewMountainBike.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];
        
        [self.IBButtonRoadBike setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewRoadBike.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];;
        
        [self.IBButtonRun setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.IBButtonRun.superview.backgroundColor = [UIColor whiteColor];
        
        [self.IBButtonSkate setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.IBButtonSkate.superview.backgroundColor = [UIColor whiteColor];
        
        [self.IBButtonSnowboard setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewSnowBoard.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];;
        
        
        [self.IBButtonSkiing setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewSking.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];;
        
        [self.IBButtonSup setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.IBButtonSup.superview.backgroundColor = [UIColor whiteColor];
        
        [self.IBButtonSurf setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        self.IBButtonSurf.superview.backgroundColor = [UIColor whiteColor];
        
        
        [self.IBButtonWakeboarding setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewWakeBoarding.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];
        
        
        
        [self.IBButtonWakeSurfing setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.IBViewWakeSurfing.backgroundColor = [UIColor colorWithRed:34.0f/255.0f green:35.0f/255.0f blue:34.0f/255.0f alpha:.65];
        
    }
    
}

- (void)viewWillLayoutSubviews{
    IBNSLayoutConstraintForScrollViewContentWidth.constant = self.view.frame.size.width - 40 ;
    IBNSLayoutConstraintForDistanceBetweenSnowButtonAndSupButton.constant = 8;
    IBNSLayoutConstraintForDistanceBetweenBikeButtonAndRunButton.constant = 8;
    IBNSLayoutConstraintForDistanceBetweenWakeButtonAndScrollView.constant = 8;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Action

- (IBAction)snowButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenSnowButtonAndSupButton.constant = 112;
            IBViewSnowBoard.alpha = 1;
            IBViewSking.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenSnowButtonAndSupButton.constant = 8;
            IBViewSnowBoard.alpha = 0;
            IBViewSking.alpha = 0;
            
        }];
        sender.selected = false;
    }
}
- (IBAction)bikeButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenBikeButtonAndRunButton.constant = 112;
            IBViewMountainBike.alpha = 1;
            IBViewRoadBike.alpha = 1;
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            
            IBNSLayoutConstraintForDistanceBetweenBikeButtonAndRunButton.constant = 8;
            IBViewMountainBike.alpha = 0;
            IBViewRoadBike.alpha = 0;
        }];
        sender.selected = false;
    }
}

- (IBAction)wakeButtonTouchUpInsideAction:(UIButton *)sender{
    
    if (sender.selected == false) {
        
        
        [UIView animateWithDuration:0.5 animations:^{
            IBNSLayoutConstraintForDistanceBetweenWakeButtonAndScrollView.constant = 112;
            IBViewWakeBoarding.alpha = 1;
            IBViewWakeSurfing.alpha = 1;
            
        }];
        sender.selected = true;
    }else if (sender.selected == true){
        
        [UIView animateWithDuration:0 animations:^{
            IBNSLayoutConstraintForDistanceBetweenWakeButtonAndScrollView.constant = 8;
            IBViewWakeBoarding.alpha = 0;
            IBViewWakeSurfing.alpha = 0;
            
        }];
        sender.selected = false;
    }
}

- (IBAction)startActivityAction:(UIButton *)sender{
    
    if (APP_DELEGATE.gActStartActivityName != nil && APP_DELEGATE.gActStartActivityName.length >0 && [sender.titleLabel.text isEqualToString:APP_DELEGATE.gActStartActivityName] == false) {
        [Helper displayAlertView:@"" message:[NSString stringWithFormat:@"%@ activity is running, please stop that before starting new activity.",APP_DELEGATE.gActStartActivityName]];
        return;
    }
    [self performSegueWithIdentifier:@"sequeStartActivity" sender:sender.titleLabel.text];
}

#pragma mark - Navigation Button Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    StartIndividualActivityViewController *startIndividualActivityViewController = [segue destinationViewController];
    startIndividualActivityViewController.strActTitle = sender;
}

@end
