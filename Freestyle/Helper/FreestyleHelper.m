

#import "FreestyleHelper.h"

@implementation FreestyleHelper

+(void)setNavigationLeftButtonWithMenu:(UIViewController*)viewController{
    
    UIButton* buttonEdit =[UIButton buttonWithType:UIButtonTypeCustom];
    [buttonEdit setFrame:CGRectMake(0, 0, 20, 20)];
    buttonEdit.imageEdgeInsets = UIEdgeInsetsMake(0, -10, 0, 0);
    [buttonEdit setImage:[UIImage imageNamed:@"img_action_menu"] forState:UIControlStateNormal];
    [buttonEdit addTarget:viewController action:@selector(btnLeftBarMenuTap:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* left =[[UIBarButtonItem alloc] initWithCustomView:buttonEdit];
    [viewController.navigationItem setLeftBarButtonItem:left];
}


+(void)setNavigationTitleView:(UIViewController*)viewController{
    
    UIImageView* titleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 20)];
    titleImageView.image = [UIImage imageNamed:@"freestyle-logo-home"];
    [titleImageView setContentMode:UIViewContentModeScaleAspectFit];
    [viewController.navigationItem setTitleView:titleImageView];
    
    /*
    
     let viewTitle = UIView(frame: CGRectMake((viewController.navigationController?.navigationBar.center.x)! - 31, -6, 63, 63))
     viewTitle.clipsToBounds = false
     
     let imgView = UIImageView(frame: CGRectMake(0, 0, 63, 63))
     
     //imgView.image = (UIImage(named: "img_action_logo.png").imageWithRenderingMode(.AlwaysOriginal))
     imgView.image = (UIImage(named: "img_action_logo.png")?.imageWithRenderingMode(.AlwaysOriginal));
     
     viewTitle.addSubview(imgView)
     viewTitle.tag = 9999;
     
     for view in viewController.navigationController!.navigationBar.subviews {
     if view.tag == 9999 {
     view.removeFromSuperview()
     }
     }
     
     viewController.navigationController?.navigationBar.addSubview(viewTitle)
    
    */
    
}
@end
