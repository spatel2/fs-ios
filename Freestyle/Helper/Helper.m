#import "Helper.h"

static Helper * _sharedHelper = nil;

@implementation Helper

@synthesize playerVideo;

+ (Helper *) sharedHelper
{
    if (_sharedHelper == nil)
    {
        _sharedHelper = [[Helper alloc] init];
    }
    
    return _sharedHelper;
}

/*!
 @method viewFromNib
 @abstract Load view from Nib Files
 */
+ (UIView *)viewFromNib:(NSString *)sNibName{
	return (UIView *)[Helper viewFromNib:sNibName sViewName:@"UIView"];
}
/*!
 @method viewFromNib
 @abstract Load view from Nib Files for dynemic class
 */
+ (id)viewFromNib:(NSString *)sNibName sViewName:(NSString *)sViewName{
//	SALog(@"sNibName..%@",sNibName);
	Class className = NSClassFromString(sViewName);
	NSArray *xib = [[NSBundle mainBundle] loadNibNamed:sNibName owner:self options:nil];
	for (id _view in xib) { // have to iterate; index varies
		if ([_view isKindOfClass:[className class]]) return _view;
	}
	return nil;
}
/*!
 @method performBlock
 @abstract Perform Block Operation after delay
 */
+(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay{
	int64_t delta = (int64_t)(1.0e9 * delay);
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (NSString *)getPREF:(NSString *)sKey {
    return (NSString *)[[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}

/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (int)getPREFint:(NSString *)sKey {
    return (int)[[NSUserDefaults standardUserDefaults] integerForKey:sKey];
}

/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFStringValue:(NSString *)sValue sKey:(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFint:(int)iValue :(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setInteger:iValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (id)getPREFID:(NSString *)sKey {
    return [[NSUserDefaults standardUserDefaults] valueForKey:sKey];
}

/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFID:(id)sValue :(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] setValue:sValue forKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method delPREF
 @abstract To delete the preference value for the key that has been passed
 */
+(void) delPREF:(NSString *)sKey {
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:sKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
/*!
 @method displayAlertView
 @abstract To Display Alert Msg
 */
+ (void) displayAlertView :(NSString *) title message :(NSString *) message
{
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    [alert show];
}

/*!
 @method randomRange
 @abstract get random index between two
 */
+(int) randomRange: (int) min max:(int) max {
    int range = max - min;
    if (range == 0) return min;
	
    return (arc4random() % range) + min;
}

/*!
 @method iSConnectedToNetwork
 @absttract To Check Network Connection
 */

+ (BOOL) iSConnectedToNetwork
{	// Create zero addy
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(NULL, (struct sockaddr *)&zeroAddress);
    SCNetworkReachabilityFlags flags;
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags);
    CFRelease(defaultRouteReachability);
    if (!didRetrieveFlags)
    {
        return NO;
    }
    BOOL isReachable = flags & kSCNetworkFlagsReachable;
    BOOL needsConnection = flags & kSCNetworkFlagsConnectionRequired;
    return (isReachable && !needsConnection) ? YES : NO;
}

/* The UI_USER_INTERFACE_IDIOM() macro is provided for use when deploying to a version of the iOS less than 3.2. If the earliest version of iPhone/iOS that you will be deploying for is 3.2 or greater, you may use -[UIDevice userInterfaceIdiom] directly.
 */
#define UI_USER_INTERFACE_IDIOM() ([[UIDevice currentDevice] respondsToSelector:@selector(userInterfaceIdiom)] ? [[UIDevice currentDevice] userInterfaceIdiom] : UIUserInterfaceIdiomPhone)

#define UIDeviceOrientationIsPortrait(orientation)  ((orientation) == UIDeviceOrientationPortrait || (orientation) == UIDeviceOrientationPortraitUpsideDown)
#define UIDeviceOrientationIsLandscape(orientation) ((orientation) == UIDeviceOrientationLandscapeLeft || (orientation) == UIDeviceOrientationLandscapeRight)



+ (NSString* )getDigitFromString:(NSString*)inputString{
    
    NSString *originalString = inputString;
    NSMutableString *strippedString = [NSMutableString
                                       stringWithCapacity:originalString.length];
    
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet
                               characterSetWithCharactersInString:@"0123456789"];
    
    while ([scanner isAtEnd] == NO) {
        NSString *buffer;
        if ([scanner scanCharactersFromSet:numbers intoString:&buffer]) {
            [strippedString appendString:buffer];
            
        } else {
            [scanner setScanLocation:([scanner scanLocation] + 1)];
        }
    }
    return strippedString;
}
+(NSString*)getDateFromString:(NSString*)dateStr{
    
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    // Convert date object to desired output format
    [dateFormat setDateFormat:@"MM-dd-YYYY"];
    dateStr = [dateFormat stringFromDate:date];
    
    return dateStr;
}

+ (BOOL)saveImageToDocumentDirectoryOfAppImage:(UIImage*)image nameOfImage:(NSString*)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:name];
     // imageView is my image from camera
    NSData *imageData = UIImageJPEGRepresentation(image,1);
    return [imageData writeToFile:savedImagePath atomically:NO];
}
+ (UIImage*)getImageFromDocumentDirectoryOfAppImageName:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,     NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *getImagePath = [documentsDirectory stringByAppendingPathComponent:imageName];
    
    return [UIImage imageWithContentsOfFile:getImagePath];
}
+(BOOL) IsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

/*
+(UIImage *)resizeImage:(UIImage *)image
          toAspectWidth:(int)aspectWidth
        andAspectHeight:(int)aspectHeight
           isAspectFill:(BOOL)isAspectFill
        ignoresRotation:(BOOL)ignoresRotataion{
    @autoreleasepool {
        if(aspectWidth <=0 || aspectHeight<=0) return nil;
        
        float imageWidth = image.size.width;
        float imageHeight = image.size.height;
        float finalWidth,finalHeight;
        float offsetX,offsetY;
        
        //Simplifying aspect ratio
        int g = gcd1(aspectWidth,aspectHeight);
        aspectWidth = aspectWidth/g;
        aspectHeight = aspectHeight/g;
        
        if(ignoresRotataion){
            //Aligning longest edge to the longest aspect ratio (not changing if image or ratio is square)
            if((imageWidth>imageHeight&&aspectWidth<aspectHeight)||(imageWidth<imageHeight&&aspectWidth>aspectHeight)){
                aspectWidth=aspectWidth+aspectHeight;
                aspectHeight=aspectWidth-aspectHeight;
                aspectWidth=aspectWidth-aspectHeight;
            }
            
            //If image is square, aligning longest aspect ratio to width
            if(imageWidth==imageHeight&&aspectWidth<aspectHeight){
                aspectWidth=aspectWidth+aspectHeight;
                aspectHeight=aspectWidth-aspectHeight;
                aspectWidth=aspectWidth-aspectHeight;
            }
        }
        
        if(isAspectFill){
            //Calculating final imagesize in pixels if aspectFill is true
            float factor = MIN((imageWidth/aspectWidth),(imageHeight/aspectHeight));
            finalWidth = (int)(aspectWidth*factor);
            finalHeight = (int)(aspectHeight*factor);
            
            //Offset for drawing the final image (so as to crop center portion)
            offsetX = (fabsf(imageWidth-finalWidth)/2);
            offsetY = (fabsf(imageHeight-finalHeight)/2);
            
            //Setting up the cropped UIImage from the source UIImage for aspectFill
            //CGRect for the crop
            CGRect rect = CGRectMake(offsetX*image.scale,offsetY*image.scale,finalWidth*image.scale,finalHeight*image.scale);
            CGImageRef ref = CGImageCreateWithImageInRect(image.CGImage, rect);
            UIImage *finalImage = [UIImage imageWithCGImage:ref scale:image.scale orientation:image.imageOrientation];
            //Returning final image
            return finalImage;
            
        }else{
            //Calculating final imagesize in pixels if aspectFill is false (aspectFit is true)
            float factor = MAX((imageWidth/aspectWidth),(imageHeight/aspectHeight));
            finalWidth = (int)(aspectWidth*factor);
            finalHeight = (int)(aspectHeight*factor);
            
            //Offset for drawing the final image (so as to crop center portion)
            offsetX = (fabsf(imageWidth-finalWidth)/2);
            offsetY = (fabsf(imageHeight-finalHeight)/2);
            
            //Setting up the cropped UIImage from the source UIImage for aspectFit
            //CGRect for the final Image
            CGRect rect = CGRectMake(0, 0, finalWidth, finalHeight);
            //Drawing original image inside context
            UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0);
            [image drawInRect:CGRectMake(offsetX, offsetY, image.size.width, image.size.height)];
            UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            //Returning Image
            return finalImage;
        }
        return nil;
    }
}

int gcd1(int m, int n) {
    int t, r;
    if (m < n) {
        t = m;
        m = n;
        n = t;
    }
    r = m % n;
    if (r == 0) {
        return n;
    } else {
        return gcd1(n, r);
    }
}*/

@end


@implementation NSMutableArray (SelfSorting)

- (void)insertNumberAtSortedLocation:(NSNumber *)aNumber
{
    NSUInteger count = [self count];
    
    // if there are no contents yet, simply add it
    if (!count)
    {
        [self addObject:aNumber];
        return;
    }
    
    
    
    NSRange searchRange;
    searchRange.location = 0;
    searchRange.length = count;
    
    
    // bubble sort finding of insert point
    do
    {
        NSInteger index = searchRange.location + searchRange.length/2;
        
        NSNumber *testNumber = [self objectAtIndex:index];
        
        switch ([aNumber compare:testNumber])
        {
            case NSOrderedAscending:
            {
                //searchRange.length = searchRange.length / 2;
                searchRange.length = index - searchRange.location;
                break;
            }
            case NSOrderedDescending:
            {
                NSUInteger oldLocation = searchRange.location;
                searchRange.location = index+1;
                searchRange.length = searchRange.length - (searchRange.location - oldLocation);
                break;
            }
            case NSOrderedSame:
            {
                searchRange.length = 0;
                searchRange.location = index;
                break;
            }
        }
    }	while  (searchRange.length > 0);
    
    // insert at found point
    [self insertObject:aNumber atIndex:searchRange.location];
}






@end



