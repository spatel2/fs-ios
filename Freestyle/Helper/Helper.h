
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import <netdb.h>

@interface Helper : NSObject<AVAudioPlayerDelegate>
{
	AVPlayerLayer *playerLayer;
}

@property (nonatomic, strong) AVPlayer *playerVideo;

+ (Helper *) sharedHelper;

/*!
 @method viewFromNib
 @abstract Load view from Nib Files
 */
+ (UIView *)viewFromNib:(NSString *)sNibName;
/*!
 @method viewFromNib
 @abstract Load view from Nib Files for dynemic class
 */
+ (id)viewFromNib:(NSString *)sNibName sViewName:(NSString *)sViewName;
/*!
 @method performBlock
 @abstract Perform Block Operation after delay
 */
+(void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (NSString *)getPREF:(NSString *)sKey;
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (int)getPREFint:(NSString *)sKey;
/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
//+(void) setPREF:(NSString *)sValue :(NSString *)sKey;
+(void) setPREFStringValue:(NSString *)sValue sKey:(NSString *)sKey;
/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFint:(int)iValue :(NSString *)sKey;
/*!
 @method getPreferenceValueForKey
 @abstract To get the preference value for the key that has been passed
 */
+ (id)getPREFID:(NSString *)sKey;
/*!
 @method setPreferenceValueForKey
 @abstract To set the preference value for the key that has been passed
 */
+(void) setPREFID:(id)sValue :(NSString *)sKey;


/*!
 @method delPREF
 @abstract To delete the preference value for the key that has been passed
 */
+(void) delPREF:(NSString *)sKey;
/*!
 @method displayAlertView
 @abstract To Display Alert Msg
 */
+(void) displayAlertView :(NSString *) title message :(NSString *) message;


/*!
 @method iSConnectedToNetwork
 @absttract To Check Network Connection
*/
+(BOOL) iSConnectedToNetwork;

/*!
 @method getDigitFromString
 @absttract To Get Digit From String
 */

+ (NSString* )getDigitFromString:(NSString*)inputString;
/*!
 @method getDateFromString
 @absttract To Get Digit From String
 */
+(NSString*)getDateFromString:(NSString*)dateStr;
/*!
 @method saveImageToDocumentDirectoryOfAppImage
 @absttract To SaveImage in Application Own Document Directory
 */
+ (BOOL)saveImageToDocumentDirectoryOfAppImage:(UIImage*)image nameOfImage:(NSString*)name;

/*!
 @method getImageFromDocumentDirectoryOfAppImageName
 @absttract To Get Image from Application Own Document Directory
 */

+ (UIImage*)getImageFromDocumentDirectoryOfAppImageName:(NSString*)imageName;


/*!
 @method resizeImage
 @absttract To Get Image from Application Own Document Directory
 */
/*
+(UIImage *)resizeImage:(UIImage *)image
          toAspectWidth:(int)aspectWidth
        andAspectHeight:(int)aspectHeight
           isAspectFill:(BOOL)isAspectFill
        ignoresRotation:(BOOL)ignoresRotataion;
*/

/*!
 @method IsValidEmail:
 @absttract To know  give email address is valid or not
*/

+ (BOOL) IsValidEmail:(NSString *)checkString;

@end

@interface NSMutableArray (SelfSorting)

- (void)insertNumberAtSortedLocation:(NSNumber *)aNumber;

@end
