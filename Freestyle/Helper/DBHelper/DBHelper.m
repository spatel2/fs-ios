//
//  DBHelper.m
//  
//  Created by BIT on 21/10/14.
//  Copyright (c) 2014 Bito1. All rights reserved.
//

#import "DBHelper.h"


static DBHelper *sharedInstance = nil;
static sqlite3 *database = nil;


@implementation DBHelper

+(DBHelper*)getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

-(BOOL)createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:
                    [docsDir stringByAppendingPathComponent: @"freeStyle.db"]];
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "create table if not exists TBL_BEACHINFO (pID INTEGER PRIMARY KEY  AUTOINCREMENT,Identity text,Latitude text,Longitude text,Title text, Cameratype text,SubRegion text,Region text)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table %s",errMsg);
                sqlite3_free(errMsg);
            }
            
            sql_stmt ="CREATE  TABLE TBL_ACT_LOG (aID INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , ActName VARCHAR NOT NULL , ActDate VARCHAR, ActTime VARCHAR, Duration VARCHAR, Distance VARCHAR, Direction VARCHAR, Altitude VARCHAR, AvgSpeed VARCHAR, MaxSpeed VARCHAR, MinSpeed VARCHAR, HeartRate VARCHAR, Calories VARCHAR, StartLat VARCHAR, StartLong VARCHAR, EndLat VARCHAR, EndLong VARCHAR, Location VARCHAR, ArrayOfLocations VARCHAR)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table %s",errMsg);
                sqlite3_free(errMsg);
            }
            
            sqlite3_close(database);
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

-(int)insetDataBeachID:(NSString*)Identity
              Latitude:(NSString*)latitude
             Longitude:(NSString*)longitude
                 Title:(NSString*)title
            Cameratype:(NSString*)cameratype
             SubRegion:(NSString*)subRegion
                Region:(NSString*)region
{
    int lastInsertBeachDataID;
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *addStmt;
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            const char *sqlStatement = "Insert into TBL_BEACHINFO (Identity,Latitude,Longitude,Title,Cameratype,SubRegion,Region) VALUES (?,?,?,?,?,?,?)";
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_text(addStmt, 1, [Identity UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 2, [latitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 3, [longitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 4, [title UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 5, [cameratype UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 6, [subRegion UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 7, [region UTF8String], -1, SQLITE_TRANSIENT);
            
            if(SQLITE_DONE != sqlite3_step(addStmt))
                NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(database));
            //NSAssert1(0, @"Error while inserting data. '%s'", sqlite3_errmsg(database));
            else
            {
                lastInsertBeachDataID = (int) sqlite3_last_insert_rowid(database);
            }
            
            //Reset the add statement.
            sqlite3_reset(addStmt);
            sqlite3_finalize(addStmt);
            
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
        sqlite3_close(database);
    }
    
    return lastInsertBeachDataID;
}

-(NSMutableArray *)getBeaches

{
    NSMutableArray *arrayBeaches = nil;
    NSString *query;
    const char *dbpath=[databasePath UTF8String];
    sqlite3_stmt *compiledStatement;
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            query=@"select * from TBL_BEACHINFO";
        }
        
        const char *sqlStatement=[query UTF8String];
        arrayBeaches = [[NSMutableArray alloc]init];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement,NULL)== SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                BeachInfo* beach=[[BeachInfo alloc] init];
                // Identity,Latitude,Longitude,Title,Cameratype
                beach.iDentity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                beach.latitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                beach.longitude =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                beach.title =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                
                beach.cameratype =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                
                beach.subRegion =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                beach.region =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                
                [arrayBeaches addObject:beach];
                
                
                
            }
            sqlite3_reset(compiledStatement);
            sqlite3_finalize(compiledStatement);
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        sqlite3_close(database);
    }
    
    return arrayBeaches;
}

-(NSMutableDictionary *)dicOfBeaches

{
    NSMutableDictionary *arrayBeaches = nil;
    NSString *query;
    const char *dbpath=[databasePath UTF8String];
    sqlite3_stmt *compiledStatement;
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            query=@"select * from TBL_BEACHINFO";
        }
        
        const char *sqlStatement=[query UTF8String];
        arrayBeaches = [[NSMutableDictionary alloc]init];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement,NULL)== SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                BeachInfo* beach=[[BeachInfo alloc] init];
                // Identity,Latitude,Longitude,Title,Cameratype
                beach.iDentity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                beach.latitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                beach.longitude =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                beach.title =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                
                beach.cameratype =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                
                beach.subRegion =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                beach.region =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
               
                [arrayBeaches setObject:beach forKey:[NSString stringWithFormat:@"%@_%@",beach.title,beach.iDentity]];
            }
            sqlite3_reset(compiledStatement);
            sqlite3_finalize(compiledStatement);
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        sqlite3_close(database);
    }
    
    return arrayBeaches;
}

-(BeachInfo*)getBeachFromID:(NSString*)sID
{
    NSMutableArray *products = nil;
    NSString *query;
    const char *dbpath=[databasePath UTF8String];
    sqlite3_stmt *compiledStatement;
    
    BeachInfo* beach =[[BeachInfo alloc] init];
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK) {
            query=[NSString stringWithFormat:@"select * from TBL_BEACHINFO where Identity = \"%@\"",sID];
        }
        
        const char *sqlStatement=[query UTF8String];
        products=[[NSMutableArray alloc]init];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement,NULL)== SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                //VendorID,Name,TagLine,Descr,LogoUrl,ShopAddress,FB,TWT,Instagram,Email,Phone,BeaconID
                beach.iDentity = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                beach.latitude = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                beach.longitude =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                beach.title =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                
                beach.cameratype =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
                
                beach.subRegion =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                beach.region =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                
                
            }
            sqlite3_reset(compiledStatement);
            sqlite3_finalize(compiledStatement);
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        sqlite3_close(database);
    }
    
    if (beach.iDentity == nil && beach.iDentity.length == 0) {
        beach = nil;
    }
    
    return beach;
}

-(void)deleteBeach:(NSString *)Identity
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *deleteStmt;
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            const char *sqlStatement = "Delete from TBL_BEACHINFO where Identity = ?";
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &deleteStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_text(deleteStmt, 1, [Identity UTF8String], -1, SQLITE_TRANSIENT);
            
            if(SQLITE_DONE != sqlite3_step(deleteStmt))
                NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(database));
            
            sqlite3_reset(deleteStmt);
            sqlite3_finalize(deleteStmt);
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
        sqlite3_close(database);
    }
    return ;
}

-(NSString* )getCurrentDateAndTime
{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:MM:SS"];
    NSString *dateString = [dateFormat stringFromDate:today];
    NSLog(@"date: %@", dateString);
    dateFormat = nil;
    
    return dateString;
}

//activity log related methods
-(void)deleteActivityLog:(int)aID{

    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *deleteStmt;
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            const char *sqlStatement = "Delete from TBL_ACT_LOG where aID = ?";
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &deleteStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
            
            //sqlite3_bind_text(deleteStmt, 1, [Identity UTF8String], -1, SQLITE_TRANSIENT);\
            
            sqlite3_bind_int(deleteStmt, 1, aID);
            
            
            if(SQLITE_DONE != sqlite3_step(deleteStmt))
                NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(database));
            
            sqlite3_reset(deleteStmt);
            sqlite3_finalize(deleteStmt);
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
        sqlite3_close(database);
    }
    return ;
}

-(int)insertActivityLog:(NSString *)actName
                ActDate:(NSString *)actDate
                ActTime:(NSString *)actTime
               Duration:(NSString *)duration
               Distance:(NSString *)distance
              Direction:(NSString *)direction
               Altitude:(NSString *)altitude
               AvgSpeed:(NSString *)avgSpeed
               MaxSpeed:(NSString *)maxSpeed
               MinSpeed:(NSString *)minSpeed
              HeartRate:(NSString *)heartRate
               Calories:(NSString *)calories
               StartLat:(NSString *)startLat
              StartLong:(NSString *)startLong
                 EndLat:(NSString *)endLat
                EndLong:(NSString *)endLong
               Location:(NSString *)location
       ArrayOfLocations:(NSString*)arrayOfLocations
{
    int lastInsertedActId;
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *addStmt;
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            const char *sqlStatement = "Insert into TBL_ACT_LOG (ActName, ActDate, ActTime, Duration, Distance, Direction, Altitude, AvgSpeed, MaxSpeed, MinSpeed, HeartRate, Calories, StartLat, StartLong, EndLat, EndLong, Location, ArrayOfLocations) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_text(addStmt, 1, [actName UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 2, [actDate UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 3, [actTime UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 4, [duration UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 5, [distance UTF8String], -1, SQLITE_TRANSIENT);
            //sqlite3_bind_double(addStmt, 5, distance);
            sqlite3_bind_text(addStmt, 6, [direction UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 7, [altitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 8, [avgSpeed UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 9, [maxSpeed UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 10, [minSpeed UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 11, [heartRate UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 12, [calories UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 13, [startLat UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 14, [startLong UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 15, [endLat UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 16, [endLong UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 17, [location UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(addStmt, 18, [arrayOfLocations UTF8String], -1, SQLITE_TRANSIENT);
            
            if(SQLITE_DONE != sqlite3_step(addStmt))
                NSLog(@"Error while inserting data. '%s'", sqlite3_errmsg(database));

            else
            {
                lastInsertedActId = (int) sqlite3_last_insert_rowid(database);
            }
            
            //Reset the add statement.
            sqlite3_reset(addStmt);
            sqlite3_finalize(addStmt);
            
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
        sqlite3_close(database);
    }
    
    return lastInsertedActId;
}

-(NSMutableArray *)getActivityLogs
{
    NSMutableArray *arrayAct = nil;
    NSString *query;
    const char *dbpath=[databasePath UTF8String];
    sqlite3_stmt *compiledStatement;
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            query=@"select * from TBL_ACT_LOG ORDER BY aID desc";
        }
        
        const char *sqlStatement=[query UTF8String];
        arrayAct = [[NSMutableArray alloc]init];
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement,NULL)== SQLITE_OK)
        {
            while (sqlite3_step(compiledStatement) == SQLITE_ROW)
            {
                ActivityInfo* act=[[ActivityInfo alloc] init];
                act.aID = (int)sqlite3_column_int(compiledStatement, 0);
                act.ActName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
                act.ActDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                act.ActTime =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                act.Duration =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
                act.Distance = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];//(float)sqlite3_column_double(compiledStatement, 5);
                act.Direction =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
                act.Altitude =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 7)];
                act.AvgSpeed =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 8)];
                act.MaxSpeed =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 9)];
                act.MinSpeed =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 10)];
                act.HeartRate =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 11)];
                act.Calories =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 12)];
                act.StartLat =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 13)];
                act.StartLong =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 14)];
                act.EndLat =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 15)];
                act.EndLong =[NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 16)];
                act.Location = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 17)];
                act.stringOfLocations = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 18)];
                [arrayAct addObject:act];
            }
            sqlite3_reset(compiledStatement);
            sqlite3_finalize(compiledStatement);
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        sqlite3_close(database);
    }
    
    return arrayAct;
}


-(int)UpdateActivityLog:(int)aID
               Location:(NSString * )location
{
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt *addStmt;
    
    @try {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            const char *sqlStatement = "Update TBL_ACT_LOG set Location = ? WHERE aID = ?";
            
            if(sqlite3_prepare_v2(database, sqlStatement, -1, &addStmt, NULL) != SQLITE_OK)
                NSAssert1(0, @"Error while creating add statement. '%s'", sqlite3_errmsg(database));
            
            sqlite3_bind_text(addStmt, 1, [location UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_int(addStmt, 2, aID);
            
            if(SQLITE_DONE == sqlite3_step(addStmt))
            {
                
            }
            else
            {
                NSLog(@"Error while updating data. '%s'", sqlite3_errmsg(database));
            }
            
            //Reset the add statement.
            sqlite3_reset(addStmt);
            sqlite3_finalize(addStmt);
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
        sqlite3_close(database);
    }
    
    return 1;
}

@end
