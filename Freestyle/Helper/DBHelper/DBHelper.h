//
//  DBHelper.h
//  
//
//  Created by BIT on 21/10/14.
//  Copyright (c) 2014 Bito1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "BeachInfo.h"
#import "ActivityInfo.h"

@interface DBHelper : NSObject
{
    NSString *databasePath;
}

+(DBHelper*)getSharedInstance;

-(BOOL)createDB;


-(int)insetDataBeachID:(NSString*)Identity
              Latitude:(NSString*)latitude
             Longitude:(NSString*)longitude
                 Title:(NSString*)title
            Cameratype:(NSString*)cameratype
             SubRegion:(NSString*)subRegion
                Region:(NSString*)region;

-(NSMutableArray *)getBeaches;
-(NSMutableDictionary *)dicOfBeaches;

-(BeachInfo*)getBeachFromID:(NSString*)sID;
-(void)deleteBeach:(NSString *)Identity;

//activity log related methods
-(void)deleteActivityLog:(int)aID;
-(int)insertActivityLog:(NSString *)actName
                ActDate:(NSString *)actDate
                ActTime:(NSString *)actTime
               Duration:(NSString *)duration
               Distance:(NSString *)distance
              Direction:(NSString *)direction
               Altitude:(NSString *)altitude
               AvgSpeed:(NSString *)avgSpeed
               MaxSpeed:(NSString *)maxSpeed
               MinSpeed:(NSString *)minSpeed
              HeartRate:(NSString *)heartRate
               Calories:(NSString *)calories
               StartLat:(NSString *)startLat
              StartLong:(NSString *)startLong
                 EndLat:(NSString *)endLat
                EndLong:(NSString *)endLong
               Location:(NSString *)location
       ArrayOfLocations:(NSString*)arrayOfLocations;
-(NSMutableArray *)getActivityLogs;

-(int)UpdateActivityLog:(int)aID
               Location:(NSString * )location;

@end
