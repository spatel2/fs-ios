

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface FreestyleHelper : NSObject

+(void)setNavigationLeftButtonWithMenu:(UIViewController*)viewController;
+(void)setNavigationTitleView:(UIViewController*)viewController;
@end
