
#import "SikinButton.h"

@implementation SikinButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [[self titleLabel] setFont:[UIFont fontWithName:@"SinkinSans-400Regular" size:self.titleLabel.font.pointSize]];
        
    }
    return self;
}

@end
