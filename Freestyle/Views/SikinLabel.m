

#import "SikinLabel.h"

@implementation SikinLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self configure:self.font.pointSize];
    }
    return self;
}

-(void)awakeFromNib{
    
    [self configure:self.font.pointSize];
}

-(void)configure:(CGFloat)fontSize
{
    self.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:fontSize];
    self.text = NSLocalizedString(self.text, nil);
}

@end
