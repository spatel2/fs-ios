//
//  SignUpViewController.m
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//

#import "SignUpViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "RegistrationForFacebookWithoutEmailViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize IBLabeltext;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - View life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    IBLabeltext.adjustsFontSizeToFitWidth = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button Action
- (IBAction)signUpWithFacebookButtonTouchUpInsideAction:(UIButton *)sender {
    
    
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    if (accessToken == nil) {
        // User is logged in, do work such as go to next view controller.
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                // Process error
            } else if (result.isCancelled) {
                // Handle cancellations
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSLog(@"fetched user:%@", result);
                             
                             NSString* userEmailID = [result objectForKey:@"email"];
                             NSDateFormatter* dateFormatter=[[NSDateFormatter alloc] init];
                             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                             
                             NSDate* birthDate =[dateFormatter dateFromString:[result objectForKey:@"birthday"]];
                             
                             [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                             
                             NSString* finalDOB=[dateFormatter stringFromDate:birthDate];
                             
                            NSMutableDictionary* dicParam=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"],@"Email",@"",@"Password",[result objectForKey:@"id"],@"FacebookId",[result objectForKey:@"first_name"],@"FirstName",[result objectForKey:@"last_name"],@"LastName",[[result objectForKey:@"gender"] uppercaseString],@"Gender",finalDOB,@"BirthDate",@"",@"PhoneNo",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.latitude],@"Lat",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.longitude],@"Long",nil];

                             //userEmailID=nil;
                             
                             if (userEmailID!=nil && userEmailID.length > 0) {
                                 
                                 //IBTextFieldEmailID.text =[result objectForKey:@"email"];
                                 //IBTextFieldFirstName.text =[result objectForKey:@"first_name"];
                                // IBTextFieldLastName.text =[result objectForKey:@"last_name"];
                                 

                                 [self signUpWebService:dicParam];
                                 
                                 
                             }else{
                                 
                                [self performSegueWithIdentifier:@"segueFromLoginForFacebook" sender:dicParam];
                             }
                         }
                     }];
                    
                }
            }
        }];
    }
    else
    {
    
    }
    
    //self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];

}

-(void)signUpWebService:(NSMutableDictionary*)userInfo{
    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    
    [self.view setUserInteractionEnabled:false];
    
    WSFrameWork* wsFrameWork=[[WSFrameWork alloc] initWithURLAndParams:@"http://likebit.com/freestyle/SignUp.php" dicParams:userInfo];
    
    wsFrameWork.isSync=false;
    
    wsFrameWork.WSDatatype=kJSON;
    
    wsFrameWork.onSuccess=^(NSDictionary* dic){
        
        NSLog(@"Registered user %@",dic);
        
        NSString* message =[dic objectForKey:@"message"];
        NSString* success =[dic objectForKey:@"success"];
        
        if ([success isEqualToString:@"1"]) {
            
            id unknown = [[dic objectForKey:@"response"] objectForKey:@"Id"];
            
          //  NSLog(@"%@",[unknown class]);
            
            [userInfo setObject:unknown forKey:@"UserId"];
            [Helper setPREFID:userInfo :@"prefUserLoginInfo"];
            
            UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }else{
            [Helper displayAlertView:@"" message:message];
        }
        
        [self.view setUserInteractionEnabled:true];
        [viewForActivity removeFromSuperview];
    };
    
    [wsFrameWork send];
}
- (IBAction)loginButtonTouchUpInsideAction:(UIButton *)sender {
    
    /*
    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
    
    
    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
    
    mFSideMenuContainerViewController.centerViewController = navigationCont;
    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
    
    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];*/
    
}
#pragma mark - Alertview Delegete
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
            [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
            
            
            MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
            
            mFSideMenuContainerViewController.centerViewController = navigationCont;
            mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
            
            [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
        }
            break;
        default:
            break;
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([[segue identifier] isEqualToString:@"segueFromLoginForFacebook"]) {
        
        RegistrationForFacebookWithoutEmailViewController* registrationForFacebookWithoutEmailViewController =[segue destinationViewController];
        registrationForFacebookWithoutEmailViewController.userInfoFacebook = sender;
    }
    
    
  
}


@end
