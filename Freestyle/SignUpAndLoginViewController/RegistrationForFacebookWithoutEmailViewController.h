//
//  RegistrationForFacebookWithoutEmailViewController.h
//  Freestyle
//
//  Created by BIT on 04/06/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface RegistrationForFacebookWithoutEmailViewController : UIViewController<UIAlertViewDelegate>{
    
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmailID;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldPassword;

@property (strong, nonatomic) NSMutableDictionary* userInfoFacebook;


@end
