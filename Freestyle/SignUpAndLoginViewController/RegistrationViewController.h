//
//  RegistrationViewController.h
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface RegistrationViewController : UIViewController<UIAlertViewDelegate>{

    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmailID;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldPassword;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldFirstName;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldLastName;

@end
