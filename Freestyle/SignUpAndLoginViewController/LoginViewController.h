//
//  LoginViewController.h
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface LoginViewController : UIViewController{

    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmailID;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldPassword;


@end
