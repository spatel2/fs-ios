//
//  LoginViewController.m
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//


#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController{

    CGFloat KEYBOARD_ANIMATION_DURATION;
    CGFloat MINIMUM_SCROLL_FRACTION;
    CGFloat MAXIMUM_SCROLL_FRACTION;
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    CGFloat LANDSCAPE_KEYBOARD_HEIGHT;
    CGFloat animatedDistance;
}

@synthesize IBTextFieldEmailID,IBTextFieldPassword;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    KEYBOARD_ANIMATION_DURATION = 0.3;
    MINIMUM_SCROLL_FRACTION = 0.2;
    MAXIMUM_SCROLL_FRACTION = 0.8;
    
    if (IS_IPAD) {
        
        PORTRAIT_KEYBOARD_HEIGHT = 266;
        LANDSCAPE_KEYBOARD_HEIGHT = 352;
        
    }else{
        
        PORTRAIT_KEYBOARD_HEIGHT = 216;
        LANDSCAPE_KEYBOARD_HEIGHT = 162;
    }
    [appDelegate.myLocationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    if (viewFrame.origin.y < 0) {
        
        viewFrame.origin.y += animatedDistance;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:IBTextFieldEmailID]) {
        
        [IBTextFieldEmailID resignFirstResponder];
        [IBTextFieldPassword becomeFirstResponder];
        
    }
    else if ([textField isEqual:IBTextFieldPassword]) {
   
        [textField resignFirstResponder];
        
    }
    return YES;
}
-(IBAction)loginButtonTouchUpInsideAction:(UIButton*)sender{

    [IBTextFieldEmailID resignFirstResponder];
    [IBTextFieldPassword resignFirstResponder];
    
    if (IBTextFieldEmailID.text.length == 0) {
        [Helper displayAlertView:@"" message:@"Enter email address."];
        return;
    }
    if ([Helper IsValidEmail:IBTextFieldEmailID.text] == false) {
        
        [Helper displayAlertView:@"" message:@"Enter valid email address."];
        return;
    }
    if (IBTextFieldPassword.text.length == 0) {
        [Helper displayAlertView:@"" message:@"Enter password."];
        return;
    }
    
    NSDictionary* dicParam=[[NSDictionary alloc] initWithObjectsAndKeys:IBTextFieldEmailID.text,@"Email",IBTextFieldPassword.text,@"Password",nil];

    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    
    [self.view setUserInteractionEnabled:false];
    
    WSFrameWork* wsFrameWork=[[WSFrameWork alloc] initWithURLAndParams:@"http://likebit.com/freestyle/Login.php" dicParams:dicParam];
    
    wsFrameWork.isSync=false;
    
    wsFrameWork.WSDatatype=kJSON;
    
    wsFrameWork.onSuccess=^(NSDictionary* dic){
        
        NSLog(@"login user:%@", dic);
        
        NSString* message =[dic objectForKey:@"message"];
        NSString* success =[dic objectForKey:@"success"];
        
        if ([success isEqualToString:@"1"]) {
            
            NSDictionary*  response = [dic objectForKey:@"response"];
            
            NSDictionary* dicParam=[[NSDictionary alloc] initWithObjectsAndKeys:[response objectForKey:@"Email"],@"Email",[response objectForKey:@"Password"],@"Password",[response objectForKey:@"FacebookId"],@"FacebookId",[response objectForKey:@"FirstName"],@"FirstName",[response objectForKey:@"LastName"],@"LastName",[response objectForKey:@"Gender"],@"Gender",[response objectForKey:@"BirthDate"],@"BirthDate",[response objectForKey:@"PhoneNo"],@"PhoneNo",[response objectForKey:@"Lat"],@"Lat",[response objectForKey:@"Long"],@"Long",[response objectForKey:@"Id"],@"UserId",nil];
           
            [Helper setPREFID:dicParam :@"prefUserLoginInfo"];
            
                UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
                [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
            
            
                MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
            
                mFSideMenuContainerViewController.centerViewController = navigationCont;
                mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
            
                [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
            
            //[Helper displayAlertView:@"" message:[dic objectForKey:@"message"]];
        }else{
            [Helper displayAlertView:@"" message:message];
        }
        
        [self.view setUserInteractionEnabled:true];
        [viewForActivity removeFromSuperview];
    };
    
    [wsFrameWork send];
    
//    UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
//    [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
//    
//    
//    MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
//    
//    mFSideMenuContainerViewController.centerViewController = navigationCont;
//    mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
//    
//    [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
}
- (IBAction)signUpWithFacebookButtonTouchUpInsideAction:(UIButton *)sender {
    
    FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
    if (accessToken == nil) {
        // User is logged in, do work such as go to next view controller.
        //[self.view setUserInteractionEnabled:false];
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                // Process error
            } else if (result.isCancelled) {
                // Handle cancellations
            } else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                if ([result.grantedPermissions containsObject:@"email"]) {
                    // Do work
                    
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSLog(@"fetched user:%@", result);
                             
                             NSString* userEmailID = [result objectForKey:@"email"];
                             NSDateFormatter* dateFormatter=[[NSDateFormatter alloc] init];
                             [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                             
                             NSDate* birthDate =[dateFormatter dateFromString:[result objectForKey:@"birthday"]];
                             
                             [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                             
                             NSString* finalDOB=[dateFormatter stringFromDate:birthDate];
                             
                             NSMutableDictionary* dicParam=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[result objectForKey:@"email"],@"Email",@"",@"Password",[result objectForKey:@"id"],@"FacebookId",[result objectForKey:@"first_name"],@"FirstName",[result objectForKey:@"last_name"],@"LastName",[[result objectForKey:@"gender"] uppercaseString],@"Gender",finalDOB,@"BirthDate",@"",@"PhoneNo",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.latitude],@"Lat",[NSString stringWithFormat:@"%f",appDelegate.myLocation.coordinate.longitude],@"Long",nil];
                             
                             //userEmailID= nil;
                             
                             if (userEmailID!=nil && userEmailID.length > 0) {
                                 
                                 //IBTextFieldEmailID.text =[result objectForKey:@"email"];
                                 //IBTextFieldFirstName.text =[result objectForKey:@"first_name"];
                                 // IBTextFieldLastName.text =[result objectForKey:@"last_name"];
                                 
                                 
                                 
                                 [self signUpWebService:dicParam];
                                 
                                 
                             }else{
                                 
                                 [self performSegueWithIdentifier:@"segueFromRegistrationForFacebook" sender:dicParam];
                             }
                         }
                     }];
                    
                }
            }
        }];
    }
    else
    {
        
    }
    
    //self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    
}
-(void)signUpWebService:(NSMutableDictionary*)userInfo{
    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    
    [self.view setUserInteractionEnabled:false];
    
    WSFrameWork* wsFrameWork=[[WSFrameWork alloc] initWithURLAndParams:@"http://likebit.com/freestyle/SignUp.php" dicParams:userInfo];
    
    wsFrameWork.isSync=false;
    
    wsFrameWork.WSDatatype=kJSON;
    
    wsFrameWork.onSuccess=^(NSDictionary* dic){
        
        NSLog(@"Registered user %@",dic);
        
        NSString* message =[dic objectForKey:@"message"];
        NSString* success =[dic objectForKey:@"success"];
        
        if ([success isEqualToString:@"1"]) {
            
            id unknown = [[dic objectForKey:@"response"] objectForKey:@"Id"];
            
            //  NSLog(@"%@",[unknown class]);
            
            [userInfo setObject:unknown forKey:@"UserId"];
            [Helper setPREFID:userInfo :@"prefUserLoginInfo"];
            
            UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }else{
            [Helper displayAlertView:@"" message:message];
        }
        
        [self.view setUserInteractionEnabled:true];
        [viewForActivity removeFromSuperview];
    };
    
    [wsFrameWork send];
}
#pragma mark - Alertview Delegete
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
            [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
            
            
            MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
            
            mFSideMenuContainerViewController.centerViewController = navigationCont;
            mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
            
            [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
        }
            break;
        default:
            break;
    }
}
#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)forgotPasswordTapGestureAction:(UITapGestureRecognizer *)sender {
    [self performSegueWithIdentifier:@"segueForgotPasswordViewCont" sender:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
