//
//  RegistrationForFacebookWithoutEmailViewController.m
//  Freestyle
//
//  Created by BIT on 04/06/15.
//
//

#import "RegistrationForFacebookWithoutEmailViewController.h"

@interface RegistrationForFacebookWithoutEmailViewController ()

@end

@implementation RegistrationForFacebookWithoutEmailViewController{

    CGFloat KEYBOARD_ANIMATION_DURATION;
    CGFloat MINIMUM_SCROLL_FRACTION;
    CGFloat MAXIMUM_SCROLL_FRACTION;
    CGFloat PORTRAIT_KEYBOARD_HEIGHT;
    CGFloat LANDSCAPE_KEYBOARD_HEIGHT;
    CGFloat animatedDistance;
}

@synthesize IBTextFieldEmailID,IBTextFieldPassword;

@synthesize userInfoFacebook;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    KEYBOARD_ANIMATION_DURATION = 0.3;
    MINIMUM_SCROLL_FRACTION = 0.2;
    MAXIMUM_SCROLL_FRACTION = 0.8;
    
    if (IS_IPAD) {
        
        PORTRAIT_KEYBOARD_HEIGHT = 266;
        LANDSCAPE_KEYBOARD_HEIGHT = 352;
        
    }else{
        
        PORTRAIT_KEYBOARD_HEIGHT = 216;
        LANDSCAPE_KEYBOARD_HEIGHT = 162;
    }
    [appDelegate.myLocationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    CGRect viewFrame = self.view.frame;
    if (viewFrame.origin.y < 0) {
        
        viewFrame.origin.y += animatedDistance;
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if ([textField isEqual:IBTextFieldEmailID]) {
        
        [IBTextFieldEmailID resignFirstResponder];
        [IBTextFieldPassword becomeFirstResponder];
        
    }
    else if ([textField isEqual:IBTextFieldPassword]) {
        
        [textField resignFirstResponder];
        
    }
    return YES;
}
-(IBAction)loginButtonTouchUpInsideAction:(UIButton*)sender{
    
    [IBTextFieldEmailID resignFirstResponder];
    [IBTextFieldPassword resignFirstResponder];
    
    if (IBTextFieldEmailID.text.length == 0) {
        [Helper displayAlertView:@"" message:@"Enter email address."];
        return;
    }
    if ([Helper IsValidEmail:IBTextFieldEmailID.text] == false) {
        
        [Helper displayAlertView:@"" message:@"Enter valid email address."];
        return;
    }
    if (IBTextFieldPassword.text.length == 0) {
        [Helper displayAlertView:@"" message:@"Enter password."];
        return;
    }
    
    //NSDictionary* dicParam=[[NSDictionary alloc] initWithObjectsAndKeys:IBTextFieldEmailID.text,@"Email",IBTextFieldPassword.text,@"Password",nil];
    
    [userInfoFacebook setObject:IBTextFieldEmailID.text forKey:@"Email"];
    [userInfoFacebook setObject:IBTextFieldPassword.text forKey:@"Password"];
    
    [self signUpWebService:userInfoFacebook];
}

-(void)signUpWebService:(NSMutableDictionary*)userInfo{
    
    UIActivityIndicatorView* activityIndicatorView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [activityIndicatorView startAnimating];
    
    UIView* viewForActivity = [[UIView alloc] initWithFrame:self.view.bounds];
    viewForActivity.backgroundColor =[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.5];
    
    activityIndicatorView.center = viewForActivity.center;
    [viewForActivity addSubview:activityIndicatorView];
    [self.view addSubview:viewForActivity];
    
    [self.view setUserInteractionEnabled:false];
    
    WSFrameWork* wsFrameWork=[[WSFrameWork alloc] initWithURLAndParams:@"http://likebit.com/freestyle/SignUp.php" dicParams:userInfo];
    
    wsFrameWork.isSync=false;
    
    wsFrameWork.WSDatatype=kJSON;
    
    wsFrameWork.onSuccess=^(NSDictionary* dic){
        
        NSLog(@"Registered user %@",dic);
        
        NSString* message =[dic objectForKey:@"message"];
        NSString* success =[dic objectForKey:@"success"];
        
        if ([success isEqualToString:@"1"]) {
            
            id unknown = [[dic objectForKey:@"response"] objectForKey:@"Id"];
            
            //  NSLog(@"%@",[unknown class]);
            
            [userInfo setObject:unknown forKey:@"UserId"];
            [Helper setPREFID:userInfo :@"prefUserLoginInfo"];
            
            UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
        }else{
            [Helper displayAlertView:@"" message:message];
        }
        
        [self.view setUserInteractionEnabled:true];
        [viewForActivity removeFromSuperview];
    };
    
    [wsFrameWork send];
}

#pragma mark - Alertview Delegete
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"] ];
            [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
            
            
            MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
            
            mFSideMenuContainerViewController.centerViewController = navigationCont;
            mFSideMenuContainerViewController.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
            
            [self presentViewController:mFSideMenuContainerViewController animated:true completion:nil];
        }
            break;
        default:
            break;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
