//
//  ForgotPasswordViewController.h
//  Freestyle
//
//  Created by BIT on 08/06/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ForgotPasswordViewController : UIViewController{
 AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldEmailID;

@end
