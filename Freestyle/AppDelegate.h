

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreData/CoreData.h>

#import "MZTimerLabel.h"

#import "Helper.h"
#import "Constant.h"
#import "MFSideMenuContainerViewController.h"
#import "MFSideMenu.h"
#import "WSFrameWork.h"
#import "BeachInfo.h"
#import "Macros.h"

#import "ProximityTag.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "FMDBServers.h"

#import "DBHelper.h"
#import "SikinLabel.h"

#import <GoogleMaps/GoogleMaps.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@class MSWeakTimer;
@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UIImagePickerControllerDelegate,MPMediaPickerControllerDelegate,UINavigationControllerDelegate>
{
    int flagcmd;//dharmesh  camera flag
    
    NSTimer *gAct_timer;
    float gAct_ticks;
    
    int gActAge;
    int gActWeight;
    int gActHeartRate;
    int gActHeight;
    CLLocation *gActStartLocation;
    CLLocation *gActTempLocation;
    CLLocation *gActEndLocation;
    CLLocationDistance gActDistance;
    CLLocationSpeed gActMinSpeed;
    CLLocationSpeed gActMaxSpeed;
    NSString* gActGender;
    
    double gActSeconds;
    double gActMinutes;
    double gActHours;
    double gActCalculateCalories;
    double dWeight;
    
    UILabel* lableTimer;
    
    int _prevMin;
}

@property(readwrite,nonatomic) double dWeight;
@property(readwrite,nonatomic)BOOL gActButtonStartSelectionState;
@property(readwrite,nonatomic)BOOL gActButtonPauseSelectionState;

@property (strong, nonatomic)NSTimer *gAct_timer;
@property(readwrite,nonatomic)float gAct_ticks;

@property(readwrite,nonatomic)int gActAge;
@property(readwrite,nonatomic)int gActWeight;
@property(readwrite,nonatomic)int gActHeartRate;
@property(readwrite,nonatomic)int gActHeight;
@property (strong, nonatomic)CLLocation *gActStartLocation;
@property (strong, nonatomic)CLLocation *gActTempLocation;
@property (strong, nonatomic)CLLocation *gActEndLocation;
@property(readwrite,nonatomic)CLLocationDistance gActDistance;
@property(readwrite,nonatomic)CLLocationSpeed gActMinSpeed;
@property(readwrite,nonatomic)CLLocationSpeed gActMaxSpeed;
@property (strong, nonatomic)NSString* gActGender;
@property (strong, nonatomic)NSString* gActUnit;
@property (strong, nonatomic)NSString* gActStartActivityName;
@property (readwrite, nonatomic) double gActCalculateCalories;
@property (readwrite, nonatomic) float gActMapViewZoomLevel;
@property(readwrite,nonatomic)int gActLastSaveActivityID;


@property(readwrite,nonatomic)double gActSeconds;
@property(readwrite,nonatomic)double gActMinutes;
@property(readwrite,nonatomic)double gActHours;

@property (strong,nonatomic) GMSMapView* gActCurrentMapView;
@property (strong,nonatomic) GMSMutablePath *gActLiveMutablePathList;



@property (strong,nonatomic) NSMutableArray* arrayOfLocations;
@property (readwrite,nonatomic) BOOL isMapOnScreen;


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSDictionary* dicWeatherData;
@property (strong, nonatomic) CLLocationManager *myLocationManager;
@property (strong, nonatomic) CLLocation *myLocation;
@property (strong, nonatomic) BeachInfo* firstNearestBeach;

//connection
@property (strong,nonatomic) ProximityTag *  appdelegateProximity;
@property (nonatomic, strong) NSString * reConnBtnTag;
- (void)saveContext;


// camera functionality

@property(nonatomic, strong) UIImagePickerController * pickerController;
@property(nonatomic, strong) NSString * xiangjiisopen;
@property(nonatomic, strong) NSString * cameraopen;
@property(nonatomic,strong) MSWeakTimer *timer;

@property(nonatomic,assign)int flagcmd;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, strong) NSMutableArray* arrayPreviousLocationTree;


- (NSURL *)applicationDocumentsDirectory;
// Public methods
- (NSString*)timeFormat:(float)value;

- (IBAction)cameraTakePicture:(id)sender;

-(void)getFirstNearestBeach;


// music functionality add

@property (nonatomic,retain)    NSString *flag;
@property (nonatomic,retain)    NSString *alertstr;

@property (nonatomic,strong) NSMutableArray *items;
@property (nonatomic)    int playNum;
@property (nonatomic, strong) AVAudioPlayer *player;
@property(nonatomic)    BOOL isPlayorPause;
@property(nonatomic,retain)IBOutlet UIImageView *img_song_app;
@property(nonatomic,retain)IBOutlet NSString *lbl_song_name_app;
@property(nonatomic,retain)IBOutlet NSString *lbl_song_detail_app;
@property(nonatomic,retain)IBOutlet NSString *btn_song_img_app;

// Public methods
- (void)initPlayer:(NSString*) audioFile fileExtension:(NSString*)fileExtension;
- (void)playAudio;
- (void)pauseAudio;
- (BOOL)isPlaying;
- (void)setCurrentAudioTime:(float)value;
- (float)getAudioDuration;
- (NSString*)timeFormat:(float)value;
- (NSTimeInterval)getCurrentAudioTime;


@property (nonatomic,strong) MPMusicPlayerController *mpc;

-(void)getActivityEndLocationDetails;

-(void)startTimer;

@property(nonatomic,strong) UILabel* lableTimer;

-(void)startTimerWithlable;
-(void)startTimerLable;
-(void)pauseTimerLabel;
-(void)stopTimerLabel;
-(void)resetTimerLabel;
-(void)timerTick;
-(void)WriteSportValue;
@end
