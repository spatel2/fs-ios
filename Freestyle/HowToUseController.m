//
//  HowToUseController.m
//  FYISportProject
//
//  Created by Hay on 14-1-2.
//  Copyright (c) 2014年 apple. All rights reserved.
//

#import "HowToUseController.h"

@interface HowToUseController ()

@end

@implementation HowToUseController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    if (iOS7) {
//        self.edgesForExtendedLayout = UIRectEdgeNone;
//    }
    
    self.navigationController.navigationBarHidden = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
