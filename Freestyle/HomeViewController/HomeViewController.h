

#import <UIKit/UIKit.h>
#import "DBHelper.h"
#import "SikinLabel.h"
#import "AppDelegate.h"
#import "SikinButton.h"
#import <GoogleMaps/GoogleMaps.h>


@interface HomeViewController : UIViewController{

    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintWidthForView;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewForMap;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewWeatherCondition;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelBeachTitle;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWeatherInfo;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelSurfHeight;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSurfHeight1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSurfHeight2;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelTideInfo;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelWaterTemp;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelWaterTemp1;

@property (strong, nonatomic) IBOutlet UILabel *IBLabelWindSpeed;
@property (strong, nonatomic) IBOutlet UILabel *IBLabelWindSpeed1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAirTemp;

@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewTideUpDown;
-(void)SendDataToWatch;
@end
