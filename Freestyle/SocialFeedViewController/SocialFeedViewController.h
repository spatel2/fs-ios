//
//  SocialFeedViewController.h
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FreestyleHelper.h"
@interface SocialFeedViewController : UIViewController<UIWebViewDelegate>{

            AppDelegate* appDelegate;
}
@property (strong, nonatomic) IBOutlet UIWebView *IBWebViewSocialFeed;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonTwitter;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonFacebook;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonInsta;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonPintrest;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonYoutube;

@end
