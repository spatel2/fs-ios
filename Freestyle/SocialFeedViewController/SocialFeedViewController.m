//
//  SocialFeedViewController.m
//  Freestyle
//
//  Created by BIT on 17/03/15.
//
//

#import "SocialFeedViewController.h"

@interface SocialFeedViewController ()

@end

@implementation SocialFeedViewController

@synthesize IBWebViewSocialFeed,IBButtonTwitter,IBButtonFacebook,IBButtonInsta,IBButtonPintrest,IBButtonYoutube;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    [self twitterButtonTouchUpInsideAction:IBButtonTwitter];

    [IBWebViewSocialFeed setDelegate:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - social Button Action

- (IBAction)twitterButtonTouchUpInsideAction:(UIButton *)sender {
   /*img_facebook_hover
    img_facebook
img_instagram_hover
img_instagram
img_pinterest_hover
img_pinterest
img_twitter_hover
img_twitter
img_youtube_hover
img_youtube*/
    
    [IBButtonFacebook setImage:[UIImage imageNamed:@"img_facebook"] forState:UIControlStateNormal];
    [IBButtonTwitter setImage:[UIImage imageNamed:@"img_twitter_hover"] forState:UIControlStateNormal];
    [IBButtonPintrest setImage:[UIImage imageNamed:@"img_pinterest"] forState:UIControlStateNormal];
    [IBButtonInsta setImage:[UIImage imageNamed:@"img_instagram"] forState:UIControlStateNormal];
    [IBButtonYoutube setImage:[UIImage imageNamed:@"img_youtube"] forState:UIControlStateNormal];
    
    NSURL*  url = [[NSURL alloc] initWithString:@"http://www.kennethcoletime.com/app_api/fs/twitter/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewSocialFeed  loadRequest:request];
}
- (IBAction)facebookButtonTouchUpInsideAction:(UIButton *)sender {
    
    [IBButtonFacebook setImage:[UIImage imageNamed:@"img_facebook_hover"] forState:UIControlStateNormal];
    [IBButtonTwitter setImage:[UIImage imageNamed:@"img_twitter"] forState:UIControlStateNormal];
    [IBButtonPintrest setImage:[UIImage imageNamed:@"img_pinterest"] forState:UIControlStateNormal];
    [IBButtonInsta setImage:[UIImage imageNamed:@"img_instagram"] forState:UIControlStateNormal];
    [IBButtonYoutube setImage:[UIImage imageNamed:@"img_youtube"] forState:UIControlStateNormal];
    
    NSURL*  url = [[NSURL alloc] initWithString:@"http://www.kennethcoletime.com/app_api/fs/facebook/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewSocialFeed  loadRequest:request];
}
- (IBAction)instagramButtonTouchUpInsideAction:(UIButton *)sender {
    [IBButtonFacebook setImage:[UIImage imageNamed:@"img_facebook"] forState:UIControlStateNormal];
    [IBButtonTwitter setImage:[UIImage imageNamed:@"img_twitter"] forState:UIControlStateNormal];
    [IBButtonPintrest setImage:[UIImage imageNamed:@"img_pinterest"] forState:UIControlStateNormal];
    [IBButtonInsta setImage:[UIImage imageNamed:@"img_instagram_hover"] forState:UIControlStateNormal];
    [IBButtonYoutube setImage:[UIImage imageNamed:@"img_youtube"] forState:UIControlStateNormal];
    
    NSURL*  url = [[NSURL alloc] initWithString:@"http://www.kennethcoletime.com/app_api/fs/instagram/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewSocialFeed  loadRequest:request];
}
- (IBAction)printrestButtonTouchUpInsideAction:(UIButton *)sender {
    [IBButtonFacebook setImage:[UIImage imageNamed:@"img_facebook"] forState:UIControlStateNormal];
    [IBButtonTwitter setImage:[UIImage imageNamed:@"img_twitter"] forState:UIControlStateNormal];
    [IBButtonPintrest setImage:[UIImage imageNamed:@"img_pinterest_hover"] forState:UIControlStateNormal];
    [IBButtonInsta setImage:[UIImage imageNamed:@"img_instagram"] forState:UIControlStateNormal];
    [IBButtonYoutube setImage:[UIImage imageNamed:@"img_youtube"] forState:UIControlStateNormal];
    
    NSURL*  url = [[NSURL alloc] initWithString:@"http://www.kennethcoletime.com/app_api/fs/pinterest/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewSocialFeed  loadRequest:request];
}
- (IBAction)youTubeButtonTouchUpInsideAction:(UIButton *)sender {
    
    [IBButtonFacebook setImage:[UIImage imageNamed:@"img_facebook"] forState:UIControlStateNormal];
    [IBButtonTwitter setImage:[UIImage imageNamed:@"img_twitter"] forState:UIControlStateNormal];
    [IBButtonPintrest setImage:[UIImage imageNamed:@"img_pinterest"] forState:UIControlStateNormal];
    [IBButtonInsta setImage:[UIImage imageNamed:@"img_instagram"] forState:UIControlStateNormal];
    [IBButtonYoutube setImage:[UIImage imageNamed:@"img_youtube_hover"] forState:UIControlStateNormal];
    
    NSURL*  url = [[NSURL alloc] initWithString:@"http://www.kennethcoletime.com/app_api/fs/youtube/feed.php"];
    NSURLRequest* request=[NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadRevalidatingCacheData timeoutInterval:5.0];
    [IBWebViewSocialFeed  loadRequest:request];
}


#pragma mark - WebView Delegate
-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}


#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    
    //self.menuContainerViewController.toggleLeftSideMenuCompletion
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
