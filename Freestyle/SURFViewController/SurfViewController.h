

#import <UIKit/UIKit.h>
#import "DBHelper.h"
#import "AppDelegate.h"


@interface SurfViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet UIButton *IBButtonAddSurfLocation;

@property (strong, nonatomic) IBOutlet UIButton *IBButtonBackToLocation;

@property (strong, nonatomic) IBOutlet UITableView *IBTableViewBeaches;
@property (strong, nonatomic) IBOutlet UIView *IBViewFooter;
@end
