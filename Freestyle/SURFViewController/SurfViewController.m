

#import "SurfViewController.h"
#import "SurfSubAddViewController.h"

#import "MFSideMenu.h"
#import "BeachInfo.h"

#import "FreestyleHelper.h"
#import "Constant.h"
#import "WeatherDetailOfBeachViewController.h"
#import "SurfTableViewCell.h"




@interface SurfViewController ()

@end

@implementation SurfViewController{
    
    //tide variabel
    int indexOfCurrentTide;
    
    int _firstMinIndex;
    int _firstMaxIndex;
    int _secondMinIndex;
    int _secondMaxIndex;
    NSMutableArray* arrayTideTime;
    
    
    //NSArray* arraySelectedBeaches;
    NSMutableDictionary* dicOfSelectedBeaches;
    NSMutableArray* arrayOfKeys;
    
    NSDictionary* dicWeatherInfo;
    //**************************testing surf************************************************//
    NSString *Location;
    NSString *WeatherType;
    
    int AirTempMin;
    int AirTempMax;
    NSString *AirTemperature;
    NSString *AirTemperaturemax;
    
    int WaterTempMin;
    int WaterTempMax;
    NSString *WaterTemperature;
    NSString *WaterTemperaturemax;
    
    NSString *Sunrise1;
    NSString *Sunset1;
    
    NSString *TideLastTimeString;
    float		TideLastSizeFloat;
    NSString	*TideLastSizeString;
    BOOL TideLastisHi;
    
    NSString   *TideNextTimeString;
    float	TideNextSizeFloat;
    NSString *TideNextSizeString;
    BOOL	TideNextisHi;
    
    NSString	*TideNextAfterNextTimeString;
    float		TideNextAfterNextSizeFloat;
    NSString	*TideNextAfterNextSizeString;
    BOOL TideNextAfterNextisHi;
    
    float SurfTempMin;
    float SurfTempMax;
    NSString *SurfSize;
    
    float WindTempMin;
    float WindTempMax;
    NSString *WindSpeed;
    
    int WindTempDirection;
    NSString *WindDirection;
    
    int SwellTempDirection;
    NSString *SwellDirection;
    
    int SwellTempPeriod;
    NSString *SwellPeriod;
    
    BOOL isMetric;
    int SpotID;
    
    NSMutableArray *arr0;
    NSMutableArray *arr1;
    NSMutableArray *arr2;
    
    float lastMinimumTide;
    
    
}

@synthesize IBButtonAddSurfLocation,IBTableViewBeaches;

#pragma mark - View Controller Life Cycle

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    //[FreestyleHelper setNavigationTitleView:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil  action:nil]];
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [self setViewData];
    
    IBTableViewBeaches.tableFooterView = self.IBViewFooter;
    IBTableViewBeaches.delegate = self;
    IBTableViewBeaches.dataSource = self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    DBHelper* dbHelper =[DBHelper getSharedInstance];
    arrayTideTime = [[NSMutableArray alloc] init];
    dicOfSelectedBeaches = [dbHelper dicOfBeaches];
    
    
    NSArray* arr=[Helper getPREFID:@"arrayOfkeysForAddedSurf"];
    
    if (arr != nil){
        arrayOfKeys =[[NSMutableArray alloc] initWithArray:arr];
        
        if (arrayOfKeys.count == 1) {
            BeachInfo* beachInfo =[dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:0]];
            [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
            Location=beachInfo.title;
            [self getWeatherForecast];
        }
    }
    else{
        arrayOfKeys=[[NSMutableArray alloc] init];
        
        for (NSString* key in [dicOfSelectedBeaches allKeys]) {
            [arrayOfKeys addObject:key];
        }
    }
    
    [IBTableViewBeaches reloadData];
    [IBTableViewBeaches setEditing:true];
    
    
    if (appDelegate.arrayPreviousLocationTree.count == 3) {
        [_IBButtonBackToLocation setAlpha:1.0];
    }else{
        [_IBButtonBackToLocation setAlpha:0];
    }
    
}

-(void)saveUserOrder{
    
    if (arrayOfKeys.count > 0) {
        [Helper setPREFID:arrayOfKeys :@"arrayOfkeysForAddedSurf"];
    }
    
    
    /*
     for (int i=0; i< arraySelectedBeaches.count; i++) {
     
     SurfTableViewCell *cell = (SurfTableViewCell*)[IBTableViewBeaches cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
     
     [Helper setPREFStringValue:cell.title.text sKey:[NSString stringWithFormat:@"%d",i]];
     
     if (i == 0) {
     
     BeachInfo* beachInfo = [dicOfSelectedBeaches objectForKey:cell.title.text];
     [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
     }
     
     }
     [Helper setPREFint:(int)arraySelectedBeaches.count :@"lastIndexPathRow"];
     
     */
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setViewData{
    
    
}
- (IBAction)addSurfButtonTouchUpInsideAction:(UIButton *)sender{
    [self performSegueWithIdentifier:@"segueSurfAddViewController" sender:@"addSurf"];
}
- (IBAction)goToPreviousSubRegion:(UIButton*)sender{
    
    
    if (appDelegate.arrayPreviousLocationTree.count ==3) {
        
    
    NSMutableArray* arry=[[NSMutableArray alloc] init];
    for (NSDictionary* dic in [appDelegate.dicWeatherData objectForKey:@"global"]){
        [arry addObject: [dic objectForKey:@"area"]];
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSMutableArray* arrayLoaction = [NSMutableArray arrayWithArray:[arry sortedArrayUsingDescriptors:@[sort]]];
    
//    NSMutableArray* arry=[[NSMutableArray alloc] init];
//
    [arry removeAllObjects];
    for (NSDictionary* dic in [[arrayLoaction objectAtIndex:[[appDelegate.arrayPreviousLocationTree objectAtIndex:0] integerValue]] objectForKey:@"content"]){
        [arry addObject: [dic objectForKey:@"region"]];
    }
//
    //NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//
//    surfSubAddViewController.arraySport = [arry sortedArrayUsingDescriptors:@[sort]];
//    surfSubAddViewController.sTitle =@"region";
    
    [arrayLoaction removeAllObjects];
    arrayLoaction = nil;
    arrayLoaction = [NSMutableArray arrayWithArray:[arry sortedArrayUsingDescriptors:@[sort]]];
    
//    NSMutableArray* arry=[[NSMutableArray alloc] init];
//    

    [arry removeAllObjects];
    for (NSDictionary* dic in [[arrayLoaction objectAtIndex:[[appDelegate.arrayPreviousLocationTree objectAtIndex:1] integerValue]] objectForKey:@"content"]){
        
        [arry addObject: [dic objectForKey:@"subregion"]];
        
    }
//
    NSSortDescriptor *sortName = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
//
//    [arrayLoaction removeAllObjects];
//    arrayLoaction = nil;
//    arrayLoaction = [NSMutableArray arrayWithArray:[arry sortedArrayUsingDescriptors:@[sortName]]];
    
    SurfSubAddViewController* surfSubAddViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"surfSubAddViewController"];
    
     NSMutableArray* arry11=[NSMutableArray arrayWithArray:[arry sortedArrayUsingDescriptors:@[sortName]]];
        
    surfSubAddViewController.sTitle = @"spots";
    surfSubAddViewController.sRegion = [[arrayLoaction objectAtIndex:[[appDelegate.arrayPreviousLocationTree objectAtIndex:1] integerValue]]  objectForKey:@"name"];
        
        
        NSString* indexaas = [appDelegate.arrayPreviousLocationTree objectAtIndex:2];
        
        
        surfSubAddViewController.sSubRegion = [[arry11 objectAtIndex:[indexaas integerValue]]  objectForKey:@"name"];
       // surfSubAddViewController.sRegion = sRegion;
        NSMutableArray* arry111= [NSMutableArray arrayWithArray:[[arry11 objectAtIndex:[indexaas integerValue]] objectForKey:@"spots"]];
        
        
        NSSortDescriptor *sortFINAL = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
        
        surfSubAddViewController.arraySport = [arry111 sortedArrayUsingDescriptors:@[sortFINAL]];
        
        
        
        
        
        
        
        
        
    [self.navigationController pushViewController:surfSubAddViewController animated:true];
    
    }
    
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return dicOfSelectedBeaches.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SurfTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"beachCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    //NSString* beachName = [Helper getPREF:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    //NSLog(@"%@",arraySelectedBeaches.description);
    //NSLog(@"%@",dicOfSelectedBeaches.description);
    
    
    NSString* beachName =[arrayOfKeys objectAtIndex:indexPath.row];
    
    BeachInfo* beachInfo;
    if (beachName!=nil && beachName.length >0 ) {
        
        beachInfo = (BeachInfo*)[dicOfSelectedBeaches objectForKey:beachName];
        
    }
    //    else{
    //
    //        beachInfo  = [arraySelectedBeaches objectAtIndex:indexPath.row];
    //
    //    }
    
    if (indexPath.row == 0) {
        cell.title.textColor = [UIColor colorWithRed:0/255.0f green:107.0/255.0 blue:207.0/255.0 alpha:1];
        cell.subRegionTitle.textColor = [UIColor colorWithRed:0/255.0 green:107.0/255.0 blue:207.0/255.0 alpha:1];
        [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
        
    }
    else
    {
        
        cell.title.textColor = [UIColor whiteColor];
        cell.subRegionTitle.textColor = [UIColor whiteColor];
    }
    cell.title.text = beachInfo.title;
    //cell.textLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
    //cell.detailTextLabel.text = beachInfo.subRegion;
    
    cell.subRegionTitle.text = beachInfo.subRegion;
    //cell.detailTextLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
    
    cell.IBButtonDelete.tag = indexPath.row + 1;
    cell.IBButtonAddHome.tag = indexPath.row + 1;
    
    if ([beachInfo.iDentity isEqualToString:[Helper getPREF:@"HomeBeach"]]) {
        [cell.IBButtonAddHome setImage:[UIImage imageNamed:@"img_surf_home"] forState:UIControlStateNormal];
    }else{
        [cell.IBButtonAddHome setImage:[UIImage imageNamed:@"img_surf_notHome"] forState:UIControlStateNormal];
    }
    
    
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 58.0;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    return 1;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    return 0.1;
//}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    SurfTableViewCell *cell = (SurfTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"segueWeatherDetailOfBeachViewController" sender:[dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:indexPath.row]]];
    
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}
-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

#pragma mark Row reordering
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    NSString* key = [arrayOfKeys objectAtIndex:fromIndexPath.row];
    [arrayOfKeys removeObjectAtIndex:fromIndexPath.row];
    [arrayOfKeys insertObject:key atIndex:toIndexPath.row];
    
    //    NSString *item=[myKeys objectAtIndex:fromIndexPath.row];
    //    [myKeys removeObject:item];
    //    [myKeys insertObject:item atIndex:toIndexPath.row];
    
    // [arrayRearranged removeAllObjects];
    //arrayRearranged = nil;
    //arrayRearranged = [[NSMutableArray alloc] initWithArray:arraySelectedBeaches];
    
    //Get original id
    /*
     BeachInfo*  original = [arraySelectedBeaches objectAtIndex:fromIndexPath.row];
     BeachInfo* destination = [arraySelectedBeaches objectAtIndex:toIndexPath.row];
     
     // [arraySelectedBeaches replaceObjectAtIndex:fromIndexPath.row withObject:destination];
     //Get destination id
     
     
     
     if (fromIndexPath.row > toIndexPath.row) {
     
     [arraySelectedBeaches removeObjectAtIndex:fromIndexPath.row];
     [arraySelectedBeaches insertObject:original atIndex:toIndexPath.row];
     if(arraySelectedBeaches.count >= toIndexPath.row + 1){
     
     [arraySelectedBeaches insertObject:destination atIndex:toIndexPath.row + 1];
     }
     }
     */
    //[arraySelectedBeaches replaceObjectAtIndex:fromIndexPath.row withObject:destination];
    
    
    
    /*
     BeachInfo* beach = [arraySelectedBeaches objectAtIndex:fromIndexPath.row];
     [arraySelectedBeaches removeObjectAtIndex:fromIndexPath.row];
     beach = [arraySelectedBeaches objectAtIndex:toIndexPath.row];
     [arraySelectedBeaches insertObject:beach atIndex:toIndexPath.row];
     */
    
    /*
     for (int i=0; i< arraySelectedBeaches.count; i++) {
     
     SurfTableViewCell *cell = (SurfTableViewCell*)[IBTableViewBeaches cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
     NSLog(@"%@",cell.title.text);
     
     [Helper setPREFStringValue:cell.title.text sKey:[NSString stringWithFormat:@"%d",i]];
     
     }
     [self.IBTableViewBeaches reloadData];
     */
    
}
- (void)tableView:(UITableView *)tableView didEndReorderingRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [IBTableViewBeaches reloadData];
    [self saveUserOrder];
    
    
    if (indexPath.row == 0) {
        BeachInfo* beachInfo = [dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:indexPath.row]];
        [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
        Location=Location=beachInfo.title;
        [self getWeatherForecast];
    }
}

#pragma mark - HomeImageTap Action
- (IBAction)addToHomeButtonTouchUpInside:(UIButton *)sender
{
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.IBTableViewBeaches];
    NSIndexPath *indexPath = [IBTableViewBeaches indexPathForRowAtPoint:rootViewPoint];
    
    if(indexPath.row > 0){
        
        BeachInfo* beachInfo = [dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:indexPath.row]];
        
        [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
        [Helper displayAlertView:@"" message:@"Beach selected for home screen"];
        
        /*
         NSString* beachName = [Helper getPREF:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
         for (long i = indexPath.row - 1; i > -1; i--) {
         
         NSString* sValue = [Helper getPREF:[NSString stringWithFormat:@"%ld",i]];
         [Helper setPREFStringValue:sValue sKey:[NSString stringWithFormat:@"%ld",i+1]];
         
         }
         [Helper setPREFStringValue:beachName sKey:@"0"];
         
         //[self saveUserOrder];
         */
        [arrayOfKeys removeObject:[NSString stringWithFormat:@"%@_%@",beachInfo.title,beachInfo.iDentity]];
        
        [arrayOfKeys insertObject:[NSString stringWithFormat:@"%@_%@",beachInfo.title,beachInfo.iDentity] atIndex:0];
        
        [IBTableViewBeaches reloadData];
        
        [self saveUserOrder];
        
        Location=beachInfo.title;
        [self getWeatherForecast];
        
    }
    //[self performSegueWithIdentifier:@"segueWeatherDetailOfBeachViewController" sender:beachInfo];
    
    
}

#pragma mark - Navigation Bar Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)deleteRowButtonTouchUpInside:(UIButton *)sender {
    
    CGPoint center= sender.center;
    CGPoint rootViewPoint = [sender.superview convertPoint:center toView:self.IBTableViewBeaches];
    NSIndexPath *indexPath = [IBTableViewBeaches indexPathForRowAtPoint:rootViewPoint];
    
    //    SurfTableViewCell *cell = (SurfTableViewCell*)[IBTableViewBeaches cellForRowAtIndexPath:indexPath];
    BeachInfo* beachInfo = (BeachInfo*)[dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:indexPath.row]];
    
    DBHelper* dbHelper =[DBHelper getSharedInstance];
    [dbHelper deleteBeach:beachInfo.iDentity];
    
    [Helper displayAlertView:@"" message:@"Beach removed from favorite"];
    [arrayOfKeys removeObject:[NSString stringWithFormat:@"%@_%@",beachInfo.title,beachInfo.iDentity]];
    [dicOfSelectedBeaches removeObjectForKey:[NSString stringWithFormat:@"%@_%@",beachInfo.title,beachInfo.iDentity]];
    
    if (arrayOfKeys.count == 0) {
        [Helper delPREF:@"HomeBeach"];
        [Helper delPREF:@"arrayOfkeysForAddedSurf"];
    }
    
    [IBTableViewBeaches reloadData];
    [self saveUserOrder];
    
    if (indexPath.row == 0)
    {
        BeachInfo* beachInfo = [dicOfSelectedBeaches objectForKey:[arrayOfKeys objectAtIndex:indexPath.row]];
        [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
        Location=beachInfo.title;
        [self getWeatherForecast];
    }
    
    /*
     NSString* beachID = [Helper getPREF:@"HomeBeach"];
     
     if ([beachID isEqualToString:beachInfo.iDentity]) {
     [Helper delPREF:@"HomeBeach"];
     }*/
    /*
     for (long i = indexPath.row + 1; i < arraySelectedBeaches.count; i++) {
     
     NSString* sValue = [Helper getPREF:[NSString stringWithFormat:@"%ld",i]];
     [Helper setPREFStringValue:sValue sKey:[NSString stringWithFormat:@"%ld",i-1]];
     if (i-1 == 0) {
     BeachInfo* beachInfo = (BeachInfo*)[dicOfSelectedBeaches objectForKey:sValue];
     [Helper setPREFStringValue:beachInfo.iDentity sKey:@"HomeBeach"];
     }
     }
     
     [Helper delPREF:[NSString stringWithFormat:@"%lu",(unsigned long)[arraySelectedBeaches count]]];
     [dicOfSelectedBeaches removeAllObjects];
     dicOfSelectedBeaches = [dbHelper dicOfBeaches];
     arraySelectedBeaches = [dicOfSelectedBeaches allValues];
     
     [IBTableViewBeaches reloadData];
     
     [Helper setPREFint:(int)arraySelectedBeaches.count :@"lastIndexPathRow"];
     
     // [self saveUserOrder];
     
     */
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using
    // Pass the selected object to the new view controller.
    
    
    if ([sender isKindOfClass:[BeachInfo class]] == true) {
        
        WeatherDetailOfBeachViewController* weatherDetailOfBeachViewController = [segue destinationViewController];
        weatherDetailOfBeachViewController.beachInfo = sender;
    }
}

#pragma mark - set Data For Watch
#pragma mark -
-(void)getWeatherForecast{
    
    NSDictionary *dicParam;
    
    
    DBHelper* dbHelper = [DBHelper getSharedInstance];
    NSString* beachID = [Helper getPREF:@"HomeBeach"];
    BeachInfo* homeBeach = [dbHelper getBeachFromID:beachID];
    
    if(appDelegate.gActUnit != nil){
        if ([appDelegate.gActUnit isEqualToString:@"IMPERIAL"]) {
            dicParam = @{
                         @"package": @"partner",
                         @"user_key" : @"abca50124d77d41504c4d53e29c8d359"
                         };
        }else if([appDelegate.gActUnit isEqualToString:@"METRIC"]){
            dicParam = @{
                         @"package": @"partner",
                         @"user_key" : @"abca50124d77d41504c4d53e29c8d359",
                         @"units": @"m"
                         };
        }
    }
    WSFrameWork *wsWeather = [[WSFrameWork alloc] initWithURLAndParams:[NSString stringWithFormat:@"forecasts/%@",homeBeach.iDentity]  dicParams:dicParam];
    //wsWeather.sDomainName = @"http://slapi01.surfline.com/v1/";
    wsWeather.sDomainName = @"http://api.surfline.com/v1/";
    wsWeather.WSType = kGET;
    wsWeather.isLogging = false;
    
    
    wsWeather.onSuccess = ^(NSDictionary *dicResponce)
    {
        //NSLog(@"==%@",dicResponce);
        //NSLog(@"dicResponce Class :: %@",[dicResponce class]);
        dicWeatherInfo = dicResponce;
        
        //   id objs = [dicResponce allValues];
        NSString* beachID = [Helper getPREF:@"HomeBeach"];
        if ([beachID isEqualToString:[dicWeatherInfo objectForKey:@"id"]]) {
            [self setTodayData:nil];
        }
        
    };
    [wsWeather send];
    
}
-(IBAction)setTodayData:(id)sender
{
    double dtimeZone =[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"timezone"] doubleValue];
    
    //Time formatter
    NSDateFormatter *tempTimeFormatter = [[NSDateFormatter alloc] init];
    [tempTimeFormatter setDateFormat:@"MMMM dd, yyyy HH:mm:ss"];
    NSTimeZone *tempTimeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [tempTimeFormatter setTimeZone:tempTimeZone];
    
    //surf height
    //-----------------------------------
    //get array element to pick
    NSDateFormatter *surfTimeFormatter = [[NSDateFormatter alloc] init];
    [surfTimeFormatter setDateFormat:@"HHmm"];
    
    NSTimeZone *timeZoneSurf = [NSTimeZone timeZoneWithName:@"UTC"];
    [surfTimeFormatter setTimeZone:timeZoneSurf];
    
    //    NSDate *dSurfDate = [NSDate dateWithTimeIntervalSince1970:[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"startDate_GMT"] doubleValue]];
    //    dSurfDate = [dSurfDate dateByAddingTimeInterval:60*60*dtimeZone];
    NSDate *dSurfDate = [tempTimeFormatter dateFromString:[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"startDate_pretty_LOCAL"]];
    
    NSString *surfString = [surfTimeFormatter stringFromDate:dSurfDate];
    int iSurfArrElement;
    
    if ([surfString intValue] < 0700) {
        iSurfArrElement = 0;
    }
    else if ([surfString intValue] >= 0700 && [surfString intValue] < 1400) {
        iSurfArrElement = 1;
    }
    else if ([surfString intValue] >= 1400 && [surfString intValue] < 1900) {
        iSurfArrElement = 2;
    }
    else if ([surfString intValue] >= 1900) {
        iSurfArrElement = 3;
    }
    //SSW = surf – swell_direction1[0] [array value]
    SwellTempDirection = [[[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"swell_direction1"] objectAtIndex:0] objectAtIndex:iSurfArrElement] intValue];
    SwellDirection=[self getDirection:SwellTempDirection];
    /*
     switch (SwellTempDirection)
     {
     case 0: SwellDirection =@"N"; break;
     case 1: SwellDirection =@"NNE"; break;
     case 2: SwellDirection =@"NE"; break;
     case 3: SwellDirection =@"ENE"; break;
     case 4: SwellDirection =@"E"; break;
     case 5: SwellDirection =@"ESE"; break;
     case 6: SwellDirection =@"SE"; break;
     case 7: SwellDirection =@"SSE"; break;
     case 8: SwellDirection =@"S"; break;
     case 9: SwellDirection =@"SSW"; break;
     case 10: SwellDirection =@"SW"; break;
     case 11: SwellDirection =@"WSW";break;
     case 12: SwellDirection =@"W"; break;
     case 13: SwellDirection =@"WNW";break;
     case 14: SwellDirection =@"NW"; break;
     case 15: SwellDirection =@"NNW";break;
     default: SwellDirection =@"N/A";break;
     }*/
    
    
    //5 – 7 ft = surf – surf_min[0] [array value] – surf_max[0] [array value]
    //get unit from settings
    NSString* sUnitForHeight = @"ft";
    isMetric = FALSE;
    if ([APP_DELEGATE.gActUnit isEqualToString:@"IMPERIAL"]) {
        sUnitForHeight = @"ft";
        isMetric = FALSE;
    }else if([APP_DELEGATE.gActUnit isEqualToString:@"METRIC"]){
        sUnitForHeight = @"m";
        isMetric = TRUE;
    }
    float fSurfMin = [[[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"surf_min"] objectAtIndex:0] objectAtIndex:iSurfArrElement] floatValue];
    float fSurfMax = [[[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"surf_max"] objectAtIndex:0] objectAtIndex:iSurfArrElement] floatValue];
    
    
    SurfTempMin=[[NSString stringWithFormat:@"%.1f",fSurfMin] floatValue];
    SurfTempMax=[[NSString stringWithFormat:@"%.1f",fSurfMax] floatValue];
    
    //1ft @8s ESE
    //1ft = surf – swell_height1[0] [array value]
    //8s = surf – swell_period1[0] [array value]
    //ESE = surf – swell_direction1 [0] [array value]
    
    float fSurfHeight = [[[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"swell_height1"] objectAtIndex:0] objectAtIndex:iSurfArrElement] floatValue];
    NSString *sSwellPeriod = [NSString stringWithFormat:@"%.0f", [[[[[dicWeatherInfo objectForKey:@"Surf"] objectForKey:@"swell_period1"] objectAtIndex:0] objectAtIndex:iSurfArrElement] floatValue]];
    SwellTempPeriod=[sSwellPeriod intValue];
    
    
    
    //air temp
    //-----------------------------------
    NSString* sUnitForWaterAndAirTemp = @"f";
    isMetric = FALSE;
    
    if ([appDelegate.gActUnit isEqualToString:@"IMPERIAL"]) {
        sUnitForWaterAndAirTemp = @"f";
        isMetric = FALSE;
        
    }else if([appDelegate.gActUnit isEqualToString:@"METRIC"]){
        sUnitForWaterAndAirTemp = @"c";
        isMetric = TRUE;
    }
    NSString* weatherType = [[[[dicWeatherInfo objectForKey:@"Weather"] objectForKey:@"weather_type"] objectAtIndex:0] uppercaseString];
    WeatherType=[[[[dicWeatherInfo objectForKey:@"Weather"] objectForKey:@"weather_type"] objectAtIndex:0] uppercaseString];
    
    
    AirTemperature=[NSString stringWithFormat:@"%.0f",[[[[dicWeatherInfo objectForKey:@"Weather"] objectForKey:@"temp_min"] objectAtIndex:0] floatValue]];
    AirTemperaturemax=[NSString stringWithFormat:@"%.0f",[[[[dicWeatherInfo objectForKey:@"Weather"] objectForKey:@"temp_max"] objectAtIndex:0] floatValue]];
    
    //water temp
    //------------------------------
    
    WaterTemperature=[NSString stringWithFormat:@"%.0f ˚%@",[[[dicWeatherInfo objectForKey:@"WaterTemp"] objectForKey:@"watertemp_max"] floatValue],sUnitForWaterAndAirTemp];
    WaterTemperaturemax=[NSString stringWithFormat:@"%.0f ˚%@",[[[dicWeatherInfo objectForKey:@"WaterTemp"] objectForKey:@"watertemp_min"] floatValue],sUnitForWaterAndAirTemp];
    
    NSMutableArray * sortedList = [[NSMutableArray alloc] init];
    
    //wind
    //-----------------------------------------
    NSDateFormatter *windTimeFormatter = [[NSDateFormatter alloc] init];
    [windTimeFormatter setDateFormat:@"HHmm"];
    
    NSTimeZone *timeZoneWind = [NSTimeZone timeZoneWithName:@"UTC"];
    [windTimeFormatter setTimeZone:timeZoneWind];
    
    //    NSDate *dWindDate = [NSDate dateWithTimeIntervalSince1970:[[[dicWeatherInfo objectForKey:@"Wind"] objectForKey:@"startDate_GMT"] doubleValue]];
    //    dWindDate = [dWindDate dateByAddingTimeInterval:60*60*dtimeZone];
    NSDate *dWindDate = [tempTimeFormatter dateFromString:[[dicWeatherInfo objectForKey:@"Wind"] objectForKey:@"startDate_pretty_LOCAL"]];
    
    NSString *windString = [windTimeFormatter stringFromDate:dWindDate];
    int iWindArrElement;
    
    if ([windString intValue] < 0200) {
        iWindArrElement = 0;
    }
    else if ([windString intValue] >= 0200 && [windString intValue] < 0500) {
        iWindArrElement = 1;
    }
    else if ([windString intValue] >= 0500 && [windString intValue] < 800) {
        iWindArrElement = 2;
    }
    else if ([windString intValue] >= 800 && [windString intValue] < 1200) {
        iWindArrElement = 3;
    }
    else if ([windString intValue] >= 1200 && [windString intValue] < 1500) {
        iWindArrElement = 4;
    }
    else if ([windString intValue] >= 1500 && [windString intValue] < 1800) {
        iWindArrElement = 5;
    }
    else if ([windString intValue] >= 1800 && [windString intValue] < 2100) {
        iWindArrElement = 6;
    }
    else if ([windString intValue] >= 2100) {
        iWindArrElement = 7;
    }
    
    //SSW = wind – wind_direction[0] [array value]
    WindTempDirection = [[[[[dicWeatherInfo objectForKey:@"Wind"] objectForKey:@"wind_direction"] objectAtIndex:0] objectAtIndex:iWindArrElement] intValue];
    NSString* sWindDirection=[self getDirection:WindTempDirection];
    /*
     switch (WindTempDirection)
     {
     case 0: sWindDirection =@"N"; break;
     case 1: sWindDirection =@"NNE"; break;
     case 2: sWindDirection =@"NE"; break;
     case 3: sWindDirection =@"ENE"; break;
     case 4: sWindDirection =@"E"; break;
     case 5: sWindDirection =@"ESE"; break;
     case 6: sWindDirection =@"SE"; break;
     case 7: sWindDirection =@"SSE"; break;
     case 8: sWindDirection =@"S"; break;
     case 9: sWindDirection =@"SSW"; break;
     case 10: sWindDirection =@"SW"; break;
     case 11: sWindDirection =@"WSW";break;
     case 12: sWindDirection =@"W"; break;
     case 13: sWindDirection =@"WNW";break;
     case 14: sWindDirection =@"NW"; break;
     case 15: sWindDirection =@"NNW";break;
     default: sWindDirection =@"N/A";break;
     }*/
    
    //5 – 7 ft = wind – wind_speed [0] [array value] – (wind_speed [0] [array value] * 1.3)
    NSString* sUnitForWindSpeed = @"kts";
    isMetric = FALSE;
    
    if ([APP_DELEGATE.gActUnit isEqualToString:@"IMPERIAL"]) {
        sUnitForWindSpeed = @"kts";
        isMetric = FALSE;
    }else if([APP_DELEGATE.gActUnit isEqualToString:@"METRIC"]){
        sUnitForWindSpeed = @"km/h";
        isMetric = TRUE;
    }
    float fWindSpeedMin = [[[[[dicWeatherInfo objectForKey:@"Wind"] objectForKey:@"wind_speed"] objectAtIndex:0] objectAtIndex:iWindArrElement] floatValue];
    float fWindSpeedMax = [[[[[dicWeatherInfo objectForKey:@"Wind"] objectForKey:@"wind_speed"] objectAtIndex:0] objectAtIndex:iWindArrElement] floatValue] * 1.3;
    
    WindTempMin=[[NSString stringWithFormat:@"%.0f",fWindSpeedMin] floatValue];
    WindTempMax=[[NSString stringWithFormat:@"%.0f",fWindSpeedMax] floatValue];
    
    //tide
    //-----------------------------------
    [self initTideData:@"today"];
    
    NSTimeZone *timeZoneTide = [NSTimeZone timeZoneWithName:@"UTC"];
    NSDateFormatter *timeFormatterTide = [[NSDateFormatter alloc] init];
    [timeFormatterTide setDateFormat:@"MMMM dd, yyyy HH:mm:ss"];
    [timeFormatterTide setTimeZone:timeZoneTide];
    
    NSString *sCurDate = [timeFormatterTide stringFromDate:[NSDate date]];
    NSDate *dCurDate = [timeFormatterTide dateFromString:sCurDate];
    dCurDate = [dCurDate dateByAddingTimeInterval:60*60*dtimeZone];
    
    for (NSDictionary* dicPoint in [[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"dataPoints"])
    {
        NSString *sTideUTC = [dicPoint objectForKey:@"utctime"];
        NSDate *dTideDate = [timeFormatterTide dateFromString:sTideUTC];
        dTideDate = [dTideDate dateByAddingTimeInterval:60*60*dtimeZone];
        
        //        NSString *tType = [[dicPoint objectForKey:@"type"] uppercaseString];
        //        // NSLog(@"%@",tType);
        //        if([tType isEqualToString:@"HIGH"])
        //        {
        //            TideNextSizeFloat=[[NSString stringWithFormat:@"%.1f",[[dicPoint objectForKey:@"height"] floatValue]]floatValue];
        //        }
        //
        
        NSComparisonResult result2 = [dCurDate compare:dTideDate];
        
        if(result2==NSOrderedAscending)
        {
            NSString *tType = [[dicPoint objectForKey:@"type"] uppercaseString];
            if (!([tType isEqualToString:@"NORMAL"] || [tType isEqualToString:@"HIGH"] || [tType isEqualToString:@"LOW"])) {
                continue;
            }
            // TideLastSizeFloat=[[NSString stringWithFormat:@"%.1f",[[dicPoint objectForKey:@"height"] floatValue]]floatValue];
            
            break;
        }
    }
    TideLastisHi		= false;
    TideNextisHi		= true;
    /*
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setDateFormat:@"yyyy-MM-dd"];
     NSTimeZone *timezone = [NSTimeZone timeZoneWithName:@"UTC"];
     [dateFormatter setTimeZone:timezone];
     [sortedList removeAllObjects];
     
     for (NSDictionary* dicPoint in [[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"dataPoints"]) {
     
     NSString *tType = [[dicPoint objectForKey:@"type"] uppercaseString];
     if (!([tType isEqualToString:@"NORMAL"] || [tType isEqualToString:@"HIGH"] || [tType isEqualToString:@"LOW"])) {
     continue;
     }
     
     NSDate *tmpDate= [NSDate dateWithTimeIntervalSince1970:[[dicPoint objectForKey:@"time"] doubleValue]];
     
     tmpDate = [tmpDate dateByAddingTimeInterval:60*60*dtimeZone];
     
     NSDate *dNow = [[NSDate date] dateByAddingTimeInterval:60*60*dtimeZone];
     
     if ([[dateFormatter stringFromDate:tmpDate] isEqualToString:[dateFormatter stringFromDate:dNow]])
     {
     NSNumber *height = [dicPoint objectForKey:@"height"];
     [sortedList insertNumberAtSortedLocation: height];
     }
     }
     if([[sortedList firstObject] floatValue] >= 0)
     {
     IBLabelTideInfo.text = [NSString stringWithFormat:@"%.1f - %.1f %@",[[sortedList firstObject] floatValue],[[sortedList lastObject] floatValue],sUnitForHeight];
     self.IBImageViewTideUpDown.image =[UIImage imageNamed:@"tide_up"];
     }
     else {
     IBLabelTideInfo.text = [NSString stringWithFormat:@"%.1f - %.1f %@",[[sortedList firstObject] floatValue],[[sortedList lastObject] floatValue],sUnitForHeight];
     self.IBImageViewTideUpDown.image =[UIImage imageNamed:@"tide_down"];
     }
     
     TideLastSizeFloat=[[NSString stringWithFormat:@"%.1f",[[sortedList firstObject] floatValue]]floatValue];
     TideNextSizeFloat=[[NSString stringWithFormat:@"%.1f",[[sortedList lastObject] floatValue]] floatValue];
     TideLastisHi		= false;
     TideNextisHi		= true;
     */
    
    //    if ([[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"] count] > 1) {
    //        NSDateFormatter *timeFormatter1 = [[NSDateFormatter alloc] init];
    //        [timeFormatter1 setDateFormat:@"hh:mm"];
    //        NSTimeInterval tidefirst = [[[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"] objectAtIndex:0] objectForKey:@"time"] doubleValue];
    //        NSDate *dateRise = [NSDate dateWithTimeIntervalSince1970:tidefirst];
    //        NSString *timeRise = [timeFormatter1 stringFromDate:dateRise];
    //       // TideLastTimeString=[NSString stringWithFormat:@"%@ AM RISE", timeRise];
    //
    //        NSTimeInterval tidelast = [[[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"] objectAtIndex:1] objectForKey:@"time"] doubleValue];
    //        NSDate *dateSet = [NSDate dateWithTimeIntervalSince1970:tidelast];
    //        NSString *timeSet = [timeFormatter1 stringFromDate:dateSet];
    //        //TideNextTimeString=[NSString stringWithFormat:@"%@ PM SET", timeSet];
    //    }
    /*
     //sunrise/sunset
     NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
     [timeFormatter setDateFormat:@"hh:mm"];
     
     NSTimeInterval sunRise = [[[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"] objectAtIndex:0] objectForKey:@"time"] doubleValue];
     NSDate *dateRise = [NSDate dateWithTimeIntervalSince1970:sunRise];
     NSString *timeRise = [timeFormatter stringFromDate:dateRise];
     IBLabelSunRise.text = [NSString stringWithFormat:@"%@ AM RISE", timeRise];
     
     NSTimeInterval sunSet = [[[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"] objectAtIndex:1] objectForKey:@"time"] doubleValue];
     NSDate *dateSet = [NSDate dateWithTimeIntervalSince1970:sunSet];
     NSString *timeSet = [timeFormatter stringFromDate:dateSet];
     IBlabelSunSet.text = [NSString stringWithFormat:@"%@ PM SET", timeSet];*/
    
    //sunrise/sunset
    //-----------------------------------------
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"hh:mm"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [timeFormatter setTimeZone:timeZone];
    
    NSArray* sunPoints = [[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"SunPoints"];
    
    NSTimeInterval sunRise;
    NSTimeInterval sunSet;
    if(sunPoints.count > 1){
        sunRise = [[[sunPoints objectAtIndex:0] objectForKey:@"time"] doubleValue];
        sunSet = [[[sunPoints objectAtIndex:1] objectForKey:@"time"] doubleValue];
        
        NSDate *dateRise = [NSDate dateWithTimeIntervalSince1970:sunRise];
        dateRise = [dateRise dateByAddingTimeInterval:60*60*dtimeZone];
        NSString *timeRise = [timeFormatter stringFromDate:dateRise];
        
        Sunrise1=[NSString stringWithFormat:@"%@",timeRise];
        
        NSTimeZone *timeZone1 = [NSTimeZone timeZoneWithName:@"UTC"];
        
        
        NSDateFormatter *timeFormatter1 = [[NSDateFormatter alloc] init];
        [timeFormatter1 setTimeZone:timeZone1];
        [timeFormatter1 setDateFormat:@"HH:mm"];
        
        NSDate *dateSet = [NSDate dateWithTimeIntervalSince1970:sunSet];
        dateSet = [dateSet dateByAddingTimeInterval:60*60*dtimeZone];
        NSString *timeSet = [timeFormatter1 stringFromDate:dateSet];
        
        
        Sunset1=[NSString stringWithFormat:@"%@",timeSet];
        
    }
    [self SendDataToWatch];
    
}

//watch code

-(void)SendDataToWatch
{
    
    if ([appDelegate.appdelegateProximity isConnected] == false) {
    
        return;
    
    }
    
    BOOL isNegative = false; //Note negative values are represented by a 1 in the highest bit!
    Byte TempByte;
    
    Byte byte0[20];
    Byte byte1[18];
    Byte byte2[12];
    
    //-------------------------------------------------------------------------
    //------------------------------- Package 0 -------------------------------
    //-------------------------------------------------------------------------
    
    byte0[0]=0x10;
    byte0[1]=20;
    
    if (isMetric){
        //Package0.Data[2] = 0;//Byte 2: Unit System ( 0: Metric, 1: Imperial )
        byte0[2]=0;
    }
    else{
        
        byte0[2]=1;
    }
    
    //Byte 3: Weather code (0 ~ 21 )
    if([WeatherType isEqualToString:@"CLEAR"])
    {
        
        byte0[3]=0;
    }
    else if ([WeatherType isEqualToString:@"MOSTLY SUNNY"])
    {
        
        byte0[3]=1;
    }
    else if ([WeatherType isEqualToString:@"SUNNY"])
    {
        
        byte0[3]=2;
    }
    else if ([WeatherType isEqualToString:@"PARTLY SUNNY"])
    {
        
        byte0[3]=3;
    }
    else if ([WeatherType isEqualToString:@"CLOUDY"])
    {
        
        byte0[3]=4;
    }
    else if ([WeatherType isEqualToString:@"MOSTLY CLOUDY"])
    {
        
        byte0[3]=5;
    }
    else if ([WeatherType isEqualToString:@"OVERCAST"])
    {
        
        byte0[3]=6;
    }
    else if ([WeatherType isEqualToString:@"PARTLY CLOUDY"])
    {
        
        byte0[3]=7;
    }
    else if ([WeatherType isEqualToString:@"RAIN"])
    {
        
        byte0[3]=8;
    }
    else if ([WeatherType isEqualToString:@"HEAVY RAIN"])
    {
        
        byte0[3]=9;
    }
    else if ([WeatherType isEqualToString:@"SCATTERED SHOWERS"])
    {
        
        byte0[3]=10;
    }
    else if ([WeatherType isEqualToString:@"SLEET"])
    {
        
        byte0[3]=11;
    }
    else if ([WeatherType isEqualToString:@"SNOW"])
    {
        
        byte0[3]=12;
    }
    else if ([WeatherType isEqualToString:@"FLURRIES"])
    {
        
        byte0[3]=13;
    }
    else if ([WeatherType isEqualToString:@"FOG"])
    {
        
        byte0[3]=14;
    }
    else if ([WeatherType isEqualToString: @"HAZY"])
    {
        
        byte0[3]=15;
    }
    else if ([WeatherType isEqualToString:@"SCATTERED SHOWERS POSSIBLE T-STORMS"])
    {
        
        byte0[3]=16;
    }
    else if ([WeatherType isEqualToString:@"HEAVY RAIN POSSIBLE T-STORMS"])
    {
        
        byte0[3]=17;
    }
    else if ([WeatherType isEqualToString:@"SCATTERED SHOWERS T-STORMS"])
    {
        
        byte0[3]=18;
    }
    else if ([WeatherType isEqualToString:@"POSSIBLE T-STORMS"])
    {
        
        byte0[3]=19;
    }
    else if ([WeatherType isEqualToString:@"THUNDERSTORMS"])
    {
        
        byte0[3]=20;
    }
    else /*if (WeatherType.equals("unknown"))*/
    {
        
        byte0[3]=21;
    }
    
    //Byte 4 ~ 15: Beach Name (Max. 12 characters )
    //Already written when parsing JSON Data
    
    
    
    int Stringlength = [Location length];
    
    const char*data = [[Location uppercaseString] UTF8String];
    
    //    NSData * testData = [[Location capitalizedString] dataUsingEncoding: NSUTF8StringEncoding];
    //
    //    Byte  *testByte = (Byte *) [testData bytes];
    
    for (int i=0; i<12; i++) {
        if (Stringlength>i) {
            byte0[4+i]=data[i];
        } else {
            byte0[4+i]=0x20;
        }
        
        
    }
    
    
    
    NSArray *arrSunrise = [Sunrise1 componentsSeparatedByString:@":"];
    NSString *str1= [[arrSunrise objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *str2= [[arrSunrise objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Byte 16, 17: Sunrise Hour, Minute
    
    byte0[16]=(Byte)[str1 integerValue];
    byte0[17]=(Byte)[str2 integerValue];
    
    //Byte 18, 19: Sunset Hour, Minute
    
    
    NSArray *arrSunset = [Sunset1 componentsSeparatedByString:@":"];
    NSString *strSunset1= [[arrSunset objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strSunset2= [[arrSunset objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    byte0[18]=(Byte)[strSunset1 integerValue];
    byte0[19]=(Byte)[strSunset2 integerValue];
    
    //-------------------------------------------------------------------------
    //------------------------------- Package 1 -------------------------------
    //-------------------------------------------------------------------------
    
    byte1[0]=0x11;
    byte1[1]=18;
    // Package1.Data[0] = 0x11;	//Data Type
    //Package1.Data[1] = 18;		//Package Size
    
    //Note: All these values can be negative, but we don't need to treat this specially
    //Byte 2: Min. Air Temperature ( -127 ~ 127 )
    //Byte 3: Max. Air Temperature ( -127 ~ 127 )
    //Byte 4: Min. Water Temperature ( -127 ~ 127 )
    //Byte 5: Max. Water Temperature ( -127 ~ 127 )
    
    
    byte1[2]=(Byte)[AirTemperature floatValue];//AirTempMin;
    byte1[3]=(Byte)[AirTemperaturemax floatValue];//AirTempMax;
    byte1[4]=(Byte)[WaterTemperature floatValue];//WaterTempMin;
    byte1[5]=(Byte)[WaterTemperaturemax floatValue];//WaterTempMax;
    
    
    //Byte 6, 7: Time of Last Tide in Hour and Minute
    if([TideLastTimeString isEqualToString:@"--:--"])
        //if (TideLastTimeString.contains("--:--"))
    {
        //No last Tide information available force tide time to be 0xff 0xff
        
        byte1[6]=(Byte)0xFF;
        byte1[7]=(Byte)0xFF;
        
        if (!TideLastisHi)
            //Package1.Data[8] = (byte) (0x00 | 10000000);
            byte1[8]=(Byte)(0x00 | 10000000);
    }
    else
    {
        NSArray *arrtidetime = [TideLastTimeString componentsSeparatedByString:@":"];
        NSString *strtide1= [[arrtidetime objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *strtide2= [[arrtidetime objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        byte1[6]=(Byte)[strtide1 integerValue];//TideLastTimeString;
        byte1[7]=(Byte)[strtide2 integerValue];//TideLastTimeString;
        
        //Byte 8, 9: Size of Last Tide ( -127.99 ~ 127.99 )
        isNegative = false;
        if (TideLastSizeFloat < 0.f)
        {
            isNegative = true;
            TideLastSizeFloat = -TideLastSizeFloat;
        }
        
        NSString *tidesize = [NSString stringWithFormat:@"%.1f", TideLastSizeFloat];
        NSArray *arrtidesize = [tidesize componentsSeparatedByString:@"." ];
        NSString *strtidesize1= [[arrtidesize objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
        //NSString *strtidesize2= [[arrtidesize objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *strtidesize2= [NSString stringWithFormat:@"%@0", [[arrtidesize objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""]];
        byte1[8]=(Byte)[strtidesize1 integerValue];//TideLastSizeFloat;
        
        //TempByte = (Byte)Integer.parseInt(SplitStringLastSize[1]);
        TempByte = (Byte)[strtidesize2 integerValue];//TideLastSizeFloat;
        if ((TempByte < 10) && (TideLastSizeFloat < 2)) //0.01 -> 1   0.1 -> 10
            TempByte = (Byte) (TempByte * 10);
        
        //Package1.Data[9]= TempByte; //Note: Always 2 digits after comma:5.1 = 10 5.06 = 6 5.64 = 64
        byte1[9]=TempByte;
        if (!TideLastisHi)
            // Package1.Data[8] = (byte) (Package1.Data[8] | 10000000);
            byte1[8]=(Byte)(byte1[8] | 10000000);
        if (isNegative)
            // Package1.Data[8] = (byte) (Package1.Data[8] | 01000000);
            byte1[8]=(Byte)(byte1[8] | 01000000);
    }
    
    //Byte 10, 11: Time of Next Tide in Hour and Minute
    
    NSArray *arrtidenexttime = [TideNextTimeString componentsSeparatedByString:@":"];
    NSString *strtidenexttime1= [[arrtidenexttime objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strtidenexttime2= [[arrtidenexttime objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    byte1[10]=(Byte)[strtidenexttime1 integerValue];//TideNextTimeString;
    byte1[11]=(Byte)[strtidenexttime2 integerValue];//TideNextTimeString;
    
    //Byte 12, 13: Size of Next Tide ( -127.99 ~ 127.99 )
    isNegative = false;
    if (TideNextSizeFloat < 0.f)
    {
        isNegative = true;
        TideNextSizeFloat = -TideNextSizeFloat;
    }
    
    
    NSString *tidenextsize = [NSString stringWithFormat:@"%.1f", TideNextSizeFloat];
    NSArray *arrtidesizenext = [tidenextsize componentsSeparatedByString:@"." ];
    NSString *strtidesizenext1= [[arrtidesizenext objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    //NSString *strtidesizenext2= [[arrtidesizenext objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strtidesizenext2= [NSString stringWithFormat:@"%@0", [[arrtidesizenext objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    byte1[12]=(Byte)[strtidesizenext1 integerValue];//TideNextSizeFloat;
    
    //TempByte = (byte)Integer.parseInt(SplitStringNextSize[1]);
    TempByte = (Byte)[strtidesizenext2 integerValue];//TideNextSizeFloat;
    if ((TempByte < 10) && (TideNextSizeFloat < 2)) //0.01 -> 1   0.1 -> 10
        TempByte = (Byte) (TempByte * 10);
    
    // Package1.Data[13]= TempByte; //Note: Always 2 digits after comma:5.1 = 10 5.06 = 6 5.64 = 64
    byte1[13]=TempByte;
    if (!TideNextisHi)
        byte1[12] = (Byte) (byte1[12] | 10000000);
    if (isNegative)
        byte1[12] = (Byte) (byte1[12] | 01000000);
    
    //Byte 14, 15: Time of Next Tide After Next in Hour and Minute
    
    NSArray *arrtidenexttimeafter = [TideNextAfterNextTimeString componentsSeparatedByString:@"."];
    NSString *strtidenexttimeafter1= [[arrtidenexttimeafter objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strtidenexttimeafter2= [[arrtidenexttimeafter objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    byte1[14]=(Byte)[strtidenexttimeafter1 integerValue];//TideNextAfterNextTimeString;
    byte1[15]=(Byte)[strtidenexttimeafter2 integerValue];//TideNextAfterNextTimeString;
    
    //Byte 16, 17: Size of Next Tide After Next( -127.99 ~ 127.99 )
    isNegative = false;
    if (TideNextAfterNextSizeFloat < 0.f)
    {
        isNegative = true;
        TideNextAfterNextSizeFloat = -TideNextAfterNextSizeFloat;
    }
    
    NSString *tidenextsizeafter = [NSString stringWithFormat:@"%f", TideNextAfterNextSizeFloat];
    NSArray *arrtidesizenextafter = [tidenextsizeafter componentsSeparatedByString:@"." ];
    NSString *strtidesizenextafter1= [[arrtidesizenextafter objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *strtidesizenextafter2= [[arrtidesizenextafter objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    byte1[16]=(Byte)[strtidesizenextafter1 integerValue];//TideNextAfterNextSizeFloat;
    
    //TempByte = (byte)Integer.parseInt(SplitStringNextAfterNextSize[1]);
    TempByte=(Byte)[strtidesizenextafter2 integerValue];//TideNextAfterNextSizeFloat;
    if ((TempByte < 10) && (TideNextAfterNextSizeFloat < 2)) //0.01 -> 1   0.1 -> 10
        TempByte = (Byte) (TempByte * 10);
    
    
    byte1[17]=TempByte;
    if (!TideNextAfterNextisHi)
        
        byte1[16]=(Byte)(byte1[16] | 10000000);
    if (isNegative)
        
        byte1[16]=(Byte)(byte1[16] | 01000000);
    
    //-------------------------------------------------------------------------
    //------------------------------- Package 2 -------------------------------
    //-------------------------------------------------------------------------
    //Byte 0: Data Type, this value is fixed as 18. (Ref to ¡®Geneva Watch Protocol Spec¡¯ )
    
    byte2[0]=18;
    
    //Byte 1: Package Size ( 5 for this package )
    
    byte2[1]=12;
    
    //Byte 2, 3: Wind Speed ( 0 ~ 255 )
    
    byte2[2]=(Byte)WindTempMin;
    byte2[3]=(Byte)WindTempMax;
    
    //Byte 4: Wind Direction ( 0 ~ 15 )
    //NSLog(@"WindDirection: %@",WindDirection);
    //NSLog(@"WindTempDirection: %d",WindTempDirection);
    
    UInt8 windir=[self getDirectionByte:WindTempDirection];//(WindTempDirection/27);
    byte2[4]= windir;
    
    //byte2[4]=(Byte)WindTempDirection;
    
    //Byte 5, 6: Size of Min Swell ( 0 ~ 255.99 )
    
    NSString *surftempmin = [NSString stringWithFormat:@"%.1f", SurfTempMin];
    NSArray *arrsurftempmintem = [surftempmin componentsSeparatedByString:@"." ];
    NSString *surftempmint1= [[arrsurftempmintem objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *surftempmint2= [[arrsurftempmintem objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    byte2[5]=(Byte)[surftempmint1 integerValue];//SurfTempMin;
    
    
    TempByte=(Byte)[surftempmint2 integerValue];//SurfTempMin;
    if (TempByte < 10) TempByte = (Byte) (TempByte * 10);
    // Package2.Data[6]= TempByte; //Note: Always 2 digits after comma:5.1 = 10 5.06 = 6 5.64 = 64
    byte2[6]=(Byte)TempByte;
    
    //Byte 7, 8: Size of Max Swell ( 0 ~ 255.99 )
    
    
    NSString *surftempmax = [NSString stringWithFormat:@"%.1f", SurfTempMax];
    NSArray *arrsurftempmmax = [surftempmax componentsSeparatedByString:@"." ];
    NSString *surftempmmax1= [[arrsurftempmmax objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *surftempmmax2= [[arrsurftempmmax objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    byte2[7]=(Byte)[surftempmmax1 integerValue];//SurfTempMax;
    
    
    TempByte=(Byte)[surftempmmax2 integerValue];//SurfTempMax;
    
    if (TempByte < 10 && TempByte > 0) TempByte = (Byte) (TempByte * 10);
    
    byte2[8]=TempByte;
    
    NSLog(@"Home SwellTempDirection: %d - %@",SwellTempDirection, Location);
    
    
    UInt8 srf=[self getDirectionByte:SwellTempDirection];//(WindTempDirection/27);
    byte2[9]= srf;
    
    //int srf=(SwellTempDirection/27);
    //byte2[9]=(Byte)(srf+2);
    
    byte2[10]=(Byte) SwellTempPeriod;
    byte2[11]=0;
    
    
    
    
    
    AppDelegate * app= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    
    if ([app.appdelegateProximity isConnected])
    {
        Byte byte=0x5B;
        [app.appdelegateProximity phoneWriteDataToTagBig:[NSData dataWithBytes:&byte length:sizeof(byte)]];
        
        [app.appdelegateProximity phoneWriteDataToTagBig:[NSData dataWithBytes:&byte0 length:sizeof(byte0)]];
        [app.appdelegateProximity phoneWriteDataToTagBig:[NSData dataWithBytes:&byte1 length:sizeof(byte1)]];
        [app.appdelegateProximity phoneWriteDataToTagBig:[NSData dataWithBytes:&byte2 length:sizeof(byte2)]];
        
        
    }
    
    
}

-(NSString*)getDirection:(int)Direction{
    
    NSString* sFinalDirection;
    if(Direction <= 190 &&  Direction >= 170){
        sFinalDirection = @"S";
    }else if (Direction <= 10){
        sFinalDirection = @"N";
    }else if (Direction >= 350){
        sFinalDirection = @"N";
    }else if (Direction <= 100 && Direction >=80){
        sFinalDirection = @"E";
    }else if (Direction<= 280  && Direction >= 260){
        sFinalDirection = @"W";
    }else if (Direction<= 190 && Direction >= 170){
        sFinalDirection = @"S";
    }else if (Direction > 280 && Direction <= 310){
        sFinalDirection = @"WNW";
    }else if (Direction > 310 && Direction <= 330){
        sFinalDirection = @"NW";
    }else if (Direction > 330 && Direction < 350){
        sFinalDirection = @"NNW";
    }else if (Direction > 10 && Direction <= 30){
        sFinalDirection = @"NNE";
    }else if (Direction > 30 && Direction <= 50){
        sFinalDirection = @"NE";
    }else if (Direction > 50 && Direction < 80){
        sFinalDirection = @"ENE";
    }else if (Direction > 100 && Direction <= 130){
        sFinalDirection = @"ESE";
    }else if (Direction > 130 && Direction <= 150){
        sFinalDirection = @"SE";
    }else if (Direction > 150 && Direction < 170){
        sFinalDirection = @"SSE";
    }else if (Direction > 190 && Direction <= 210){
        sFinalDirection = @"SSW";
    }else if (Direction > 210 && Direction <= 230){
        sFinalDirection = @"SW";
    }else if (Direction > 230 && Direction < 260){
        sFinalDirection = @"WSW";
    }
    return sFinalDirection;
    
}

-(UInt8)getDirectionByte:(int)Direction{
    UInt8 sFinalDirection;
    if(Direction <= 190 &&  Direction >= 170){
        //        sFinalDirection = @"S";
        sFinalDirection = 8;
    }else if (Direction <= 10){
        //sFinalDirection = @"N";
        sFinalDirection =0;
    }else if (Direction >= 350){
        //sFinalDirection = @"N";
        sFinalDirection = 0;
    }else if (Direction <= 100 && Direction >=80){
        //sFinalDirection = @"E";
        sFinalDirection = 4;
    }else if (Direction<= 280  && Direction >= 260)
    {
        //sFinalDirection = @"W";
        sFinalDirection = 12;
    }else if (Direction<= 190 && Direction >= 170){
        //sFinalDirection = @"S";
        sFinalDirection = 8;
    }else if (Direction > 280 && Direction <= 310){
        // sFinalDirection = @"WNW";
        sFinalDirection = 13;
    }else if (Direction > 310 && Direction <= 330){
        //sFinalDirection = @"NW";
        sFinalDirection = 14;
    }else if (Direction > 330 && Direction < 350){
        // sFinalDirection = @"NNW";
        sFinalDirection = 15;
    }else if (Direction > 10 && Direction <= 30){
        // sFinalDirection = @"NNE";
        sFinalDirection = 1;
    }else if (Direction > 30 && Direction <= 50){
        //sFinalDirection = @"NE";
        sFinalDirection = 2;
    }else if (Direction > 50 && Direction < 80){
        // sFinalDirection = @"ENE";
        sFinalDirection = 3;
    }else if (Direction > 100 && Direction <= 130){
        // sFinalDirection = @"ESE";
        sFinalDirection = 5;
    }else if (Direction > 130 && Direction <= 150){
        //sFinalDirection = @"SE";
        sFinalDirection = 6;
    }else if (Direction > 150 && Direction < 170){
        // sFinalDirection = @"SSE";
        sFinalDirection = 7;
    }else if (Direction > 190 && Direction <= 210){
        //sFinalDirection = @"SSW";
        sFinalDirection = 9;
    }else if (Direction > 210 && Direction <= 230){
        // sFinalDirection = @"SW";
        sFinalDirection = 10;
    }else if (Direction > 230 && Direction < 260){
        //sFinalDirection = @"WSW";
        sFinalDirection = 11;
    }
    return sFinalDirection;
    
}


- (void)initTideData:(NSString*)sForcast
{
    NSMutableArray *mutableLineCharts = [NSMutableArray array];
    NSMutableArray *mutableChartData = [NSMutableArray array];
    
    NSArray* arr =  [[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"dataPoints"];
    
    if (arr.count <= 0){
        return;
    }
    
    
    double dtimeZone =[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"timezone"] doubleValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM dd, yyyy"];
    
    // Add this part to your code
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [formatter setTimeZone:timeZone];
    
    
    NSDate *nowOrNext = [NSDate date];
    
    if ([sForcast isEqualToString:@"today"] == false){
        int daysToAdd = 1;
        nowOrNext = [nowOrNext dateByAddingTimeInterval:60*60*24*daysToAdd];
    }
    
    nowOrNext = [nowOrNext dateByAddingTimeInterval:60*60*dtimeZone];
    
    NSString *sCurrentDate = [formatter stringFromDate:nowOrNext];
    
    Boolean _curAdded = false;
    //get today data
    
    //Index to get tide data for watch
    int _todayIndex = 0;
    
    int x = 0;
    for (int i = 0; i < [arr count]; i++)
    {
        NSDictionary* tideHeight = [arr objectAtIndex: i];
        NSString *timeStemp = [tideHeight objectForKey:@"utctime"];
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat:@"MMMM dd, yyyy HH:mm:ss"];
        [timeFormatter setTimeZone:timeZone];
        NSDate *dTideDate = [timeFormatter dateFromString:timeStemp];
        dTideDate = [dTideDate dateByAddingTimeInterval:60*60*dtimeZone];
        
        if([sCurrentDate isEqualToString:[formatter stringFromDate:dTideDate]])
        {
            NSString *tType = [[tideHeight objectForKey:@"type"] uppercaseString];
            if (!([tType isEqualToString:@"NORMAL"] || [tType isEqualToString:@"HIGH"] || [tType isEqualToString:@"LOW"])) {
                continue;
            }
            if(i>0 && _curAdded == false && [sForcast isEqualToString:@"today"])
            {
                NSString *sCurDate = [timeFormatter stringFromDate:[NSDate date]];
                NSDate *dCurDate = [timeFormatter dateFromString:sCurDate];
                dCurDate = [dCurDate dateByAddingTimeInterval:60*60*dtimeZone];
                
                NSComparisonResult result2 = [dCurDate compare:dTideDate];
                
                //NSLog(@"dTideDate :: %@",dTideDate.description);
                //NSLog(@"dCurDate :: %@",dCurDate.description);
                if(result2==NSOrderedAscending && _curAdded == false)
                {
                    _todayIndex = x;
                    indexOfCurrentTide = (int)mutableChartData.count;
                    
                    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
                    [timeFormatter setDateFormat:@"hh:mm aa"];
                    [timeFormatter setTimeZone:timeZone];
                    
                    //[arrayTideTime addObject:[timeFormatter stringFromDate:nowOrNext]];
                    
                    float _prevHeight = [[mutableChartData objectAtIndex:x-1] floatValue];
                    float _curHeight = [[tideHeight objectForKey:@"height"] floatValue];
                    NSNumber *_height = [NSNumber numberWithFloat: ABS((_prevHeight + 1.5 + _curHeight + 1.5)/2) - 1.5];
                    //[mutableChartData addObject: _height];
                    _curAdded = true;
                }
            }
            
            [timeFormatter setDateFormat:@"hh:mm aa"];
            [arrayTideTime addObject:[timeFormatter stringFromDate:dTideDate]];
            NSNumber* heightOfTide = [tideHeight objectForKey:@"height"];
            
            if ([heightOfTide floatValue] < lastMinimumTide) {
                lastMinimumTide = [heightOfTide floatValue];
            }
            
            [mutableChartData addObject:heightOfTide];
            x++;
        }
    }
    
    if (_curAdded == false && indexOfCurrentTide == 0 && [sForcast isEqualToString:@"today"])
        indexOfCurrentTide = (int)mutableChartData.count;
    
    [mutableLineCharts removeAllObjects];
    
    //get print indexes
    //-------------------------------
    
    TideLastTimeString = @"--:--";
    TideNextTimeString = @"--:--";
    TideLastSizeFloat = 0.0f;
    TideNextSizeFloat = 0.0f;
    
    NSArray *firstHalfOfArray;
    NSArray *secondHalfOfArray;
    if ([mutableChartData count]>0) {
        NSRange someRange;
        
        someRange.location = 0;
        someRange.length = [mutableChartData count] / 2;
        
        firstHalfOfArray = [mutableChartData subarrayWithRange:someRange];
        
        _firstMinIndex = 0;
        _firstMaxIndex = 0;
        float _firstMinHeight = 0;
        float _firstMaxHeight = 0;
        for (int i=2; i < [firstHalfOfArray count]; i++)
        {
            float _height = [[firstHalfOfArray objectAtIndex:i] floatValue];
            if (i == 2) {
                _firstMinIndex = 2;
                _firstMinHeight = _height;
                _firstMaxIndex = 2;
                _firstMaxHeight = _height;
            }
            
            if (_height < _firstMinHeight) {
                _firstMinHeight = _height;
                _firstMinIndex = i;
            }
            if (_height > _firstMaxHeight) {
                _firstMaxHeight = _height;
                _firstMaxIndex = i;
            }
        }
        
        someRange.location = someRange.length;
        someRange.length = [mutableChartData count] - someRange.length;
        
        secondHalfOfArray = [mutableChartData subarrayWithRange:someRange];
        
        _secondMinIndex = 0;
        _secondMaxIndex = 0;
        float _secondMinHeight = 0;
        float _secondMaxHeight = 0;
        for (int i=1; i < [secondHalfOfArray count]-2; i++)
        {
            float _height = [[secondHalfOfArray objectAtIndex:i] floatValue];
            if (i == 1) {
                _secondMinIndex = 1;
                _secondMinHeight = _height;
                _secondMaxIndex = 1;
                _secondMaxHeight = _height;
            }
            
            if (_height < _secondMinHeight) {
                _secondMinHeight = _height;
                _secondMinIndex = i;
            }
            if (_height > _secondMaxHeight) {
                _secondMaxHeight = _height;
                _secondMaxIndex = i;
            }
        }
        
        _secondMinIndex = _secondMinIndex + (int)[firstHalfOfArray count];
        _secondMaxIndex = _secondMaxIndex + (int)[firstHalfOfArray count];
    }
    
    //-------------------------------
    //fine tune idexes
    if ([mutableChartData count]>0)
    {
        if (_firstMinIndex < _firstMaxIndex && _secondMaxIndex < _secondMinIndex) {
            //calculate first max again, which should be less then firstmin index
            _firstMaxIndex = 0;
            float _firstMaxHeight = 0;
            for (int i=1; i < _firstMinIndex; i++)
            {
                float _height = [[firstHalfOfArray objectAtIndex:i] floatValue];
                if (i == 1) {
                    _firstMaxIndex = 1;
                    _firstMaxHeight = _height;
                }
                if (_height > _firstMaxHeight) {
                    _firstMaxHeight = _height;
                    _firstMaxIndex = i;
                }
            }
        }
        
        if (_firstMaxIndex < _firstMinIndex && _secondMinIndex < _secondMaxIndex && _secondMinIndex - _firstMinIndex < 5)
        {
            //calculate second min again, which should be greater then secondmax index
            _secondMinIndex = -1;
            float _secondMinHeight = 0;
            for (int i=_secondMaxIndex - (int)[firstHalfOfArray count] +1; i < [secondHalfOfArray count]-2; i++)
            {
                float _height = [[secondHalfOfArray objectAtIndex:i] floatValue];
                if (i == _secondMaxIndex - (int)[firstHalfOfArray count] +1) {
                    _secondMinIndex = _secondMaxIndex - (int)[firstHalfOfArray count] +1;
                    _secondMinHeight = _height;
                }
                
                if (_height < _secondMinHeight) {
                    _secondMinHeight = _height;
                    _secondMinIndex = i;
                }
            }
            if (_secondMinIndex != -1) {
                _secondMinIndex = _secondMinIndex + (int)[firstHalfOfArray count];
            }
        }
    }
    
    //get values to display in watch
    //-------------------------------
    NSMutableArray *tempChartData = [[NSMutableArray alloc] init];
    NSMutableArray *tempTimeData = [[NSMutableArray alloc] init];
    
    for (int i = _todayIndex; i < [mutableChartData count]; i++) {
        [tempChartData addObject:[mutableChartData objectAtIndex:i]];
        [tempTimeData addObject:[arrayTideTime objectAtIndex:i]];
    }
    
    //get min index
    float _minHeight = 0;
    int _minIndex = 0;
    
    float _firstMinHeight = [[mutableChartData objectAtIndex:_firstMinIndex] floatValue];
    float _secondMinHeight = 0;
    if (_secondMinIndex >=0) {
        _secondMinHeight = [[mutableChartData objectAtIndex:_secondMinIndex] floatValue];
    }
    
    if (_todayIndex <= _firstMinIndex)
    {
        _minHeight = _firstMinHeight;
        _minIndex = _firstMinIndex;
        if (_secondMinHeight < _firstMinHeight) {
            _minHeight = _secondMinHeight;
            _minIndex = _secondMinIndex;
        }
        TideLastSizeFloat = roundf([[NSString stringWithFormat:@"%.3f",[[mutableChartData objectAtIndex:_minIndex] floatValue]] floatValue] * 10.0)/10.0;
        TideLastTimeString=[arrayTideTime objectAtIndex:_minIndex];
    }
    else if (_todayIndex > _firstMinIndex && _todayIndex <= _secondMinIndex)
    {
        _minHeight = _secondMinHeight;
        _minIndex = _secondMinIndex;
        TideLastSizeFloat = roundf([[NSString stringWithFormat:@"%.3f",[[mutableChartData objectAtIndex:_minIndex] floatValue]] floatValue] * 10.0)/10.0;
        TideLastTimeString=[arrayTideTime objectAtIndex:_minIndex];
    }
    else
    {
        if ([tempChartData count]>0) {
            for (int i = 0; i < [tempChartData count]; i++) {
                float _height = [[tempChartData objectAtIndex:i] floatValue];
                if (i == 0) {
                    _minIndex = 0;
                    _minHeight = _height;
                }
                
                if (_height < _minHeight) {
                    _minHeight = _height;
                    _minIndex = i;
                }
            }
            TideLastSizeFloat = roundf([[NSString stringWithFormat:@"%.3f",[[tempChartData objectAtIndex:_minIndex] floatValue]] floatValue] * 10.0)/10.0;
            TideLastTimeString=[tempTimeData objectAtIndex:_minIndex];
        }
    }
    //get max index
    float _maxHeight = 0;
    int _maxIndex = 0;
    
    float _firstMaxHeight = [[mutableChartData objectAtIndex:_firstMaxIndex] floatValue];
    float _secondMaxHeight = [[mutableChartData objectAtIndex:_secondMaxIndex] floatValue];
    
    if (_todayIndex <= _firstMaxIndex)
    {
        _maxHeight = _firstMaxHeight;
        _maxIndex = _firstMaxIndex;
        if (_secondMaxHeight > _firstMaxHeight) {
            _maxHeight = _secondMaxHeight;
            _maxIndex = _secondMaxIndex;
        }
        TideNextSizeFloat=roundf([[NSString stringWithFormat:@"%.3f",[[mutableChartData objectAtIndex:_maxIndex] floatValue]] floatValue] * 10.0)/10.0;
        TideNextTimeString=[arrayTideTime objectAtIndex:_maxIndex];
    }
    else if (_todayIndex > _firstMaxIndex && _todayIndex <= _secondMaxIndex)
    {
        _maxHeight = _secondMaxHeight;
        _maxIndex = _secondMaxIndex;
        TideNextSizeFloat=roundf([[NSString stringWithFormat:@"%.3f",[[mutableChartData objectAtIndex:_maxIndex] floatValue]] floatValue] * 10.0)/10.0;
        TideNextTimeString=[arrayTideTime objectAtIndex:_maxIndex];
    }
    else
    {
        if ([tempChartData count]>0) {
            for (int i = 0; i < [tempChartData count]; i++) {
                float _height = [[tempChartData objectAtIndex:i] floatValue];
                if (i == 0) {
                    _maxIndex = 0;
                    _maxHeight = _height;
                }
                
                if (_height > _maxHeight) {
                    _maxHeight = _height;
                    _maxIndex = i;
                }
            }
            TideNextSizeFloat=roundf([[NSString stringWithFormat:@"%.3f",[[tempChartData objectAtIndex:_maxIndex] floatValue]] floatValue] * 10.0)/10.0;
            TideNextTimeString=[tempTimeData objectAtIndex:_maxIndex];
        }
    }
    
    //convert it to 24 hour
    NSDateFormatter *h12TimeFormatter = [[NSDateFormatter alloc] init];
    [h12TimeFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    
    NSDateFormatter *h24TimeFormatter = [[NSDateFormatter alloc] init];
    [h24TimeFormatter setDateFormat:@"HH:mm"];
    
    if (![TideNextTimeString isEqualToString:@"--:--"])
    {
        NSString *sNextTime = [NSString stringWithFormat:@"01-01-2001 %@", TideNextTimeString];
        NSDate *nextTime = [h12TimeFormatter dateFromString:sNextTime];
        TideNextTimeString = [h24TimeFormatter stringFromDate:nextTime];
    }
    if (![TideLastTimeString isEqualToString:@"--:--"])
    {
        NSString *sLastTime = [NSString stringWithFormat:@"01-01-2001 %@", TideLastTimeString];
        NSDate *lastTime = [h12TimeFormatter dateFromString:sLastTime];
        TideLastTimeString = [h24TimeFormatter stringFromDate:lastTime];
    }
    //----------------------------------------------------
    
    //if (lastMinimumTide <= 0)
    for (int i=0; i<mutableChartData.count; i++) {
        
        NSNumber* number=[mutableChartData objectAtIndex:i];
        float fNumber = [number floatValue];
        
        fNumber += ABS(lastMinimumTide);
        fNumber += 1.5;
        
        [mutableChartData  removeObjectAtIndex:i];
        [mutableChartData insertObject:[NSNumber numberWithFloat:fNumber] atIndex:i];
        
    }
    
    [mutableLineCharts addObject:mutableChartData];
    //_chartData = [NSArray arrayWithArray:mutableLineCharts];
}


/*
 //tide
 - (void)initTideData:(NSString*)sForcast
 {
 NSMutableArray *mutableLineCharts = [NSMutableArray array];
 NSMutableArray *mutableChartData = [NSMutableArray array];
 
 NSArray* arr =  [[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"dataPoints"];
 
 double dtimeZone =[[[dicWeatherInfo objectForKey:@"Tide"] objectForKey:@"timezone"] doubleValue];
 
 NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
 [formatter setDateFormat:@"MMMM dd, yyyy"];
 
 // Add this part to your code
 NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
 [formatter setTimeZone:timeZone];
 
 
 NSDate *nowOrNext = [NSDate date];
 
 if ([sForcast isEqualToString:@"today"] == false){
 int daysToAdd = 1;
 nowOrNext = [nowOrNext dateByAddingTimeInterval:60*60*24*daysToAdd];
 }
 
 nowOrNext = [nowOrNext dateByAddingTimeInterval:60*60*dtimeZone];
 
 NSString *sCurrentDate = [formatter stringFromDate:nowOrNext];
 
 Boolean _curAdded = false;
 //get today data
 int x = 0;
 for (int i = 0; i < [arr count]; i++)
 {
 NSDictionary* tideHeight = [arr objectAtIndex: i];
 NSString *timeStemp = [tideHeight objectForKey:@"utctime"];
 NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
 [timeFormatter setDateFormat:@"MMMM dd, yyyy HH:mm:ss"];
 [timeFormatter setTimeZone:timeZone];
 NSDate *dTideDate = [timeFormatter dateFromString:timeStemp];
 dTideDate = [dTideDate dateByAddingTimeInterval:60*60*dtimeZone];
 
 if([sCurrentDate isEqualToString:[formatter stringFromDate:dTideDate]])
 {
 NSString *tType = [[tideHeight objectForKey:@"type"] uppercaseString];
 if (!([tType isEqualToString:@"NORMAL"] || [tType isEqualToString:@"HIGH"] || [tType isEqualToString:@"LOW"])) {
 continue;
 }
 //if ([[tideHeight objectForKey:@"height"] doubleValue] > 0)
 //{
 if(i>0 && _curAdded == false && [sForcast isEqualToString:@"today"])
 {
 NSString *sCurDate = [timeFormatter stringFromDate:[NSDate date]];
 NSDate *dCurDate = [timeFormatter dateFromString:sCurDate];
 dCurDate = [dCurDate dateByAddingTimeInterval:60*60*dtimeZone];
 
 NSComparisonResult result2 = [dCurDate compare:dTideDate];
 
 if(result2==NSOrderedAscending && _curAdded == false)
 {
 //  indexOfCurrentTide = (int)mutableChartData.count;
 
 NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
 [timeFormatter setDateFormat:@"hh:mm aa"];
 [timeFormatter setTimeZone:timeZone];
 
 [arrayTideTime addObject:[timeFormatter stringFromDate:nowOrNext]];
 
 float _prevHeight = [[mutableChartData objectAtIndex:x-1] floatValue];
 float _curHeight = [[tideHeight objectForKey:@"height"] floatValue];
 NSNumber *_height = [NSNumber numberWithFloat: ABS((_prevHeight + _curHeight)/2)];
 [mutableChartData addObject: _height];
 _curAdded = true;
 }
 }
 
 [timeFormatter setDateFormat:@"hh:mm aa"];
 [arrayTideTime addObject:[timeFormatter stringFromDate:dTideDate]];
 NSNumber* heightOfTide = [tideHeight objectForKey:@"height"];
 
 if ([heightOfTide floatValue] < lastMinimumTide) {
 lastMinimumTide = [heightOfTide floatValue];
 }
 
 //NSLog(@"arr - %f - %f", [[tideHeight objectForKey:@"height"] floatValue], lastMinimumTide);
 
 [mutableChartData addObject:heightOfTide];
 x++;
 //}
 }
 }
 
 [mutableLineCharts removeAllObjects];
 
 //get print indexes
 //-------------------------------
 if ([mutableChartData count]>0) {
 NSArray *firstHalfOfArray;
 NSArray *secondHalfOfArray;
 NSRange someRange;
 
 someRange.location = 0;
 someRange.length = [mutableChartData count] / 2;
 
 firstHalfOfArray = [mutableChartData subarrayWithRange:someRange];
 
 _firstMinIndex = 0;
 _firstMaxIndex = 0;
 float _firstMinHeight = 0;
 float _firstMaxHeight = 0;
 for (int i=1; i < [firstHalfOfArray count]-1; i++)
 {
 float _height = [[firstHalfOfArray objectAtIndex:i] floatValue];
 if (i == 1) {
 _firstMinIndex = 1;
 _firstMinHeight = _height;
 _firstMaxIndex = 1;
 _firstMaxHeight = _height;
 }
 
 if (_height < _firstMinHeight) {
 _firstMinHeight = _height;
 _firstMinIndex = i;
 }
 if (_height > _firstMaxHeight) {
 _firstMaxHeight = _height;
 _firstMaxIndex = i;
 }
 }
 
 someRange.location = someRange.length;
 someRange.length = [mutableChartData count] - someRange.length;
 
 secondHalfOfArray = [mutableChartData subarrayWithRange:someRange];
 
 _secondMinIndex = 0;
 _secondMaxIndex = 0;
 float _secondMinHeight = 0;
 float _secondMaxHeight = 0;
 for (int i=1; i < [secondHalfOfArray count]-1; i++)
 {
 float _height = [[secondHalfOfArray objectAtIndex:i] floatValue];
 if (i == 1) {
 _secondMinIndex = 1;
 _secondMinHeight = _height;
 _secondMaxIndex = 1;
 _secondMaxHeight = _height;
 }
 
 if (_height < _secondMinHeight) {
 _secondMinHeight = _height;
 _secondMinIndex = i;
 }
 if (_height > _secondMaxHeight) {
 _secondMaxHeight = _height;
 _secondMaxIndex = i;
 }
 }
 
 _secondMinIndex = _secondMinIndex + (int)[firstHalfOfArray count];
 _secondMaxIndex = _secondMaxIndex + (int)[firstHalfOfArray count];
 
 //for watch data
 float _minHeight = _firstMinHeight;
 int _minIndex = _firstMinIndex;
 if (_secondMinHeight < _firstMinHeight) {
 _minHeight = _secondMinHeight;
 _minIndex = _secondMinIndex;
 }
 
 float _maxHeight = _firstMaxHeight;
 int _maxIndex = _firstMaxIndex;
 if (_secondMaxHeight > _firstMaxHeight) {
 _maxHeight = _secondMaxHeight;
 _maxIndex = _secondMaxIndex;
 }
 TideLastSizeFloat=[[NSString stringWithFormat:@"%.1f",[[mutableChartData objectAtIndex:_minIndex] floatValue]]floatValue];
 TideNextSizeFloat=[[NSString stringWithFormat:@"%.1f",[[mutableChartData objectAtIndex:_maxIndex] floatValue]]floatValue];
 
 TideLastTimeString=[arrayTideTime objectAtIndex:_minIndex];
 TideNextTimeString=[arrayTideTime objectAtIndex:_maxIndex];
 
 
 }
 
 //if (lastMinimumTide <= 0)
 for (int i=0; i<mutableChartData.count; i++) {
 
 NSNumber* number=[mutableChartData objectAtIndex:i];
 float fNumber = [number floatValue];
 
 //fNumber += (ABS(lastMinimumTide) + 1);
 fNumber += 1.5;
 
 [mutableChartData  removeObjectAtIndex:i];
 [mutableChartData insertObject:[NSNumber numberWithFloat:fNumber] atIndex:i];
 
 }
 //NSLog(@"%d - %d - %d - %d", _firstMinIndex, _firstMaxIndex, _secondMinIndex, _secondMaxIndex);
 
 //-------------------------------
 
 [mutableLineCharts addObject:mutableChartData];
 //_chartData = [NSArray arrayWithArray:mutableLineCharts];
 }
 */

@end
