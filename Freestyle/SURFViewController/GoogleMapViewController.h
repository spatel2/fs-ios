//
//  GoogleMapViewController.h
//  Freestyle
//
//  Created by BIT on 19/03/15.
//
//

#import <UIKit/UIKit.h>
#import "FreestyleHelper.h"
#import "AppDelegate.h"
#import "SikinLabel.h"
#import "SikinButton.h"
#import "BeachInfo.h"
#import <GoogleMaps/GoogleMaps.h>

@interface GoogleMapViewController : UIViewController{
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelBeachTitle;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelBeachRegion;
@property (strong, nonatomic) IBOutlet UIView* viewBeachInfo;
@property (strong, nonatomic) BeachInfo * beachInfo;

@end
