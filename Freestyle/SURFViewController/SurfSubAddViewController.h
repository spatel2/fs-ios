

#import <UIKit/UIKit.h>

#import "SikinLabel.h"

#import "AppDelegate.h"
#import "FreestyleHelper.h"


@interface SurfSubAddViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
    AppDelegate* appDelegate;
}
@property (strong, nonatomic)  NSArray* arraySport;
@property (strong, nonatomic) NSString* sTitle;
@property (strong, nonatomic) NSString* sSubRegion;
@property (strong, nonatomic) NSString* sRegion;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelTitle;
@property (strong, nonatomic) IBOutlet UITableView *IBTableViewBeaches;


@end
