

#import "SurfSubAddViewController.h"
#import "DBHelper.h"
@interface SurfSubAddViewController ()

@end

@implementation SurfSubAddViewController

@synthesize IBTableViewBeaches,IBLabelTitle;
@synthesize sTitle,sSubRegion,sRegion;

@synthesize arraySport;

- (BOOL)prefersStatusBarHidden {
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    
    if(sTitle == nil && [sTitle isEqualToString:@"spots"] != true)
        
        if (arraySport != nil && arraySport.count > 0 ) {
            
            sTitle = [[[arraySport objectAtIndex:0] allKeys] objectAtIndex:0];
        }
    
    IBLabelTitle.text = [sTitle capitalizedString];
    
    IBTableViewBeaches.delegate = self;
    IBTableViewBeaches.dataSource = self;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    return arraySport.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"subRegionCell" forIndexPath:indexPath];
    
    
    // Configure the cell...//region
    
    NSString *regionName = nil;
    if ([sTitle isEqualToString:@"spots"]) {
        
        regionName = [[arraySport objectAtIndex:indexPath.row] objectForKey:@"title"];
        
    }else {
        
        regionName = [[arraySport objectAtIndex:indexPath.row] objectForKey:@"name"];
        
        //[[[arraySport objectAtIndex:indexPath.row] objectForKey:sTitle] objectForKey:@"name"];
    }
    
    cell.textLabel.text = regionName;
    cell.textLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
    cell.separatorInset = UIEdgeInsetsZero;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}







- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([sTitle isEqualToString:@"spots"]) {
        NSDictionary* obj =[arraySport objectAtIndex:indexPath.row];
        NSString* sID = [[obj objectForKey:@"id"] stringValue];
        
        DBHelper* dbHelper =[DBHelper getSharedInstance];
        BeachInfo * beach = [dbHelper getBeachFromID:sID];
        
        if (beach == nil) {
            [dbHelper insetDataBeachID:sID  Latitude:[obj objectForKey:@"lat"] Longitude:[obj objectForKey:@"lon"] Title:[obj objectForKey:@"title"] Cameratype:[obj objectForKey:@"cameratype"] SubRegion:sSubRegion Region:sRegion];
            
            /*
             int lastRow = [Helper getPREFint:@"lastIndexPathRow"];
             [Helper setPREFStringValue:[obj objectForKey:@"title"] sKey:[NSString stringWithFormat:@"%d",lastRow]];
             
             
             if (lastRow == 0) {
             [Helper setPREFStringValue:sID sKey:@"HomeBeach"];
             }
             
             */
            
            NSMutableArray* arrayOfKeys = nil;
            NSArray* arr=[Helper getPREFID:@"arrayOfkeysForAddedSurf"];
            
            if (arr != nil){
                arrayOfKeys =[[NSMutableArray alloc] initWithArray:arr];
            }
            else{
                arrayOfKeys=[[NSMutableArray alloc] init];
            }
            
            [arrayOfKeys addObject:[NSString stringWithFormat:@"%@_%@",[obj objectForKey:@"title"],sID]];
            
            [Helper setPREFID:arrayOfKeys :@"arrayOfkeysForAddedSurf"];
            
            [self.navigationController popToRootViewControllerAnimated:false];
            
        }
        else
        {
            UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Already added as favorite." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            
        }
    }
    else{
        
        SurfSubAddViewController* surfSubAddViewController =[self.storyboard instantiateViewControllerWithIdentifier:@"surfSubAddViewController"];
        
        if ([sTitle isEqualToString:@"subregion"]) {
            
            
             [appDelegate.arrayPreviousLocationTree addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            
            //surfSubAddViewController.arraySport = [[[arraySport objectAtIndex:indexPath.row] objectForKey:sTitle] objectForKey:@"spots"];
            surfSubAddViewController.sTitle = @"spots";
            
            surfSubAddViewController.sSubRegion = [[arraySport objectAtIndex:indexPath.row]  objectForKey:@"name"];
            
            //surfSubAddViewController.sSubRegion = [[[arraySport objectAtIndex:indexPath.row] objectForKey:sTitle] objectForKey:@"name"];
            surfSubAddViewController.sRegion = sRegion;
            
            
            
            
            NSMutableArray* arry=[NSMutableArray arrayWithArray:[[arraySport objectAtIndex:indexPath.row] objectForKey:@"spots"]];
            
            //            for (NSDictionary* dic in [[arraySport objectAtIndex:indexPath.row] objectForKey:@"spots"]){
            //
            //                [arry addObject: [dic objectForKey:@"spots"]];
            //
            //            }
            //
            
           
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"title" ascending:YES];
            
            surfSubAddViewController.arraySport = [arry sortedArrayUsingDescriptors:@[sort]];
            
            
        }else if ([sTitle isEqualToString:@"region"]){
            //surfSubAddViewController.arraySport = [[[arraySport objectAtIndex:indexPath.row] objectForKey:sTitle] objectForKey:@"content"];
            // surfSubAddViewController.sRegion = [[[arraySport objectAtIndex:indexPath.row] objectForKey:sTitle] objectForKey:@"name"];
            surfSubAddViewController.sTitle = @"subregion";
            surfSubAddViewController.sRegion = [[arraySport objectAtIndex:indexPath.row]  objectForKey:@"name"];
            
            [appDelegate.arrayPreviousLocationTree addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            
            
            NSMutableArray* arry=[[NSMutableArray alloc] init];
            
            for (NSDictionary* dic in [[arraySport objectAtIndex:indexPath.row] objectForKey:@"content"]){
                
                [arry addObject: [dic objectForKey:@"subregion"]];
                
            }
            
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            
            surfSubAddViewController.arraySport = [arry sortedArrayUsingDescriptors:@[sort]];
            
            
        }
        
        [self.navigationController pushViewController:surfSubAddViewController animated:true];
        
    }
}

#pragma mark - Navigation Bar Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma mark - Button Action
- (IBAction)backButtonTouchUpInsideAction:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:true];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
