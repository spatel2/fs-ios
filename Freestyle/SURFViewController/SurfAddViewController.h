

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FreestyleHelper.h"
#import "DBHelper.h"


@interface SurfAddViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    
    AppDelegate* appDelegate;
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintBottomForSearchResultTableView;

@property (strong, nonatomic) IBOutlet UITableView *IBTableViewNearestBeaches;
@property (strong, nonatomic) IBOutlet UITableView *IBTableViewBeachesByLocation;
@property (strong, nonatomic) IBOutlet UITextField *IBTextFieldSearch;

@property (strong, nonatomic) IBOutlet UIButton *IBButtonSearch;
@property (strong, nonatomic) IBOutlet UITableView *IBTableViewSearchResult;

@property (strong, nonatomic) IBOutlet UILabel* IBLabelNearestBeaches;
@property (strong, nonatomic) IBOutlet UILabel* IBLabelByLocation;
@property (strong, nonatomic) IBOutlet UIButton* IBButtonClose;

@property (strong, nonatomic) IBOutlet UIView* IBViewForSearch;

@property (strong, nonatomic) IBOutlet UIView* IBViewBorder;

@end
