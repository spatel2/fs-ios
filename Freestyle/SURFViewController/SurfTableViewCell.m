//
//  SurfTableViewCell.m
//  Freestyle
//
//  Created by BIT on 11/03/15.
//
//

#import "SurfTableViewCell.h"

@implementation SurfTableViewCell

@synthesize IBButtonAddHome,title,subRegionTitle;

- (void)awakeFromNib {
    // Initialization code
    title.adjustsFontSizeToFitWidth = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

@end
