//
//  BeachInfo.h
//  Freestyle
//
//  Created by BIT on 09/03/15.
//
//

#import <Foundation/Foundation.h>

@interface BeachInfo : NSObject

@property (nonatomic, retain)NSString* iDentity;
@property (nonatomic, retain)NSString *title;
@property (nonatomic, retain)NSString *latitude;
@property (nonatomic, retain)NSString *longitude;
@property (nonatomic, retain)NSString *cameratype;
@property (nonatomic, retain)NSString *subRegion;
@property (nonatomic, retain)NSString *region;

@property (nonatomic, readwrite)double distanceFromCurrentLocation;


@end
