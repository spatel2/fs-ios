//
//  SurfTableViewCell.h
//  Freestyle
//
//  Created by BIT on 11/03/15.
//
//

#import <UIKit/UIKit.h>
#import "SikinLabel.h"

@interface SurfTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *IBButtonAddHome;
@property (strong, nonatomic) IBOutlet UIButton *IBButtonDelete;
@property (strong, nonatomic) IBOutlet SikinLabel *title;
@property (strong, nonatomic) IBOutlet SikinLabel *subRegionTitle;
@end
