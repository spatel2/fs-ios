//
//  WeatherDetailOfBeachViewController.h
//  Freestyle
//
//  Created by BIT on 10/03/15.
//
//

#import <UIKit/UIKit.h>

#import "FreestyleHelper.h"
#import "MFSideMenu.h"
#import "SikinLabel.h"

#import "BeachInfo.h"
#import "SikinButton.h"
#import "WSFrameWork.h"

#import "Helper.h"

#import "JBLineChartView.h"

@interface WeatherDetailOfBeachViewController : UIViewController<JBLineChartViewDataSource,JBLineChartViewDelegate>

@property (strong, nonatomic) IBOutlet SikinButton *IBButtonToday;
@property (strong, nonatomic) IBOutlet SikinButton *IBButtonForecast;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintWidthForView;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLableBeachName;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelBeachAreaName;

@property (strong, nonatomic) BeachInfo* beachInfo;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSurfHeight;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSurfHeight1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSurfHeight2;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAirTemp;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAirTemp1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelAirTemp2;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWaterTemp;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWaterTemp1;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWindSpeed;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelWindSpeed1;




@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelTideInfo;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelSunRise;
@property (strong, nonatomic) IBOutlet SikinLabel *IBlabelSunSet;

@property (strong, nonatomic) IBOutlet UIView *IBViewForChart;
@property (strong, nonatomic) IBOutlet SikinLabel *IBlabelTideTitle;


@property (nonatomic, strong) JBLineChartView *lineChartView;
@property (nonatomic, strong) NSArray *chartData;

@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewWeatherCondition;
-(void)SendDataToWatch;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewTideUpDown;

@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelTideChartTemp1;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelTideChartTemp2;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelCurrentTideText;
@property (strong, nonatomic) IBOutlet SikinLabel *IBLabelCurrentTideValue;
@property (strong, nonatomic) IBOutlet UIImageView *IBImageViewCurrentTide;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *IBNSLayoutConstraintForCurrentTideImageWidth;


@property (strong, nonatomic) IBOutlet SikinLabel *IBLableTomorrow;
@end



