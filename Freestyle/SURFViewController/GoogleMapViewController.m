//
//  GoogleMapViewController.m
//  Freestyle
//
//  Created by BIT on 19/03/15.
//
//

#import "GoogleMapViewController.h"

@interface GoogleMapViewController ()

@end

@implementation GoogleMapViewController{
    GMSMapView *mapView_;
}
@synthesize IBLabelBeachRegion,IBLabelBeachTitle,beachInfo,viewBeachInfo;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@" " style:UIBarButtonItemStylePlain target:nil action:nil]];
    
    IBLabelBeachTitle.text = beachInfo.title;
    IBLabelBeachRegion.text = beachInfo.subRegion;
   
    // Create a GMSCameraPosition that tells the map to display the
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[[beachInfo latitude] doubleValue]
                                                            longitude:[[beachInfo longitude] doubleValue]                                                                 zoom:15.5];
    mapView_ = [GMSMapView mapWithFrame:[UIScreen mainScreen].bounds camera:camera];
    mapView_.mapType = kGMSTypeHybrid;
    mapView_.myLocationEnabled = YES;
    //self.view = mapView_;

    [self.view addSubview:mapView_];
    
    [self.view bringSubviewToFront:viewBeachInfo];
    
//    // Creates a marker in the center of the map.
//    GMSMarker *marker = [[GMSMarker alloc] init];
//    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
//    marker.title = @"Sydney";
//    marker.snippet = @"Australia";
//    marker.map = mapView_;
    
  
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeButtonTouchUpInsideAction:(SikinButton *)sender {
    [self.navigationController popViewControllerAnimated:false];
}
#pragma mark - Navigation Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
