

#import "SurfAddViewController.h"
#import "SurfSubAddViewController.h"
#import "BeachInfo.h"

@interface SurfAddViewController ()

@end

@implementation SurfAddViewController{
    NSArray* arrayLoaction;
    NSMutableArray* arrayNearestBeaches;
    // NSMutableArray* arrayAllBeaches;
    NSMutableArray* arraySearchResult;
}


@synthesize IBTableViewNearestBeaches;
@synthesize IBTableViewBeachesByLocation;
@synthesize IBTableViewSearchResult;

@synthesize IBTextFieldSearch;
@synthesize IBButtonSearch;

@synthesize IBLabelNearestBeaches,
IBLabelByLocation,
IBButtonClose,IBViewBorder,IBViewForSearch;

@synthesize IBNSLayoutConstraintBottomForSearchResultTableView;


- (BOOL)prefersStatusBarHidden {
    return YES;
}
#pragma mark - View Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //    NSUInteger count = [[dicResponce objectForKey:@"global"] count];
    //    NSString *areaId = [[[[dicResponce objectForKey:@"global"] objectAtIndex:0] objectForKey:@"area"] objectForKey:@"id"];
    //    NSString *areaName = [[[[dicResponce objectForKey:@"global"] objectAtIndex:0] objectForKey:@"area"] objectForKey:@"name"];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    [FreestyleHelper setNavigationLeftButtonWithMenu:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    
    
    
    NSMutableArray* arry=[[NSMutableArray alloc] init];
    
    for (NSDictionary* dic in [appDelegate.dicWeatherData objectForKey:@"global"]){
        
        [arry addObject: [dic objectForKey:@"area"]];
        
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    arrayLoaction = [arry sortedArrayUsingDescriptors:@[sort]];
    
    
    // arrayLoaction = [appDelegate.dicWeatherData objectForKey:@"global"];
 
    
    
    
    arrayNearestBeaches = [[NSMutableArray alloc] initWithCapacity:4];
    //arrayAllBeaches=[[NSMutableArray alloc] init];
    arraySearchResult =[[NSMutableArray alloc] init];
    
    
    for (NSDictionary* dic in arrayLoaction) {
        
        // NSDictionary * data = [[[[[dic objectForKey:@"area"] objectForKey:@"content"] objectForKey:@"region"]  objectForKey:@"subregion"] objectForKey:@"spots"];
        
        for (NSDictionary* newDic in [dic objectForKey:@"content"]) {
            
            for (NSDictionary* n in [[newDic objectForKey:@"region"] objectForKey:@"content"]) {
                for (NSDictionary* sport in [[n objectForKey:@"subregion"] objectForKey:@"spots"]) {
                    
                    CLLocationDegrees latitude = [[sport objectForKey:@"lat"] doubleValue];
                    CLLocationDegrees longitude = [[sport objectForKey:@"lon"] doubleValue];
                    
                    CLLocation* beachLocation =[[CLLocation alloc] initWithLatitude:latitude
                                                                          longitude:longitude];
                    CLLocationDistance distance = [appDelegate.myLocation distanceFromLocation:beachLocation];
                    
                    
                    
                    // [arrayAllBeaches addObject:beach];
                    
                    
                    if (arrayNearestBeaches.count < 4) {
                        
                        BeachInfo* beach = [[BeachInfo alloc] init];
                        beach.iDentity = [[sport objectForKey:@"id"] stringValue];
                        beach.latitude = [sport objectForKey:@"lat"];
                        beach.longitude = [sport objectForKey:@"lon"];
                        beach.title = [sport objectForKey:@"title"];
                        beach.cameratype = [sport objectForKey:@"cameratype"];
                        beach.distanceFromCurrentLocation =  distance;
                        beach.subRegion = [[n objectForKey:@"subregion"] objectForKey:@"name"];
                        beach.region = [[newDic objectForKey:@"region"] objectForKey:@"name"];
                        [arrayNearestBeaches addObject:beach];
                        
                        beach=nil;
                        
                    } else {
                        
                        for (BeachInfo* beach in arrayNearestBeaches) {
                            
                            if (beach.distanceFromCurrentLocation > distance){
                                
                                beach.distanceFromCurrentLocation = distance;
                                beach.iDentity = [[sport objectForKey:@"id"] stringValue];
                                beach.latitude = [sport objectForKey:@"lat"];
                                beach.longitude = [sport objectForKey:@"lon"];
                                beach.title = [sport objectForKey:@"title"];
                                beach.cameratype = [sport objectForKey:@"cameratype"];
                                beach.distanceFromCurrentLocation =  distance;
                                beach.subRegion = [[n objectForKey:@"subregion"] objectForKey:@"name"];
                                beach.region = [[newDic objectForKey:@"region"] objectForKey:@"name"];
                                
                                break;
                            }
                            
                        }
                        
                        
                    }
                    
                    
                    
                }
                
            }
        }
    }
    
    IBTableViewBeachesByLocation.delegate = self;
    IBTableViewBeachesByLocation.dataSource = self;
    IBTableViewBeachesByLocation.layoutMargins = UIEdgeInsetsZero;
    
    IBTableViewNearestBeaches.delegate = self;
    IBTableViewNearestBeaches.dataSource = self;
    IBTableViewNearestBeaches.layoutMargins = UIEdgeInsetsZero;
    
    IBTableViewSearchResult.delegate = self;
    IBTableViewSearchResult.dataSource = self;
    
    IBTableViewBeachesByLocation.layoutMargins = UIEdgeInsetsZero;
    
    [IBTableViewSearchResult setHidden:true];
    
    IBViewForSearch.layer.borderWidth = 1;
    IBViewForSearch.layer.borderColor = [UIColor whiteColor].CGColor;
    
    IBTextFieldSearch.textColor = [UIColor whiteColor];
    IBTextFieldSearch.font =[UIFont fontWithName:@"SinkinSans-400Regular" size:14.0];
    [IBTextFieldSearch setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    IBNSLayoutConstraintBottomForSearchResultTableView.constant = 0;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    
    NSInteger totalRow;
    if ([tableView isEqual:IBTableViewBeachesByLocation]) {
        totalRow = arrayLoaction.count - 1;
    }
    else if([tableView isEqual:IBTableViewNearestBeaches]){
        
        totalRow = arrayNearestBeaches.count;
        
    }else if ([tableView isEqual:IBTableViewSearchResult]){
        
        totalRow = arraySearchResult.count;
    }
    
    return totalRow;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    if ([tableView isEqual:IBTableViewBeachesByLocation]) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
        
        NSString *areaName = [[arrayLoaction objectAtIndex:indexPath.row]  objectForKey:@"name"];
        cell.textLabel.text = areaName;
        cell.textLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
        
        
    }else if([tableView isEqual:IBTableViewNearestBeaches]){
        
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"cellNearestBeaches" forIndexPath:indexPath];
        
        BeachInfo *beach = [arrayNearestBeaches objectAtIndex:indexPath.row];
        cell.textLabel.text = beach.title;
        cell.textLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2lf MI",beach.distanceFromCurrentLocation/1000];
        cell.detailTextLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
        
    }else if ([tableView isEqual:IBTableViewSearchResult]){
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"subRegionCell" forIndexPath:indexPath];
        
        BeachInfo *beach = [arraySearchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = beach.title;
        cell.textLabel.font = [UIFont fontWithName:@"SinkinSans-400Regular" size:13];
        
        
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 36.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0.1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if ([tableView isEqual:IBTableViewBeachesByLocation]) {
        
        [self performSegueWithIdentifier:@"segueSurfSubAddViewController" sender:indexPath];
    } else if ([tableView isEqual:IBTableViewNearestBeaches]){
        
        BeachInfo * beachNearest =[arrayNearestBeaches objectAtIndex:indexPath.row];
        NSString* sID = beachNearest.iDentity;
        
        DBHelper* dbHelper =[DBHelper getSharedInstance];
        BeachInfo * beach = [dbHelper getBeachFromID:sID];
        
        if (beach == nil) {
            [dbHelper insetDataBeachID:sID  Latitude:beachNearest.latitude Longitude:beachNearest.longitude Title:beachNearest.title Cameratype:beachNearest.cameratype SubRegion:beachNearest.subRegion Region:beachNearest.region];
            
            /*
             int lastRow = [Helper getPREFint:@"lastIndexPathRow"];
             
             [Helper setPREFStringValue:beachNearest.title sKey:[NSString stringWithFormat:@"%d",lastRow]];
             if (lastRow == 0) {
             [Helper setPREFStringValue:beachNearest.iDentity sKey:@"HomeBeach"];
             }
             */
            NSMutableArray* arrayOfKeys = nil;
            NSArray* arr=[Helper getPREFID:@"arrayOfkeysForAddedSurf"];
            
            if (arr != nil){
                arrayOfKeys =[[NSMutableArray alloc] initWithArray:arr];
            }
            else{
                arrayOfKeys=[[NSMutableArray alloc] init];
            }
            
            [arrayOfKeys addObject:[NSString stringWithFormat:@"%@_%@",beachNearest.title,beachNearest.iDentity]];
            
            [Helper setPREFID:arrayOfKeys :@"arrayOfkeysForAddedSurf"];
            
            [self.navigationController popToRootViewControllerAnimated:false];
            
        }
        else
        {
            UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Already added as favorite." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        
    } else if ([tableView isEqual:IBTableViewSearchResult]){
        
        BeachInfo* obj =[arraySearchResult objectAtIndex:indexPath.row];
        
        DBHelper* dbHelper =[DBHelper getSharedInstance];
        BeachInfo * beach = [dbHelper getBeachFromID:obj.iDentity];
        
        if (beach == nil) {
            [dbHelper insetDataBeachID:obj.iDentity  Latitude:obj.latitude Longitude:obj.longitude Title:obj.title Cameratype:obj.cameratype SubRegion:obj.subRegion Region:obj.region];
            
            /*
             int lastRow = [Helper getPREFint:@"lastIndexPathRow"];
             [Helper setPREFStringValue:obj.title sKey:[NSString stringWithFormat:@"%d",lastRow]];
             
             if (lastRow == 0) {
             [Helper setPREFStringValue:obj.iDentity sKey:@"HomeBeach"];
             }
             */
            
            NSMutableArray* arrayOfKeys = nil;
            NSArray* arr=[Helper getPREFID:@"arrayOfkeysForAddedSurf"];
            
            if (arr != nil){
                arrayOfKeys =[[NSMutableArray alloc] initWithArray:arr];
            }
            else{
                arrayOfKeys=[[NSMutableArray alloc] init];
            }
            
           // [arrayOfKeys addObject:obj.title];
            
             [arrayOfKeys addObject:[NSString stringWithFormat:@"%@_%@",obj.title,obj.iDentity]];
            
            [Helper setPREFID:arrayOfKeys :@"arrayOfkeysForAddedSurf"];
            
            [self.navigationController popToRootViewControllerAnimated:false];
            
        }
        else
        {
            UIAlertView* alert =[[UIAlertView alloc] initWithTitle:@"" message:@"Already added as favorite." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
}
#pragma mark - text Field Delegate
- (IBAction)textFieldDidChange:(UITextField *)sender {
    [self searchDisplayOnView:sender.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    IBNSLayoutConstraintBottomForSearchResultTableView.constant = 0;
    return true;
}
- (IBAction)searchButtonTouchUpInsideAction:(UIButton *)sender
{
    [IBTextFieldSearch resignFirstResponder];
    [self searchDisplayOnView:IBTextFieldSearch.text];
    
}
#pragma mark - Private Method

-(void)searchDisplayOnView:(NSString*)senderText
{
    
    if (appDelegate.arrayPreviousLocationTree.count > 1) {
        [appDelegate.arrayPreviousLocationTree removeAllObjects];
    }
    [arraySearchResult removeAllObjects];
    if (senderText.length>0)
    {
        //        for (int i=0; i<arrayAllBeaches.count; i++)
        //        {
        //            BeachInfo *beach = [arrayAllBeaches objectAtIndex:i];
        //
        //            NSRange titleResultsRange = [beach.title rangeOfString:senderText options:NSAnchoredSearch | NSCaseInsensitiveSearch ];
        //            if (titleResultsRange.length>0)
        //            {
        //                [arraySearchResult addObject:beach];
        //            }
        //        }
        //
        for (NSDictionary* dic in arrayLoaction) {
            
            // NSDictionary * data = [[[[[dic objectForKey:@"area"] objectForKey:@"content"] objectForKey:@"region"]  objectForKey:@"subregion"] objectForKey:@"spots"];
            
            for (NSDictionary* newDic in [dic objectForKey:@"content"]) {
                
                for (NSDictionary* n in [[newDic objectForKey:@"region"] objectForKey:@"content"]) {
                    for (NSDictionary* sport in [[n objectForKey:@"subregion"] objectForKey:@"spots"]) {
                        
                        
                        NSRange titleResultsRange = [[sport objectForKey:@"title"]rangeOfString:senderText options:NSAnchoredSearch | NSCaseInsensitiveSearch];
                        if (titleResultsRange.length>0)
                        {
                            
                            CLLocationDegrees latitude = [[sport objectForKey:@"lat"] doubleValue];
                            CLLocationDegrees longitude = [[sport objectForKey:@"lon"] doubleValue];
                            
                            CLLocation* beachLocation =[[CLLocation alloc] initWithLatitude:latitude
                                                                                  longitude:longitude];
                            CLLocationDistance distance = [appDelegate.myLocation distanceFromLocation:beachLocation];
                            
                            BeachInfo* beach = [[BeachInfo alloc] init];
                            beach.iDentity = [[sport objectForKey:@"id"] stringValue];
                            beach.latitude = [sport objectForKey:@"lat"];
                            beach.longitude = [sport objectForKey:@"lon"];
                            beach.title = [sport objectForKey:@"title"];
                            beach.cameratype = [sport objectForKey:@"cameratype"];
                            beach.distanceFromCurrentLocation =  distance;
                            beach.subRegion = [[n objectForKey:@"subregion"] objectForKey:@"name"];
                            beach.region = [[newDic objectForKey:@"region"] objectForKey:@"name"];
                            
                            [arraySearchResult addObject:beach];
                        }
                        
                        
                    }
                    
                }
            }
        }
        
        
        if(arraySearchResult.count == 0){
            
            UIView* lable = [IBTableViewSearchResult viewWithTag:989898];
            if (lable != nil) {
                [lable removeFromSuperview];
            }
            
            SikinLabel* label=[[SikinLabel alloc] initWithFrame:CGRectMake(IBTableViewSearchResult.center.x - 90, 10, 180, 26)];
            label.text = @"No Results Found";
            label.textColor = [UIColor whiteColor];
            label.tag =989898;
            [IBTableViewSearchResult addSubview:label];
            
        }else{
            
            UIView* lable = [IBTableViewSearchResult viewWithTag:989898];
            if (lable != nil) {
                [lable removeFromSuperview];
            }
            
        }
        
        
        [IBTableViewSearchResult reloadData];
        
        [IBTableViewBeachesByLocation setHidden:true];
        [IBTableViewNearestBeaches setHidden:true];
        
        [IBButtonClose setHidden:true];
        [IBLabelByLocation setHidden:true];
        [IBLabelNearestBeaches setHidden:true];
        [IBViewBorder setHidden:true];
        
        [IBTableViewSearchResult setHidden:false];
        
        [self.view bringSubviewToFront:IBTableViewSearchResult];
        
    }
    else
    {
        //[IBTextFieldSearch resignFirstResponder];
        
        [IBTableViewBeachesByLocation setHidden:false];
        [IBTableViewNearestBeaches setHidden:false];
        [IBButtonClose setHidden:false];
        [IBLabelByLocation setHidden:false];
        [IBLabelNearestBeaches setHidden:false];
        [IBViewBorder setHidden:false];
        
        [IBTableViewSearchResult setHidden:true];
        
    }
    
    
}
#pragma mark - Navigation Bar Buttom Tap Action
-(void)btnLeftBarMenuTap:(id)sender{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)closeButtonTouchUpInsideAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:false];
}
- (void)keyboardWasShown:(NSNotification *)notification
{
    
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    int height = MIN(keyboardSize.height,keyboardSize.width);
    //int width = MAX(keyboardSize.height,keyboardSize.width);
    
    IBNSLayoutConstraintBottomForSearchResultTableView.constant = height;
    //your other code here..........
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Pass the selected object to the new view controller.
    NSIndexPath* indexPath = (NSIndexPath*)sender;
    
    SurfSubAddViewController* surfSubAddViewController = [segue destinationViewController];
    
    /*
     surfSubAddViewController.arraySport = [[[arrayLoaction objectAtIndex:indexPath.row] objectForKey:@"area"] objectForKey:@"content"];
     */
    
    [appDelegate.arrayPreviousLocationTree removeAllObjects];
    
    [appDelegate.arrayPreviousLocationTree addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    
    NSMutableArray* arry=[[NSMutableArray alloc] init];
    
    for (NSDictionary* dic in [[arrayLoaction objectAtIndex:indexPath.row] objectForKey:@"content"]){
        
        [arry addObject: [dic objectForKey:@"region"]];
        
    }
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    
    
    surfSubAddViewController.arraySport = [arry sortedArrayUsingDescriptors:@[sort]];
    surfSubAddViewController.sTitle =@"region";
    
    
    
    /*
     NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
     surfSubAddViewController.arraySport = [[[[arrayLoaction objectAtIndex:indexPath.row] objectForKey:@"area"] objectForKey:@"content"] sortedArrayUsingDescriptors:@[sort]];
     */
}



@end
