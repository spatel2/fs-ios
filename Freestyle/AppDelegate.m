

#import "AppDelegate.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "ProximityTagStorage.h"
#import "ConnectionManager.h"
#import "ServerManager.h"
#import "Proxy.h"
#import "MSWeakTimer.h"

#import <AudioToolbox/AudioToolbox.h>

@interface AppDelegate ()
@property(nonatomic,strong)NSMutableData *myMutableData;
@property(nonatomic,strong)NSDate *SendSportInfoDate;
@end

@implementation AppDelegate
{
    IBOutlet UIView *overlayView;
    IBOutlet UIView *confirmOverlayView;
    IBOutlet UIImageView *confirmImageView;
    int flashmode;
    IBOutlet UIButton *flashButton;
    UIImage *clickedImage;
    NSString *mediaType;
    IBOutlet UILabel *imageSavedLabel;
    GMSMarker *locationMarker_;
    BOOL isAppInstallNow;
    
    MZTimerLabel *timerlbl;
    
    
    int avgspeed;
    int hartrate;
    float distance;
    
    
}
@synthesize gActButtonStartSelectionState,gActButtonPauseSelectionState;

@synthesize gActHeartRate,gAct_ticks,gAct_timer,gActAge,gActDistance,gActEndLocation,gActGender,gActHeight,gActUnit,gActMaxSpeed,gActMinSpeed,gActWeight,gActStartLocation,gActTempLocation,gActStartActivityName,gActHours,gActMinutes,gActSeconds,gActCalculateCalories,gActCurrentMapView,gActLiveMutablePathList,gActMapViewZoomLevel,gActLastSaveActivityID,dWeight,lableTimer;

@synthesize arrayOfLocations,isMapOnScreen;

@synthesize appdelegateProximity=_appdelegateProximity,reConnBtnTag=_reConnBtnTag;

@synthesize pickerController=_pickerController,xiangjiisopen=_xiangjiisopen,cameraopen, flagcmd;

@synthesize myLocation;
@synthesize myLocationManager;

@synthesize dicWeatherData;

@synthesize firstNearestBeach;

@synthesize   items=_items,playNum=_playNum,isPlayorPause=_isPlayorPause,lbl_song_name_app,img_song_app,lbl_song_detail_app,btn_song_img_app,alertstr;

@synthesize player=_player;


@synthesize arrayPreviousLocationTree;

#pragma mark - Application Life Cycle
- (void)getUserData{
    if ([Helper getPREF:@"prfAge"]!=nil) {

        NSString* weight= [Helper getPREF:@"prfWeight"];
        if ([weight containsString:@"lbs"]) {
            
            float fWeight = [[Helper getDigitFromString:weight] floatValue] * 0.453592;
            gActWeight = fWeight;
        }
        
        if ([[Helper getPREF:@"prfGender"] isEqualToString:@"MALE"]) {
            gActGender = @"MALE";
            gActHeartRate = 110;
        }
        else{
            gActGender = @"FEMALE";
            gActHeartRate = 110;
        }
        
        gActAge = [[Helper getDigitFromString:[Helper getPREF:@"prfAge"]] intValue];
        gActWeight= [[Helper getDigitFromString:[Helper getPREF:@"prfWeight"]] intValue];
        gActHeight = [[Helper getDigitFromString:[Helper getPREF:@"prfHeight"]] intValue];
        
        //@"IMPERIAL"]) {
        
        //@"METRIC"]){
        
    }
    else{
        gActGender = @"MALE";
        gActAge = 18;
        gActWeight = 195;
        gActHeight = 182;
        gActHeartRate = 110;
        
        [Helper setPREFStringValue:@"195 lbs"sKey:@"prfWeight"];
        [Helper setPREFStringValue:@"182.88 cm" sKey:@"prfHeight"];
        
        [Helper setPREFStringValue:@"18" sKey:@"prfAge"];
        [Helper setPREFStringValue:@"MALE" sKey:@"prfGender"];
        
        /*
         o    Default weight to 195 lbs. and 88 kg in picker
         o    Default height to 6’0” ft. and 182 cm in picker
         o    Default age to 18 in picker
         o    Default to male in picker
         */
        
        
    }
    gActUnit=[Helper getPREF:@"prfUnit"];
    if (gActUnit == nil) {
        gActUnit = @"IMPERIAL";
    }

    arrayPreviousLocationTree =[NSMutableArray array];

    
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
  
    
    // Override point for customization after application launch.
    isMapOnScreen = false;
    isAppInstallNow=false;
    [self getUserData];
    
    [[ProximityTagStorage sharedInstance] loadTags];
    [ServerManager sharedInstance];
    
    [self SetDefaultUserInfo];
    [self InitMusicPlayer];
    //[self PalyMusic];
    
    //[GMSServices provideAPIKey:@"AIzaSyDOGncMwEIH9C4qZvrvfud64QAX_3wpQIU"];
    [GMSServices provideAPIKey:@"AIzaSyARb4zedRp8hIuUagIqnMLRtwolE2GCyFE"];
    
    if ([Helper getPREFint:seeTutorialNextTime] == 1) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        
        FBSDKAccessToken* accessToken = [FBSDKAccessToken currentAccessToken];
        
        NSString* viewControllerID=@"signUpViewController";
        
        if (accessToken!=nil) {
            viewControllerID = @"homeViewController";
        }else if([Helper getPREFID:@"prefUserLoginInfo"]){
            viewControllerID = @"homeViewController";
        }
        
         UIViewController* ViewController =  [mainStoryboard instantiateViewControllerWithIdentifier:viewControllerID];
         
         UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:ViewController];
        
        
//        UIViewController* homeViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"homeViewController"];
//        UINavigationController* navigationCont = [[UINavigationController alloc] initWithRootViewController:homeViewController];
       
        
        [navigationCont.navigationBar setBarTintColor:COLOR_DARKBLUE];
        
        
        MFSideMenuContainerViewController*  mFSideMenuContainerViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"mfSideMenuContainerViewController"];
        mFSideMenuContainerViewController.centerViewController = navigationCont;
        mFSideMenuContainerViewController.leftMenuViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"leftSliderView"];
        
        self.window.rootViewController = mFSideMenuContainerViewController; //navigationCont
    }
    
    [self findMyCurrentLocation];
    
    if([Helper iSConnectedToNetwork] == true){
        
        
        NSUserDefaults* stndUserDefault=[NSUserDefaults standardUserDefaults];
        
        
        NSDate* savedDataDate =[stndUserDefault objectForKey:@"prfDate"];
        
        if (savedDataDate == nil) {
            
            [self getWeatherData];
            
        }
        else
        {
            
            dicWeatherData = [stndUserDefault objectForKey:@"prfWeatherData"];
            NSDate *today = [NSDate date];
            NSTimeInterval distanceBetweenDates = [today timeIntervalSinceDate:savedDataDate];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            
            int iHour=3;
            NSString* sHour= [Helper getPREF:@"prfManual"];
            if (sHour != nil) {
                iHour = [[Helper getDigitFromString:sHour] intValue];
            }
            
            if (hoursBetweenDates >= iHour) {
                [self getWeatherData];
            }
        }
    }
    
    
    
    //music functionality
    
    //song detail
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    NSString *stralert = [savedStock objectForKey:@"alertstr"]?[savedStock objectForKey:@"alertstr"]:@"";
    
    
    
    if ([strUserName isEqualToString:@"0"]) {
        
    }
    else
    {
        [data setObject:@"1" forKey:@"flag"];
        self.flag=@"1";
        
        [data writeToFile: path atomically:YES];
        
    }
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    flagcmd=1;
    [[ProximityTagStorage sharedInstance] saveTags];
    
    /*
     if (gActButtonStartSelectionState != true)
     {
     UIApplication* app = [UIApplication sharedApplication];
     __block UIBackgroundTaskIdentifier bgTask;
     bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
     dispatch_async(dispatch_get_main_queue(), ^{
     if (bgTask != UIBackgroundTaskInvalid) {
     bgTask = UIBackgroundTaskInvalid;
     }
     });
     }];
     
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
     
     dispatch_async(dispatch_get_main_queue(), ^{
     
     if (bgTask != UIBackgroundTaskInvalid) {
     bgTask = UIBackgroundTaskInvalid;
     }
     });
     });
     }
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    [[ConnectionManager sharedInstance] retrieveKnownPeripherals];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RefereshImage" object:self];
    
    flagcmd=0;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //
    
      [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[[ProximityTagStorage sharedInstance] tags] removeAllObjects];
    [self saveContext];
}
#pragma mark - Private Methods
-(void)getWeatherData{
    NSDictionary *dicParam = @{
                               @"service": @"cams",
                               @"user_key" : @"abca50124d77d41504c4d53e29c8d359"
                               };
    WSFrameWork *wsWeather = [[WSFrameWork alloc] initWithURLAndParams:@"header" dicParams:dicParam];
    //wsWeather.sDomainName = @"http://slapi01.surfline.com/v1/";
    wsWeather.sDomainName = @"http://api.surfline.com/v1/";
    wsWeather.WSType = kGET;
    wsWeather.isLogging = false;
    wsWeather.onSuccess = ^(NSDictionary *dicResponce)
    {
        //NSLog(@"==%@",dicResponce);
        //NSLog(@"dicResponce Class :: %@",[dicResponce class]);
        dicWeatherData = dicResponce;
        
        //   id objs = [dicResponce allValues];
        
        NSUserDefaults* stndUserDefault=[NSUserDefaults standardUserDefaults];
        [stndUserDefault setObject:dicWeatherData forKey:@"prfWeatherData"];
        [stndUserDefault setObject:[NSDate date] forKey:@"prfDate"];
        
        if ([Helper getPREF:@"HomeBeach"] == nil) {
            [self getFirstNearestBeach];
        }
        
    };
    [wsWeather send];
    
}

-(void)findMyCurrentLocation{
    //	return;
    if (self.myLocationManager == nil) {
        self.myLocationManager = [[CLLocationManager alloc] init];
    }
    [self.myLocationManager setDelegate:self];
    self.myLocationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;//kCLLocationAccuracyBest;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([self.myLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)])) {
            // choose one request according to your business.
            if ([self.myLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                // choose one request according to your business.
                if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]) {
                    [self.myLocationManager  requestAlwaysAuthorization];
                } else {
                    NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription");
                }
            }else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.myLocationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationWhenInUseUsageDescription");
            }
        }
        [self.myLocationManager startUpdatingLocation];
    }
    else{
        if([CLLocationManager locationServicesEnabled])
        {
            [self.myLocationManager startUpdatingLocation];
        }
    }
}

-(void)getFirstNearestBeach{
    
    firstNearestBeach = [[BeachInfo alloc] init];
    for (NSDictionary* dic in [dicWeatherData objectForKey:@"global"]) {
        for (NSDictionary* newDic in [[dic objectForKey:@"area"] objectForKey:@"content"]){
            for (NSDictionary* n in [[newDic objectForKey:@"region"] objectForKey:@"content"]) {
                for (NSDictionary* sport in [[n objectForKey:@"subregion"] objectForKey:@"spots"]) {
                    
                    CLLocationDegrees latitude = [[sport objectForKey:@"lat"] doubleValue];
                    CLLocationDegrees longitude = [[sport objectForKey:@"lon"] doubleValue];
                    CLLocation* beachLocation =[[CLLocation alloc] initWithLatitude:latitude
                                                                          longitude:longitude];
                    CLLocationDistance distance = [myLocation distanceFromLocation:beachLocation];
                    
                    if (firstNearestBeach.iDentity == nil || firstNearestBeach.iDentity.length == 0) {
                        
                        firstNearestBeach.distanceFromCurrentLocation = distance;
                        firstNearestBeach.iDentity = [[sport objectForKey:@"id"] stringValue];
                        firstNearestBeach.latitude = [sport objectForKey:@"lat"];
                        firstNearestBeach.longitude = [sport objectForKey:@"lon"];
                        firstNearestBeach.title = [sport objectForKey:@"title"];
                        firstNearestBeach.cameratype = [sport objectForKey:@"cameratype"];
                        firstNearestBeach.distanceFromCurrentLocation =  distance;
                        firstNearestBeach.subRegion = [[n objectForKey:@"subregion"] objectForKey:@"name"];
                        firstNearestBeach.region = [[newDic objectForKey:@"region"] objectForKey:@"name"];
                        
                        
                    }else if (firstNearestBeach.distanceFromCurrentLocation > distance){
                        
                        firstNearestBeach.distanceFromCurrentLocation = distance;
                        firstNearestBeach.iDentity = [[sport objectForKey:@"id"] stringValue];
                        firstNearestBeach.latitude = [sport objectForKey:@"lat"];
                        firstNearestBeach.longitude = [sport objectForKey:@"lon"];
                        firstNearestBeach.title = [sport objectForKey:@"title"];
                        firstNearestBeach.cameratype = [sport objectForKey:@"cameratype"];
                        firstNearestBeach.distanceFromCurrentLocation =  distance;
                        firstNearestBeach.subRegion = [[n objectForKey:@"subregion"] objectForKey:@"name"];
                        firstNearestBeach.region = [[newDic objectForKey:@"region"] objectForKey:@"name"];
                        
                    }
                    
                }
                
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NearestBeach" object:nil];
    
}
#pragma mark -  Location Delegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    if (status == kCLAuthorizationStatusAuthorizedAlways ||
        status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        
        [self.myLocationManager startUpdatingLocation];
        
        if ([Helper getPREF:@"isFirstInstalled"] == nil) {
            isAppInstallNow = true;
            [Helper setPREFStringValue:@"No" sKey:@"isFirstInstalled"];
        }
        
    }else if(status == kCLAuthorizationStatusNotDetermined){
        
        if ([self.myLocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]) {
                [self.myLocationManager  requestAlwaysAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription");
            }
        }else if ([self.myLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [self.myLocationManager  requestWhenInUseAuthorization];
            } else {
                NSLog(@"Info.plist does not contain NSLocationWhenInUseUsageDescription");
            }
        }
    }
    
    
}

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    [self setMyLocation:[locations lastObject]];
    
    
    if (isAppInstallNow == true) {
        [self getFirstNearestBeach];
        isAppInstallNow = false;
        
        [myLocationManager stopUpdatingLocation];
        
    }else if (gActButtonStartSelectionState == false){
        
        [myLocationManager stopUpdatingLocation];
        
    }
    
    if (arrayOfLocations != nil && gActButtonStartSelectionState == true) {
        
        [arrayOfLocations addObject:[locations lastObject]];
        
    }
    
    if (gActCurrentMapView!=nil) {
        
        /*if (locationMarker_ == nil) {
         locationMarker_ = [[GMSMarker alloc] init];
         locationMarker_.position = myLocation.coordinate;
         locationMarker_.groundAnchor = CGPointMake(0.5f, 0.97f);
         locationMarker_.title = self.gActStartActivityName;//@"CURRENT";
         //locationMarker_.snippet = self.gActStartActivityName;
         locationMarker_.map = gActCurrentMapView;
         }else
         */{
             [gActLiveMutablePathList addCoordinate:myLocation.coordinate];
             
             GMSPolyline *poly = [[GMSPolyline alloc] init];
             poly.path = gActLiveMutablePathList;
             poly.strokeWidth = 2;
             poly.geodesic = YES;
             poly.map = gActCurrentMapView;
             
             GMSCameraUpdate *move = [GMSCameraUpdate setTarget:myLocation.coordinate zoom:    gActMapViewZoomLevel];
             [gActCurrentMapView animateWithCameraUpdate:move];
             
             [CATransaction begin];
             [CATransaction setAnimationDuration:2.0];
             //locationMarker_.position = myLocation.coordinate;
             [CATransaction commit];
         }
        
    }/*else{
      locationMarker_ = nil;
      }*/
}

#pragma mark -
//**** create camera ******
///========== CAMERA ACTIONS =============

- (IBAction)cameraSwitch:(id)sender {
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (app.pickerController.cameraDevice == UIImagePickerControllerCameraDeviceRear) {
        app.pickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    else {
        app.pickerController.cameraDevice = UIImagePickerControllerCameraDeviceRear;
    }
}

- (IBAction)cameraTakePicture:(id)sender {
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIView *flashCameraView = [[UIView alloc]initWithFrame:overlayView.frame];
    flashCameraView.backgroundColor = [UIColor whiteColor];
    flashCameraView.alpha = 0;
    [overlayView addSubview:flashCameraView];
    
    [UIView animateWithDuration:0.1 animations:^{
        flashCameraView.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 animations:^{
            flashCameraView.alpha = 0.0;
        } completion:^(BOOL finished) {
            [flashCameraView removeFromSuperview];
        }];
    }];
    
    
    [app.pickerController takePicture];
}

- (IBAction)flashToggle:(id)sender {
    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    if (flashmode == 0) {
        flashmode = 1;
        [flashButton setImage:[UIImage imageNamed:@"flash_auto"] forState:UIControlStateNormal];
        app.pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
    }
    else if(flashmode == 1){
        flashmode = 2;
        [flashButton setImage:[UIImage imageNamed:@"flash_off"] forState:UIControlStateNormal];
        app.pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOff;
    }else{
        flashmode = 0;
        [flashButton setImage:[UIImage imageNamed:@"flash_on"] forState:UIControlStateNormal];
        app.pickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeOn;
    }
}

- (IBAction)cameraDismiss:(id)sender {
    UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ([activeController isKindOfClass:[UINavigationController class]]) {
        activeController = [(UINavigationController*) activeController visibleViewController];
    }
    self.xiangjiisopen=@"no";
    [activeController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)overlayFocusTouch:(id)sender {
    if(![[overlayView subviews]containsObject:confirmOverlayView]){
        UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
        CGPoint location = [tap locationInView:overlayView];
        
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(location.x - 25, location.y - 25, 50, 50)];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        imgView.image = [UIImage imageNamed:@"autofocus_up"];
        imgView.alpha = 0.5;
        [overlayView addSubview:imgView];
        
        [UIView animateWithDuration:0.3 animations:^{
            imgView.alpha = 1;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3 animations:^{
                imgView.alpha = 0.5;
            } completion:^(BOOL finished) {
                [imgView removeFromSuperview];
            }];
        }];
    }
}



#pragma mark -
//#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.iBlazing.KennethCole" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Freestyle" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Freestyle.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            abort();
        }
    }
}

#pragma mark -
//拍照delegate 保存图片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    clickedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    confirmImageView.image = [self fixOrientation:clickedImage];
    confirmOverlayView.frame = overlayView.frame;
    [overlayView addSubview:confirmOverlayView];
    
    
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if ([mediaType isEqualToString:@"public.image"]) {
            UIImageWriteToSavedPhotosAlbum(clickedImage,
                                           self, // send the message to 'self' when calling the callback
                                           @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), // the selector to tell the method to call on completion
                                           NULL);
            [UIView animateWithDuration:1 animations:^{
                imageSavedLabel.alpha = 1;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:1 animations:^{
                    imageSavedLabel.alpha = 0;
                }];
            }];
        }
        [confirmOverlayView removeFromSuperview];
    });
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    self.xiangjiisopen=@"no";
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    if (error) {
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefereshImage" object:self];
    }
}


- (UIImage *)fixOrientation:(UIImage *)img {
    
    UIImage *returnImage;
    if (self.pickerController.cameraDevice == 0) {
        if (img.imageOrientation == 3) {
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }else if(img.imageOrientation == 0){
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }else if(img.imageOrientation == 1){
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }else{
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }
    }else{
        if (img.imageOrientation == 3) {
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }else if(img.imageOrientation == 0){
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:2];
        }else if(img.imageOrientation == 1){
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:2];
        }else{
            returnImage = [UIImage imageWithCGImage:img.CGImage scale:1.0 orientation:3];
        }
    }
    return returnImage;
}

-(void)SetDefaultUserInfo{
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"UserName"]){
        return ;
        
    }
    [[NSUserDefaults standardUserDefaults] setValue:@"FYI User" forKey:@"UserName"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Male" forKey:@"Gender"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"25" forKey:@"Age"];
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@"180cm" forKey:@"Height"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"70kg" forKey:@"Weight"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Camera" forKey:@"C1"];
    [[NSUserDefaults standardUserDefaults] setValue:@"Play/Pause Music" forKey:@"C2"];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Find Phone" forKey:@"C3"];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
}

#pragma mark -
//music method
-(void)PalyMusic{
    return;
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setDelegate:self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    
    UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
    AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(sessionCategory), &sessionCategory);
    
    UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
    AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute,sizeof (audioRouteOverride),&audioRouteOverride);
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}


- (float)getAudioDuration {
    return [self.player duration];
}
/*
 * Simply fire the play Event
 */
- (void)playAudio {
    // [self.player play];
}

/*
 * Simply fire the pause Event
 */
- (void)pauseAudio {
    // [self.player pause];
}

/*
 * get playingState
 */
- (BOOL)isPlaying {
    return [self.player isPlaying];
}

-(NSString*)timeFormat:(float)value{
    
    float minutes = floor(lroundf(value)/60);
    float seconds = lroundf(value) - (minutes * 60);
    
    int roundedSeconds = lroundf(seconds);
    int roundedMinutes = lroundf(minutes);
    
    NSString *time = [[NSString alloc]
                      initWithFormat:@"%d:%02d",
                      roundedMinutes, roundedSeconds];
    return time;
}

/*
 * To set the current Position of the
 * playing audio File
 */
- (void)setCurrentAudioTime:(float)value {
    // [self.player setCurrentTime:value];
}

/*
 * Get the time where audio is playing right now
 */
- (NSTimeInterval)getCurrentAudioTime {
    return [self.player currentTime];
}
/*
 * BackGroundActivity
 */



#pragma mark - ActivityTimer
-(void)startTimerWithlable{
    _prevMin = 0;
    
    self.myMutableData=[NSMutableData data];
    NSString* weight= [Helper getPREF:@"prfWeight"];
    
    if ([weight containsString:@"kg"]) {
        
        dWeight =  [[Helper getDigitFromString:weight] doubleValue] * 2.20462;
    }else{
        dWeight = [[Helper getDigitFromString:weight] doubleValue];
    }
    lableTimer = [[UILabel alloc] init];
    timerlbl = [[MZTimerLabel alloc] initWithLabel:lableTimer andTimerType:MZTimerLabelTypeStopWatch];
    timerlbl.timeFormat = @"HH:mm:ss";
    [self startTimerLable];
    
    [[Proxy shared] StartSportTimer];
    
    if ([self.appdelegateProximity isConnected]) {
        Byte byte =0x58;//stop Sport Data Sending
        [self.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
        NSLog(@"First issued instructions 0x58");
    }
    
}
-(void)startTimerLable{
    
    [timerlbl start];
}
-(void)pauseTimerLabel{
    if ([timerlbl counting]) {
        [timerlbl pause];
    }
}

-(void)stopTimerLabel{
    [[Proxy shared] StopSportTimer];
    
    if ([timerlbl counting]) {
        [timerlbl pause];
    }
}
-(void)resetTimerLabel{
    
    [timerlbl reset];
    
}
-(void)startTimer{
    
    
    gAct_timer  = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTick) userInfo:nil repeats:true];
    
    [[NSRunLoop currentRunLoop] addTimer:gAct_timer forMode:NSRunLoopCommonModes];
    [[NSRunLoop currentRunLoop] run];
    [gAct_timer fire];
}
- (void)timerTick
{
    // Timers are not guaranteed to tick at the nominal rate specified, so this isn't technically accurate.
    // However, this is just an example to demonstrate how to stop some ongoing activity, so we can live with that inaccuracy.
    gAct_ticks += 0.1;
    gActSeconds = fmod(gAct_ticks, 60.0);
    gActMinutes = fmod(trunc(gAct_ticks / 60.0), 60.0);
    gActHours = trunc(gAct_ticks / 3600.0);
    
    if (self.gActButtonStartSelectionState == true && self.flagcmd != 1) {
        [self WriteSportValue];
    }
    //    NSArray *arrTime = [self.lableTimer.text componentsSeparatedByString:@":"];
    //    int _seconds = [[arrTime objectAtIndex:2] intValue];
    //
    //    //if ((int)gActSeconds % 10 == 0 && gActSeconds >= 10) {
    //    if (_seconds % 10 == 0 && _seconds >= 10) {
    //NSLog(@"%d", _seconds);
    NSArray* timeArray=[lableTimer.text componentsSeparatedByString:@":"];
    
    int second =[[timeArray objectAtIndex:2] intValue];
    
    if (second % 10 == 0 && second >= 10) {
        
        gActDistance += [gActTempLocation distanceFromLocation:myLocation];
        gActTempLocation = myLocation;
        
        CLLocationSpeed currentSpeed = myLocation.speed * 3.6;
        
        if (currentSpeed> 0 ){
            if (gActMinSpeed > currentSpeed) {
                gActMinSpeed = currentSpeed;
            }
            
            if (gActMaxSpeed < currentSpeed) {
                gActMaxSpeed = currentSpeed;
            }
        }
        
    }
    
    gActCalculateCalories = [self calculateCalories];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TimeElapsed" object:nil];
    
}
#pragma mark - Private Method
- (double)calculateCalories{
    
    double caloriesBurned = 0;
    
    /*
     [23/04/15 2:05:25 pm] Mitika: Women:
     
     C = (0.4472 x H – 0.05741 x W + 0.074 x A – 20.4022) x T / 4.184. C is the number of calories that you burned, H is your average heart rate, W is your weight, A is your age and T is the length of your exercise session in minutes
     [23/04/15 2:05:36 pm] Mitika: Men:
     
     C = (0.4472 x 138 – 0.05741 x 146 + 0.074 x 28 – 20.4022) x 36 / 4.184
     */
    
    if ([gActGender isEqualToString:@"MALE"]) {
        
        //caloriesBurned = (((gActAge * 0.2017) - (gActWeight * 0.1988) + (gActHeartRate * 0.6309) - 55.0969 ) * ((gAct_ticks * 10) / 3600)) / 4.184 ;
        ///caloriesBurned = ((-55.0969 + (0.6309 * gActHeartRate) + (0.1988 * dWeight) + (0.2017 * gActAge))/4.184) * ((gAct_ticks * 10) / 3600);
        
        caloriesBurned = (((0.4472 * gActHeartRate) - (0.05741 * dWeight) + (0.074 * gActAge) - 20.4022) * ([timerlbl getTimeCounted] / 60)) / 4.184 ;
        
    }else if ([gActGender isEqualToString:@"FEMALE"]){
        
        //caloriesBurned = (((gActAge * 0.074) - (gActWeight * 0.1263) + (gActHeartRate * 0.4472) - 20.4022 ) * ((gAct_ticks * 10) / 3600)) / 4.184;
        //caloriesBurned = ((-20.4022 + (0.4472 * gActHeartRate) + (0.1263 * dWeight) + (0.074 * gActAge))/4.184) * ((gAct_ticks * 10) / 3600);
        
        caloriesBurned = (((0.4472 * gActHeartRate) - (0.05741 * dWeight) + (0.074 * gActAge) - 20.4022) * ([timerlbl getTimeCounted] / 60)) / 4.184 ;
        
    }
    //Male: ((-55.0969 + (0.6309 x HR) + (0.1988 x W) + (0.2017 x A))/4.184) x 60 x T
    //Female: ((-20.4022 + (0.4472 x HR) - (0.1263 x W) + (0.074 x A))/4.184) x 60 x T
    /*
     Men use the following formula:
     Calories Burned = [(Age x 0.2017) — (Weight x 0.09036) + (Heart Rate x 0.6309) — 55.0969] x Time / 4.184.
     
     Women use the following formula:
     Calories Burned = [(Age x 0.074) — (Weight x 0.05741) + (Heart Rate x 0.4472) — 20.4022] x Time / 4.184.
     */
    
    
    
    return caloriesBurned;
}
-(void)WriteSportValue{
    
    NSArray *arrTime = [self.lableTimer.text componentsSeparatedByString:@":"];
    int _seconds = [[arrTime objectAtIndex:2] intValue];
    
    int _curMin = [[arrTime objectAtIndex:1] intValue];
    
    //if ((int)gActSeconds % 10 == 0 && gActSeconds >= 10) {
    if ((_seconds % 10 == 0 && _seconds >= 10) || _prevMin != _curMin) {
        if ([self.appdelegateProximity isConnected]) {
            
            Byte byte[20];
            byte[0]=0x06;//leixing
            byte[1]=0x14;//changdu
            
            NSArray *arrTime = [self.lableTimer.text componentsSeparatedByString:@":"];
            
            Byte *hourbyte=[self FloatToData:[[arrTime objectAtIndex:0] intValue]];
            byte[2]=hourbyte[3];//Hour
            Byte *minutebyte=[self FloatToData:[[arrTime objectAtIndex:1] intValue]];
            byte[3]=minutebyte[3];//minut
            Byte *secondbyte=[self FloatToData:[[arrTime objectAtIndex:2] intValue]];
            byte[4]=secondbyte[3];//second
            
            distance=(gActDistance / 1000);
            Byte *distbyte=[self FloatToData:distance*1000.0];
            byte[5]=(Byte) distbyte[0];//Distance in meters
            byte[6]=(Byte) distbyte[1];
            byte[7]=(Byte) distbyte[2];
            
            if (distbyte[3] < 10) distbyte[3] = (Byte) (distbyte[3] * 10);
            byte[8]=(Byte) distbyte[3];
            
            Byte *Calbyte=[self FloatToData:(int)(gActCalculateCalories)];
            byte[9]=(Byte) Calbyte[0];//Calorie consumption units kcal
            byte[10]=(Byte) Calbyte[1];
            byte[11]=(Byte) Calbyte[2];
            byte[12]=(Byte) Calbyte[3];
            
            byte[13]=(Byte)avgspeed;//Speedbyte[2];//Speed km/h
            byte[14]=(Byte)00;//Speedbyte[3];
            
            Byte *GaoDubyte=[self FloatToData:myLocation.altitude];
            byte[15]=  GaoDubyte[2];//Height m
            byte[16]=  GaoDubyte[3];
            
            int TempDirection = (int)([self getDirectionByte:myLocation.course]);
            byte[18]=(Byte) (TempDirection);//(TempDirection %100);//FXbyte[3];
            byte[17]=(Byte)00;//(TempDirection %100);//FXbyte[2];//   Degree direction;
            
            byte[19]=(Byte)hartrate;//byteFR;//Heart Rate
            
            NSData * data = [NSData dataWithBytes:&byte length:sizeof(byte)];
            [self.appdelegateProximity  phoneWriteDataToTagBig :data];
            
            NSLog(@"%d %d %d",[[arrTime objectAtIndex:0] intValue], [[arrTime objectAtIndex:1] intValue],[[arrTime objectAtIndex:2] intValue]);
            
        }
        
    }
    _prevMin = _curMin;
}
#pragma mark - 写入数据
-(Byte *)FloatToData:(NSInteger )floatvalue{
    NSString *string=[NSString stringWithFormat:@"%d",floatvalue];
    NSInteger zhuanInt=0;
    
    for (int i=1; i<string.length; i++) {
        int sixty=(int)pow(16,(double)i);
        int tenty=(int)pow(10, (double)i);
        zhuanInt=zhuanInt+(floatvalue/(tenty)%10)*sixty;
        
    }
    zhuanInt=zhuanInt+floatvalue%10;
    
    int32_t unswapped = zhuanInt;
    int32_t swapped = CFSwapInt32HostToBig(unswapped);
    char* a = (char*) &swapped;
    //NSMutableData 清空
    [self.myMutableData resetBytesInRange:NSMakeRange(0, [self.myMutableData length])];
    [self.myMutableData setLength:0];
    [self.myMutableData appendBytes:a length:sizeof(int32_t)];
    Byte *testbyte=(Byte *)[self.myMutableData bytes];
    
    return testbyte;
    
}
//direction degree method
-(int)getDirectionByte:(int)Direction{
    
    int sFinalDirection;
    if(Direction <= 190 &&  Direction >= 170){
        //        sFinalDirection = @"S";
        sFinalDirection = 180;
    }else if (Direction <= 10){
        //sFinalDirection = @"N";
        sFinalDirection =0;
    }else if (Direction >= 350){
        //sFinalDirection = @"N";
        sFinalDirection = 0;
    }else if (Direction <= 100 && Direction >=80){
        //sFinalDirection = @"E";
        sFinalDirection = 90;
    }else if (Direction<= 280  && Direction >= 260)
    {
        //sFinalDirection = @"W";
        sFinalDirection = 270;
    }else if (Direction<= 190 && Direction >= 170){
        //sFinalDirection = @"S";
        sFinalDirection = 180;
    }else if (Direction > 280 && Direction <= 310){
        // sFinalDirection = @"WNW";
        sFinalDirection = 292;
    }else if (Direction > 310 && Direction <= 330){
        //sFinalDirection = @"NW";
        sFinalDirection = 315;
    }else if (Direction > 330 && Direction < 350){
        // sFinalDirection = @"NNW";
        sFinalDirection = 338;
    }else if (Direction > 10 && Direction <= 30){
        // sFinalDirection = @"NNE";
        sFinalDirection = 23;
    }else if (Direction > 30 && Direction <= 50){
        //sFinalDirection = @"NE";
        sFinalDirection = 45;
    }else if (Direction > 50 && Direction < 80){
        // sFinalDirection = @"ENE";
        sFinalDirection = 69;
    }else if (Direction > 100 && Direction <= 130){
        // sFinalDirection = @"ESE";
        sFinalDirection = 112;
    }else if (Direction > 130 && Direction <= 150){
        //sFinalDirection = @"SE";
        sFinalDirection = 135;
    }else if (Direction > 150 && Direction < 170){
        // sFinalDirection = @"SSE";
        sFinalDirection = 158;
    }else if (Direction > 190 && Direction <= 210){
        //sFinalDirection = @"SSW";
        sFinalDirection = 202;
    }else if (Direction > 210 && Direction <= 230){
        // sFinalDirection = @"SW";
        sFinalDirection = 225;
    }else if (Direction > 230 && Direction < 260){
        //sFinalDirection = @"WSW";
        sFinalDirection = 249;
    }
    return sFinalDirection;
    
}

#pragma mark - Get Activity End Location and Area Name
-(void)getActivityEndLocationDetails{
    CLGeocoder *geoCoder = [[CLGeocoder alloc] init];
    
    [geoCoder reverseGeocodeLocation:gActEndLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         CLPlacemark *placemark= [placemarks firstObject];
         NSString* sLocation = [[NSString  stringWithFormat:@"%@, %@",[placemark locality],[placemark administrativeArea]] uppercaseString];
         
         DBHelper* dbHelper=[DBHelper getSharedInstance];
         [dbHelper UpdateActivityLog:gActLastSaveActivityID Location:sLocation];
     }];
}


-(void) InitMusicPlayer
{
    self.mpc = [MPMusicPlayerController systemMusicPlayer];
    [self.mpc setRepeatMode:MPMusicRepeatModeAll];
    [self.mpc setShuffleMode:MPMusicShuffleModeOff];
    [self.mpc beginGeneratingPlaybackNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CommandNextSong:)
                                                 name:@"CommandNextSong"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CommandPlayPause:)
                                                 name:@"CommandPlayPause"
                                               object:nil];
}

- (void) CommandNextSong:(NSNotification *) notification
{
    [self.mpc skipToNextItem];
    
    MPMusicPlaybackState playbackState = [self.mpc playbackState];
    if (playbackState == MPMusicPlaybackStateStopped || playbackState == MPMusicPlaybackStatePaused)
    {
        [self.mpc play];
    }
}

- (void) CommandPlayPause:(NSNotification *) notification
{
    MPMusicPlaybackState playbackState = [self.mpc playbackState];
    
    if (playbackState == MPMusicPlaybackStateStopped || playbackState == MPMusicPlaybackStatePaused)
    {
        [self.mpc play];
    }
    else if (playbackState == MPMusicPlaybackStatePlaying)
    {
        [self.mpc pause];
    }
    else
    {
        [self.mpc play];//[self.mpc skipToNextItem];
    }
}

@end






