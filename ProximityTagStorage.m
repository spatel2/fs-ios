//
//  ProximityTagStorage.m
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//

#import "ProximityTagStorage.h"
@implementation ProximityTagStorage
@synthesize tags = _tags;

static ProximityTagStorage* sharedProximityTagStorage = nil;

+ (ProximityTagStorage*) sharedInstance 
{
    if (sharedProximityTagStorage == nil) {
        sharedProximityTagStorage = [[ProximityTagStorage alloc] init];
    }
    return sharedProximityTagStorage;
}

- (id) init
{
    if(self = [super init])
    {
        
        // Initialize array to store tags
        _tags = [[NSMutableArray alloc] init];
        
       [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(telCallStateInconming) name:@"telCall" object:nil];
        
    }
    return self;
    
    
}



- (void) insertTag:(ProximityTag*) tag
{
    
    if(![self isTagInList:tag])
    {
        [_tags addObject:tag];
    }
    
}

- (void) removeTag:(ProximityTag*) tag
{
    [_tags removeObject:tag];
}

- (bool) isTagInList:(ProximityTag*) tag
{
    return [_tags containsObject:tag];
}

//遍历所有的设备 如果有设备的就每个设备都叫醒
-(void)telCallStateInconming{
    
    NSString *  isok= [[NSUserDefaults standardUserDefaults] valueForKey:@"iscallalarmset"];
    
    if ([@"yes" isEqualToString:isok]){
        
        
        for(ProximityTag* tag in _tags)
        {
            [tag findTag:PROXIMITY_TAG_ALERT_LEVEL_MILD];
        }
        
    }
}

- (bool) isPeripheralInList:(CBPeripheral*) peripheral
{
    for(ProximityTag* tag in _tags) 
    {
        if(tag.peripheral == peripheral)
        {
            return YES;
        }
    }
    return NO;
}

- (ProximityTag*) tagAtIndex:(NSUInteger) index
{
    return (ProximityTag*) [_tags objectAtIndex:index];
}

- (ProximityTag*) tagWithPeripheral:(CBPeripheral*) peripheral
{
    for (ProximityTag* tag in _tags)
    {
        if ([tag.peripheral isEqual:peripheral])
        {
            return tag;
        }
    }
    return nil;
}

- (ProximityTag*) tagWithUUID:(CFUUIDRef) UUIDRef
{
    for (ProximityTag* tag in _tags)
    {
        if (tag.peripheralCFUUIDRef == UUIDRef)
        {
            return tag;
        }
    }
    return nil;
}

- (NSMutableArray*) tagsWithoutPeripheral
{
    NSMutableArray *a = [NSMutableArray arrayWithArray:_tags];
    [a filterUsingPredicate:[NSPredicate predicateWithFormat:@"_peripheral == nil"]];
    return a;
}

- (NSMutableArray*) tagsWithUUID
{
    
    NSMutableArray *a = [NSMutableArray array];
    for (ProximityTag* tag in _tags)
    {
        if (tag.peripheral.UUID)
        {
            [a addObject:tag];
        }
    }
    return a;
}

- (void) saveTags
{
    NSMutableArray *tagsWithUUID = self.tagsWithUUID;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:[tagsWithUUID count] forKey:@"ProximityAppTagCount"];
    
    for (NSUInteger i = 0; i < [tagsWithUUID count]; i++)
    {
        
        NSData *encodedTag = [NSKeyedArchiver archivedDataWithRootObject:[tagsWithUUID objectAtIndex:i]];
        [defaults setObject:encodedTag forKey:[NSString stringWithFormat:@"ProximityAppTag%d", i]];
   
    }

    
}

- (void) loadTags
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    for (NSUInteger i = 0; i < [defaults integerForKey:@"ProximityAppTagCount"]; i++) 
    {
        NSData* encodedTag = [defaults objectForKey:[NSString stringWithFormat:@"ProximityAppTag%d", i]];
        
        [self insertTag:(ProximityTag*) [NSKeyedUnarchiver unarchiveObjectWithData:encodedTag]];
    }
    
     
    
}


 
@end
