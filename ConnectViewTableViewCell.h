//
//  ConnectViewTableViewCell.h
//  KennethCole
//
//  Created by iBlazing Mac Mini 3 on 13/01/15.
//  Copyright (c) 2015 iBlazing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConnectViewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@property (weak, nonatomic) IBOutlet UIImageView *markimg;

@end
