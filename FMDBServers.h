//
//  FMDBServers.h
//  FootballPrj
//
//  Created by mokbid on 13-5-10.
//  Copyright (c) 2013年 ytt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet.h"


@interface FMDBServers : NSObject{
    
}
@property(strong,nonatomic)FMDatabase * dbs;
@property(nonatomic,strong)NSMutableArray *arrayLocations;


+(FMDBServers *)ShareFMDB;
-(BOOL)OpenDatabase;
-(void)setDatabase;
-(BOOL)InsertDataWithIndex:(NSInteger)index InsertData:(NSArray *)DataArray;
-(BOOL)UpdateDataWithIndex:(NSInteger)index UpdateData:(NSArray *)DataArray Condition:(int)ConditionArray;
-(BOOL)DeleteDataWithKey:(NSInteger)key;
-(NSArray *)getDateFromTable:(NSInteger)index Condition:(NSArray *)ConditionArray;
-(NSArray *)getDateFromTable;
-(NSArray *)getLastDate:(NSInteger)index Condition:(NSString *)Condition;
-(void)close;


//BLE
-(BOOL)InsertBLETable:(NSString *)name UUID:(NSString *)uuid imageName:(NSString *)image;
-(BOOL)UpdateDataBLEInfo:(NSString *)name imageName:(NSString *)image Condition:(NSString *)uuid;
//-(BOOL)DeleteBLEInfo:(NSString *)UUID;
-(NSArray *)getDateFromBLETable:(NSString *)uuid;
@end
