//
//  SharedUI.m
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//
//

#import "SharedUI.h"

@implementation SharedUI
+ (void) showFailedConnectDialog:(ProximityTag*) tag
{
    
    UIAlertView * alt =[[UIAlertView alloc] initWithTitle:@"提示" message:[NSString stringWithFormat:@"设备%@连接失败，请删除设备%@重新连接。",tag.name, tag.name] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alt show];
    
}

+ (void)showOutOfRangeDialog:(ProximityTagAlertLevel) alertLevel forTag:(ProximityTag*) tag
{
    //if (alertLevel != PROXIMITY_TAG_ALERT_LEVEL_NONE)
    //{
      
        UILocalNotification *alarm = [[UILocalNotification alloc] init];
        alarm.alertBody = [NSString stringWithFormat:NSLocalizedString(@"outofrange", nil), tag.name];
        alarm.alertAction =NSLocalizedString(@"alertok", nil);
        alarm.soundName = @"Alarm-sound.wav";
        [[UIApplication sharedApplication] presentLocalNotificationNow:alarm];
    //}
}


+ (void) showFindPhoneDialog:(ProximityTagAlertLevel) alertLevel forTag:(ProximityTag*) tag
{
    
    UILocalNotification *alarm = [[UILocalNotification alloc] init];
    
    alarm.alertBody = [NSString stringWithFormat:@"%@正在寻找手机", tag.name];
    alarm.alertAction = @"OK";
    [[UIApplication sharedApplication] presentLocalNotificationNow:alarm];
    
}

@end
