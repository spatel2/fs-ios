//
//  FYIInfoViewController.h
//  FYIProject
//
//  Created by Hay on 13-10-29.
//  Copyright (c) 2013年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProximityTag.h"
#import "FMDBServers.h"


@interface FYIInfoViewController : UIViewController<ProximityTagDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property(nonatomic) ProximityTag * proximityTag;
@property(nonatomic,strong)UIImagePickerController* pickerController;

@property(nonatomic)  IBOutlet UIImageView * iamgeView;

@property(nonatomic) UILabel * nameLable;
@property(nonatomic) UILabel * rssiLable;

@property (weak, nonatomic) IBOutlet UILabel *Connect;

-(IBAction)clickRename:(id)sender;
-(IBAction)clickConnect:(id)sender;
-(IBAction)clickForget:(id)sender;
-(IBAction)clickCramera:(id)sender;

@end
