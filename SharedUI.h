//
//  SharedUI.h
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//
//

#import <Foundation/Foundation.h>

#import "ProximityTag.h"

@interface SharedUI : NSObject

+ (void) showFindPhoneDialog:(ProximityTagAlertLevel) alertLevel forTag:(ProximityTag*) tag;
+ (void) showFailedConnectDialog:(ProximityTag*) tag;
+ (void) showOutOfRangeDialog:(ProximityTagAlertLevel) alertLevel forTag:(ProximityTag*) tag;

@end
