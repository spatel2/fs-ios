//
//  FMDBServers.m
//  FootballPrj
//
//  Created by mokbid on 13-5-10.
//  Copyright (c) 2013年 ytt. All rights reserved.
//


#import "FMDBServers.h"
#import <UIKit/UIKit.h>

static FMDBServers *_sharedFMDB = nil;


@implementation FMDBServers
@synthesize dbs;

+(FMDBServers *)ShareFMDB{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _sharedFMDB = [[FMDBServers alloc] init];
        
    });
    return _sharedFMDB;
}

-(id)init{
    self=[super init];
    if (self) {
        [self setDatabase];
        [self CreatTableBLE];
        self.arrayLocations=[NSMutableArray arrayWithCapacity:500];
    }
    return self;
}

-(BOOL)OpenDatabase{
    //创建数据库
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [paths objectAtIndex:0];
    
   //NSString *documentDirectory = @"/Users/mobkidmobkid/Desktop/";

    //dbPath： 数据库路径，在Document中。
    NSString *dbPath = [documentDirectory stringByAppendingPathComponent:@"FYISport.sql"];
    self.dbs= [FMDatabase databaseWithPath:dbPath];
    return [self.dbs open];
    
}
-(void)setDatabase{

    if (![self OpenDatabase]) {
        
    }else {
        //在数据库打开的情况下，查询表  如果存在 就不重新建立
        //球队财务
        NSString *sql = [NSString stringWithFormat:@"select count(*) from sqlite_master where type='table' and table='%@'", @"FYISport"];
        int tableCount = [self.dbs intForQuery:sql] ;
        if (tableCount <= 0) {
            [self.dbs executeUpdate:@"CREATE TABLE FYISport (ID INTEGER PRIMARY KEY AUTOINCREMENT, Type text,Time text,AveSpeed text,MaxSpeed text,MinSpeed text,Distance text,HeartRate text,Calories text,Direction text,Altitide text,Position text,DateID text,City text,Weather text,Speed text)"];
        }
        
    }
    [self.dbs close];
}

//BLE 创建设备表格
-(void)CreatTableBLE{
    
    if (![self OpenDatabase]) {
        
    }else {
        //在数据库打开的情况下，查询表  如果存在 就不重新建立
        //球队财务
        NSString *sql = [NSString stringWithFormat:@"select count(*) from sqlite_master where type='table' and table='%@'", @"BLETable"];
        int tableCount = [self.dbs intForQuery:sql] ;
        if (tableCount <= 0) {
            [self.dbs executeUpdate:@"CREATE TABLE BLETable (ID INTEGER PRIMARY KEY AUTOINCREMENT, UUID  text,Name text,Photo text)"];
        }
        
    }
    [self.dbs close];
    
}


-(BOOL)InsertDataWithIndex:(NSInteger)index InsertData:(NSArray *)DataArray{
    BOOL result=YES;

    if (![self OpenDatabase]) {
        return NO;
    }
   

            //队员信息
           result=[self.dbs executeUpdate:@"INSERT INTO FYISport (Type,Time ,AveSpeed ,MaxSpeed ,MinSpeed ,Distance ,HeartRate ,Calories ,Direction ,Altitide ,Position,DateID,City,Weather,Speed) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[DataArray objectAtIndex:0],[DataArray objectAtIndex:1],[DataArray objectAtIndex:2],[DataArray objectAtIndex:3],[DataArray objectAtIndex:4],[DataArray objectAtIndex:5],[DataArray objectAtIndex:6],[DataArray objectAtIndex:7],[DataArray objectAtIndex:8],[DataArray objectAtIndex:9],[DataArray objectAtIndex:10],[DataArray objectAtIndex:11],[DataArray objectAtIndex:12],[DataArray objectAtIndex:13],[DataArray objectAtIndex:14]];
    
    if (result) {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Tips" message:@"Saved Successful" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    
    }

    return result;
}

//插入一条新的BLE设备信息
-(BOOL)InsertBLETable:(NSString *)name UUID:(NSString *)uuid imageName:(NSString *)image{
    BOOL result=YES;
    
    if (![self OpenDatabase]) {
        return NO;
    }
    
    //插入一条新的BLE设备信息
    result=[self.dbs executeUpdate:@"INSERT INTO BLETable (UUID,Name,Photo) VALUES (?,?,?)",uuid,name,image];
    
    return result;
    
}

-(BOOL)UpdateDataWithIndex:(NSInteger)index UpdateData:(NSArray *)DataArray Condition:(int)ConditionArray{
    if (![self OpenDatabase]) {
        return NO;
    }
    BOOL result = NO;

        result=[self.dbs executeUpdate:@"UPDATE  FYISport set Type = ?, Time = ?,  AveSpeed = ?, MaxSpeed=?, MinSpeed =? , Distance =? ,HeartRate= ?,Calories=?,Direction=?,Altitide=?,Position=? where ID = ? ",[DataArray objectAtIndex:0],[DataArray objectAtIndex:1],[DataArray objectAtIndex:2],[DataArray objectAtIndex:3],[DataArray objectAtIndex:4],[DataArray objectAtIndex:5],[DataArray objectAtIndex:6],[DataArray objectAtIndex:7],[DataArray objectAtIndex:8],[DataArray objectAtIndex:9],[DataArray objectAtIndex:10],[NSNumber numberWithInt:ConditionArray] ];

  
            if ([self.dbs hadError]) {
                
                //success = NO;
            }
            
            return result;
}

//更改BLE设备信息
-(BOOL)UpdateDataBLEInfo:(NSString *)name imageName:(NSString *)image Condition:(NSString *)uuid{
    if (![self OpenDatabase]) {
        return NO;
    }
    BOOL result = NO;
    
    result=[self.dbs executeUpdate:@"UPDATE  BLETable set Name = ?, Photo = ? where UUID = ? ",name,image,uuid];
    
    
    if ([self.dbs hadError]) {
        
        //success = NO;
    }
    
    return result;
}

-(BOOL)DeleteDataWithKey:(NSInteger)key{
    if (![self OpenDatabase]) {
        return NO;
    }
    BOOL result = YES;

        result=[self.dbs executeUpdate:@"DELETE from FYISport where ID = ?",key];

    return result;
}


-(NSArray *)getDateFromTable:(NSInteger)index Condition:(NSArray *)ConditionArray{
    if (![self OpenDatabase]) {
        return NO;
    }
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:50];

            FMResultSet *rs = [self.dbs executeQuery:@"SELECT * FROM FYISport where ID = ?",[ConditionArray objectAtIndex:0]];
    
            while ([rs next]) {
                NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithCapacity:50];
                [dic setValue:[rs stringForColumn:@"Type"] forKey:@"Type"];
                [dic setValue:[rs stringForColumn:@"Time"] forKey:@"Time"];
                [dic setValue:[rs stringForColumn:@"AveSpeed"] forKey:@"AveSpeed"];
                [dic setValue:[rs stringForColumn:@"MaxSpeed"] forKey:@"MaxSpeed"];
                [dic setValue:[rs stringForColumn:@"MinSpeed"] forKey:@"MinSpeed"];
                [dic setValue:[rs stringForColumn:@"Distance"] forKey:@"Distance"];
                [dic setValue:[rs stringForColumn:@"HeartRate"] forKey:@"HeartRate"];
                [dic setValue:[rs stringForColumn:@"DateID"] forKey:@"DateID"];
                [dic setValue:[rs stringForColumn:@"Calories"] forKey:@"Calories"];
                [dic setValue:[rs stringForColumn:@"Direction"] forKey:@"Direction"];
                [dic setValue:[rs stringForColumn:@"Altitide"] forKey:@"Altitide"];
                [dic setValue:[rs stringForColumn:@"Position"] forKey:@"Position"];
                [dic setValue:[rs stringForColumn:@"City"] forKey:@"City"];
                [dic setValue:[rs stringForColumn:@"Speed"] forKey:@"Speed"];
                [dic setValue:[rs stringForColumn:@"Weather"] forKey:@"Weather"];


                
                [array addObject:dic];
            }
            
            [rs close];
    

    return array;

}

-(NSArray *)getLastDate:(NSInteger)index Condition:(NSString *)Condition{
    if (![self OpenDatabase]) {
        return NO;
    }
    
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:50];
    
    FMResultSet *rs = [self.dbs executeQuery:@"SELECT * FROM FYISport where DateID = ?",Condition];
    
    while ([rs next]) {
        NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithCapacity:50];
        [dic setValue:[rs stringForColumn:@"Type"] forKey:@"Type"];
        [dic setValue:[rs stringForColumn:@"Time"] forKey:@"Time"];
        [dic setValue:[rs stringForColumn:@"AveSpeed"] forKey:@"AveSpeed"];
        [dic setValue:[rs stringForColumn:@"MaxSpeed"] forKey:@"MaxSpeed"];
        [dic setValue:[rs stringForColumn:@"MinSpeed"] forKey:@"MinSpeed"];
        [dic setValue:[rs stringForColumn:@"Distance"] forKey:@"Distance"];
        [dic setValue:[rs stringForColumn:@"HeartRate"] forKey:@"HeartRate"];
        [dic setValue:[rs stringForColumn:@"DateID"] forKey:@"DateID"];
        [dic setValue:[rs stringForColumn:@"Calories"] forKey:@"Calories"];
        [dic setValue:[rs stringForColumn:@"Direction"] forKey:@"Direction"];
        [dic setValue:[rs stringForColumn:@"Altitide"] forKey:@"Altitide"];
        [dic setValue:[rs stringForColumn:@"Position"] forKey:@"Position"];
        [dic setValue:[rs stringForColumn:@"City"] forKey:@"City"];
        [dic setValue:[rs stringForColumn:@"Speed"] forKey:@"Speed"];
        [dic setValue:[rs stringForColumn:@"Weather"] forKey:@"Weather"];
        
        [array addObject:dic];
    }
    
    [rs close];
    return array;
    
}

-(NSArray *)getDateFromTable{
    if (![self OpenDatabase]) {
        return NO;
    }
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:50];
    
    FMResultSet *rs = [self.dbs executeQuery:@"SELECT * FROM FYISport order by ID desc"];
    
    while ([rs next]) {
        NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithCapacity:50];
        [dic setValue:[rs stringForColumn:@"ID"] forKey:@"ID"];
        [dic setValue:[rs stringForColumn:@"DateID"] forKey:@"DateID"];
        [dic setValue:[rs stringForColumn:@"Time"] forKey:@"Time"];
        [dic setValue:[rs stringForColumn:@"Distance"] forKey:@"Distance"];
        
        [array addObject:dic];
    }
    [rs close];
    return array;
}

//获取制定BLE设备信息
-(NSArray *)getDateFromBLETable:(NSString *)uuid{
    if (![self OpenDatabase]) {
        return NO;
    }
    NSMutableArray *array=[NSMutableArray arrayWithCapacity:50];
    
    FMResultSet *rs = [self.dbs executeQuery:@"SELECT * FROM BLETable where UUID= ?",uuid];
    
    while ([rs next]) {
        NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithCapacity:50];
        [dic setValue:[rs stringForColumn:@"UUID"] forKey:@"UUID"];
        [dic setValue:[rs stringForColumn:@"Photo"] forKey:@"Photo"];
        [dic setValue:[rs stringForColumn:@"Name"] forKey:@"Name"];
        [array addObject:dic];
    }
    [rs close];
    return array;
}

-(void)close{
    [self.dbs close];
}

-(void)dealloc{
    self.dbs=nil;
}
@end
