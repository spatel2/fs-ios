
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
 
typedef enum {
    PROXIMITY_TAG_ALERT_LEVEL_NONE,
    PROXIMITY_TAG_ALERT_LEVEL_MILD,
    PROXIMITY_TAG_ALERT_LEVEL_HIGH,
} ProximityTagAlertLevel;

typedef enum {
    PROXIMITY_TAG_STATE_UNINITIALIZED,
    PROXIMITY_TAG_STATE_BONDING,
    PROXIMITY_TAG_STATE_BONDED,
    PROXIMITY_TAG_STATE_CLOSING,
    PROXIMITY_TAG_STATE_CLOSE,
    PROXIMITY_TAG_STATE_REMOTING,
    PROXIMITY_TAG_STATE_REMOTE,
    PROXIMITY_TAG_STATE_LINK_LOST,
    PROXIMITY_TAG_STATE_DISCONNECTED,
    
} ProximityTagState;

@protocol ProximityTagDelegate
- (void) didwatchTophone:(id) type;
- (void) didUpdateData:(id) tag;
@optional
- (void) didFailToConnect:(id) tag;
@end

@interface ProximityTag : NSObject <CBPeripheralDelegate>{
    //int  isonetwoclick;//设备呼叫手机  双击开始播放音乐 单击停止音乐
    int  rssitag;
    NSNumber * rssinum;
    NSTimer *timer;
    int findmeTag;
    
    
    
}

@property (nonatomic,strong) NSString * c1;
@property (nonatomic,strong) NSString * c2;
@property (nonatomic,strong) NSString * c3;



@property (nonatomic, strong) id<ProximityTagDelegate> delegate;
@property (nonatomic) ProximityTagState state;
@property (nonatomic, strong) CBPeripheral* peripheral;
@property CFUUIDRef peripheralCFUUIDRef;

@property (nonatomic, strong)NSString *UUID;
@property (nonatomic, strong) NSString* name;//名字
@property (nonatomic, strong)UIImage  * pothoimage;//头像
@property (nonatomic, assign) BOOL isSing;//是否呼叫
@property (nonatomic) float rssiLevel;//信号强度
@property (nonatomic) NSUInteger batteryLevel;//电池电量
@property (nonatomic, strong) NSString * singName;//个性化铃声
@property BOOL isAlowSing;//防丢开关
@property BOOL isZhenDong;//震动开关
@property float juli;//设置距离
@property (nonatomic, strong) NSString  * connInterval;//响应时间
//@property (nonatomic) BOOL isLocation;//检测location按钮
@property (nonatomic ,strong) NSMutableArray *  rssiArray;//滤波数组
@property (nonatomic,strong)NSData *reverseData;
@property (nonatomic)BOOL isConnect;


@property BOOL isIncomingCall;
@property BOOL isMissedCall;
@property BOOL isNewSMSmms;
@property BOOL isNewFB;
@property BOOL isGmail;
@property BOOL isHotmail;
@property BOOL isYahoomail;
@property BOOL isOthermail;
@property BOOL isWatchAlarm;

@property BOOL isNewCalendatEvent;
@property BOOL isRemoinders;
@property BOOL isLeavePhoneAlert;
@property BOOL isPhonebatteryLowTweenty;
@property BOOL isPhoneBatteryLowTen;


@property (nonatomic) ProximityTagAlertLevel linkLossAlertLevelOnTag;
@property (nonatomic) ProximityTagAlertLevel linkLossAlertLevelOnPhone;
@property (nonatomic) BOOL isfindPhone;
@property float rssiThreshold;
@property BOOL hasBeenBonded;@property (nonatomic) BOOL locationTrackingIsEnabled;
@property (nonatomic) BOOL rangeMonitoringIsEnabled;



+ (CBUUID*) FYIServiceUUID;
//guangbo
+ (CBUUID*) FYINOTIFYCharacteristicUUID;
//phoneToWatch
+ (CBUUID*) FYIphoneTowatchCharacteristicUUID;
//运动
+ (CBUUID*) FYIphoneTowatchYunDongCharacteristicUUID;



+ (CBUUID*) linkLossServiceUUID;
+ (CBUUID*) findMeServiceUUID;
+ (CBUUID*) alertLevelCharacteristicUUID;
+ (CBUUID*) batteryServiceUUID;
+ (CBUUID*) batteryLevelCharacteristicUUID;

- (id) init;
- (NSString*) connectionImageNameBasedOnRSSI;
- (void) didConnect;
- (void) didDisconnect;
- (bool) findTag:(ProximityTagAlertLevel) level;
- (BOOL) isConnected;
- (BOOL) isBonded;
//3-19修改  为了设置来电提醒修改，方便调用
- (void) startRangeMonitoring;
- (void) stopRangeMonitoring;
- (void) startRangeMonitoringytt;
- (void) stopRangeMonitoringytt;

- (void) phoneWriteDataToTag:(NSData * )data;
- (void) phoneWriteDataToTagBig:(NSData * )data;

@end
