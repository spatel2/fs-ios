//
//  ConnectionManager.m
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//

#import "ConnectionManager.h"
#import "AppDelegate.h"
#import "Proxy.h"

@implementation ConnectionManager {
    CBCentralManager *cm;
}
@synthesize delegate = _delegate,cp=_cp;

static ConnectionManager *sharedConnectionManager;

+ (ConnectionManager*) sharedInstance
{
    if (sharedConnectionManager == nil)
    {
        sharedConnectionManager = [[ConnectionManager alloc] initWithDelegate:nil];
    }
    return sharedConnectionManager;
}

- (ConnectionManager*) initWithDelegate:(id<ConnectionManagerDelegate>) delegate
{
    if (self = [super init])
    {
        _delegate = delegate;
        cm = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        
    }
    return self;
}

- (void) retrieveKnownPeripherals
{
    NSMutableArray *uuidArray = [[NSMutableArray alloc] init];
    for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tagsWithoutPeripheral]) 
    {
        if([tag peripheralCFUUIDRef])
            [uuidArray addObject:(id) [tag peripheralCFUUIDRef]];
    }
    
    [cm retrievePeripherals:uuidArray];
    
}

- (void) startScanForTags
{
    
    NSDictionary* scanOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:CBCentralManagerScanOptionAllowDuplicatesKey];
   // NSArray* serviceArray = [NSArray arrayWithObjects:ProximityTag.linkLossServiceUUID, ProximityTag.findMeServiceUUID,ProximityTag.FYIServiceUUID, nil];
        NSArray* serviceArray = [NSArray arrayWithObjects:ProximityTag.FYIServiceUUID, nil];
    // Make sure we start scan from scratch
    [cm stopScan];
    [cm scanForPeripheralsWithServices:serviceArray options:scanOptions];
    
    //[cm  scanForPeripheralsWithServices:nil options:nil];
    
    [NSTimer timerWithTimeInterval:10.0 target:self selector:@selector(stopScanForTags) userInfo:Nil repeats:NO];
    
    
}

- (void) stopScanForTags
{
    [cm stopScan];
}

- (void) connectToTag:(ProximityTag*) tag
{
    NSDictionary* connectOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:CBConnectPeripheralOptionNotifyOnDisconnectionKey];
    
    
    
    [cm connectPeripheral:[tag peripheral] options:connectOptions];
}

- (void) disconnectTag:(ProximityTag*) tag
{
    if (tag.peripheral)
    {
        
        
        [cm cancelPeripheralConnection:[tag peripheral]];
    }
    else 
    {
        
    }
}

//Invoked whenever the central manager's state has been updated. Commands should only be
//issued when the state is CBCentralManagerStatePoweredOn. A state below CBCentralManagerStatePoweredOn
//implies that scanning has stopped and any connected peripherals have been disconnected. If the state
//moves below CBCentralManagerStatePoweredOff, all CBPeripheral objects obtained from this central
//manager become invalid and must be retrieved or discovered again.
- (void) centralManagerDidUpdateState:(CBCentralManager *)central
{
    
    if ([central state] == CBCentralManagerStatePoweredOn)
    {
        [_delegate isBluetoothEnabled:YES];
        
        [self retrieveKnownPeripherals];
    }
    else 
    {
        [_delegate isBluetoothEnabled:NO];
    }
}

//This method is invoked while scanning, upon the discovery of peripheral by central. A discovered
//peripheral must be retained in order to use it; otherwise, it is assumed to not be of interest
//and will be cleaned up by the central manager. For a list of advertisementData keys, see
//CBAdvertisementDataLocalNameKey and other similar constants.
- (void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    //NSString *localName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];
    
    
    // Abort if this device is already in the list
    if([[ProximityTagStorage sharedInstance] tagWithUUID:[peripheral UUID]])
        return;
    
    ProximityTag *tag = [[ProximityTag alloc] init];
    tag.peripheral = peripheral;
    tag.rssiLevel = [RSSI floatValue];
    tag.peripheralCFUUIDRef =[peripheral UUID];
    
    NSLog(@"%@", tag.name);
    if([tag.name isEqualToString:@"Geneva BLE Watch"]) {
        [[ProximityTagStorage sharedInstance] insertTag:tag];
        
        [self.delegate didUpdateData:tag];
        [self.delegate didDiscoverTag:tag];
    }
    
    
}

//This method returns the result of a retrievePeripherals call, with the peripheral(s) that
//the central manager was able to match to the provided UUID(s).
- (void) centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
    for (CBPeripheral* p in peripherals)
    {
        for (ProximityTag* tag in [[ProximityTagStorage sharedInstance] tags])
        {
            if ([tag.peripheral UUID] == [p UUID])
                tag.peripheral=p;
        }
        
        NSData* encodedTag = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"lastDisconn"]];
        
        ProximityTag * taglast =(ProximityTag*) [NSKeyedUnarchiver unarchiveObjectWithData:encodedTag];
        
        //AutoConnect
        if ([taglast.peripheral UUID] == [p UUID])
        {
            [self connectToTag:taglast];
        }
    }
}


//This method is invoked when a connection initiated by connectPeripheral:options: has succeeded.
- (void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    ProximityTag *tag = [[ProximityTagStorage sharedInstance] tagWithPeripheral:peripheral];
    if (!tag)
    {
        
        return;
    }
    
    // Cannot save before we have connected, since the UUID might be empty
    [[ProximityTagStorage sharedInstance] saveTags];
    [tag didConnect];
    AppDelegate  * app= (AppDelegate *)[[UIApplication sharedApplication]delegate];
    app.appdelegateProximity = tag;
    
    for (int i=0; i<[[[ProximityTagStorage sharedInstance] tags] count]; i++) {
        
        ProximityTag *Ptag = [[[ProximityTagStorage sharedInstance] tags] objectAtIndex:i];
        if (Ptag.peripheral!=tag.peripheral) {
            [[ProximityTagStorage sharedInstance] removeTag:Ptag];
        }
    }
    
    
    [self.delegate ChangeLayoutDataConnected];
    
    [[Proxy shared] StartBleTimer];
}

//This method is invoked upon the disconnection of a peripheral that was connected by
//connectPeripheral:options:. If the disconnection was not initiated by cancelPeripheralConnection,
//the cause will be detailed in the error parameter. Once this method has been called, no more methods
//will be invoked on peripheral's CBPeripheralDelegate.
- (void) centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    [[Proxy shared] StopBleTimer];
    
    //Save the current Bluetooth devices disconnected objects
    ProximityTag *tag = [[ProximityTagStorage sharedInstance] tagWithPeripheral:peripheral];
    
    NSUserDefaults * ns = [NSUserDefaults standardUserDefaults];
    NSData *encodedTag = [NSKeyedArchiver archivedDataWithRootObject:tag];
    
    [ns setObject:encodedTag forKey:@"lastDisconn"];
    
    if (!tag) {
        
        return;
    }
    
    AppDelegate  * app= (AppDelegate *)[[UIApplication sharedApplication]delegate];
   
    //Instead of manually disconnect when it is re- connected
    if (![@"1" isEqualToString:app.reConnBtnTag])
    {
        [self connectToTag:tag];
    }
    
    [self.delegate didUpdateData:tag];
    [self.delegate ChangeLayoutData];
}


@end
