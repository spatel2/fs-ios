//
//  TagController.m
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//

#import "ProximityTag.h"
#import "SharedUI.h"
#import "AppDelegate.h"
#import "ConnectViewController.h"
#import "ServerManager.h"

#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1

@implementation ProximityTag {
    NSTimer *rssiTimer;
    NSTimer *readTimer;

    NSTimer *rssiTimerytt;
    NSTimer *readTimerytt;
    
    CBService *immediateAlertService;
    CBCharacteristic *immediateAlertAlertLevelCharacteristic;
    
    CBService *linkLossService;
    CBCharacteristic *linkLossAlertLevelCharacteristic;
    
    CBService *batteryService;
    CBCharacteristic *batteryLevelCharacteristic;
    
    CBService *FYIService;
    CBCharacteristic *FYINOTIFYCharacteristic;
    
    CBCharacteristic *FYIphoneTowatchCharacteristic;
    
    CBCharacteristic *FYIphoneTowatchYunDongCharacteristic;
    
}

@synthesize pothoimage=_pothoimage,
            isSing=_isSing,
            rssiLevel=_rssiLevel,
            batteryLevel=_batteryLevel,
            singName=_singName,
            isAlowSing=_isAlowSing,
            isZhenDong=_isZhenDong,
            juli=_juli,
            connInterval=_connInterval,
            //isLocation=_isLocation,
            rssiArray =_rssiArray;



@synthesize isIncomingCall=_isIncomingCall,
            isMissedCall=_isMissedCall,
            isNewSMSmms=_isNewSMSmms,
            isNewFB=_isNewFB,
            isGmail=_isGmail,
            isHotmail = _isHotmail,
            isYahoomail = _isYahoomail,
            isOthermail = _isOthermail,
            isWatchAlarm=_isWatchAlarm;


@synthesize isNewCalendatEvent=_isNewCalendatEvent,
            isRemoinders=_isRemoinders,
            isLeavePhoneAlert=_isLeavePhoneAlert,
            isPhonebatteryLowTweenty=_isPhonebatteryLowTweenty,
            isPhoneBatteryLowTen=_isPhoneBatteryLowTen;



@synthesize c1=_c1,c2=_c2,c3=_c3;


@synthesize delegate = _delegate;
@synthesize state = _state;
@synthesize peripheral = _peripheral;
@synthesize peripheralCFUUIDRef = _peripheralCFUUIDRef;
@synthesize name = _name;
@synthesize isfindPhone=_isfindPhone;
@synthesize linkLossAlertLevelOnTag = _linkLossAlertLevelOnTag;
@synthesize linkLossAlertLevelOnPhone = _linkLossAlertLevelOnPhone;
@synthesize rangeMonitoringIsEnabled = _rangeMonitoringIsEnabled;
@synthesize rssiThreshold = _rssiThreshold;
@synthesize locationTrackingIsEnabled = _locationTrackingIsEnabled;
@synthesize hasBeenBonded = _hasBeenBonded;

+ (CBUUID*) FYIServiceUUID
{
    return [CBUUID UUIDWithString:@"FFF0"];
}
//guangbo
+ (CBUUID*) FYINOTIFYCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"FFF1"];
}
//phoneToWatch
+ (CBUUID*) FYIphoneTowatchCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"FFF2"];
}

//运动
+ (CBUUID*) FYIphoneTowatchYunDongCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"FFF3"];
}



+ (CBUUID*) linkLossServiceUUID
{
    return [CBUUID UUIDWithString:@"1803"];
}

+ (CBUUID*) findMeServiceUUID
{
    return [CBUUID UUIDWithString:@"1802"];
}

+ (CBUUID*) alertLevelCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"2A06"];
    
}

+ (CBUUID*) batteryServiceUUID
{
    return [CBUUID UUIDWithString:@"180F"];
}

+ (CBUUID*) batteryLevelCharacteristicUUID
{
    return [CBUUID UUIDWithString:@"2A19"];
}

- (id) init 
{
    if (self = [super init]) {        
        _linkLossAlertLevelOnTag = PROXIMITY_TAG_ALERT_LEVEL_HIGH;
        _linkLossAlertLevelOnPhone = PROXIMITY_TAG_ALERT_LEVEL_HIGH;
        _rssiThreshold = -50.0f;
        _juli=100.0f;
        _hasBeenBonded = NO;
        _singName=@"Alarm-sound";
        _connInterval=@"500ms";
        _state = PROXIMITY_TAG_STATE_UNINITIALIZED;
        _rssiArray=[[NSMutableArray alloc] initWithCapacity:5];
        
        _isIncomingCall=([[NSUserDefaults standardUserDefaults] objectForKey:@"isIncomingCall"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isIncomingCall"] boolValue]:TRUE;
        _isMissedCall=([[NSUserDefaults standardUserDefaults] objectForKey:@"isMissedCall"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isMissedCall"] boolValue]:TRUE;
        _isNewSMSmms=NO;
        _isNewFB=NO;
        _isGmail=([[NSUserDefaults standardUserDefaults] objectForKey:@"isGmail"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isGmail"] boolValue]:TRUE;
        _isHotmail = ([[NSUserDefaults standardUserDefaults] objectForKey:@"isHotmail"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isHotmail"] boolValue]:TRUE;
        _isYahoomail = ([[NSUserDefaults standardUserDefaults] objectForKey:@"isYahoomail"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isYahoomail"] boolValue]:TRUE;
        _isOthermail = ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOthermail"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isOthermail"] boolValue]:TRUE;
        _isWatchAlarm=([[NSUserDefaults standardUserDefaults] objectForKey:@"isWatchAlarm"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isWatchAlarm"] boolValue]:TRUE;
        
        _isNewCalendatEvent=([[NSUserDefaults standardUserDefaults] objectForKey:@"isNewCalendatEvent"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isNewCalendatEvent"] boolValue]:TRUE;
        _isPhoneBatteryLowTen=([[NSUserDefaults standardUserDefaults] objectForKey:@"isPhoneBatteryLowTen"])?[[[NSUserDefaults standardUserDefaults] objectForKey:@"isPhoneBatteryLowTen"] boolValue]:TRUE;
        
        _c1=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"C1"] ];

        _c2=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"C2"] ];

        _c3=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"C3"] ];

        
        self.reverseData=[NSData data];
        
    }
    return self;
}

- (id) initWithCoder:(NSCoder *) decoder
{
    if (self = [super init]) {
        _peripheralCFUUIDRef = CFUUIDCreateFromString(NULL, (__bridge_retained CFStringRef) [decoder decodeObjectForKey:@"UUIDString"]);
        
        _c1=[decoder decodeObjectForKey:@"c1"];
         _c2=[decoder decodeObjectForKey:@"c2"];
         _c3=[decoder decodeObjectForKey:@"c3"];
        _name = [decoder decodeObjectForKey:@"name"];
        _pothoimage = [decoder decodeObjectForKey:@"pothoimage"];
        _singName = [decoder decodeObjectForKey:@"singName"];
        _isSing = [decoder decodeBoolForKey:@"isSing"];
        _isAlowSing = [decoder decodeBoolForKey:@"isAlowSing"];
        _isZhenDong = [decoder decodeBoolForKey:@"isZhenDong"];
        //_isLocation = [decoder decodeBoolForKey:@"isLocation"];
        _juli=[decoder decodeFloatForKey:@"juli"];
        _connInterval = [decoder decodeObjectForKey:@"connInterval"];
        
        _isIncomingCall=[decoder decodeBoolForKey:@"isIncomingCall"];
        _isMissedCall=[decoder decodeBoolForKey:@"isMissedCall"];
        _isNewSMSmms=[decoder decodeBoolForKey:@"isNewSMSmms"];
        _isNewFB=[decoder decodeBoolForKey:@"isNewFB"];
        _isGmail=[decoder decodeBoolForKey:@"isGmail"];
        _isHotmail = [decoder decodeBoolForKey:@"isHotmail"];
        _isYahoomail = [decoder decodeBoolForKey:@"isYahoomail"];
        _isOthermail = [decoder decodeBoolForKey:@"isOthermail"];
        
        _isWatchAlarm=[decoder decodeBoolForKey:@"isWatchAlarm"];
        

        
        _isNewCalendatEvent=[decoder decodeBoolForKey:@"isNewCalendatEvent"];
        _isRemoinders=[decoder decodeBoolForKey:@"isRemoinders"];
        _isLeavePhoneAlert=[decoder decodeBoolForKey:@"isLeavePhoneAlert"];
        _isPhonebatteryLowTweenty=[decoder decodeBoolForKey:@"isPhonebatteryLowTweenty"];
        _isPhoneBatteryLowTen=[decoder decodeBoolForKey:@"isPhoneBatteryLowTen"];
        
        
        
        _linkLossAlertLevelOnTag = [decoder decodeIntegerForKey:@"linkLossAlertLevelOnTag"];
        _linkLossAlertLevelOnPhone = [decoder decodeIntegerForKey:@"linkLossAlertLevelOnPhone"];
        _rangeMonitoringIsEnabled = [decoder decodeBoolForKey:@"rangeMonitoringIsEnabled"];
        _rssiThreshold = [decoder decodeDoubleForKey:@"RSSIThreshold"];
        _locationTrackingIsEnabled = [decoder decodeBoolForKey:@"locationTrackingIsEnabled"];
        _hasBeenBonded = [decoder decodeBoolForKey:@"hasBeenBonded"];
        //_isLocation =[decoder decodeBoolForKey:@"isLocation"];
        
        
        _state = PROXIMITY_TAG_STATE_DISCONNECTED;
    }
    return self;
}

- (void) setState:(ProximityTagState) newState
{
    
    
    
    
    switch (newState)
    {
            
        case PROXIMITY_TAG_STATE_BONDING:
            break;
            
        case PROXIMITY_TAG_STATE_BONDED:
        {
          
            AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            app.appdelegateProximity = self;
            self.isConnect=YES;
            
            
 
            break;
        }
        case PROXIMITY_TAG_STATE_DISCONNECTED:{
            
            break;
        }
            
        case PROXIMITY_TAG_STATE_LINK_LOST:
            self.rssiLevel = 0;
            self.isConnect=NO;
         
            // Only store the location if we are currently bonded (i.e. the link was lost now, and we are not just initializing this object)
            if ([self isBonded])
            {
    
                
                
            }
            
            // If the tag has never been bonded, and then disconnected, this indicates a failed connect.
            else if (!self.hasBeenBonded)
            {
                [SharedUI showFailedConnectDialog:self];
            }
            break;
            
        case PROXIMITY_TAG_STATE_CLOSE:
            [self startLocationMonitoringIfEnabled];   
            break;
            
        case PROXIMITY_TAG_STATE_REMOTE:
        {
            
            
            
            
            [SharedUI showOutOfRangeDialog:self.linkLossAlertLevelOnPhone forTag:self];
            break;
            
            
        }
        default:
            break;
    }
    _state = newState;
    [self.delegate didUpdateData:self];
}

- (void) setPeripheral:(CBPeripheral*)peripheral
{
    _peripheral = peripheral;
    _peripheral.delegate = self;

    self.rssiLevel = [[_peripheral RSSI] doubleValue];
    
    if (!self.name && [_peripheral name])
    {
        self.name = [_peripheral name];
    }
    
}

- (void) phoneWriteDataToTagBig:(NSData * )data
{
    
    
    [self.peripheral writeValue:data forCharacteristic:FYIphoneTowatchYunDongCharacteristic  type:CBCharacteristicWriteWithResponse];
    
}

- (void) phoneWriteDataToTag:(NSData * )data
{
    
//     AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.peripheral writeValue:data forCharacteristic:FYIphoneTowatchCharacteristic type:CBCharacteristicWriteWithResponse];
    
    
}


- (void) writeLinkLossAlertLevelToTag:(ProximityTagAlertLevel) linkLossAlertLevel
{
    [self.peripheral writeValue:[NSData dataWithBytes:&linkLossAlertLevel length:1] forCharacteristic:linkLossAlertLevelCharacteristic type:CBCharacteristicWriteWithResponse];
}

-(void)readCharacteristic:(CBPeripheral *)peripheral sUUID:(NSString *)sUUID cUUID:(NSString *)cUUID  cdata:(NSData *)data
{
    
    
    for ( CBService *service in peripheral.services ) {
        
        if([service.UUID isEqual:[CBUUID UUIDWithString:sUUID]]) {
            for ( CBCharacteristic *characteristic in service.characteristics ) {
                
                if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:cUUID]]) {
                    /* Everything is found, read characteristic ! */
                    
                    
                    [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
                    
                }
            }
        }
    }
}

- (void) writeImmediateAlertLevelToTag:(ProximityTagAlertLevel) immediateAlertLevel
{
    [self.peripheral writeValue:[NSData dataWithBytes:&immediateAlertLevel length:1] forCharacteristic:immediateAlertAlertLevelCharacteristic type:CBCharacteristicWriteWithoutResponse];
}

- (bool) findTag:(ProximityTagAlertLevel) level
{
    
    if(immediateAlertAlertLevelCharacteristic != nil) {
        
        [self writeImmediateAlertLevelToTag:level];
        return YES;
    }
    
    return NO;
}

- (void) setLinkLossAlertLevelOnTag:(ProximityTagAlertLevel)linkLossAlertLevel
{
    _linkLossAlertLevelOnTag = linkLossAlertLevel;
    
    if (self.isBonded)
    {
        [self writeLinkLossAlertLevelToTag:linkLossAlertLevel];
    }
}

- (void) setLocationTrackingIsEnabled:(BOOL)locationTrackingIsEnabled
{
    _locationTrackingIsEnabled = locationTrackingIsEnabled;
    
    if (self.locationTrackingIsEnabled)
    {
        [self startLocationMonitoringIfEnabled];
    } 
    else 
    {
        [self stopLocationMonitoring];
    }
    
}

- (void) setRangeMonitoringIsEnabled:(BOOL)rangeMonitoringIsEnabled
{
    _rangeMonitoringIsEnabled = rangeMonitoringIsEnabled;
    
    if (self.rangeMonitoringIsEnabled)
    {
        [self startRangeMonitoring];
    } 
    else 
    {
        [self stopRangeMonitoring];
    }
    
}

- (void) encodeWithCoder:(NSCoder *) encoder
{
    // Trying to save a tag without UUID will fail. Assert to be able to know what happens from crash log.
    assert(self.peripheral.UUID != nil);
    
    NSString* uuidString = (__bridge_transfer NSString*) CFUUIDCreateString(NULL, self.peripheral.UUID);
    [encoder encodeObject:uuidString forKey:@"UUIDString"];
    [encoder encodeObject:self.name forKey:@"name"];
    
    [encoder encodeObject:self.c1 forKey:@"c1"];
    [encoder encodeObject:self.c2 forKey:@"c2"];
    [encoder encodeObject:self.c3 forKey:@"c3"];

    
    [encoder encodeObject:self.pothoimage forKey:@"pothoimage"];
    [encoder encodeBool:self.isSing forKey:@"isSing"];
    [encoder encodeObject:self.singName forKey:@"singName"];
    [encoder encodeBool:self.isAlowSing forKey:@"isAlowSing"];
    [encoder encodeBool:self.isZhenDong forKey:@"isZhenDong"];
    [encoder encodeFloat:self.juli forKey:@"juli"];
    [encoder encodeObject:self.connInterval forKey:@"connInterval"];
    //[encoder encodeBool:self.isLocation forKey:@"isLocation"];

    
    [encoder encodeBool:self.isIncomingCall forKey:@"isIncomingCall"];
    [encoder encodeBool:self.isMissedCall forKey:@"isMissedCall"];
    [encoder encodeBool:self.isNewSMSmms forKey:@"isNewSMSmms"];
    [encoder encodeBool:self.isNewFB forKey:@"isNewFB"];
    [encoder encodeBool:self.isGmail forKey:@"isGmail"];
    [encoder encodeBool:self.isHotmail forKey:@"isHotmail"];
    [encoder encodeBool:self.isYahoomail forKey:@"isYahoomail"];
    [encoder encodeBool:self.isOthermail forKey:@"isOthermail"];
    [encoder encodeBool:self.isWatchAlarm forKey:@"isWatchAlarm"];
    

    [encoder encodeBool:self.isNewCalendatEvent forKey:@"isNewCalendatEvent"];
    [encoder encodeBool:self.isRemoinders forKey:@"isRemoinders"];
    [encoder encodeBool:self.isLeavePhoneAlert forKey:@"isLeavePhoneAlert"];
    [encoder encodeBool:self.isPhonebatteryLowTweenty forKey:@"isPhonebatteryLowTweenty"];
    [encoder encodeBool:self.isPhoneBatteryLowTen forKey:@"isPhoneBatteryLowTen"];
    
    
    
    [encoder encodeInteger:self.linkLossAlertLevelOnTag forKey:@"linkLossAlertLevelOnTag"];
    [encoder encodeInteger:self.linkLossAlertLevelOnPhone forKey:@"linkLossAlertLevelOnPhone"];
    [encoder encodeDouble:self.rssiThreshold forKey:@"RSSIThreshold"];
    [encoder encodeBool:self.rangeMonitoringIsEnabled forKey:@"rangeMonitoringIsEnabled"];
    [encoder encodeBool:self.locationTrackingIsEnabled forKey:@"locationTrackingIsEnabled"];

    [encoder encodeBool:self.hasBeenBonded forKey:@"hasBeenBonded"];
    //[encoder encodeBool:self.isLocation forKey:@"isLocation"];
}

- (void) didConnect
{
    
    [self setState:PROXIMITY_TAG_STATE_BONDED];
    
    
    
    if (!self.name && [self.peripheral name])
    {
        self.name = [self.peripheral name];
    }
    self.isConnect=YES;
    self.peripheralCFUUIDRef = [self.peripheral UUID];
    [self.peripheral readRSSI];
    [self.peripheral discoverServices:nil];
}

- (void) didDisconnect
{
    self.isConnect=NO;
    [self setState:PROXIMITY_TAG_STATE_LINK_LOST];
    
    
}

- (void) startRangeMonitoringIfEnabled
{
    if (self.rangeMonitoringIsEnabled)
    {
        [self startRangeMonitoring];
    }
}
- (void) startRangeMonitoringytt
{
    if (![self isConnected])
    {
        
        return;
    }
    
 
}


- (void) stopRangeMonitoringytt
{
    
    
    [rssiTimerytt invalidate];
    [readTimerytt invalidate];
    
}
- (void) startRangeMonitoring
{
    if (![self isConnected])
    {
        
        return;
    }
    
    [rssiTimer invalidate];
    rssiTimer = [NSTimer timerWithTimeInterval:1.0 target:_peripheral selector:@selector(readRSSI)    userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:rssiTimer forMode:NSRunLoopCommonModes];
    
    
    [readTimer invalidate];
    readTimer = [NSTimer timerWithTimeInterval:5 target:self selector:@selector(readLinkLossAlert) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:readTimer forMode:NSRunLoopCommonModes];
}        

- (void) stopRangeMonitoring
{
    
    
    [rssiTimer invalidate];
    [readTimer invalidate];
    
}



- (void) handleRSSIReading
{
    
    [self.delegate didUpdateData:self];
   
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopActivityIndicator" object:self];
}

- (void) startLocationMonitoringIfEnabled
{
    if (self.locationTrackingIsEnabled && self.isConnected)
    {
        [self startLocationMonitoring];
    }
}

- (void) startLocationMonitoring
{
    //[[TagLocationManager sharedInstance] startMonitoringForTag:self];
}

- (void) stopLocationMonitoring
{
    //self.lastSeenLocation = nil;
    //[[TagLocationManager sharedInstance] stopMonitoringForTag:self];
}
//超出范围开始定位
- (void) storeLocationIfEnabled
{
    
}

- (void) readLinkLossAlert
{
    //[_peripheral readValueForCharacteristic:linkLossAlertLevelCharacteristic];
}

-(NSNumber*)maopao{
    
    NSNumber * num ;
    
    
    
    
    
    
    NSNumber * temp = 0;
    for (int i = 5 - 1; i > 0; --i) {
        for (int j = 0; j < i; ++j) {
            
            if ([[self.rssiArray objectAtIndex:j+1] intValue]<[[self.rssiArray objectAtIndex:j] intValue]) {
                temp=[self.rssiArray objectAtIndex:j];
                [self.rssiArray replaceObjectAtIndex:j withObject:[self.rssiArray objectAtIndex:j+1]];
                [self.rssiArray replaceObjectAtIndex:j+1 withObject:temp];
            }
        }
    }
    num= [self.rssiArray objectAtIndex:2];
    return num;
    
}

- (void) peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error 
{
    
    self.rssiLevel = [[peripheral RSSI] floatValue];
    return;
            [self handleRSSIReading];
    
    
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    
    
    for(CBService *s in [peripheral services]) {
        if([[s UUID] isEqual:ProximityTag.findMeServiceUUID])
            immediateAlertService = s;
        
        if([[s UUID] isEqual:ProximityTag.linkLossServiceUUID]) 
            linkLossService = s;
        
        if([[s UUID] isEqual:ProximityTag.batteryServiceUUID])
            batteryService = s;
        
        //保存fyi的服务
        if ([[s UUID] isEqual:ProximityTag.FYIServiceUUID]) {
            FYIService = s;
        }
        
        //[peripheral discoverCharacteristics:[NSArray arrayWithObjects:ProximityTag.alertLevelCharacteristicUUID,ProximityTag.batteryLevelCharacteristicUUID, nil] forService:s];
        
        [peripheral discoverCharacteristics:nil forService:s];
        //731
        [self startRangeMonitoringytt];
    }
}

- (void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    
    
    for(CBCharacteristic *c in [service characteristics]) 
    {
        if([service isEqual:linkLossService] && 
           [[c UUID] isEqual:ProximityTag.alertLevelCharacteristicUUID]) 
        {
            linkLossAlertLevelCharacteristic = c;
            // Write the desired link loss alert to the device. Will also trigger bonding.
            //[self writeLinkLossAlertLevelToTag:self.linkLossAlertLevelOnTag];
        }
        
        //报警
        if([service isEqual:immediateAlertService] &&
           [[c UUID] isEqual:ProximityTag.alertLevelCharacteristicUUID]) 
        {
            immediateAlertAlertLevelCharacteristic = c;
        }
        
        //电池电量
        if([service isEqual:batteryService] &&
           [[c UUID] isEqual:ProximityTag.batteryLevelCharacteristicUUID])
        {
            batteryLevelCharacteristic = c;
            [peripheral setNotifyValue:YES forCharacteristic:batteryLevelCharacteristic];
            [peripheral readValueForCharacteristic:batteryLevelCharacteristic];
        }

        //FYI 私有广播  NOTIFY
        if ([service isEqual:FYIService]&&[[c UUID] isEqual:ProximityTag.FYINOTIFYCharacteristicUUID]) {
            
            
            FYINOTIFYCharacteristic = c;
            [peripheral setNotifyValue:YES forCharacteristic:FYINOTIFYCharacteristic];
            [peripheral readValueForCharacteristic:FYINOTIFYCharacteristic];
            
            
        }
        //FYI  phoneToWatch   读写
        if ([service isEqual:FYIService]&&[[c UUID] isEqual:ProximityTag.FYIphoneTowatchCharacteristicUUID]) {
            FYIphoneTowatchCharacteristic = c;
            [peripheral setNotifyValue:YES forCharacteristic:FYIphoneTowatchCharacteristic];
            [peripheral readValueForCharacteristic:FYIphoneTowatchCharacteristic];
        }
        
        //FYI  phoneToWatch big  读写
        if ([service isEqual:FYIService]&&
            [[c UUID] isEqual:ProximityTag.FYIphoneTowatchYunDongCharacteristicUUID]) {
            FYIphoneTowatchYunDongCharacteristic = c;
            [peripheral setNotifyValue:YES forCharacteristic:FYIphoneTowatchYunDongCharacteristic];
            [peripheral readValueForCharacteristic:FYIphoneTowatchYunDongCharacteristic];
            
        }
    }
}
-(UInt16) CBUUIDToInt:(CBUUID *) UUID {
    
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
    
}

//=================================================================================

//=================================================================================
- (void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if ([@"<ff>" isEqualToString:[NSString stringWithFormat:@"%@",characteristic.value]]){
   
        self.reverseData=characteristic.value;

        
    }
    
    if ([characteristic isEqual:batteryLevelCharacteristic]) 
    {
        ushort value;
        NSMutableData *data = [NSMutableData dataWithData:characteristic.value];
        [data increaseLengthBy:8];
        [data getBytes:&value length:sizeof(value)];
        self.batteryLevel=value;
    }
    
  
    
    if ([characteristic.UUID isEqual:[CBUUID  UUIDWithString:@"FFF1"]]) {
//        ConnectViewController * conn = [[ConnectViewController alloc] init];
//         self.delegate =  conn;
        
        if ([@"<b0>" isEqualToString:[NSString stringWithFormat:@"%@",characteristic.value]]){//c1
            
            [self.delegate didwatchTophone:@"b0"];
        }else if ([@"<b1>" isEqualToString:[NSString stringWithFormat:@"%@",characteristic.value]]){//c2
            [self.delegate didwatchTophone:@"b1"];
            
            
        }else if ([@"<b2>" isEqualToString:[NSString stringWithFormat:@"%@",characteristic.value]]){//c3
            
            [self.delegate didwatchTophone:@"b2"];
            
            
        }
    }
    
    [self.delegate didUpdateData:self];
}

- (void) peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error)
    {
        
    }
    else
    {
         
        
        // If we have successfully written something, bonding has been successful
        if (self.state == PROXIMITY_TAG_STATE_BONDING)
        {
            [self setState:PROXIMITY_TAG_STATE_BONDED];
        }
    }
    
    
}


- (NSString*) connectionImageNameBasedOnRSSI
{
    NSString *connectionImageName;
    if (!self.isConnected)
        connectionImageName = @"cammer-black.png";
    else 
        connectionImageName = @"cammer1.png";
    
    return connectionImageName;
    
}

- (BOOL) isConnected
{
    
    // If we are initialized, not disconnected and not link lost, we are connected
    return self.state != PROXIMITY_TAG_STATE_UNINITIALIZED &&
        self.state != PROXIMITY_TAG_STATE_DISCONNECTED &&
        self.state != PROXIMITY_TAG_STATE_LINK_LOST;
    
    
}

- (BOOL) isBonded
{
    // If we are connected and not bonding, we are bonded
    return self.isConnected && (self.state != PROXIMITY_TAG_STATE_BONDING);
}

@end
