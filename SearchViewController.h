//
//  SearchViewController.h
//  FYIProject
//
//  Created by Hay on 13-10-16.
//  Copyright (c) 2013年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FYIInfoViewController.h"
#import "ProximityTagStorage.h"
#import "ConnectionManager.h"
@interface SearchViewController : UIViewController<ConnectionManagerDelegate>

@property (nonatomic) IBOutlet UITableView  *  deviceTabview;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityView;

- (IBAction)SearchTags:(id)sender;

@end
