//
//  ServerManager.h
//  ProximityApp
//
//  Copyright (c) 2012 Nordic Semiconductor. All rights reserved.
//
//
#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <AVFoundation/AVFoundation.h>
#import "ProximityTagStorage.h"
#import "SharedUI.h"

@interface ServerManager : NSObject <CBPeripheralManagerDelegate,AVAudioPlayerDelegate>{
    
    int  isopen;
    bool didpausemusic;

}
@property (retain) AVAudioPlayer *player;
@property (retain) AVAudioPlayer *playerytt;

@property (nonatomic)bool didpausemusic;

+ (ServerManager*) sharedInstance;
- (void) startPlayingAlarmSound;
- (void) stopPlayingAlarmSound;

@end
