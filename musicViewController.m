
//  musicViewController.m
//  KennethCole
//
//  Created by Dharmesh on 26/12/14.
//  Copyright (c) 2014 iBlazing. All rights reserved.
//

#import "musicViewController.h"

#import <MediaPlayer/MediaPlayer.h>
#import  "AppDelegate.h"
#import "MusicTableViewCell.h"
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface musicViewController ()

@end

@implementation musicViewController
@synthesize homeview,sliderview,btn_music,btn_slider,img_logo,tableview_slider,lblslider,img_song,lbl_song_name,muslistview,songdetail1,songname,songgimg;
@synthesize tableView=_tableView,palyOrpausebtn=_palyOrpausebtn;

- (void)viewDidLoad {
    return;
    setflag=0;
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
 
    
    app.items = [NSMutableArray array];
    [self initMusicItems];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:@"" forKey:@"flag"];
        //        [data setObject:@"" forKey:@"password"];
        //        [data setObject:@"" forKey:@"userid"];
        
        [data writeToFile: path atomically:YES];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    
    
    
         if ([strUserName isEqualToString:@"1"])
    {
        
        MPMediaItem *item = [app.items objectAtIndex:0];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
         
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=img;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        
        self.currentTimeSlider.maximumValue = [app getAudioDuration];
        
        
        

        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
        self.timeElapsed.text = @"0:00";
        
        self.duration.text = [NSString stringWithFormat:@"-%@",
                              [app timeFormat:[app getAudioDuration]]];
        
        
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];

        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:name forKey:@"name"];
        [defaults setObject:detailname forKey:@"detailname"];
        
        
        NSString *curentslidertime= [NSString stringWithFormat:@"%@",self.timer];
        NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                [app timeFormat:[app getAudioDuration]]];
        [defaults setObject:curentslidertime forKey:@"curentslidertime"];
        [defaults setObject:durationtime forKey:@"durationtime"];
        
        NSLog (@"curentslidertime %@", curentslidertime);
        NSLog (@"durationtime %@", durationtime);

        
        [defaults synchronize];
        NSLog(@"data saved");
        
        NSLog (@" name %@",  name);
        NSLog (@"detailname %@", detailname);
        
        
        
        app.flag=@"0";
    }
    else if ([strUserName isEqualToString:@"0"])
    {
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        NSString *curentslidertime= [NSString stringWithFormat:@"%@",[defaults objectForKey:@"curentslidertime"]];
        NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                [defaults objectForKey:@"durationtime"]];
        
        self.currentTimeSlider.maximumValue = [curentslidertime floatValue] ;//;[app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
       // self.timeElapsed.text = @"0:00";
        
        self.duration.text = durationtime;//;[NSString stringWithFormat:@"-%@",
                            //  [app timeFormat:[app getAudioDuration]]];
        
        
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];

        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);
        
        
    }

    
//
//    
//    //get the stored data
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    
//    //create instace of NSData
//    NSData *imagedata=[defaults dataForKey:@"image"];
//    UIImage *sng_img=[UIImage imageWithData:imagedata];
//    songgimg.image=sng_img;
//    
//    NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
//    NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
//    songname.text=[NSString stringWithFormat:@"%@",name];
//    songdetail1.text=[NSString stringWithFormat:@"%@",detailname];
//
//    int playnum=[defaults integerForKey:@"playnum"];
//   
//
//      NSLog (@" songname.text %@",  name);
//      NSLog (@"songdetail1.text %@", detailname);
//       NSLog (@"playnum %d", playnum);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [volumeSlider setValue:[self.mpc volume]];
    
    sliderview.hidden=YES;
    muslistview.hidden=YES;
    
    
//    _tableView.estimatedRowHeight=68.0;
//    _tableView.rowHeight=UITableViewAutomaticDimension;
    
    slidercell_array = [[NSMutableArray alloc] initWithObjects:@"CONNECT", @"WATCH", @"ALERTS", @"CAMERA", @"MY GALLERY", @"KC NEWS", @"SOCIAL BUZZ", @"MUSIC",@"HELP",nil];
    
    
    lblslider.text=@"MENU";
    if ([[UIScreen mainScreen] bounds].size.height == 736)
    {
        lblslider.textColor=[UIColor blackColor];
        lblslider.font=[UIFont boldSystemFontOfSize:30];
        //iphone 6+
    }
    else if ([[UIScreen mainScreen] bounds].size.height==667)
    {
        
        lblslider.textColor=[UIColor blackColor];
        lblslider.font=[UIFont boldSystemFontOfSize:20];
        //iphone 6
    }
    
    else
    {
        
        lblslider.textColor=[UIColor blackColor];
        lblslider.font=[UIFont boldSystemFontOfSize:15];
        //iphone 3.5,4,5,6 inch screen iphone 3g,4s
    }
    
    if (floorf(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = YES;
        self.extendedLayoutIncludesOpaqueBars = YES;
       // self.navigationController.interactivePopGestureRecognizer.enabled=NO;
        
    }
    
  //  self.navigationController.navigationBarHidden=NO;
    
    //    UIView *titlView=[[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 200.0, 44.0)];
    //    [titlView setBackgroundColor:[UIColor clearColor]];
    //    UILabel *titlelable=[[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 200, 44.0)];
    //    [titlelable setBackgroundColor:[UIColor clearColor]];
    //    [titlelable setTextColor:[UIColor whiteColor]];
    //    [titlelable setText:@"SONG LIST"];
    //    [titlelable setFont:[UIFont boldSystemFontOfSize:15]];
    //    [titlelable setLineBreakMode:NSLineBreakByWordWrapping];
    //    [titlelable setTextAlignment:NSTextAlignmentCenter];
    //    [titlelable setNumberOfLines:1];
    //    [titlView addSubview:titlelable];
    //    self.navigationItem.titleView=titlView;
    self.tabBarController.tabBar.hidden = YES;
    // Do any additional setup after loading the view from its nib.
    
//    app.flag=@"0";
    
    if ([app.player isPlaying]) {
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        
    }else{
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePlayIcon:) name:@"changePlayIcon" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(GetMusicDetail:) name:@"GetMusicDetail" object:nil];
//    songname.text=[NSString stringWithFormat:@"%@",app.lbl_song_name_app];
//    NSLog(@"%@",songname.text);

}

-(void)changePlayIcon:(NSNotification*) notification  {
    return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ([app.player isPlaying]) {
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        self.isPaused = TRUE;

    }else{
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
                self.isPaused = FALSE;
    }
}

-(void)GetMusicDetail:(NSNotification*) notification  {
    //get the stored data
    return;
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ([app.player isPlaying]) {
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        
    }else{
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
    }
    
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    //create instace of NSData
    NSData *imagedata=[defaults dataForKey:@"image"];
    UIImage *sng_img=[UIImage imageWithData:imagedata];
    songgimg.image=sng_img;
    
    NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
    NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
    songname.text=name;
    songdetail1.text=detailname;
    
    int playnum=[defaults integerForKey:@"playnum"];
    
    
    NSLog (@" songname.text %@",  name);
    NSLog (@"songdetail1.text %@", detailname);
    NSLog (@"playnum %d", playnum);
}


-(void)viewWillAppear:(BOOL)animated
{
    return;
 
   
     AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:@"" forKey:@"flag"];
        
        [data writeToFile: path atomically:YES];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    
    
   // AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    if ([strUserName isEqualToString:@"1"])
    {
 
        MPMediaItem *item = [app.items objectAtIndex:0];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        //    CGSize itemsize=CGSizeMake(40, 40);
        //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;

            //create instace of NSData
            UIImage *song_img=img;
            NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
            NSString *name=[songname text];
            NSString *detailname=[songdetail1 text];
        
            //store the data
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
            [defaults setObject:imagedata forKey:@"image"];
            [defaults setObject:name forKey:@"name"];
            [defaults setObject:detailname forKey:@"detailname"];
        
            [defaults synchronize];
            NSLog(@"data saved");
            
            NSLog (@" name %@",  name);
            NSLog (@"detailname %@", detailname);
        
        self.currentTimeSlider.maximumValue = [app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
        self.timeElapsed.text = @"0:00";
        
        self.duration.text = [NSString stringWithFormat:@"-%@",
                              [app timeFormat:[app getAudioDuration]]];
        


        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        
        app.flag=@"0";
    }
    else if ([strUserName isEqualToString:@"0"])
    {
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);

        
        self.currentTimeSlider.maximumValue = [app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
        self.timeElapsed.text = @"0:00";
        
        self.duration.text = [NSString stringWithFormat:@"-%@",
                              [app timeFormat:[app getAudioDuration]]];
        

        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
    }
    
    if([app.player isPlaying])
    {
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);
        
        
        
        NSString *curentslidertime= [NSString stringWithFormat:@"%@",[defaults objectForKey:@"curentslidertime"]];
        NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                [defaults objectForKey:@"durationtime"]];
        
        self.currentTimeSlider.maximumValue = [curentslidertime floatValue] ;//;[app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
        // self.timeElapsed.text = @"0:00";
        
        self.duration.text = durationtime;//;[NSString stringWithFormat:@"-%@",
        //  [app timeFormat:[app getAudioDuration]]];
        
        NSLog(@"%@",self.duration.text);

        
        
    }
    else
    {
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
        
        //get the stored data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        //create instace of NSData
        NSData *imagedata=[defaults dataForKey:@"image"];
        UIImage *sng_img=[UIImage imageWithData:imagedata];
        songgimg.image=sng_img;
        
        NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
        NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
        songname.text=name;
        songdetail1.text=detailname;
        
        int playnum=[defaults integerForKey:@"playnum"];
        
        
        NSLog (@" songname.text %@",  name);
        NSLog (@"songdetail1.text %@", detailname);
        NSLog (@"playnum %d", playnum);
        
        
        NSString *curentslidertime= [NSString stringWithFormat:@"%@",[defaults objectForKey:@"curentslidertime"]];
        NSString *durationtime=[NSString stringWithFormat:@"%@",
                                [defaults objectForKey:@"durationtime"]];
        
        self.currentTimeSlider.maximumValue = [curentslidertime floatValue] ;//;[app getAudioDuration];
        
        //init the current timedisplay and the labels. if a current time was stored
        //for this player then take it and update the time display
        
        
        // self.timeElapsed.text = @"0:00";
        
        self.duration.text = durationtime;//;[NSString stringWithFormat:@"-%@",
        //  [app timeFormat:[app getAudioDuration]]];
        
        
        
        NSLog(@"%@",self.duration.text);

        
        

    }
   
    
    
    
   
}
- (IBAction)volumeChanged:(id)sender
{return;
    [self.mpc setVolume:[volumeSlider value]];
}

#pragma mark - Delegate
//选中后调用
- (void)mediaPicker:(MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection{
    return;
    NSArray *items = mediaItemCollection.items;
    MPMediaItem *item = [items objectAtIndex:0];
    NSString *name = [item valueForProperty:MPMediaItemPropertyTitle];
    NSLog(@"name= %@",name);
    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    UIImage *image = [artwork imageWithSize:CGSizeMake(100, 100)];
    //获取图片
    // MPMediaItemPropertyPlaybackDuration 总时间的属性名称 11
    MPMusicPlayerController *mpc = [MPMusicPlayerController iPodMusicPlayer];
    //调用ipod播放器
   // MPMusicPlayerController *mpc = [MPMusicPlayerController applicationMusicPlayer];
    //设置播放集合
    [mpc setQueueWithItemCollection:mediaItemCollection];
    [mpc play];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//点击取消时回调
- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(Back)];
//    self.navigationItem.leftBarButtonItem = backButton;
//}
- (IBAction)Back
{return;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    if (![fileManager fileExistsAtPath: path])
//    {
//        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
//    }
//    
//    NSFileManager *fM = [NSFileManager defaultManager];
//    NSMutableDictionary *data;
//    
//    if ([fM fileExistsAtPath: path])
//    {
//        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
//    }
//    else
//    {
//        // If the file doesn’t exist, create an empty dictionary
//        data = [[NSMutableDictionary alloc] init];
//    }
//    
//    //To insert the data into the plist
//    //int value = 5;
//    [data setObject:@"0" forKey:@"flag"];
//   
//    /* [data setObject:btn_song_img_app forKey:@"btn_img"];
//     [data setObject:img_song_app forKey:@"mainimagview"];
//     [data setObject:lbl_song_name_app forKey:@"songname"];
//     [data setObject:lbl_song_detail_app forKey:@"songdetail"];*/
//    
//    [data writeToFile: path atomically:YES];
//    
//    //To reterive the data from the plist
//    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
//    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
//    NSLog(@"strUserName %@",strUserName);
//    
     AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    app.flag=@"0";
    [self dismissViewControllerAnimated:YES completion:nil]; // ios 6
}
-(void)musicInit{
    return;
    //创建播放器控制器 3
    MPMediaPickerController *mpc = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeAnyAudio];
    //设置代理 5
    mpc.delegate = self;
    mpc.showsCloudItems = NO;
    [self presentViewController:mpc animated:YES completion:nil];
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(IBAction)addSongClick:(id)sender{
    return;
    [self musicInit];
    
}
#pragma mark - Private Method

-(void)initMusicItems{
    return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"songList"]){
        
        NSData *MusicData=[[NSUserDefaults standardUserDefaults]objectForKey:@"songList"];
        NSArray *SongList= (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:MusicData];
        
        
        if([SongList count]>0){
            MPMediaItemCollection *_mediaCollection = [[MPMediaItemCollection alloc]initWithItems:(NSArray *)SongList];
            
            for (MPMediaItem *item in _mediaCollection.items) {
                [app.items addObject:item];
            }
            
            //获得应用播放器
             self.mpc = [MPMusicPlayerController iPodMusicPlayer];
           // self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:_mediaCollection];
            
        }
        
        
    }else{
        
        
        //获得query，用于请求本地歌曲集合
        MPMediaQuery *query = [MPMediaQuery songsQuery];
        
        [query addFilterPredicate:[MPMediaPropertyPredicate predicateWithValue:[NSNumber numberWithBool:NO] forProperty:MPMediaItemPropertyIsCloudItem]];
        
        NSArray *albumlists = query.collections;

        //循环获取得到query获得的集合
        for (MPMediaItemCollection *conllection in albumlists) {
            //MPMediaItem为歌曲项，包含歌曲信息
            for (MPMediaItem *item in conllection.items) {
                [app.items addObject:item];
            }
        }
        
        if ([app.items count]>0) {
            //通过歌曲items数组创建一个collection
            MPMediaItemCollection *mic = [[MPMediaItemCollection alloc] initWithItems:app.items];
            //获得应用播放器
            self.mpc = [MPMusicPlayerController applicationMusicPlayer];
            //开启播放通知，不开启，不会发送歌曲完成，音量改变的通知
            [self.mpc beginGeneratingPlaybackNotifications];
            //设置播放的集合
            [self.mpc setQueueWithItemCollection:mic];
        }
    }
}
-(void)reloadytt{
    return;
    NSLog(@"------------ytttytyytytytyoopp----------");
    //音乐播放完成刷新table
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ((app.playNum+1)<[app.items count]) {
        
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum+1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        if ([app.player prepareToPlay]) {
            app.playNum = app.playNum+1;
            [app.player play];
        }
    }
    //[self.tableView reloadData];
}
#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    return app.items.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MusicTableViewCell"];
    if(cell==nil){
        cell=[[MusicTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MusicTableViewCell"];
    }
    //static NSString *CellIdentifier = @"MusicCellIdentifier";
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
  //  static NSString *CellIdentifier = @"CELL";
   // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    MusicTableViewCell *cell=[tableView dequeueReusableHeaderFooterViewWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        cell = [[MusicTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
//    }
    _tableView.backgroundColor=[UIColor blackColor];
    cell.backgroundColor=[UIColor clearColor];
//    cell.textLabel.textColor=[UIColor whiteColor];
//    cell.detailTextLabel.textColor=[UIColor whiteColor];
    //cell.textLabel.numberOfLines=2;
    
    MPMediaItem *item = app.items[indexPath.row];
    //获得专辑对象
    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    //专辑封面
    
//    CGSize itemsize=CGSizeMake(40, 40);
//    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
    
    UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
    if (!img) {
        img = [UIImage imageNamed:@"my music"];
    }
    //cell.imageView.image = img;
    cell.imgalbum.image=img;
    
   // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
    cell.lblsongname.text=[item valueForProperty:MPMediaItemPropertyTitle];
    //歌曲名称
    //cell.detailTextLabel.text = [item valueForProperty:MPMediaItemPropertyArtist];
    cell.lblartistname.text=[item valueForProperty:MPMediaItemPropertyArtist];
    //歌手名称
    if (self.mpc.nowPlayingItem == app.items[indexPath.row])
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}


-(IBAction)previousPlay:(id)sender{
    return;
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    if ((app.playNum-1)>0) {
        
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum-1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        app.playNum=app.playNum-1;
        
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        
        [app.player play];
        
        
        
        MPMediaItem *item = app.items[app.playNum];
        //获得专辑对象
        MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
        //专辑封面
        
        //    CGSize itemsize=CGSizeMake(40, 40);
        //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
        
        UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
        if (!img) {
            img = [UIImage imageNamed:@"my music"];
        }
        
        
        NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
        NSLog (@"%@", songTitle);
        songname.text=songTitle;
        
        // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
        NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
        NSLog (@"songdetail %@", songdetail);
        songdetail1.text=songdetail;
        
        songgimg.image=img;
        
        //create instace of NSData
        UIImage *song_img=songgimg.image;
        NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
        
        NSString *name=[songname text];
        NSString *detailname=[songdetail1 text];
        
        //store the data
        NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
        
        [defaults setObject:imagedata forKey:@"image"];
        [defaults setObject:name forKey:@"name"];
        [defaults setObject:detailname forKey:@"detailname"];
        
        NSString *curentslidertime= [NSString stringWithFormat:@"%@",self.timer];
        NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                [app timeFormat:[app getAudioDuration]]];
        [defaults setObject:curentslidertime forKey:@"curentslidertime"];
        [defaults setObject:durationtime forKey:@"durationtime"];
        
        NSLog (@"curentslidertime %@", curentslidertime);
        NSLog (@"durationtime %@", durationtime);

        
        [defaults synchronize];

        
        
//        if ([app.player prepareToPlay]) {
//            //[self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
//            [app.player play];
//            
//            
//            
//            MPMediaItem *item = app.items[app.playNum-1];
//            //获得专辑对象
//            MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
//            //专辑封面
//            
//            //    CGSize itemsize=CGSizeMake(40, 40);
//            //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
//            
//            UIImage *img = [artwork imageWithSize:CGSizeMake(55, 50)];
//            if (!img) {
//                img = [UIImage imageNamed:@"my music"];
//            }
//            
//        
//            NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
//            NSLog (@"%@", songTitle);
//            songname.text=songTitle;
//            
//            // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
//            NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
//            NSLog (@"songdetail %@", songdetail);
//            songdetail1.text=songdetail;
//            
//            songgimg.image=img;
//            
//            
//            
//
//        }
//        
    }else if((app.playNum -1)==0){
        
        app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[0] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
        app.playNum=0;
        if ([app.player prepareToPlay]) {
            [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
            [app.player play];
            
            
            
            
            MPMediaItem *item = app.items[app.playNum];
            //获得专辑对象
            MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
            //专辑封面
            
            //    CGSize itemsize=CGSizeMake(40, 40);
            //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
            
            UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
            if (!img) {
                img = [UIImage imageNamed:@"my music"];
            }
            
            
            NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
            NSLog (@"%@", songTitle);
            songname.text=songTitle;
            
            // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
            NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
            NSLog (@"songdetail %@", songdetail);
            songdetail1.text=songdetail;
            
            songgimg.image=img;
            
            
            //create instace of NSData
            UIImage *song_img=songgimg.image;
            NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
            
            NSString *name=[songname text];
            NSString *detailname=[songdetail1 text];
            int playnum=app.playNum;
            NSLog (@"playnum %d", playnum);
            
            //store the data
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            
            [defaults setObject:imagedata forKey:@"image"];
            [defaults setObject:name forKey:@"name"];
            [defaults setObject:detailname forKey:@"detailname"];
            [defaults setInteger:playnum forKey:@"playnum"];
            
            NSString *curentslidertime= [NSString stringWithFormat:@"%@",self.timer];
            NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                    [app timeFormat:[app getAudioDuration]]];
            [defaults setObject:curentslidertime forKey:@"curentslidertime"];
            [defaults setObject:durationtime forKey:@"durationtime"];
            
            NSLog (@"curentslidertime %@", curentslidertime);
            NSLog (@"durationtime %@", durationtime);

            
            [defaults synchronize];
        }
    }
}

-(IBAction)nextPlay:(id)sender{
    return;
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
        if ((app.playNum+1)<[app.items count]) {
            
            app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[app.playNum+1] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
            
            app.playNum = app.playNum+1;
            
            [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
            [app.player play];
            
            
            app.isPlayorPause=YES;
            
            
            MPMediaItem *item = app.items[app.playNum];
            //获得专辑对象
            MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
            //专辑封面
            
            //    CGSize itemsize=CGSizeMake(40, 40);
            //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
            
            UIImage *img = [artwork imageWithSize:CGSizeMake(200, 200)];
            if (!img) {
                img = [UIImage imageNamed:@"my music"];
            }
            
            
            NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
            NSLog (@"%@", songTitle);
            songname.text=songTitle;
            
            // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
            NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
            NSLog (@"songdetail %@", songdetail);
            songdetail1.text=songdetail;
            
            self.currentTimeSlider.maximumValue = [app getAudioDuration];
            
            //init the current timedisplay and the labels. if a current time was stored
            //for this player then take it and update the time display
            
            
            self.timeElapsed.text = @"0:00";
            
            self.duration.text = [NSString stringWithFormat:@"-%@",
                                  [app timeFormat:[app getAudioDuration]]];
            
            //start a timer to update the time label display
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                          target:self
                                                        selector:@selector(updateTime:)
                                                        userInfo:nil
                                                         repeats:YES];
            
            
            songgimg.image=img;
            
            //create instace of NSData
            UIImage *song_img=songgimg.image;
            NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
            
            NSString *name=[songname text];
            NSString *detailname=[songdetail1 text];
            int playnum=app.playNum;
            NSLog (@"playnum %d", playnum);
            
            //store the data
            NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
            
            [defaults setObject:imagedata forKey:@"image"];
            [defaults setObject:name forKey:@"name"];
            [defaults setObject:detailname forKey:@"detailname"];
            [defaults setInteger:playnum forKey:@"playnum"];
            
            NSString *curentslidertime= [NSString stringWithFormat:@"%@",self.timer];
            NSString *durationtime=[NSString stringWithFormat:@"-%@",
                                    [app timeFormat:[app getAudioDuration]]];
            [defaults setObject:curentslidertime forKey:@"curentslidertime"];
            [defaults setObject:durationtime forKey:@"durationtime"];
            
            NSLog (@"curentslidertime %@", curentslidertime);
            NSLog (@"durationtime %@", durationtime);
            
            
            [defaults synchronize];
            
            
            
        }
    
    
    
    
}

-(IBAction)playOrPause:(id)sender{
    return;
   AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    
    
    
    //get the stored data
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    //create instace of NSData
    NSData *imagedata=[defaults dataForKey:@"image"];
    UIImage *sng_img=[UIImage imageWithData:imagedata];
    songgimg.image=sng_img;
    
    
    NSString *name=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"name"]];
    NSString *detailname=[NSString stringWithFormat:@"%@",[defaults objectForKey:@"detailname"]];
    songname.text=name;
    songdetail1.text=detailname;
    
    self.currentTimeSlider.maximumValue = [app getAudioDuration];
    
    //init the current timedisplay and the labels. if a current time was stored
    //for this player then take it and update the time display
    
    
    self.timeElapsed.text = @"0:00";
    
    self.duration.text = [NSString stringWithFormat:@"-%@",
                          [app timeFormat:[app getAudioDuration]]];
    
    //start a timer to update the time label display
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateTime:)
                                                userInfo:nil
                                                 repeats:YES];

    
    int playnum=[defaults integerForKey:@"playnum"];
    
    
    NSLog (@" songname.text %@",  name);
    NSLog (@"songdetail1.text %@", detailname);
    NSLog (@"playnum %d", playnum);

    NSLog(@"----------------执行通知------------");
 
    if (app.isPlayorPause==YES) {
        
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_play"] forState:UIControlStateNormal];
        
   //    [app.player stop];
        
        [app.player pause];
        self.isPaused = FALSE;

        app.isPlayorPause=NO;
        
        
        
        
    }
    else if(app.isPlayorPause ==NO){
        app.isPlayorPause=YES;
        
        
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
       // app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[playnum] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];

        
        //start a timer to update the time label display
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTime:)
                                                    userInfo:nil
                                                     repeats:YES];
        
        [app.player play];
        self.isPaused = TRUE;

   
        
        
        
        if ([app.player prepareToPlay]) {
            [app.player play];
            }

        }
    
    
    
    
    
    
}
    

- (void)updateTime:(NSTimer *)timer {
     AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //to don't update every second. When scrubber is mouseDown the the slider will not set
    if (!self.scrubbing) {
        self.currentTimeSlider.value = [app getCurrentAudioTime];
    }
    self.timeElapsed.text = [NSString stringWithFormat:@"%@",
                             [app timeFormat:[app getCurrentAudioTime]]];
    
    self.duration.text = [NSString stringWithFormat:@"-%@",
                          [app timeFormat:[app getAudioDuration] - [app getCurrentAudioTime]]];
    
    //When resetted/ended reset the playButton

    if ([self.duration.text isEqualToString:@"-0:00"]) {
        [self nextPlay:nil];
        //[self previousPlay:nil];
        
    }else{
        
    }
    
    //When resetted/ended reset the playButton
    if (![app.player prepareToPlay]) {
        [self.playButton setBackgroundImage:[UIImage imageNamed:@"mus_paus.png"]
                                   forState:UIControlStateNormal];
        [app.player pause];
        self.isPaused = FALSE;
    }

//    if (![app.player isPlaying]) {
//        [self.playButton setBackgroundImage:[UIImage imageNamed:@"audioplayer_play.png"]
//                                   forState:UIControlStateNormal];
//        [app.player pause];
//        
//        self.isPaused = FALSE;
//       // [self nextPlay:nil];
//    }
}

/*
 * Sets the current value of the slider/scrubber
 * to the audio file when slider/scrubber is used
 */
- (IBAction)setCurrentTime:(id)scrubber {
     AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    //if scrubbing update the timestate, call updateTime faster not to wait a second and dont repeat it
    [NSTimer scheduledTimerWithTimeInterval:0.01
                                     target:self
                                   selector:@selector(updateTime:)
                                   userInfo:nil
                                    repeats:NO];
    
    [app setCurrentAudioTime:self.currentTimeSlider.value];
    self.scrubbing = FALSE;
}

/*
 * Sets if the user is scrubbing right now
 * to avoid slider update while dragging the slider
 */
- (IBAction)userIsScrubbing:(id)sender {
    self.scrubbing = TRUE;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    app.playNum = (int)indexPath.row;
    //--------------
    //后台播放音频设置
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:nil];
    [session setCategory:AVAudioSessionCategoryPlayback error:nil];
    //让app支持接受远程控制事件
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //-------------
    
    NSLog(@"--------------------URL:%@",[app.items[indexPath.row] valueForProperty:MPMediaItemPropertyAssetURL]);
    //AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    app.player=[[AVAudioPlayer alloc] initWithContentsOfURL:[app.items[indexPath.row] valueForProperty:MPMediaItemPropertyAssetURL] error:nil];
    
    
    self.currentTimeSlider.maximumValue = [app getAudioDuration];
    
    //init the current timedisplay and the labels. if a current time was stored
    //for this player then take it and update the time display
    self.timeElapsed.text = @"0:00";
    
    self.duration.text = [NSString stringWithFormat:@"-%@",
                          [app timeFormat:[app getAudioDuration]]];
    
    [app.player prepareToPlay];
    
    
//    MPMediaItem *item = app.items[indexPath.row];
//    //获得专辑对象
//    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
//    //专辑封面
//    UIImage *img = [artwork imageWithSize:CGSizeMake(100, 100)];
//    if (!img) {
//        img = [UIImage imageNamed:@"my music"];
//    }
    // cell.imageView.image = img;
    
    MPMediaItem *item = app.items[indexPath.row];
    //获得专辑对象
    MPMediaItemArtwork *artwork = [item valueForProperty:MPMediaItemPropertyArtwork];
    //专辑封面
    
    //    CGSize itemsize=CGSizeMake(40, 40);
    //    UIGraphicsBeginImageContextWithOptions(itemsize, NO, 0.0);
    
    UIImage *img = [artwork imageWithSize:CGSizeMake(200,200)];
    if (!img) {
        img = [UIImage imageNamed:@"my music"];
    }
    

    
    
    
    NSString *songTitle= [item valueForProperty:MPMediaItemPropertyTitle];
    NSLog (@"%@", songTitle);
    songname.text=songTitle;
    
    // cell.textLabel.text = [item valueForProperty:MPMediaItemPropertyTitle];
    NSString *songdetail= [item valueForProperty:MPMediaItemPropertyArtist];
    NSLog (@"songdetail %@", songdetail);
    songdetail1.text=songdetail;
    
       songgimg.image=img;
    
    app.lbl_song_name_app=[NSString stringWithFormat:@"%@",songTitle];
    NSLog(@"%@",app.lbl_song_name_app);

    
    [app.player play];
    
    //new

        [self.timer invalidate];
    //            //play audio for the first time or if pause was pressed
    //            if (!self.isPaused) {
    //                [self.playButton setBackgroundImage:[UIImage imageNamed:@"audioplayer_pause.png"]
    //                                           forState:UIControlStateNormal];
    //
    //start a timer to update the time label display
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                  target:self
                                                selector:@selector(updateTime:)
                                                userInfo:nil
                                                 repeats:YES];
    //

     muslistview.hidden=YES;
    
    if (app.isPlayorPause==NO) {
        
        [self.palyOrpausebtn setImage:[UIImage imageNamed:@"mus_paus"] forState:UIControlStateNormal];
        app.isPlayorPause=YES;
       
        
    }
    
    //create instace of NSData
    UIImage *song_img=songgimg.image;
    NSData *imagedata=UIImageJPEGRepresentation(song_img, 100);
    
    NSString *name=[songname text];
    NSString *detailname=[songdetail1 text];
    int playnum=app.playNum;
    NSLog (@"playnum %d", playnum);
    
    //store the data
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    
    [defaults setObject:imagedata forKey:@"image"];
    [defaults setObject:name forKey:@"name"];
    [defaults setObject:detailname forKey:@"detailname"];
    [defaults setInteger:playnum forKey:@"playnum"];
    
    NSString *curentslidertime= [NSString stringWithFormat:@"%@",self.timer];
    NSString *durationtime=[NSString stringWithFormat:@"-%@",
                            [app timeFormat:[app getAudioDuration]]];
    [defaults setObject:curentslidertime forKey:@"curentslidertime"];
    [defaults setObject:durationtime forKey:@"durationtime"];
    
    NSLog (@"curentslidertime %@", curentslidertime);
    NSLog (@"durationtime %@", durationtime);

    
    [defaults synchronize];
    //[self.tableView reloadData];
    
}

//滑动删除的委托方法
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
        
        [app.items removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self saveToData];
    
}

- (void)saveToData
{
    
    AppDelegate * app =(AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[app items]];
    [[NSUserDefaults standardUserDefaults]setObject:data forKey:@"songList"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)music:(id)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"imaginfo.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath: path])
    {
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat: @"imaginfo.plist"] ];
    }
    
    NSFileManager *fM = [NSFileManager defaultManager];
    NSMutableDictionary *data;
    
    if ([fM fileExistsAtPath: path])
    {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    }
    else
    {
        // If the file doesn’t exist, create an empty dictionary
        data = [[NSMutableDictionary alloc] init];
    }
    
    //To insert the data into the plist
    //int value = 5;
    [data setObject:@"0" forKey:@"flag"];
    
    /* [data setObject:btn_song_img_app forKey:@"btn_img"];
     [data setObject:img_song_app forKey:@"mainimagview"];
     [data setObject:lbl_song_name_app forKey:@"songname"];
     [data setObject:lbl_song_detail_app forKey:@"songdetail"];*/
    
    [data writeToFile: path atomically:YES];
    
    //To reterive the data from the plist
    NSMutableDictionary *savedStock = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
    NSString *strUserName = [savedStock objectForKey:@"flag"]?[savedStock objectForKey:@"flag"]:@"";
    NSLog(@"strUserName %@",strUserName);
    

    
            // [self.navigationController popViewControllerAnimated:YES];
}
-(IBAction)slider_btn:(id)sender
{
    
    
 
}


//#pragma tableviewmethod slider view
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    
//    return [slidercell_array count];
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    if ([[UIScreen mainScreen] bounds].size.height == 736)
//    {
//        return 100;
//        //iphone 6+
//    }
//    else if ([[UIScreen mainScreen] bounds].size.height==667)
//    {
//        return 80;
//        //iphone 6
//    }
//    
//    else
//    {
//        return 55;
//        //iphone 3.5,4,5,6 inch screen iphone 3g,4s
//    }
//    
//    
//}
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"CustomTableCell";
//    sliderTableViewCell *cell = (sliderTableViewCell *)[self.tableview_slider dequeueReusableCellWithIdentifier:CellIdentifier];
//    // cell.backgroundColor=[UIColor clearColor];
//    
//    
//    
//    
//    
//    
//    // Configure the cell...
//    if (cell == nil) {
//        cell = [[sliderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    cell.lbl_nameslidre.text=[slidercell_array objectAtIndex:indexPath.row];
//    return cell;
//    
//    
//    
//    
//    
//    
//}
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//     
//}
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    
//    
//}


-(IBAction)backwrd:(id)sender
{
    
}
-(IBAction)forwrd:(id)sender
{
    
}
-(IBAction)playsong:(id)sender
{
    
}
-(IBAction)songlist:(id)sender
{
    
    
    
        muslistview.hidden=NO;
        
    
}
-(IBAction)clos:(id)sender
{
     muslistview.hidden=YES;
}



//defualt player

- (IBAction)openMediaPicker:(id)sender {
    MPMediaPickerController *mediaPicker = [[MPMediaPickerController alloc] initWithMediaTypes:MPMediaTypeMusic];
    mediaPicker.delegate = self;
    mediaPicker.allowsPickingMultipleItems = YES; // this is the default
    mediaPicker.showsCloudItems = NO;
    [self presentModalViewController:mediaPicker animated:YES];
}
//#pragma mark MPMediaPickerController delegate methods
//
//- (void)mediaPicker: (MPMediaPickerController *)mediaPicker didPickMediaItems:(MPMediaItemCollection *)mediaItemCollection {
//    // We need to dismiss the picker
//    [self dismissModalViewControllerAnimated:YES];
//    
//    // Assign the selected item(s) to the music player and start playback.
//    [self.mpc stop];
//    [self.mpc setQueueWithItemCollection:mediaItemCollection];
//    [self.mpc play];
//}
//
//- (void)mediaPickerDidCancel:(MPMediaPickerController *)mediaPicker {
//    // User did not select anything
//    // We need to dismiss the picker
//    [self dismissModalViewControllerAnimated:YES];
//}


 

@end
