//
//  Proxy.h
//  TheCaseList
//
//  Created by iBlazing Mac User 2 on 27/09/14.
//  Copyright (c) 2014 iBlazing. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "ConnectionManager.h"
#import <CoreText/CoreText.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "ServerManager.h"

#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
//info view
#import "ProximityTagStorage.h"

#define DELAY_TIMER 4.0
typedef void (^CTCallback) (CTCall *call);

@protocol proxyDelegate;
@interface Proxy : NSObject <EKEventEditViewDelegate>
{
    
    NSString *strCurrentView;
    NSString *strConnectSetting;
    NSString *strCurrentConnectSetting;
    
    BOOL flagConnectView;
    
    
    int deviceLevel11;
    int deviceLevel22;
    
    
    
}

@property (nonatomic, strong) EKEventStore *eventStore;
@property (nonatomic, strong) EKCalendar *defaultCalendar;
@property (nonatomic, strong) NSMutableArray *eventsList;
@property (nonatomic, retain) CTCallCenter *cts;
@property (nonatomic) NSTimer *    batteryTimerytt;
@property (nonatomic) NSTimer *    RiliTimerytt;
@property (nonatomic) NSTimer *    TimerBleStable;
@property (nonatomic) NSTimer *    TimerSportToWatch;


@property (nonatomic,copy)CTCallback IncommingBlock;


@property(nonatomic, retain)id<proxyDelegate> delegate;
+(Proxy*)shared;

@property(nonatomic, retain)NSString *strCurrentView;
@property(nonatomic, retain)NSString *strConnectSetting;
@property(nonatomic, retain) NSString *strCurrentConnectSetting;

@property(nonatomic, assign) BOOL flagConnectView;

-(void)StartBleTimer;
-(void)StopBleTimer;
-(void)alarmdatetimerset;
-(void)StartSportTimer;
-(void)StopSportTimer;

@end

@protocol proxyDelegate <NSObject>

@optional

@end