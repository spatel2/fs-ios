//
//  Proxy.m
//  TheCaseList
//
//  Created by iBlazing Mac User 2 on 27/09/14.
//  Copyright (c) 2014 iBlazing. All rights reserved.
//

#import "Proxy.h"

#import "AppDelegate.h"



static Proxy *sharedProxy = nil;

@implementation Proxy
@synthesize strCurrentView, strConnectSetting, strCurrentConnectSetting, flagConnectView;
@synthesize batteryTimerytt=_batteryTimerytt,RiliTimerytt=_RiliTimerytt, TimerBleStable=_TimerBleStable, TimerSportToWatch = _TimerSportToWatch;
@synthesize cts=_cts;

//*** Proxy class object
+(Proxy*)shared
{
    @synchronized(self)
    {
        if (sharedProxy == nil)
        {
            sharedProxy = [[self alloc] init];
        }
    }
    return sharedProxy;
}

-(id)init
{
    if(self = [super init])
    {
        strCurrentView=@"";
        strConnectSetting=@"";
        strCurrentConnectSetting = @"";
        flagConnectView = FALSE;
        [self BasicConfiguration];
        //[self StartBleTimer];
        //Get current time
        NSDate* now = [NSDate date];
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *dateComponents = [gregorian components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:now];
        NSInteger second = [dateComponents second];
        float NewSecond = 60-second;
        [self performSelector:@selector(StartTimer) withObject:nil afterDelay:NewSecond];
        //        [self StartTimer];
        
    }
    return self;
}

-(void)BasicConfiguration {
    
    self.eventStore = [[EKEventStore alloc] init];
    self.eventsList = [[NSMutableArray alloc] initWithCapacity:0];
    
    //  [self readBattery];
    
    BOOL __block isFromIncomingCall;
    self.IncommingBlock = ^(CTCall* inCTCall) {
        
        
        if ([inCTCall.callState isEqualToString: CTCallStateConnected]) {
            
            isFromIncomingCall = NO;
            //           AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
            //             app.isPlayorPause =YES;
            
        } else if ([inCTCall.callState isEqualToString: CTCallStateDialing]) {
            
            isFromIncomingCall = NO;
            
        } else if ([inCTCall.callState isEqualToString: CTCallStateDisconnected]) {
            
            // app.isPlayorPause =NO;
            //[musivobj playOrPause:self];
            
            if (isFromIncomingCall)
            {
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isMissedCall"] boolValue]) {
                    
                    
                    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    Byte byte =0x35;
                    [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
                }
                
                isFromIncomingCall = NO;
                
            }
            
        }else if ([inCTCall.callState isEqualToString: CTCallStateIncoming]) {
            
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isIncomingCall"] boolValue]) {
                
                AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                Byte byte =0x31;
                [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
                
                
            }
            isFromIncomingCall = YES;
            //            app.isPlayorPause =NO;
            //           // __weak typeof(self) weakSelf = self;
            //            [musivobj playOrPause:self];
            
        }
    };
    
    //电话监听
    self.cts = [[CTCallCenter alloc] init];
    self.cts.callEventHandler =self.IncommingBlock;
    
    deviceLevel11=0;
    deviceLevel22=0;
    
    UIDevice *device = [UIDevice currentDevice];
    device.batteryMonitoringEnabled = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(batteryLevelChanged:)
                                                 name:UIDeviceBatteryLevelDidChangeNotification object:device];
    
}

//----------------------
-(void)StartSportTimer{
    [_TimerBleStable invalidate];
    
    //start timer for sports to watch
    _TimerSportToWatch = [NSTimer timerWithTimeInterval:10.0 target:self selector:@selector(SendSportDataToWatch)    userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_TimerSportToWatch forMode:NSDefaultRunLoopMode];
    
    _TimerBleStable = [NSTimer timerWithTimeInterval:10.0 target:self selector:@selector(CheckBLEConnection)    userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_TimerBleStable forMode:NSDefaultRunLoopMode];
}

-(void)StopSportTimer {
    [_TimerSportToWatch invalidate];
    [_TimerBleStable invalidate];
    
    [self StartBleTimer];
}

-(void)SendSportDataToWatch
{
    //NSLog(@"data to watch");
    AppDelegate *app1 = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (app1.gActButtonStartSelectionState == true && app1.flagcmd == 1)
    {
        [app1 WriteSportValue];
    }
}
//----------------------

-(void)StartBleTimer {
    _TimerBleStable = [NSTimer timerWithTimeInterval:120.0 target:self selector:@selector(CheckBLEConnection)    userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_TimerBleStable forMode:NSDefaultRunLoopMode];
}

-(void)StopBleTimer {
    [_TimerBleStable invalidate];
}

-(void)CheckBLEConnection
{
    //NSLog(@"chk conn");
    AppDelegate * app= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([app.appdelegateProximity isConnected])
    {
        Byte byte =0x0D;
        [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
        
    }
}

-(void)StartTimer{
    
    // NSString* alarmvalue = [[NSUserDefaults standardUserDefaults] objectForKey:@"alarm"];
    
    //日程提醒监听
    _RiliTimerytt = [NSTimer timerWithTimeInterval:60.0 target:self selector:@selector(checkRili)    userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_RiliTimerytt forMode:NSRunLoopCommonModes];
    
}

-(void)checkRili{
    
    // NSString* alarmvalue = [[NSUserDefaults standardUserDefaults] objectForKey:@"alarm"];
    
    //[self checkAlarmDate];
    
    //  [self readBattery];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isNewCalendatEvent"] boolValue]) {
        [self checkEventStoreAccessForCalendar];
    }
    
    NSDate * date = [NSDate date];
    if ([self.eventsList count]>0) {
        for (EKEvent * event  in self.eventsList) {
            NSString * evendatestring =[[NSString stringWithFormat:@"%@",event.startDate] substringToIndex:16];
            NSString * datestring = [[NSString stringWithFormat:@"%@",date] substringToIndex:16];
            
            
            if ([evendatestring isEqualToString:datestring]) {
                //todo
                
                if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"isNewCalendatEvent"] boolValue]) {
                    
                    AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                    Byte byteytt = 0x45;
                    NSData * dataytt = [NSData dataWithBytes:&byteytt length:sizeof(byteytt)];
                    [app.appdelegateProximity  phoneWriteDataToTag :dataytt];
                }
            }
        }
    }
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 555) {
        [[ServerManager sharedInstance] stopPlayingAlarmSound];
    }
}

-(NSInteger)GetWeak:(NSDate *)now
{
    //initializtion parameter   NSGregorianCalendar
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    int week =(int)[comps weekday];
    return week;
}

- (BOOL)GetHourAndMinute:(NSDate *)date
{
    NSString *date1;
    NSDateFormatter *dateformatter1=[[NSDateFormatter alloc] init];
    [dateformatter1 setDateFormat:@"HH:mm"];
    [dateformatter1 setLocale:[NSLocale systemLocale]];
    date1=[dateformatter1 stringFromDate:date];
    
    NSString *now;
    NSDateFormatter *dateformatter2=[[NSDateFormatter alloc] init];
    [dateformatter2 setDateFormat:@"HH:mm"];
    [dateformatter2 setLocale:[NSLocale systemLocale]];
    now=[dateformatter2 stringFromDate:[NSDate date]];
    
    if ([date1 isEqualToString:now]) {
        return YES;
    }
    return NO;
}

- (void)batteryLevelChanged:(NSNotification *)notification
{
    [self readBattery];
}

-(void)readBattery{
    
    UIDevice *myDevice = [UIDevice currentDevice];
    
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    double deviceLevel = [UIDevice currentDevice].batteryLevel;
    
    
    NSString *strBatteryStatus = [NSString stringWithFormat:@"%.2f", deviceLevel];
    
    if ([strBatteryStatus isEqualToString:@"0.20"]) {
        NSString* checklow = [[NSUserDefaults standardUserDefaults] objectForKey:@"lowbattery"];
        if([checklow isEqualToString:@"batterylowON"] )
        {
            if ([myDevice batteryState] != UIDeviceBatteryStateCharging) {
                deviceLevel11 = 1;
                AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                Byte byte =0x21;
                [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
            }
        }
    }else if([strBatteryStatus isEqualToString:@"0.10"]){
        NSString* checklow = [[NSUserDefaults standardUserDefaults] objectForKey:@"lowbattery"];
        if([checklow isEqualToString:@"batterylowON"] )
        {
            if ([myDevice batteryState] != UIDeviceBatteryStateCharging) {
                
                AppDelegate * app = (AppDelegate *)[[UIApplication sharedApplication]delegate];
                Byte byte =0x25;
                [app.appdelegateProximity phoneWriteDataToTag:[NSData dataWithBytes:&byte length:sizeof(byte)]];
                
            }
        }
    }
}
////-------------------------------------------------日历提醒---------}
-(void)checkEventStoreAccessForCalendar
{
    
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status)
    {
            // Update our UI if the user has granted access to their Calendar
        case EKAuthorizationStatusAuthorized: [self accessGrantedForCalendar];
            break;
            // Prompt the user for access to Calendar if there is no definitive answer
        case EKAuthorizationStatusNotDetermined: [self requestCalendarAccess];
            break;
            // Display a message if the user has denied or restricted access to Calendar
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning" message:@"Permission was not granted for Calendar"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}
-(void)requestCalendarAccess
{
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             //             ConnectViewController * __weak weakSelf = self;
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [self accessGrantedForCalendar];
             });
         }
     }];
}
// This method is called when the user has granted permission to Calendar
-(void)accessGrantedForCalendar
{
    // Let's get the default calendar associated with our event store
    self.defaultCalendar = self.eventStore.defaultCalendarForNewEvents;
    self.eventsList = [self fetchEvents];
    
}

// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller
          didCompleteWithAction:(EKEventEditViewAction)action
{
    
    if (action != EKEventEditViewActionCanceled)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.eventsList = [self fetchEvents];
        });
    }
    
    
}

// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller
{
    return self.defaultCalendar;
}


- (NSMutableArray *)fetchEvents
{
    
    NSDate *startDate = [NSDate date];
    NSDateComponents *tomorrowDateComponents = [[NSDateComponents alloc] init];
    tomorrowDateComponents.day = 1;
    NSDate *endDate = [[NSCalendar currentCalendar] dateByAddingComponents:tomorrowDateComponents
                                                                    toDate:startDate
                                                                   options:0];
    NSArray *calendarArray = [NSArray arrayWithObject:self.defaultCalendar];
    NSPredicate *predicate = [self.eventStore predicateForEventsWithStartDate:startDate
                                                                      endDate:endDate
                                                                    calendars:calendarArray];
    NSMutableArray *events = [NSMutableArray arrayWithArray:[self.eventStore eventsMatchingPredicate:predicate]];
    return events;
}






@end
