//
//  musicViewController.h
//  KennethCole
//
//  Created by Dharmesh on 26/12/14.
//  Copyright (c) 2014 iBlazing. All rights reserved.
//

#import <UIKit/UIKit.h>


#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "Constant.h"
#import "Proxy.h"
#import <QuartzCore/QuartzCore.h>
 

@interface musicViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,MPMediaPickerControllerDelegate,AVAudioPlayerDelegate>
{
    IBOutlet UIView *homeview;
    IBOutlet UIView *sliderview;
    IBOutlet UIImageView *img_logo;
    IBOutlet UIButton *btn_music;
    IBOutlet UIButton *btn_slider;
    
    IBOutlet UITableView *tableview_slider;
    NSMutableArray *slidercell_array;
    
    IBOutlet UIButton *btn_player_forword;
    IBOutlet UIButton *btn_player_backwrd;
    IBOutlet UIButton *btn_player_start;
    IBOutlet UIButton *btn_player_songlist;
    IBOutlet UIImageView *img_song;
    IBOutlet UILabel *lbl_song_name;
    
    int flag;
    
    IBOutlet UISlider *volumeSlider;
    
     int setflag;

}



@property(nonatomic,retain)IBOutlet UIView *homeview;
@property(nonatomic,retain)IBOutlet UIView *sliderview;
@property(nonatomic,retain)IBOutlet UIImageView *img_logo;
@property(nonatomic,retain)IBOutlet UIButton *btn_music;
@property(nonatomic,retain)IBOutlet UIButton *btn_slider;
@property(nonatomic,retain)IBOutlet UITableView *tableview_slider;
@property(nonatomic,retain)IBOutlet UILabel *lblslider;

@property(nonatomic,retain)IBOutlet UIView *muslistview;

//@property (nonatomic,strong) MPMusicPlayerController *mpc;
@property(nonatomic,retain)IBOutlet UIImageView *img_song;
@property(nonatomic,retain)IBOutlet UILabel *lbl_song_name;


-(IBAction)backwrd:(id)sender;
-(IBAction)forwrd:(id)sender;
-(IBAction)playsong:(id)sender;
-(IBAction)songlist:(id)sender;

-(IBAction)music:(id)sender;
-(IBAction)slider_btn:(id)sender;
-(IBAction)clos:(id)sender;


@property (nonatomic,strong) MPMusicPlayerController *mpc; //播放器对象
@property (nonatomic, weak) IBOutlet  UITableView * tableView;

@property(nonatomic, weak)IBOutlet UIButton * palyOrpausebtn;

@property(nonatomic,retain)IBOutlet UILabel *songname;
@property(nonatomic,retain)IBOutlet UILabel *songdetail1;
@property(nonatomic,retain)IBOutlet UIImageView *songgimg;



- (IBAction)volumeChanged:(id)sender;
-(IBAction)addSongClick:(id)sender;

-(IBAction)nextPlay:(id)sender;
-(IBAction)previousPlay:(id)sender;
-(IBAction)playOrPause:(id)sender;
-(void)initMusicItems;

//seek control
@property (weak, nonatomic) IBOutlet UISlider *currentTimeSlider;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UILabel *duration;
@property (weak, nonatomic) IBOutlet UILabel *timeElapsed;


@property BOOL isPaused;
@property BOOL scrubbing;

@property NSTimer *timer;

 - (IBAction)openMediaPicker:(id)sender;
@end
